﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ceilingFan : MonoBehaviour {

	public LightSwitch lightSwitch;
	private AudioSource audioBoy;
	private float speed = 200;
	private float startVol;
	private float speedGoal;
	private powerContainer manager;
	private float t = 0;
	private bool powerOn;
	private bool powerTemp;

	void Start () {
		audioBoy = gameObject.GetComponent<AudioSource>();
		startVol = audioBoy.volume;
		manager = GameObject.Find ("Manager").GetComponent<powerContainer>();
		powerTemp = powerOn;
	}

	void Update () {
		audioBoy.volume = startVol * (speed / 275);
		if (powerOn != powerTemp) {
			t = 0;
			powerTemp = powerOn;
		}
		if (powerOn) {
			speedGoal = 275;
			t += 0.25f * Time.deltaTime;
		} else {
			speedGoal = 0;
			t += 0.001f * Time.deltaTime;
		}
		speed = Mathf.Lerp (speed, speedGoal, t);
		transform.Rotate(0, 0, speed*Time.deltaTime);
		if (manager.powerOn) {
			if (lightSwitch.switchActive) {
				powerOn = true;
			} else {
				powerOn = false;
			}
		} else {
			powerOn = false;
		}
	}
}
