﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class slidingDoor : MonoBehaviour {

	private AudioSource audioBoy;
	public GameObject lDoor;
	public GameObject rDoor;
	public bool closed = true;
	public float speedMultiplier = 10f;
	private float lDoorStartX;
	private float rDoorStartX;
	public GameObject lightSource;

	void Start () {
		audioBoy = gameObject.GetComponent<AudioSource>();
		lDoorStartX = lDoor.transform.position.x;
		rDoorStartX = rDoor.transform.position.x;
	}

	void Update () {
		if (closed) {
			lDoor.transform.position = Vector3.Lerp (lDoor.transform.position, new Vector3 (lDoorStartX, lDoor.transform.position.y, lDoor.transform.position.z), Time.deltaTime * speedMultiplier);
			rDoor.transform.position = Vector3.Lerp (rDoor.transform.position, new Vector3 (rDoorStartX, rDoor.transform.position.y, rDoor.transform.position.z), Time.deltaTime * speedMultiplier);
		} else {
			lightSource.SetActive (true);
			lDoor.transform.position = Vector3.Lerp (lDoor.transform.position, new Vector3 (lDoorStartX + 1f, lDoor.transform.position.y, lDoor.transform.position.z), Time.deltaTime * speedMultiplier);
			rDoor.transform.position = Vector3.Lerp (rDoor.transform.position, new Vector3 (rDoorStartX - 1f, rDoor.transform.position.y, rDoor.transform.position.z), Time.deltaTime * speedMultiplier);
		}
	}

	public void soundPlay () {
		audioBoy.Play();
	}
}
