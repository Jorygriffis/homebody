﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class clockHands : MonoBehaviour {

	public ArrayContainer Container;
	[Dropdown("handTypes")]
	public string handType;
	private string[] handTypes = new string[] {"hourHand", "minuteHand", "secondHand"};
	private float turnRate;
	private bool finished = false;

	void Init () {
		if (handType == "hourHand") {
			turnRate = 432000f;
			transform.localEulerAngles = new Vector3 (0f, Container.GetComponent<timeContainer>().currentTime/60*-30, 0f);
		} else if (handType == "minuteHand") {
			turnRate = 3600f;
			transform.localEulerAngles = new Vector3 (0f, Container.GetComponent<timeContainer>().currentMinute * 6, 0f);
		} else if (handType == "secondHand") {
			turnRate = 60f;
		} else {
			turnRate = 432000f;
			transform.localEulerAngles = new Vector3 (0f, Container.GetComponent<timeContainer>().currentTime/60*-30, 0f);
		}
	}

	void Update () {
		if (Container.GetComponent<timeContainer> ().init == true && finished == false) {
			Init ();
			finished = true;
		} else if (finished == true) {
			iTween.RotateBy (gameObject, iTween.Hash ("easetype", "linear", "looptype", "loop", "x", 0, "y", -1f, "z", 0, "time", turnRate));
		}
	}
}
