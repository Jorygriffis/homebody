﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class atticStairs : MonoBehaviour {

	public bool isOpen = false;
	private bool openToggle = false;
	public float t = 0;
	public Vector3 startRotation;
	public float goalRotation = 42.785f;
	private float moveFactor;
	public GameObject colliderObj;
	public GameObject moveTriggerObj;

	void Start () {
		colliderObj.SetActive (false);
		startRotation = gameObject.transform.rotation.eulerAngles;
	}

	void Update () {
		t += moveFactor * Time.deltaTime;
		if (Input.GetKeyDown (KeyCode.O)) {
			isOpen = !isOpen;
			openToggle = true;
		}
		if (openToggle == true) {
			t = 0;
			openToggle = false;
		}
		if (isOpen == true) {
			if (t > 0.5f) {
				moveFactor = 0.5f;
				if (t > 0.75f) {
					moveFactor = 0.8f;
					if (t > 0.9f) {
						colliderObj.SetActive (true);
						moveTriggerObj.SetActive (true);
						gameObject.GetComponent<Animation> ().Play ("AtticStairsOpening");
					}
				}
			} else {
				moveFactor = 1.1f;
			}
			gameObject.transform.eulerAngles = new Vector3 (startRotation.x, startRotation.y, Mathf.Lerp (startRotation.z, goalRotation, t));
		} else {
			colliderObj.SetActive (false);
			moveTriggerObj.SetActive (false);
			if (t > 0.5f) {
				moveFactor = 1.1f;
				if (t > 0.75f) {
					moveFactor = 0.8f;
				}
			} else {
				moveFactor = 0.5f;
			}
			gameObject.GetComponent<Animation> ().Play ("AtticStairsClosing");
			gameObject.transform.eulerAngles = new Vector3 (startRotation.x, startRotation.y, Mathf.Lerp (goalRotation, startRotation.z, t));
		}
	}
}
