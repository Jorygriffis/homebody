﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class genericDoorAnimated : MonoBehaviour {

	public InteractionContainer iContainer;
	public bool switchHands = false;
	private bool leftHandStart = false;
	private Animation animator;
	public bool open = false;
	private bool transitionDone = true;
	private float t = 0;
	public float delayTime = 0.5f;
	public AnimationClip openingAnimation;
	public AnimationClip openAnimation;
	public AnimationClip closingAnimation;
	public AnimationClip closedAnimation;


	void Start () {
		animator = gameObject.GetComponent<Animation>();
		if (iContainer.leftHand == true) {
			leftHandStart = true;
		} else {
			leftHandStart = false;
		}
	}

	void Update () {
		if (iContainer.activated) {
			iContainer.activated = false;
			StartCoroutine (openTimer ());
		} 
		if (open) {
			if (switchHands == true) {
				if (leftHandStart == true) {
					iContainer.leftHand = false;
				} else {
					iContainer.leftHand = true;
				}
			}
			if (transitionDone) {
				animator.clip = openAnimation;
				animator.Stop ();
			} else {
				if (!animator.IsPlaying(openingAnimation.name)) {
					transitionDone = true;
				}
			}
		} else {
			if (switchHands == true) {
				if (leftHandStart == true) {
					iContainer.leftHand = true;
				} else {
					iContainer.leftHand = false;
				}
			}
			if (transitionDone) {
				animator.clip = closedAnimation;
				animator.Stop ();
			} else {
				if (!animator.IsPlaying(closingAnimation.name)) {
					transitionDone = true;
				}
			}
		}
	}

	public IEnumerator openTimer () {
		yield return new WaitForSeconds (0.35f);
		t = 0;
		open = !open;
		if (open) {
			animator.clip = openingAnimation;
			animator.Play ();
		} else {
			animator.clip = closingAnimation;
			animator.Play ();
		}
		transitionDone = false;
	}
}
