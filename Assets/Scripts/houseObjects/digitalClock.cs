﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class digitalClock : MonoBehaviour {
	public bool affectedByPower = true;
	public bool off;
	public bool brokenOnceOff = true;
	public bool broken;
	public float brokenFlashInterval = 1;
	public GameObject light;
	public GameObject digit1;
	public GameObject digit2;
	public GameObject colon;
	public GameObject digit3;
	public GameObject digit4;
	public timeContainer manager;
	private string minuteStringW;
	private string hourStringW;
	private string minuteString;
	private string hourString;
	private char modString;
	private int thisMinute;
	private int thisHour;
	private float brokenTime = 0;
	private powerContainer powerManager;

	void Start () {
		powerManager = manager.GetComponent<powerContainer>();
	}

	void Update () {
		if (affectedByPower) {
			if (powerManager.powerOn == false) {
				off = true;
			} else {
				off = false;
			}
		}
		if (off) {
			if (brokenOnceOff) {
				broken = true;
			}
			light.SetActive (false);
			digit1.SetActive (false);
			digit2.SetActive (false);
			colon.SetActive (false);
			digit3.SetActive (false);
			digit4.SetActive (false);
		} else {
			if (broken) {
				brokenTime += Time.deltaTime;
				hourString = "12";
				minuteString = "00";
				setDigital (digit1);
				setDigital (digit2);
				setDigital (digit3);
				setDigital (digit4);
				if (brokenTime >= brokenFlashInterval) {
					if (light.activeInHierarchy == false) {
						light.SetActive (true);
						digit1.SetActive (true);
						digit2.SetActive (true);
						colon.SetActive (true);
						digit3.SetActive (true);
						digit4.SetActive (true);
					} else {
						light.SetActive (false);
						digit1.SetActive (false);
						digit2.SetActive (false);
						colon.SetActive (false);
						digit3.SetActive (false);
						digit4.SetActive (false);
					}
					brokenTime = 0;
				}
			} else {
				light.SetActive (true);
				digit1.SetActive (true);
				digit2.SetActive (true);
				colon.SetActive (true);
				digit3.SetActive (true);
				digit4.SetActive (true);
				minuteStringW = manager.currentMinute.ToString ();
				hourStringW = manager.currentHour.ToString ();	
				if (manager.currentMinute < 10) {
//			Debug.Log ("1 digit minute");
					minuteString = string.Format ("{0}{1}", '0', minuteStringW [0]);
				} else {
//			Debug.Log ("2 digit minute");
					minuteString = minuteStringW;
				}
				if (manager.currentHour < 10) {
//			Debug.Log ("1 digit hour");
					hourString = string.Format ("{0}{1}", "0", hourStringW [0]);
				} else {
//			Debug.Log ("2 digit hour");
					hourString = hourStringW;
				}
				if (thisHour != manager.currentHour) {
					setDigital (digit1);
					setDigital (digit2);
					thisHour = manager.currentHour;
				}
				if (thisMinute != manager.currentMinute) {
					setDigital (digit3);
					setDigital (digit4);
					thisMinute = manager.currentMinute;
				}
			}
		}
	}

	void setDigital (GameObject digit){
		if (digit == digit1) {
			modString = hourString [0];
		} else if (digit == digit2) {
			modString = hourString [1];
		} else if (digit == digit3) {
			modString = minuteString [0];
		} else if (digit == digit4) {
			modString = minuteString [1];
		}
		for (int i = 0; i < digit.transform.childCount; i++) {
			var child = digit.transform.GetChild (i);
			if (child != null)
				child.GetComponent<MeshRenderer> ().enabled = false;
		}
		if (modString == '0') {
			if (digit == digit1) {

			} else {
				digit.transform.GetChild (0).GetComponent<MeshRenderer> ().enabled = true;
			}
		} else if (modString == '1') {
			digit.transform.GetChild (1).GetComponent<MeshRenderer> ().enabled = true;
		} else if (modString == '2') {
			digit.transform.GetChild (2).GetComponent<MeshRenderer> ().enabled = true;
		} else if (modString == '3') {
			digit.transform.GetChild (3).GetComponent<MeshRenderer> ().enabled = true;
		} else if (modString == '4') {
			digit.transform.GetChild (4).GetComponent<MeshRenderer> ().enabled = true;
		} else if (modString == '5') {
			digit.transform.GetChild (5).GetComponent<MeshRenderer> ().enabled = true;
		} else if (modString == '6') {
			digit.transform.GetChild (6).GetComponent<MeshRenderer> ().enabled = true;
		} else if (modString == '7') {
			digit.transform.GetChild (7).GetComponent<MeshRenderer> ().enabled = true;
		} else if (modString == '8') {
			digit.transform.GetChild (8).GetComponent<MeshRenderer> ().enabled = true;
		} else if (modString == '9') {
			digit.transform.GetChild (9).GetComponent<MeshRenderer> ().enabled = true;
		} 

	}
}
