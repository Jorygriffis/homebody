﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class powerBox : MonoBehaviour {

	public GameObject powerBoxDoor;
	public GameObject atticUp;
	public GameObject atticDown;
	public GameObject houseUp;
	public GameObject houseDown;

	private bool powerBoxDoorOpen = false;
	private powerContainer pContainer;

	private GameObject atticLite1;
	private GameObject atticLite2;
	private GameObject atticLite3;
	private GameObject houseLite1;
	private GameObject houseLite2;
	private GameObject reserveLite1;
	private GameObject reserveLite2;
	private GameObject reserveLite3;
	private GameObject reserveLite4;
	private GameObject reserveLite5;

	private Vector3 startRot;
	private Vector3 targetRot;

	private float t;

	void Start () {
		startRot = powerBoxDoor.transform.eulerAngles;
		targetRot = startRot;
		pContainer = GameObject.Find ("Manager").GetComponent<powerContainer> ();
		atticLite1 = transform.Find ("atticLite1").gameObject;
		atticLite2 = transform.Find ("atticLite2").gameObject;
		atticLite3 = transform.Find ("atticLite3").gameObject;
		houseLite1 = transform.Find ("houseLite1").gameObject;
		houseLite2 = transform.Find ("houseLite2").gameObject;
		reserveLite1 = transform.Find ("reserveLite1").gameObject;
		reserveLite2 = transform.Find ("reserveLite2").gameObject;
		reserveLite3 = transform.Find ("reserveLite3").gameObject;
		reserveLite4 = transform.Find ("reserveLite4").gameObject;
		reserveLite5 = transform.Find ("reserveLite5").gameObject;
	}

	void Update () {
		if (powerBoxDoor.GetComponent<InteractionContainer> ().activated == true) {
			if (powerBoxDoorOpen == true) {
				doorClose ();
			} else {
				doorOpen ();
			}
			powerBoxDoor.GetComponent<InteractionContainer> ().activated = false;
		}
		t = Mathf.Clamp01 (t + Time.deltaTime);
		if (atticUp.GetComponent<PushButton> ().Pressed == true) {
			if (pContainer.atticPower < 3 && pContainer.reservePower > 0) {
				pContainer.atticPower = pContainer.atticPower + 1;
				pContainer.reservePower = pContainer.reservePower - 1;
			}
			atticUp.GetComponent<PushButton> ().Pressed = false;
		}
		if (atticDown.GetComponent<PushButton> ().Pressed == true) {
			if (pContainer.atticPower > 0) {
				pContainer.atticPower = pContainer.atticPower - 1;
				pContainer.reservePower = pContainer.reservePower + 1;
			}
			atticDown.GetComponent<PushButton> ().Pressed = false;
		}
		if (houseUp.GetComponent<PushButton> ().Pressed == true) {
			if (pContainer.housePower < 2 && pContainer.reservePower > 0) {
				pContainer.housePower = pContainer.housePower + 1;
				pContainer.reservePower = pContainer.reservePower - 1;
			}
			houseUp.GetComponent<PushButton> ().Pressed = false;
		}
		if (houseDown.GetComponent<PushButton> ().Pressed == true) {
			if (pContainer.housePower > 0) {
				pContainer.housePower = pContainer.housePower - 1;
				pContainer.reservePower = pContainer.reservePower + 1;
			}
			houseDown.GetComponent<PushButton> ().Pressed = false;
		}

		if (pContainer.housePower == 0) {
			houseLite1.SetActive (false);
			houseLite2.SetActive (false);
		} else if (pContainer.housePower == 1) {
			houseLite1.SetActive (true);
			houseLite2.SetActive (false);
		} else if (pContainer.housePower == 2) {
			houseLite1.SetActive (true);
			houseLite2.SetActive (true);
		}

		if (pContainer.atticPower == 0) {
			atticLite1.SetActive (false);
			atticLite2.SetActive (false);
			atticLite3.SetActive (false);
		} else if (pContainer.atticPower == 1) {
			atticLite1.SetActive (true);
			atticLite2.SetActive (false);
			atticLite3.SetActive (false);
		} else if (pContainer.atticPower == 2) {
			atticLite1.SetActive (true);
			atticLite2.SetActive (true);
			atticLite3.SetActive (false);
		} else if (pContainer.atticPower == 3) {
			atticLite1.SetActive (true);
			atticLite2.SetActive (true);
			atticLite3.SetActive (true);
		}

		if (pContainer.reservePower == 0) {
			reserveLite1.SetActive (false);
			reserveLite2.SetActive (false);
			reserveLite3.SetActive (false);
			reserveLite4.SetActive (false);
			reserveLite5.SetActive (false);
		} else if (pContainer.reservePower == 1) {
			reserveLite1.SetActive (true);
			reserveLite2.SetActive (false);
			reserveLite3.SetActive (false);
			reserveLite4.SetActive (false);
			reserveLite5.SetActive (false);
		} else if (pContainer.reservePower == 2) {
			reserveLite1.SetActive (true);
			reserveLite2.SetActive (true);
			reserveLite3.SetActive (false);
			reserveLite4.SetActive (false);
			reserveLite5.SetActive (false);
		} else if (pContainer.reservePower == 3) {
			reserveLite1.SetActive (true);
			reserveLite2.SetActive (true);
			reserveLite3.SetActive (true);
			reserveLite4.SetActive (false);
			reserveLite5.SetActive (false);
		} else if (pContainer.reservePower == 4) {
			reserveLite1.SetActive (true);
			reserveLite2.SetActive (true);
			reserveLite3.SetActive (true);
			reserveLite4.SetActive (true);
			reserveLite5.SetActive (false);
		} else if (pContainer.reservePower == 5) {
			reserveLite1.SetActive (true);
			reserveLite2.SetActive (true);
			reserveLite3.SetActive (true);
			reserveLite4.SetActive (true);
			reserveLite5.SetActive (true);
		}
		powerBoxDoor.transform.eulerAngles = Vector3.Lerp (transform.eulerAngles, targetRot, t * 3);
	}

	void doorOpen(){
		t = 0;
		targetRot = new Vector3 (startRot.x, startRot.y, startRot.z + 110);
		powerBoxDoorOpen = true;
	}

	void doorClose(){
		t = 0;
		targetRot = startRot;
		powerBoxDoorOpen = false;
	}
}
