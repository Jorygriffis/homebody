﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class closetDoor : MonoBehaviour {

	private InteractionContainer iContainer;
	private Animation animator;
	[HideInInspector]
	public bool open = false;
	[HideInInspector]
	public bool transitionDone = true;
	public BoxCollider openCollider;
	public BoxCollider closedCollider;
	private float t = 0;

	void Start () {
		iContainer = transform.Find("closetDoorHinge").transform.Find("closetDoorKnob").gameObject.GetComponent<InteractionContainer>();
		animator = gameObject.GetComponent<Animation>();
	}

	void Update () {
		if (iContainer.activated) {
			iContainer.activated = false;
			StartCoroutine (openTimer ());
		} 
		if (open) {
			if (transitionDone) {
				animator.CrossFade ("bedroomClosetOpen", 0f);
			} else {
				t += Time.deltaTime;
				closedCollider.enabled = false;
				openCollider.enabled = true;
				animator.CrossFade ("bedroomClosetOpening", 0f);
				if (t > 0.5f) {
					transitionDone = true;
				}
			}
		} else {
			if (transitionDone) {
				animator.CrossFade ("bedroomClosetClosed", 0f);
			} else {
				t += Time.deltaTime;
				closedCollider.enabled = true;
				openCollider.enabled = false;
				animator.CrossFade ("bedroomClosetClosing", 0f);
				if (t > 0.5f) {
					transitionDone = true;
				}
			}
		}
	}

	public IEnumerator openTimer () {
		yield return new WaitForSeconds (0.35f);
		t = 0;
		open = !open;
		transitionDone = false;
	}

	public void openExt () {
		t = 0;
		open = true;
		transitionDone = false;
	}

	public void closeExt () {
		t = 0;
		open = false;
		transitionDone = false;
	}
}
