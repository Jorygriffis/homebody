﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grandfatherClockSound : MonoBehaviour {

	private AudioSource audioBoy;
	public List<AudioClip> upTickSounds = new List<AudioClip>();
	public List<AudioClip> downTickSounds = new List<AudioClip>();
	private float t = 0;
	public float yDiff = 0;
	public GameObject Player;
	private bool upPlayed = false;

	void Start () {
		audioBoy = gameObject.GetComponent<AudioSource>();
		Player = GameObject.Find("player");
	}
	
	void Update () {
		yDiff = Player.transform.position.y - gameObject.transform.position.y;
		if (yDiff < -2) {
			audioBoy.volume = 0f;
		} else {
			audioBoy.volume = 1f;
		}
		t += Time.deltaTime;
		if (t > 1 && upPlayed == false){
			audioBoy.clip = upTickSounds[Random.Range (0, upTickSounds.Count)];
			audioBoy.Play();
			upPlayed = true;
		}
		if (t > 2){
			audioBoy.clip = downTickSounds[Random.Range (0, upTickSounds.Count)];
			audioBoy.Play();
			upPlayed = false;
			t = 0;
		}
	}
}
