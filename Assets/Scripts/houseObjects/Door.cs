﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RootMotion.FinalIK;

public class Door : MonoBehaviour {

	private AudioSource audioBoy;
	public List<AudioClip> openSounds = new List<AudioClip>();
	public List<AudioClip> closeSounds = new List<AudioClip>();
	private Transform LTag;
	private Transform RTag;
	private GameObject Player;
	public InteractionContainer innerTagL;
	public InteractionContainer innerTagR;
	public GameObject doorBlocker;
	public GameObject walkPointL;
	public GameObject walkPointL2;
	public GameObject walkPointR;
	public bool flip;
	private InteractionContainer LTagContainer;
	private InteractionContainer RTagContainer;
	private float t;
	public Vector3 startRotation;
	public Vector3 currentRotation;
	private float goalRotation;
	private bool doorOpen = false;
	public bool locked = false;
	private float lDist;
	private float rDist;
	public GameObject offMeshLink;

	void Start () {
		audioBoy = gameObject.GetComponent<AudioSource>();
		Player = GameObject.FindGameObjectWithTag ("Player");
		LTag = gameObject.transform.Find ("DoorTagL");
		RTag = gameObject.transform.Find ("DoorTagR");
		LTagContainer = LTag.GetComponent<InteractionContainer> ();
		RTagContainer = RTag.GetComponent<InteractionContainer> ();
		startRotation = gameObject.transform.rotation.eulerAngles;
		if (flip) {
			goalRotation = (startRotation.z + 100);
		} else {
			goalRotation = (startRotation.z - 100);
		}
	}

	void Update () {

		if (locked) {
			offMeshLink.SetActive (false);
		} else {
			offMeshLink.SetActive (true);
		}

		currentRotation = transform.eulerAngles;

		lDist = Vector3.Distance (Player.transform.position, walkPointL.transform.position);
		rDist = Vector3.Distance (Player.transform.position, walkPointR.transform.position);

		if (doorOpen == true) {
			if (lDist > rDist) {
				LTagContainer.gameObject.SetActive(false);
				RTagContainer.gameObject.SetActive(true);
				innerTagL.gameObject.SetActive(false);
				innerTagR.gameObject.SetActive(true);
			} else {
				LTagContainer.gameObject.SetActive(true);
				RTagContainer.gameObject.SetActive(false);
				innerTagL.gameObject.SetActive(true);
				innerTagR.gameObject.SetActive(false);
			}
			doorBlocker.SetActive (false);
			gameObject.transform.eulerAngles = new Vector3 (startRotation.x, startRotation.y, Mathf.Lerp (goalRotation, startRotation.z, t));
			t += 0.5f * Time.deltaTime;
			if (LTagContainer.activated || innerTagL.activated) {
				LTagContainer.activated = false;
				RTagContainer.activated = false;
				innerTagL.activated = false;
				innerTagR.activated = false;
				if (locked == false) {
					Player.GetComponent<playerController> ().canInteract = false;
					StartCoroutine (OpenFastL ());
				}
			}
			if (RTagContainer.activated || innerTagR.activated) {
				LTagContainer.activated = false;
				RTagContainer.activated = false;
				innerTagL.activated = false;
				innerTagR.activated = false;
				if (locked == false) {
					Player.GetComponent<playerController> ().canInteract = false;
					StartCoroutine (OpenFastR ());
				}
			}
			if (t > 0.5f && currentRotation.y == startRotation.y) {
				closeSound ();
				gameObject.transform.rotation = Quaternion.Euler (startRotation);
				doorBlocker.SetActive (true);
				doorOpen = false;
			}
		} else {
			if (lDist > rDist) {
				LTagContainer.gameObject.SetActive (false);
				RTagContainer.gameObject.SetActive (true);
			} else {
				LTagContainer.gameObject.SetActive(true);
				RTagContainer.gameObject.SetActive(false);
			}
			innerTagL.gameObject.SetActive(false);
			innerTagR.gameObject.SetActive(false);
			if (LTagContainer.activated) {
				LTagContainer.activated = false;
				RTagContainer.activated = false;
				if (locked == false) {
					Player.GetComponent<playerController> ().canInteract = false;
					StartCoroutine (OpenL ());
				}
			}
			if (RTagContainer.activated) {
				LTagContainer.activated = false;
				RTagContainer.activated = false;
				if (locked == false) {
					Player.GetComponent<playerController> ().canInteract = false;
					StartCoroutine (OpenR ());
				}
			}
		}
}

	void OnTriggerEnter (Collider doorHit){
		if (doorHit.gameObject == Player && locked == false) {
			if (Player.GetComponent<playerController> ().running == true) {
//OPEN DOOR ON COLLISION WHILE RUNNING -- GOOD IDEA OR NO? I'M NOT INTO IT SO FAR
				//StartCoroutine (OpenEasy ());
			} else if (doorOpen == true) {
				StartCoroutine (OpenEasy ());
			} 
		} else if (doorHit.gameObject.tag == "NPC" && locked == false) {
			StartCoroutine (OpenEasy ());
		}
	}

	public IEnumerator OpenL(){
		Debug.Log ("ACTIVATE IT");
		doorOpen = false;
		t = 0;
		Player.GetComponent<playerController> ().canWalkExt = false;
		yield return new WaitForSeconds (0.15f);
		Player.GetComponent<UnityEngine.AI.NavMeshAgent> ().angularSpeed = 0;
		Player.GetComponent<playerController> ().ScreenPickPos = walkPointL2.transform.position;
		Player.GetComponent<playerController> ().walkOverride ();
		yield return new WaitForSeconds (0.2f);
		openSound ();
		iTween.RotateTo(gameObject, iTween.Hash ("x", startRotation.x, "y", startRotation.y, "z", goalRotation, "time", 1.1f, "easetype", "easeOutQuad"));
		yield return new WaitForSeconds (0.3f);
		doorBlocker.SetActive (false);
		Player.GetComponent<UnityEngine.AI.NavMeshAgent> ().angularSpeed = 9999;
		Player.GetComponent<playerController> ().ScreenPickPos = walkPointR.transform.position;
		Player.GetComponent<playerController> ().walkOverride ();
		yield return new WaitForSeconds (0.8f);
		Player.GetComponent<playerController> ().canWalkExt = true;
		Player.GetComponent<playerController> ().canInteract = true;
		doorOpen = true;
		yield break;
	}

	public IEnumerator OpenR(){
		Debug.Log ("ACTIVATE IT");
		doorOpen = false;
		t = 0;
		Player.GetComponent<playerController> ().canWalkExt = false;
		yield return new WaitForSeconds (0.25f);
		openSound ();
		iTween.RotateTo(gameObject, iTween.Hash ("x", startRotation.x, "y", startRotation.y, "z", goalRotation, "time", 1.1f, "easetype", "easeOutQuad"));
		doorBlocker.SetActive (false);
		Player.GetComponent<playerController> ().ScreenPickPos = walkPointL2.transform.position;
		Player.GetComponent<playerController> ().walkOverride ();
		yield return new WaitForSeconds (1.1f);
		Player.GetComponent<playerController> ().canWalkExt = true;
		Player.GetComponent<playerController> ().canInteract = true;
		doorOpen = true;
		yield break;
	}

	public IEnumerator OpenFastL(){
		Debug.Log ("ACTIVATE IT");
		doorOpen = false;
		Player.GetComponent<playerController> ().canWalkExt = false;
		if (t > 0.25f) {
			Player.GetComponent<UnityEngine.AI.NavMeshAgent> ().angularSpeed = 0;
			Player.GetComponent<playerController> ().ScreenPickPos = walkPointL2.transform.position;
			Player.GetComponent<playerController> ().walkOverride ();
			yield return new WaitForSeconds (0.1f);
			openSound ();
			iTween.RotateTo(gameObject, iTween.Hash ("x", startRotation.x, "y", startRotation.y, "z", goalRotation, "time", 1.1f, "easetype", "easeOutQuad"));
			yield return new WaitForSeconds (0.3f);
			Player.GetComponent<playerController> ().pInteractSys.StopInteraction(FullBodyBipedEffector.RightHand);
			Player.GetComponent<playerController> ().pInteractSys.StopInteraction(FullBodyBipedEffector.LeftHand);
		} else {
			Player.GetComponent<playerController> ().pInteractSys.StopInteraction(FullBodyBipedEffector.RightHand);
			openSound ();
			iTween.RotateTo(gameObject, iTween.Hash ("x", startRotation.x, "y", startRotation.y, "z", goalRotation, "time", 1.1f, "easetype", "easeOutQuad"));
//			yield return new WaitForSeconds (0.1f);
			Player.GetComponent<playerController> ().pInteractSys.StopInteraction(FullBodyBipedEffector.RightHand);
			Player.GetComponent<playerController> ().pInteractSys.StopInteraction(FullBodyBipedEffector.LeftHand);
		}
		t = 0;
		yield return new WaitForSeconds (0.1f);
		doorBlocker.SetActive (false);
		Player.GetComponent<UnityEngine.AI.NavMeshAgent> ().angularSpeed = 9999;
		Player.GetComponent<playerController> ().ScreenPickPos = walkPointR.transform.position;
		Player.GetComponent<playerController> ().walkOverride ();
		yield return new WaitForSeconds (0.8f);
		Player.GetComponent<playerController> ().canWalkExt = true;
		Player.GetComponent<playerController> ().canInteract = true;
		doorOpen = true;
		yield break;
	}

	public IEnumerator OpenFastR(){
		Debug.Log ("ACTIVATE IT");
		doorOpen = false;
		Player.GetComponent<playerController> ().canWalkExt = false;
		if (t < 0.25f) {
			Player.GetComponent<InteractionSystem> ().StopInteraction(FullBodyBipedEffector.RightHand);
			Player.GetComponent<InteractionSystem> ().StopInteraction(FullBodyBipedEffector.LeftHand);
		}
		t = 0;
//		yield return new WaitForSeconds (0.25f);
		openSound ();
		iTween.RotateTo(gameObject, iTween.Hash ("x", startRotation.x, "y", startRotation.y, "z", goalRotation, "time", 1.1f, "easetype", "easeOutQuad"));
		doorBlocker.SetActive (false);
		Player.GetComponent<playerController> ().ScreenPickPos = walkPointL2.transform.position;
		Player.GetComponent<playerController> ().walkOverride ();
		yield return new WaitForSeconds (1.1f);
		Player.GetComponent<playerController> ().canWalkExt = true;
		Player.GetComponent<playerController> ().canInteract = true;
		doorOpen = true;
		yield break;
	}

	public IEnumerator OpenEasy(){
		Debug.Log ("ACTIVATE IT");
		doorOpen = false;
		if (t < 0.25f) {
			Player.GetComponent<playerController>().humeObject.GetComponent<InteractionSystem> ().StopInteraction(FullBodyBipedEffector.RightHand);
			Player.GetComponent<playerController>().humeObject.GetComponent<InteractionSystem> ().StopInteraction(FullBodyBipedEffector.LeftHand);
		}
		t = 0;
		openSound ();
		iTween.RotateTo(gameObject, iTween.Hash ("x", startRotation.x, "y", startRotation.y, "z", goalRotation, "time", 1.1f, "easetype", "easeOutQuad"));
		doorBlocker.SetActive (false);
		yield return new WaitForSeconds (1.1f);
		doorOpen = true;
		yield break;
	}

	public void openSound () {
		if (!audioBoy.isPlaying) {
			audioBoy.clip = openSounds [Random.Range (0, openSounds.Count)];
			audioBoy.Play ();
		}
	}

	public void closeSound () {
		if (!audioBoy.isPlaying) {
			audioBoy.clip = closeSounds [Random.Range (0, closeSounds.Count)];
			audioBoy.Play ();
		}
	}
}
