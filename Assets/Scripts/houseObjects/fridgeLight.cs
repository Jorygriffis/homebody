﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fridgeLight : MonoBehaviour {

	public GameObject lightObj;
	private genericDoorAnimated doorScript;

	void Start () {
		doorScript = gameObject.GetComponent<genericDoorAnimated> ();
	}

	void Update () {
		if (doorScript.open) {
			lightObj.SetActive (true);
		} else {
			lightObj.SetActive (false);
		}
	}
}
