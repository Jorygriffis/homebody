﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stickToObject : MonoBehaviour {

	public Vector3 startPos;

	void Start () {
		startPos = gameObject.transform.position;
	}

	void Update () {
		transform.position = startPos;
	}
}
