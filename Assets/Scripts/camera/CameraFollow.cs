﻿using UnityEngine;
using System.Collections;
using NaughtyAttributes;

public class CameraFollow : MonoBehaviour {

	[Tooltip("Multiplier for camera transform/rotation lerp. Usually should be set to a high value.")]
	public float lerpRate = 15;
	[Tooltip("Experimental camera relative control joystick input offset--theoretically used to adjust joystick input to 8-way angles. Doesn't feel very good but I'm leaving it in.")]
	public float relativeControlOffset;
	public GameObject Player;
//	public float xOffset;
//	public float yOffset;
//	public float xMinClamp;
//	public float yMinClamp;

//	public bool xEnable;
//	public bool yEnable;

	[Tooltip("If true, camera transforms on local Z to follow player.")]
	public bool zEnable = false;
	[Tooltip("If true, camera transforms on local X to follow player.")]
	public bool xEnable = false;
	public float currentX;
	public float playerCurrentX;
	[ShowIf("xEnable")]
	public float startX;
	[ShowIf("xEnable")][Tooltip("Maximum X position relative to startX.")]
	public float maxX;
	[ShowIf("xEnable")][Tooltip("Maximum X position relative to startX.")]
	public float minX;

	[ShowIf("zEnable")][Tooltip("Optional start Z position--if 0, defaults to onEnable Z position.")]
	public float startZ;
	[ShowIf("zEnable")][Tooltip("If true, Z follow direction (including offset direction) is flipped.")]
	public bool flip;
	[ShowIf("zEnable")][Tooltip("Distance offset from player for Z follow.")]
	public float zOffset = 4f;
	[ShowIf("zEnable")][Tooltip("If true, Z position is clamped to a minimum value to present a static opening shot.")]
	public bool zClamp;
	[ShowIf("zClamp")][Tooltip("Starting Z position for Z clamp.")]
	public float zMinClamp;
	[Tooltip("If true, camera rotates on Y axis to face player.")]
	public bool lookAtPlayer;
	[ShowIf("lookAtPlayer")]
	public float lookMinClamp;
	[ShowIf("lookAtPlayer")]
	public float lookMaxClamp;

	private float startRotZ;
	private float startRotX;

	void Start () {
		startX = transform.position.x;
	}

	void OnEnable () {
		startRotZ = gameObject.transform.localEulerAngles.z;
		startRotX = gameObject.transform.localEulerAngles.x;
		if (startZ == 0) {
			startZ = gameObject.transform.position.z;
		}
		if (zEnable) {
			if (!flip) {
				if (Player.transform.position.z > (startZ + zOffset)) {
					float playerPosZ = Player.transform.position.z;
					if (playerPosZ >= zMinClamp) {
						if (zClamp == true) {
							gameObject.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, (zMinClamp - zOffset));
						} else {
							gameObject.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, (playerPosZ - zOffset));
						}
					} else {
						gameObject.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, (playerPosZ - zOffset));
					}
				} else {
					gameObject.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, startZ);
				}
			} else {
				if (Player.transform.position.z < (startZ - zOffset)) {
					float playerPosZ = Player.transform.position.z;
					gameObject.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, (playerPosZ + zOffset));
				} else {
					gameObject.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, startZ);
				}
			}
		}
		if (xEnable && startX != 0f) {
			currentX = transform.position.x;
			float playerPosX = Player.transform.position.x;
			playerCurrentX = playerPosX;
			if (playerPosX < startX + minX) {
				gameObject.transform.position = new Vector3 (startX + minX, gameObject.transform.position.y, gameObject.transform.position.z);
			} else if (playerPosX > startX + maxX) {
				gameObject.transform.position = new Vector3 (startX + maxX, gameObject.transform.position.y, gameObject.transform.position.z);
			} else if (playerPosX < startX + maxX && playerPosX > startX + minX) {
				gameObject.transform.position = new Vector3 (playerPosX, gameObject.transform.position.y, gameObject.transform.position.z);
			}
		}
		if (lookAtPlayer) {
			Vector3 lookTarget = new Vector3 (Player.transform.position.x, gameObject.transform.position.y, Player.transform.position.z);
			Vector3 targetDir = lookTarget - transform.position;
			Quaternion rotation = Quaternion.LookRotation (targetDir, Vector3.up);
			transform.rotation = rotation;
			gameObject.transform.localEulerAngles = new Vector3 (startRotX, Mathf.Clamp (gameObject.transform.localEulerAngles.y, lookMinClamp, lookMaxClamp), startRotZ);
		}
	}

	void Update () {
		if (zEnable) {
			if (!flip) {
				if (Player.transform.position.z > (startZ + zOffset)) {
					float playerPosZ = Player.transform.position.z;
					if (playerPosZ >= zMinClamp) {
						if (zClamp == true) {
							gameObject.transform.position = Vector3.Lerp(transform.position, new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, (zMinClamp - zOffset)), Time.deltaTime * lerpRate);
						} else {
							gameObject.transform.position = Vector3.Lerp(transform.position, new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, (playerPosZ - zOffset)), Time.deltaTime * lerpRate);
						}
					} else {
						gameObject.transform.position = Vector3.Lerp(transform.position, new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, (playerPosZ - zOffset)), Time.deltaTime * lerpRate);
					}
				} else {
					gameObject.transform.position = Vector3.Lerp(transform.position, new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, startZ), Time.deltaTime * lerpRate);
				}
			} else {
				if (Player.transform.position.z < (startZ - zOffset)) {
					float playerPosZ = Player.transform.position.z;
					gameObject.transform.position = Vector3.Lerp(transform.position, new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, (playerPosZ + zOffset)), Time.deltaTime * lerpRate);
				} else {
					gameObject.transform.position = Vector3.Lerp(transform.position, new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, startZ), Time.deltaTime * lerpRate);
				}
			}
		}
		if (xEnable && startX != 0f) {
			currentX = transform.position.x;
			float playerPosX = Player.transform.position.x;
			playerCurrentX = playerPosX;
			if (playerPosX < startX + maxX && playerPosX > startX + minX) {
				gameObject.transform.position = Vector3.Lerp (transform.position, new Vector3 (playerPosX, gameObject.transform.position.y, gameObject.transform.position.z), Time.deltaTime * lerpRate);
			}
		}
		if (lookAtPlayer) {
			Vector3 lookTarget = new Vector3 (Player.transform.position.x, gameObject.transform.position.y, Player.transform.position.z);
			Vector3 targetDir = lookTarget - transform.position;
			Quaternion rotation = Quaternion.LookRotation (targetDir, Vector3.up);
			transform.rotation = Quaternion.Lerp (transform.rotation, rotation, Time.deltaTime * lerpRate);
			gameObject.transform.localEulerAngles = new Vector3 (startRotX, Mathf.Clamp(gameObject.transform.localEulerAngles.y, lookMinClamp, lookMaxClamp), startRotZ);
		}
	}
}
