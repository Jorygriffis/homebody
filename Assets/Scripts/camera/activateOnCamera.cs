﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class activateOnCamera : MonoBehaviour {

	public GameObject targetCam;

	void Update () {
		if (targetCam.tag == "MainCamera") {
			gameObject.GetComponent<MeshRenderer> ().enabled = true;
		} else {
			gameObject.GetComponent<MeshRenderer> ().enabled = false;
		}
	}
}
