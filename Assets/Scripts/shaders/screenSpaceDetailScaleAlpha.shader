﻿Shader "Custom/ScreenPosAlpha" {
    Properties {
      _Color ("Color", Color) = (1,1,1,1)
      _MainTex ("Texture", 2D) = "white" {}
      _Detail ("Detail", 2D) = "gray" {}
    }
    SubShader {
      Tags { "RenderType" = "Opaque" }
      CGPROGRAM
      #pragma surface surf Lambert alpha
      struct Input {
          float2 uv_MainTex;
          float4 screenPos;
          float3 worldPos;
      };
      sampler2D _MainTex;
      sampler2D _Detail;
      fixed4 _Color;

      void surf (Input IN, inout SurfaceOutput o) {
          o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb;
          float2 screenUV = IN.screenPos.x * IN.screenPos.y / IN.screenPos.w * 0.5;
          float camDist = distance(IN.worldPos, _WorldSpaceCameraPos) * 1.1;
          //screenUV *= float2(8,6);
          screenUV *= float2(camDist * 3, camDist * 2.25);
          o.Albedo *= tex2D (_Detail, screenUV).rgb * 2;
          o.Alpha = _Color.a;
      }
      ENDCG
    } 
    Fallback "Diffuse"
  }