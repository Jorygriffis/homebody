﻿Shader "ColorAdditiveShader" {
	Properties {
		_Color ("Main Color, Alpha", Color) = (1,1,1,1)
	}
	Category {
		ZWrite Off
		ZTest LEqual
		Lighting Off
		Tags {"Queue" = "AlphaTest+505" "IgnoreProjector" = "True"}
		Blend Zero OneMinusSrcAlpha
		Color [_Color]
		SubShader {
			Pass {
				Cull Back
				Fog {Mode Off}
			}
		}
	} 
}