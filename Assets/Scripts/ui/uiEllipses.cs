﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class uiEllipses : MonoBehaviour {

	private float t = 0;
	public Sprite frame1;
	public Sprite frame2;
	public Sprite frame3;
	public float waitTime = 0.75f;
	private Image targetImage;
	private bool animate = true;

	void Start () {
		targetImage = gameObject.GetComponent<Image>();
		targetImage.enabled = false;
	}

	void OnEnable () {
		t = 0;
		gameObject.GetComponent<Image>().sprite = frame1;
		StartCoroutine (getReady());
	}

	void Update () {
		if (targetImage.enabled == true) {
			t += Time.deltaTime;
			if (t > 1) {
				t = 0;
			} else if (t > 0.666f) {
				targetImage.sprite = frame3;
			} else if (t > 0.333f) {
				targetImage.sprite = frame2;
			} else {
				targetImage.sprite = frame1;
			}
		}
	}

	IEnumerator getReady() {
		yield return new WaitForSeconds (waitTime);
		targetImage.enabled = true;
		yield break;
	}

	void OnDisable () {
		targetImage.enabled = false;
	}
}
