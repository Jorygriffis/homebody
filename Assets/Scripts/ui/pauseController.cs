using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pauseController : MonoBehaviour {

	public Toggle tankControlToggle;
	public Toggle rightStickModeToggle;
	public Toggle autoFlashlightToggle;
	public Toggle resumeGame;
	public Toggle quitGame;
	public GameObject inkCanvas;
	private ArrayContainer manager;
	private GameObject Player;
	private GameObject pausePanel;
	private bool pausePanelBool = false;

	void Start () {
		pausePanel = GameObject.Find("Pause Panel");
		manager = GameObject.Find("Manager").GetComponent<ArrayContainer>();
		Player = manager.Player;
	}

	void Update () {
		if (gameObject.GetComponent<Canvas>().enabled == true){
			inkCanvas.SetActive (false);
			pausePanel.SetActive(true);
			if (pausePanelBool == false){
				resumeGame.Select();
				resumeGame.OnSelect(null);
				pausePanelBool = true;
			}
			if (tankControlToggle.isOn == true){
				Player.GetComponent<playerController>().tankMode = true;
			} else {
				Player.GetComponent<playerController>().tankMode = false;
			}
			if (rightStickModeToggle.isOn == true){
				manager.rightStickMode = true;
			} else {
				manager.rightStickMode = false;
			}
			if (autoFlashlightToggle.isOn == true){
				Player.GetComponent<playerController>().flashlightAuto = true;
			} else {
				Player.GetComponent<playerController>().flashlightAuto = false;
			}
			if (resumeGame.isOn == true){
				resumeGame.isOn = false;
				manager.togglePauseMenu ();
			}
			if (quitGame.isOn == true){
				quitGame.isOn = false;
				Application.Quit ();
			}
		} else {
			inkCanvas.SetActive (true);
			pausePanelBool = false;
			pausePanel.SetActive(false);
		}
	}
}
