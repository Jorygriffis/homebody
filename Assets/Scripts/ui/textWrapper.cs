﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Text;

public class textWrapper : MonoBehaviour {

	public string textObjAnimation = "none";
	public string textObjPAnimation = "none";
	public GameObject textPrefab;
	public Font boldFont;
	public string text;
	public float delay;
	public float baseDelay;
	public Transform contentBox;
	public int selfOrder;
	public float textSpeed;
	private bool textPresent = false;
	public inkDialogue iDial;

	void Start () {
		
	}

	void Update () {
		delay -= Time.deltaTime / iDial.typeSpeed;
		if (delay <= 0 && textPresent == false) {
			if (iDial.newestTyper != null) {
				iDial.newestTyper.GetComponent<TypeWriterEffect> ().forceFinished = true;
			}
			GameObject storyText = Instantiate (textPrefab) as GameObject;
			iDial.newestTyper = storyText;
			storyText.gameObject.GetComponent<TypeWriterEffect> ().iDial = iDial;
			storyText.gameObject.GetComponent<TypeWriterEffect> ().textObjAnimation = textObjAnimation;
			storyText.gameObject.GetComponent<TypeWriterEffect> ().textObjPAnimation = textObjPAnimation;
			if (selfOrder <= 1) {
				storyText.gameObject.GetComponent<TypeWriterEffect>().fullText = text;
				storyText.gameObject.GetComponent<TypeWriterEffect>().forceFinished = true;
				storyText.gameObject.GetComponent<TypeWriterEffect>().finished = true;
				storyText.gameObject.GetComponent<TypeWriterEffect>().enabled = false;
				iDial.ellipses.SetActive (true);
				storyText.gameObject.GetComponent<Text>().text = text;
				storyText.gameObject.GetComponent<Text>().font = boldFont;
			} else {
				storyText.gameObject.GetComponent<TypeWriterEffect>().fullText = text;
			}
			storyText.transform.SetParent (contentBox, false);
			textPresent = true;
		}
	}		
}
