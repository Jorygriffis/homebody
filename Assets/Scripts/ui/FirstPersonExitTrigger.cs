﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonExitTrigger : MonoBehaviour {

	public InteractionContainer iContainer;
	public InteractionContainer cameraTrigger;

	void Update () {
		if (iContainer.activated) {
			cameraTrigger.activated = true;
			iContainer.activated = false;
	}
}
}
