﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Ink.Runtime;
using Rewired;
using System.IO;
using System.Text;
using UnityEngine.SceneManagement;

public class inkDialogue : MonoBehaviour {

	public string playerNameTest = "crud";
	public string missingKidNameTest = "crap";
	public bool housePuzzleStates = true;
	public string savedJson;
	public TextAsset inkJSONAsset;
	public worldStateManager stateManager;
	public GameObject dialogStarter;
	public float textDelay = 3.2f;
	public float textSpeed = 0.2f;
	public float charMultiplier = 0.15f;
	public GameObject ellipses;

	private bool readyToEnd = false;
	private bool firstButton = false;
	private playerController Player;
	private ArrayContainer Container;
	private Player player;
	[HideInInspector]
	public GameObject textBoxy;
	private string savePath = "homebody_Data/Resources/save.txt";
	private GameObject[] textObjects;
	private float leastDelay = 666f;
	public Story story;
	[HideInInspector]
	public bool textBoxPresent = false;
	private GameObject BottomBox;
	private int textOrder = 0;
	[HideInInspector]
	public int canvasChildCount = 0;
	[HideInInspector]
	public float buttonDelay = 0.3f;
	[HideInInspector]
	public float totalDelay = 0;
	[HideInInspector]
	public GameObject wrapperToAdvance;
	private float delaySubtractor = 0f;
	private List<GameObject> textWrappers = new List<GameObject> ();
	[HideInInspector]
	public GameObject newestTyper;
	private float choiceWait = 666f;
	[HideInInspector]
	public float typeSpeed = 1f;
	public bool choicesPresent = false;

	[SerializeField]
	private Canvas canvas;

// UI Prefabs
	public GameObject textWrapperPrefab;
	[SerializeField]
	private Button buttonPrefab;
	public GameObject textBoxPrefab;

	void Start () {
		#if UNITY_EDITOR
		savePath = "Assets/Resources/save.txt";
		#endif
		Player = GameObject.Find ("player").GetComponent<playerController> ();
		Container = GameObject.Find ("Manager").GetComponent<ArrayContainer> ();
		player = Rewired.ReInput.players.GetPlayer (0);
		story = new Story (inkJSONAsset.text);
		if (File.Exists (savePath) != false) {
			savedJson = System.IO.File.ReadAllText (savePath);
		}
		if (savedJson != string.Empty) {
			story.state.LoadJson (savedJson);
			story.ResetCallstack ();
		}

		if ((string) story.variablesState ["playerName"] == "null") {
			Debug.Log ("getting player name from container");
			story.variablesState ["playerName"] = Container.playerName;
		} else {
			Debug.Log ("giving container player name");
			Container.playerName = (string) story.variablesState ["playerName"];
		}
		Container.playerInit ();
		playerNameTest = (string) story.variablesState ["playerName"];
		if ((string) story.variablesState ["MissingKid"] == "null") {
			Debug.Log ("choosing missing kid");
			Container.chooseMissingKid ();
			story.variablesState ["MissingKid"] = Container.missingKidName;
		} else {
			Debug.Log ("giving container missing kid name");
			Container.missingKidName = (string) story.variablesState ["MissingKid"];
		}
		missingKidNameTest = (string) story.variablesState ["MissingKid"];
		saveGame ();
		Debug.Log ("disable bad kids");
		Container.disableBadKids ();
	}

// Creates a new Story object with the compiled story which we can then play!
	public void StartStory (string lineString, GameObject activator) {

		dialogStarter = activator;

		if (dialogStarter.GetComponent<NPC> () != null) {
			story.variablesState ["Speaker"] = dialogStarter.GetComponent<NPC> ().npcName;
			dialogStarter.GetComponent<NPC> ().lookAtPlayer = true;
		}

		if (File.Exists (savePath) != false) {
			savedJson = System.IO.File.ReadAllText (savePath);
		}
		if (savedJson != string.Empty) {
			//story.state.LoadJson (savedJson);
			story.ResetCallstack ();
		}

		if (housePuzzleStates == true) {
			NPCRoomTestAll ();
		}

		if (lineString != "start") {
			story.ChoosePathString (lineString);
		} else {
			story.ChoosePathString ("startPoint");
		}

//		if (lineString == "resetStatus") {
//			RefreshViewSilent ();
//		} else {
//			story.ResetCallstack ();
			RefreshView ();
//		}
	}
	
// This is the main function called every time the story changes. It does a few things:
// Destroys all the old content and choices.
// Continues over all the lines of text, then displays all the choices. If there are no choices, the story is finished!
	void RefreshView () {
//REMOVE OPTION TEXT
		if (BottomBox != null) {
			RemoveChildrenBottom ();
		}

		firstButton = false;

		if (textBoxPresent == false) {
			textBoxPresent = true;
			textOrder = 666;
			textBoxy = Instantiate (textBoxPrefab) as GameObject;
			ellipses = textBoxy.transform.Find ("TopBox").transform.Find ("Ellipses").gameObject;
			textBoxy.transform.SetParent (canvas.transform, false);
		} else {
			int textObjects = textBoxy.transform.Find ("TopBox").transform.Find ("Content").childCount;
			for (int i = 0; i < textObjects; ++i) {
				GameObject textObj = textBoxy.transform.Find ("TopBox").transform.Find ("Content").GetChild (i).gameObject;
				if (textObj.GetComponent<Text> () != null) {
					textObj.GetComponent<Text> ().color = new Color32 (105, 105, 105, 255);
				}
			}
		}

// Read all the content until we can't continue any more
		while (story.canContinue) {
// Continue gets the next line of the story
			string text = story.Continue ();
// This removes any white space from the text.
			text = text.Trim ();
//SPECIAL ACTIONS
			if (text.Split (' ') [0] == "LOADSCENE") {
				Debug.Log ("load scene");
				saveGame ();
				StartCoroutine (loadScene (text.Split (' ') [1]));
			} else if (text == "CLEARSAVE") {
				savedJson = string.Empty;
				saveGame ();
			} else if (text == "FOLLOWPLAYER" && dialogStarter.GetComponent<NPC> () != null) {
				dialogStarter.GetComponent<NPC> ().followPlayer = true;
			} else if (text == "STOPFOLLOWPLAYER" && dialogStarter.GetComponent<NPC> () != null) {
				dialogStarter.GetComponent<NPC> ().followPlayer = false;
			} else if (text == "FOLLOWTEST" && dialogStarter.GetComponent<NPC> () != null) {
				if (dialogStarter.GetComponent<NPC> ().followPlayer == false) {
					story.variablesState ["NPCFollowTest"] = 0;
				} else {
					story.variablesState ["NPCFollowTest"] = 1;
				}
			} else {
//PLAIN TEXT
				StartCoroutine (CreateContentViewDelayW (text));
			}
		}

// Display all the choices, if there are any!
		if(story.currentChoices.Count > 0) {
			StartCoroutine(PrepareChoices());
		}
			
//OUT OF CHOICES--DISPLAY LAST DIALOGUE, SAVE STORY STATE, SET READYTOEND
		else {
			StartCoroutine (delayInput ());
			saveGame ();
		}
	}

	void RefreshViewSilent () {
		// Read all the content until we can't continue any more
		while (story.canContinue) {
			// Continue gets the next line of the story
			string text = story.Continue ();
			// This removes any white space from the text.
			text = text.Trim ();
		}
	}

	void Update (){

//READY TO END--SUBMIT BUTTON OR MOUSE CLICK CLOSES DIALOG
		if (player.GetButtonDown ("Submit") || Input.GetMouseButtonDown (0)) {
			if (readyToEnd == true) {
				RemoveChildren ();
				Player.canInteract = true;
				textBoxPresent = false;
				readyToEnd = false;
			} else if (newestTyper != null && newestTyper.GetComponent<TypeWriterEffect> ().finished == false) {
				newestTyper.GetComponent<TypeWriterEffect> ().forceFinished = true;
				newestTyper.GetComponent<TypeWriterEffect> ().finished = true;
				LayoutRebuilder.ForceRebuildLayoutImmediate (textBoxy.GetComponent<RectTransform> ());
				LayoutRebuilder.ForceRebuildLayoutImmediate (newestTyper.GetComponent<RectTransform> ());
			} else {
				wrapperToAdvance = null;
				leastDelay = 666f;
				delaySubtractor = 0f;
				foreach (GameObject wrapper in textWrappers) {
					if (wrapper.GetComponent<textWrapper> ().delay > 0 && wrapper.GetComponent<textWrapper> ().delay < leastDelay) {
						leastDelay = wrapper.gameObject.GetComponent<textWrapper> ().delay;
						wrapperToAdvance = wrapper.gameObject;
					}
				}
				if (wrapperToAdvance != null) {
					delaySubtractor = wrapperToAdvance.GetComponent<textWrapper> ().delay;
					totalDelay -= (wrapperToAdvance.GetComponent<textWrapper> ().delay / textSpeed);
				}
				foreach (GameObject wrapper in textWrappers) {
					if (wrapper.GetComponent<textWrapper> () != null) {
						wrapper.GetComponent<textWrapper> ().delay -= delaySubtractor;
					}
				}
			}
		}

		if (player.GetButton ("Submit") == true || Input.GetMouseButton (0) == true) {
			typeSpeed = 0.4f;
		} else {
			typeSpeed = 1f;
		}
			
		if (newestTyper != null && newestTyper.GetComponent<TypeWriterEffect> ().finished == false || choicesPresent == true) {
			ellipses.SetActive (false);
		}

		if (newestTyper != null) {
			if (newestTyper.GetComponent<TypeWriterEffect>().textObjAnimation != "none") {
				Debug.Log ("npc animating");
				dialogStarter.GetComponent<NPC> ().dialogOverride = true;
				dialogStarter.GetComponent<NPC> ().crossFadeAnim (newestTyper.GetComponent<TypeWriterEffect>().textObjAnimation, 0.35f);
			} 
			if (newestTyper.GetComponent<TypeWriterEffect> ().textObjPAnimation == "none") {
				if (Player.dialogOverride == true) {
					StartCoroutine (stopPlayerAnimDelay ());
				}
			} else {
				Debug.Log ("player animating");
				Player.dialogOverride = true;
				Player.gameObject.GetComponent<playerController> ().crossFadeAnim (newestTyper.GetComponent<TypeWriterEffect>().textObjPAnimation, 0.35f);
			}
		}

		int childCount = canvas.transform.childCount;
		canvasChildCount = childCount;
		if (childCount > 0) {
			Player.canWalkExt = false;
		} else {
			Player.canWalkExt = true;
		}
	}

// When we click the choice button, tell the story to choose that choice!
	void OnClickChoiceButton (Choice choice) {
		story.ChooseChoiceIndex (choice.index);
		RefreshView();
	}

	public IEnumerator CreateContentViewDelayW (string text) {
		float previousDelay = totalDelay;
		textOrder += 1;
		int selfOrder = textOrder;
		if (text == string.Empty) {
			textDelay = 0.1f;
		} else {
			textDelay = 3.2f;
			GameObject storyText = Instantiate (textWrapperPrefab) as GameObject;
			textWrappers.Add (storyText);

//ANIMATION TAGS
			foreach (string Tag in story.currentTags) {
				if (Tag == "anim_idle") {
					storyText.GetComponent<textWrapper> ().textObjAnimation = "BaseTestIdle";
				} else if (Tag == "anim_handsonhips") {
					storyText.GetComponent<textWrapper> ().textObjAnimation = "HandsOnHips";
				} else if (Tag == "anim_basetestnod") {
					storyText.GetComponent<textWrapper> ().textObjAnimation = "BaseTestNod";
				} else if (Tag == "anim_armscrossed") {
					storyText.GetComponent<textWrapper> ().textObjAnimation = "ArmsCrossed";
				} else if (Tag == "anim_talkhandsboth") {
					storyText.GetComponent<textWrapper> ().textObjAnimation = "TalkHandsBoth";
				} else if (Tag == "anim_talkhandsr") {
					storyText.GetComponent<textWrapper> ().textObjAnimation = "TalkHandsR";
				} else if (Tag == "anim_talkhandsl") {
					storyText.GetComponent<textWrapper> ().textObjAnimation = "TalkHandsL";
				}
				if (Tag == "player_idle") {
					storyText.GetComponent<textWrapper> ().textObjPAnimation = "BaseTestIdle";
				} else if (Tag == "player_handsonhips") {
					storyText.GetComponent<textWrapper> ().textObjPAnimation = "HandsOnHips";
				} else if (Tag == "player_basetestnod") {
					storyText.GetComponent<textWrapper> ().textObjPAnimation = "BaseTestNod";
				} else if (Tag == "player_armscrossed") {
					storyText.GetComponent<textWrapper> ().textObjPAnimation = "ArmsCrossed";
				} else if (Tag == "player_talkhandsboth") {
					storyText.GetComponent<textWrapper> ().textObjPAnimation = "TalkHandsBoth";
				} else if (Tag == "player_talkhandsr") {
					storyText.GetComponent<textWrapper> ().textObjPAnimation = "TalkHandsR";
				} else if (Tag == "player_talkhandsl") {
					storyText.GetComponent<textWrapper> ().textObjPAnimation = "TalkHandsL";
				}
			}
			storyText.GetComponent<textWrapper> ().iDial = gameObject.GetComponent<inkDialogue> ();
			storyText.GetComponent<textWrapper> ().text = text;
			storyText.GetComponent<textWrapper> ().delay = previousDelay * textSpeed;
			storyText.GetComponent<textWrapper> ().baseDelay = previousDelay * textSpeed;
			storyText.GetComponent<textWrapper> ().selfOrder = selfOrder;
			storyText.GetComponent<textWrapper> ().textSpeed = textSpeed;
			storyText.GetComponent<textWrapper>().contentBox = textBoxy.transform.Find("TopBox").transform.Find("Content");
			storyText.transform.SetParent (textBoxy.transform.Find("TopBox").transform.Find("Content"), false);
		}
		float delay = textDelay + (text.Length * charMultiplier);
		totalDelay += delay;

		yield break;
	}

	public IEnumerator PrepareChoices() {
		choiceWait = 0;
		while (choiceWait < totalDelay) {
			choiceWait += Time.deltaTime / textSpeed / typeSpeed;
			yield return null;
		}
		while (choiceWait > totalDelay) {
			if (dialogStarter.GetComponent<NPC> () != null) {
				Debug.Log ("stop anim delay");
				StartCoroutine (stopAnimDelay());
			}
			for (int i = 0; i < story.currentChoices.Count; i++) {
				yield return new WaitForSeconds (buttonDelay * textSpeed);
				choicesPresent = true;
				Choice choice = story.currentChoices [i];
				Button button = CreateChoiceView (choice.text.Trim ());
// Tell the button what to do when we press it
				button.onClick.AddListener (delegate {
					OnClickChoiceButton (choice);
				});
			}
			yield break;
		}
	}

// Creates a button showing the choice text
	Button CreateChoiceView (string text) {
// Creates the button from a prefab
		Button choice = Instantiate (buttonPrefab) as Button;
		BottomBox = textBoxy.transform.Find ("BottomBox").gameObject;
		choice.transform.SetParent (textBoxy.transform.Find("BottomBox"), false);

//IF FIRST BUTTON IN A REFRESH, SELECT IT
		if (firstButton == false) {
			choice.Select();
			choice.OnSelect(null);
			firstButton = true;
		}
		
// Gets the text from the button prefab
		Text choiceText = choice.GetComponentInChildren<Text> ();
		choiceText.text = text;

// Make the button expand to fit the text
		HorizontalLayoutGroup layoutGroup = choice.GetComponent <HorizontalLayoutGroup> ();
		layoutGroup.childForceExpandHeight = false;

		return choice;
	}

// Destroys all the children of this gameobject (all the UI)
	void RemoveChildren () {
		textOrder = 0;
		totalDelay = 0;
		choiceWait = 0f;
		choicesPresent = false;
		textWrappers.Clear();
		if (dialogStarter.GetComponent<NPC> () != null) {
			dialogStarter.GetComponent<NPC> ().dialogOverride = false;
		}
		Player.dialogOverride = false;
		if (dialogStarter.GetComponent<NPC> () != null) {
			dialogStarter.GetComponent<NPC> ().lookAtPlayer = false;
		}
		BottomBox = null;
		int childCount = canvas.transform.childCount;
		for (int i = childCount - 1; i >= 0; --i) {
			GameObject.Destroy (canvas.transform.GetChild (i).gameObject);
		}
	}

	void RemoveChildrenBottom () {
		textOrder = 0;
		totalDelay = 0;
		choiceWait = 0f;
		choicesPresent = false;
		textWrappers.Clear();
		if (dialogStarter.GetComponent<NPC> () != null) {
			dialogStarter.GetComponent<NPC> ().dialogOverride = false;
		}
		Player.dialogOverride = false;
		int childCount = BottomBox.transform.childCount;
		for (int i = childCount - 1; i >= 0; --i) {
			GameObject.Destroy (BottomBox.transform.GetChild (i).gameObject);
		}
		LayoutRebuilder.ForceRebuildLayoutImmediate (textBoxy.GetComponent<RectTransform> ());
	}

	public void saveGame () {
		savedJson = story.state.ToJson ();
		StreamWriter writer = new StreamWriter (savePath, false);
		writer.WriteLine (savedJson);
		writer.Close ();
	}

	public void NPCRoomTestAll () {
		List<GameObject> NPCS = new List<GameObject> ();
		NPCS.AddRange (GameObject.FindGameObjectsWithTag ("NPC"));
		foreach (GameObject kid in NPCS) {
			if (kid.GetComponent<NPC> () != null && kid.activeInHierarchy) {
				Container.NPCRoomTest (kid);
			}
		}
	}

	public IEnumerator stopAnimDelay () {
		yield return new WaitForSeconds (1f);
		newestTyper = null;
		if (dialogStarter.GetComponent<NPC> () != null) {
			dialogStarter.GetComponent<NPC> ().dialogOverride = false;
		}
		Player.dialogOverride = false;
		yield break;
	}

	public IEnumerator stopPlayerAnimDelay () {
		yield return new WaitForSeconds (1f);
		Player.dialogOverride = false;
		yield break;
	}

	public IEnumerator delayInput() {
		yield return new WaitForSeconds (0.5f);
		readyToEnd = true;
		yield break;
	}

	public IEnumerator loadScene(string levelName) {
		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync (levelName);
		while (!asyncLoad.isDone) {
			yield return null;
		}
	}
}