﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dialogueTrigger : MonoBehaviour {

	public bool deactivateInteraction = false;
	public string lineString = "start";
	public inkDialogue dialogManager;
	private InteractionContainer iContainer;

	void Start() {
		iContainer = gameObject.GetComponent<InteractionContainer> ();
	}
	void Update () {
		if (iContainer.activated == true) {
			dialogManager.StartStory (lineString, transform.root.gameObject);
			if (deactivateInteraction == true) {
				iContainer.activated = false;
			}
		}
	}
}
