﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clarityTestDoor : MonoBehaviour {

	public LightsOutButton puzzle1;
	public LightsOutButton puzzle2;
	public LightsOutButton puzzle3;
	public slidingDoor exitDoor;

	void Start () {
		
	}

	void Update () {
		if (puzzle1.winActive == true && puzzle2.winActive == true && puzzle3.winActive == true) {
			exitDoor.closed = false;
		} else {
			exitDoor.closed = true;
		}
	}
}
