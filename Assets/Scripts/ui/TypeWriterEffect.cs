﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TypeWriterEffect : MonoBehaviour {

	public string textObjAnimation = "none";
	public string textObjPAnimation = "none";
	public float delay = 0.1f;
	public string fullText;
	public string lastChar;
	private string currentText = "";
	public bool forceFinished = false;
	public bool finished = false;
	public inkDialogue iDial;
	private float typeSpeedMod = 1f;

	// Use this for initialization
	void Start () {
		StartCoroutine(ShowText());
	}
	
	IEnumerator ShowText(){
		if (iDial.typeSpeed == 0.4f) {
			typeSpeedMod = 0.2f;
		} else {
			typeSpeedMod = 1f;
		}

		for (int i = 0; i <= fullText.Length; i++) {
			currentText = fullText.Substring (0, i);
			this.GetComponent<Text> ().text = currentText;
			if (forceFinished == false) {
				if (currentText.Length > 0) {
					lastChar = currentText.Substring (currentText.Length - 1);
				}
				if (lastChar == " ") {
					yield return new WaitForSeconds (delay * 1.8f * typeSpeedMod);
				} else if (lastChar == "." || lastChar == ",") {
					yield return new WaitForSeconds (delay * 5f * typeSpeedMod);
				} else if (lastChar == "!" || lastChar == "?") {
					yield return new WaitForSeconds (delay * 10f * typeSpeedMod);
				} else {
					yield return new WaitForSeconds (delay * typeSpeedMod);
				}
			} else {
				this.GetComponent<Text> ().text = fullText;
				finished = true;
				iDial.ellipses.SetActive (true);
				LayoutRebuilder.ForceRebuildLayoutImmediate (gameObject.GetComponent<RectTransform> ());
			}
		}
		finished = true;
		iDial.ellipses.SetActive (true);
	}
}
