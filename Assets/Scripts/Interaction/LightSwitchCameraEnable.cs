﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwitchCameraEnable : MonoBehaviour {

	public bool ready = true;
	public float yOffset = 90f;
	private bool rotatePlayer = false;
	public GameObject doorObject;
	public bool movePlayer;
	private Vector3 playerStart;
	public Transform playerTarget;
	public GameObject startSelection;
	public GameObject targetCamera;
	private GameObject previousCamera;
	public ArrayContainer Container;
	public bool disablePlayer;
	public GameObject playerMesh;
	private bool previousCamTransitionLimit;
	public InteractionContainer enableTrigger;
	public InteractionContainer disableTrigger;
	public float t = 0f;

	void Start () {
		//playerMesh = Container.Player.GetComponent<playerController> ().humeObject.GetComponent<humeHelper> ().meshObject;
	}

	void Update () {
		if (rotatePlayer) {
			//Container.Player.transform.eulerAngles = new Vector3 (Container.Player.transform.rotation.x, Mathf.Lerp (Container.Player.transform.rotation.y, yOffset, t), Container.Player.transform.rotation.z);
			Container.Player.transform.eulerAngles = new Vector3 (Container.Player.transform.rotation.x, Mathf.Lerp (Container.Player.transform.rotation.y -90f, Container.Player.transform.rotation.y + yOffset, t), Container.Player.transform.rotation.z);
			t += 8f * Time.deltaTime;
			if (t >= 1) {
				rotatePlayer = false;
			}
		} else {
			t = 0;
		}
		if (ready) {
			if (enableTrigger != null && enableTrigger.activated == true) {
				enableTrigger.activated = false;
				if (movePlayer) {
					StartCoroutine (moveIn ());
				} else {
					enableCamera ();
				}
			} 
			if (disableTrigger != null && disableTrigger.activated == true) {
				disableTrigger.activated = false;
				if (movePlayer) {
					StartCoroutine (moveOut ());
				} else {
					disableCamera ();
				}
			}
		} else {
			enableTrigger.activated = false;
		}
		} 

	public void enableCamera () {
		if (disablePlayer) {
			Container.Player.GetComponent<playerController> ().humeObject.GetComponent<humeHelper> ().meshObject.GetComponent<SkinnedMeshRenderer>().enabled = false;
		}
		Container.colliderMode = false;
		gameObject.GetComponent<BoxCollider> ().enabled = false;
		previousCamera = GameObject.FindGameObjectWithTag ("MainCamera");
		previousCamTransitionLimit = previousCamera.transform.parent.GetComponent<CameraControl> ().transitionLimit;
		previousCamera.transform.parent.GetComponent<CameraControl> ().transitionLimit = false;
		targetCamera.GetComponent<CameraControl> ().RoomControl ();
		}

	public void disableCamera () {
		Container.colliderMode = true;
		if (disablePlayer) {
			Container.Player.GetComponent<playerController> ().humeObject.GetComponent<humeHelper> ().meshObject.GetComponent<SkinnedMeshRenderer>().enabled = true;
		}
		gameObject.GetComponent<BoxCollider> ().enabled = true;
		previousCamera.transform.parent.GetComponent<CameraControl> ().RoomControl ();
		previousCamera.transform.parent.GetComponent<CameraControl> ().transitionLimit = previousCamTransitionLimit;
		}

	public IEnumerator moveIn () {
		ready = false;
		Container.Player.GetComponent<UnityEngine.AI.NavMeshAgent> ().enabled = false;
		Container.Player.GetComponent<playerController> ().hidingAnimation = true;
		playerStart = Container.Player.transform.position;
		iTween.MoveTo (Container.Player, playerTarget.position, 0.6f);
		yield return new WaitForSeconds (0.1f);
		closeThing ();
		rotatePlayer = true;
		yield return new WaitForSeconds (0.6f);
		enableCamera ();
		ready = true;
		Container.playerConcealed = true;
		yield break;
	}

	public IEnumerator moveOut () {
		Container.Player.transform.eulerAngles = new Vector3 (Container.Player.transform.rotation.x, Container.Player.transform.rotation.y + yOffset, Container.Player.transform.rotation.z);
		disableCamera ();
		openThing ();
		ready = false;
		yield return new WaitForSeconds (0.2f);
		Container.Player.GetComponent<playerController> ().hidingAnimation = false;
		iTween.MoveTo (Container.Player, playerStart, 0.6f);
		yield return new WaitForSeconds (0.4f);
		Container.Player.GetComponent<UnityEngine.AI.NavMeshAgent> ().enabled = true;
		ready = true;
		Container.playerConcealed = false;
		yield break;
	}

	public void openThing () {
		if (doorObject.GetComponent<closetDoor> () != null) {
			doorObject.GetComponent<closetDoor> ().openExt ();
		}
	}

	public void closeThing () {
		if (doorObject.GetComponent<closetDoor> () != null) {
			doorObject.GetComponent<closetDoor> ().closeExt ();
		}
	}
}