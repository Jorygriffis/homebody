﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwitchObjectToggle : MonoBehaviour {

	public LightSwitch lightSwitch;
	public GameObject onObject;
	public GameObject offObject;
	private powerContainer manager;

	void Start () {
		manager = GameObject.Find ("Manager").GetComponent<powerContainer>();
	}

	void Update () {
		if (manager.powerOn == true) {
			if (lightSwitch.switchActive) {
				onObject.SetActive (true);
				if (offObject != null) {
					offObject.SetActive (false);
				}
			} else {
				onObject.SetActive (false);
				if (offObject != null) {
					offObject.SetActive (true);
				}
			}
		} else {
			onObject.SetActive (false);
			if (offObject != null) {
				offObject.SetActive (true);
			}
		}
	} 
}
