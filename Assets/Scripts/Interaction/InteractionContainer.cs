﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

public class InteractionContainer : MonoBehaviour {

	[Tooltip("Interactable name to show ingame.")]
	public string interactName;

	[BoxGroup("Animation options")][Tooltip("Use left hand for interaction.")]
	public bool leftHand = false;
	[BoxGroup("Animation options")][Tooltip("Delay on player input during interaction.")]
	public float interruptTime = 0.5f;

	[BoxGroup("UI options")][Tooltip("Use door icon/look offset. If false, icon point is at center of object.")]
	public bool doorOffset = false;	
	[BoxGroup("UI options")][Tooltip("If true, no interactable icon/name is shown ingame.")]
	public bool iconOverride = false;
	[BoxGroup("UI options")][Tooltip("Y offset for UI icon/text in local space.")]
	public float uiOffset = 0;
	[BoxGroup("UI options")][Tooltip("If true, disregards line-of-sight to player on interactable check.")]
	public bool rayOverride = false;
	[BoxGroup("UI options")][Tooltip("Interactables to navigate to in order: N, S, E, W")]
	public List<GameObject> interactViewNav = new List<GameObject>();

	[Tooltip("Target GameObject for a variety of functions depending on script. Can be null.")]
	public GameObject targetObj;

	[BoxGroup("Navigation")][Tooltip("Disables need to walk to walkPoint so interactable can be used from anywhere.")]
	public bool walkToOverride = false;
	[BoxGroup("Navigation")][Tooltip("Transform defining point to walk to on interaction.")]
	public Transform walkPoint;
	[BoxGroup("Navigation")][Tooltip("Transform defining point to turn to on interaction. If blank, defaults to local X and Z at player Y.")]
	public Transform turnPoint;
	[BoxGroup("Navigation")][Tooltip("If true, turn to turnPoint on interaction.")]
	public bool rotatePlayer = true;
	[BoxGroup("Navigation")][Tooltip("Interaction start distance.")]
	public float usableDistance = 0.5f;

	[HideInInspector]
	public Vector3 targetPos;
	[HideInInspector]
	public bool inInteractRange = false;
	[HideInInspector]
	public float heightDifference;
	[HideInInspector]
	public GameObject hitObject;

	[HideInInspector]
	public float distanceToPlayer = 666;
	[HideInInspector]
	public bool activated = false;
	[HideInInspector]
	public float angleToPlayer = 666;
	[HideInInspector]
	public GameObject player;
	private ArrayContainer manager;
	[HideInInspector]
	public Vector3 turnPointPos;

	void Start () {
		player = GameObject.Find ("player");
		manager = GameObject.Find ("Manager").GetComponent<ArrayContainer> ();
		if (turnPoint == null && rotatePlayer == true) {
			if (transform.root.gameObject.tag == "NPC") {
				turnPointPos = transform.root.position;
			} else {
				turnPointPos = new Vector3 (gameObject.transform.position.x, walkPoint.position.y, gameObject.transform.position.z);
			}
		} else if (rotatePlayer == true) {
			turnPointPos = turnPoint.position;
		}
	}

	void Update () {
		if (doorOffset == false){
			targetPos = gameObject.transform.position;
		} else {
			targetPos = transform.TransformPoint(-2.5f, 0f, 6f);
		}
		Vector3 playerOffset = new Vector3 (player.transform.position.x, targetPos.y, player.transform.position.z);
		heightDifference = Mathf.Abs (player.transform.position.y - targetPos.y);
		distanceToPlayer = Vector3.Distance (targetPos, playerOffset);
		if (distanceToPlayer <= manager.interactableDistance && (heightDifference < 3) && (targetPos.y - player.transform.position.y) > -0.1f) {
			inInteractRange = true;
			RaycastHit hit;
			if (Physics.Raycast (playerOffset, targetPos - playerOffset, out hit, 5f, manager.InteractMask)) {
				hitObject = hit.transform.gameObject;
				if (hitObject != null && hitObject == gameObject || rayOverride == true) {
					Debug.DrawRay (playerOffset, targetPos - playerOffset, Color.green);
					Vector3 targetDir = new Vector3 (targetPos.x, player.transform.position.y, targetPos.z) - player.transform.position;
					angleToPlayer = Vector3.Angle (targetDir, player.transform.forward);
				} else {
					cancelRange();
				}
			} else {
				cancelRange();
			}
		} else {
			cancelRange();
		}
	}

	void cancelRange () {
		hitObject = null;
		inInteractRange = false;
		angleToPlayer = 666;
	}

	void OnDisable(){
		cancelRange();
	}
}