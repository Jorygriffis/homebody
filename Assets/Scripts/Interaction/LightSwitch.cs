﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwitch : MonoBehaviour {

	private bool active = true;
	public bool startOn = true;
	public bool switchActive = true;
	private bool status;
	public GameObject switchOn;
	public GameObject switchOff;
	public InteractionContainer iContainer;
	private powerContainer powerManager;

	void Start () {
		powerManager = GameObject.Find ("Manager").GetComponent<powerContainer>();
		if (startOn == false) {
			active = false;
			switchActive = false;
			if (switchOff != null && switchOn != null) {
				switchOff.SetActive (true);
				switchOn.SetActive (false);
			}
		}
		status = active;
	}

	void Update () {
		if (iContainer.activated) {
			iContainer.activated = false;
			active = !active;
		} 
		if (active != status) {
			if (active) {
				StartCoroutine (On ());
			} else {
				StartCoroutine (Off ());
			}
			status = active;
		}
	}

	public IEnumerator On(){
		yield return new WaitForSeconds (0.25f);
		switchActive = true;
		if (switchOff != null && switchOn != null) {
			switchOn.SetActive (true);
			switchOff.SetActive (false);
		}
		yield break;
	}

	public IEnumerator Off(){
		yield return new WaitForSeconds (0.25f);
		switchActive = false;
		if (switchOff != null && switchOn != null) {
			switchOff.SetActive (true);
			switchOn.SetActive (false);
		}
		yield break;
	}
}
