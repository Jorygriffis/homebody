﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class PushButton : MonoBehaviour {

	private Vector3 basePos;
	private Vector3 pressedPos;
	private InteractionContainer iContainer;
	public bool holdable = false;
	public float pressedDistance = 0.1f;
	public float pressDelay = 0f;
	public float pressTime = 0.1f;
	public float returnTime = 0.1f;
	public float returnDelay = 0.1f;
	public bool Pressed = false;
	private Player player;

	public void Awake () {
		player = Rewired.ReInput.players.GetPlayer (0);
	}

	void Start () {
		iContainer = gameObject.GetComponent<InteractionContainer> ();
		basePos = gameObject.transform.localPosition;
		pressedPos = new Vector3 (basePos.x, basePos.y, basePos.z - pressedDistance);
	}

	void Update () {
		if (iContainer.activated == true) {
			StartCoroutine (Press());
			iContainer.activated = false;
		}
		if (holdable == true && Pressed == true) {
			if (iContainer.player.GetComponent<playerController> ().keyInput == true) {
				if (player.GetButton ("Submit") == false) {
					StartCoroutine (Release ());
				}
			} else {
				if (Input.GetMouseButton (0) == false) {
					StartCoroutine (Release ());
				}
			}
		}
	}

	public IEnumerator Press() {
		Debug.Log ("PRESSD");
		Pressed = true;
		yield return new WaitForSeconds (pressDelay);
		iTween.MoveTo(gameObject, iTween.Hash("islocal", true, "x", basePos.x, "y", basePos.y, "z", (basePos.z - pressedDistance), "time", returnTime));
		yield return new WaitForSeconds (returnDelay);
		if (holdable == false) {
			Pressed = false;
			iTween.MoveTo (gameObject, iTween.Hash ("islocal", true, "x", basePos.x, "y", basePos.y, "z", basePos.z, "time", returnTime));
			yield return new WaitForSeconds (returnTime);
			gameObject.transform.localPosition = basePos;
		}
		yield break;
	}

	public IEnumerator Release() {
		Pressed = false;
		iTween.MoveTo (gameObject, iTween.Hash ("islocal", true, "x", basePos.x, "y", basePos.y, "z", basePos.z, "time", returnTime));
		yield return new WaitForSeconds (returnTime);
		gameObject.transform.localPosition = basePos;
		yield break;
	}
}
