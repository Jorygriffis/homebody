﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwitchToggle : MonoBehaviour {

	public LightSwitch lightSwitch;
	public Light lightComponent;
	private powerContainer manager;

	void Start () {
		manager = GameObject.Find ("Manager").GetComponent<powerContainer>();
	}

	void Update () {
		if (manager.powerOn) {
			if (lightSwitch.switchActive) {
				lightComponent.enabled = true;
			} else {
				lightComponent.enabled = false;
			}
		} else {
			lightComponent.enabled = false;
		}
	}
}
