﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class subFader : MonoBehaviour {

	public GameObject playerMeshObj;
	public Material selfMaterial;

	void Start () {
		if (gameObject.GetComponent<SkinnedMeshRenderer> () != null) {
			selfMaterial = Material.Instantiate (gameObject.GetComponent<SkinnedMeshRenderer> ().material);
			gameObject.GetComponent<SkinnedMeshRenderer> ().material = selfMaterial;
		} else if (gameObject.GetComponent<MeshRenderer> () != null) {
			selfMaterial = Material.Instantiate (gameObject.GetComponent<MeshRenderer> ().material);
			gameObject.GetComponent<MeshRenderer> ().material = selfMaterial;
		}
	}

	void Update () {
		if (gameObject.GetComponent<SkinnedMeshRenderer> () != null) {
			if (playerMeshObj.GetComponent<SkinnedMeshRenderer> ().enabled == true) {
				GetComponent<SkinnedMeshRenderer> ().enabled = true;
				selfMaterial.SetFloat ("_Cutoff", playerMeshObj.GetComponent<NPCCameraFade> ().cutoffValue);
			} else {
				GetComponent<SkinnedMeshRenderer> ().enabled = false;
			}
		} else if (gameObject.GetComponent<MeshRenderer> () != null) {
			if (playerMeshObj.GetComponent<SkinnedMeshRenderer> ().enabled == true) {
				GetComponent<MeshRenderer> ().enabled = true;
				selfMaterial.SetFloat ("_Cutoff", playerMeshObj.GetComponent<NPCCameraFade> ().cutoffValue);
			} else {
				GetComponent<MeshRenderer> ().enabled = false;
			}
		}
	}
}
