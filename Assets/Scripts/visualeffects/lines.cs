﻿using UnityEngine;
using System.Collections;

public class lines : MonoBehaviour {
	public Material mat;
	public Vector3 startVertex;
	public Vector3 endVertex;
	//void Update() {
		//mousePos = Input.mousePosition;
		//if (Input.GetKeyDown(KeyCode.Space))
			//startVertex = new Vector3(mousePos.x / Screen.width, mousePos.y / Screen.height, 0);
		

	void OnPostRender() {
		if (!mat) {
			Debug.LogError("Please Assign a material on the inspector");
			return;
		}
		GL.PushMatrix();
		mat.SetPass(0);
		GL.LoadOrtho();
		GL.Begin(GL.LINES);
		GL.Color(Color.red);
		GL.Vertex(startVertex);
		GL.Vertex(endVertex);
		GL.End();
		GL.PopMatrix();
	}
}