﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class billboardBlocker : MonoBehaviour {

	public bool faceCamera = false;
	private GameObject billboardCollider;
	public bool useContainer = false;
	private GameObject billboardContainer;
	public GameObject mainCam;
	public GameObject billboard;
	public GameObject billboard2;
	public GameObject hitObject;

	void Start () {
		billboardCollider = gameObject;
		if (useContainer == true) {
			billboardContainer = transform.Find ("billboardContainer").gameObject;
		} else {
			billboardContainer = gameObject;
		}
	}

	void Update () {
		
//FIND MAIN CAMERA
		mainCam = GameObject.FindGameObjectWithTag ("MainCamera");

//LOOK AT CAMERA IF FACECAMERA TRUE
		if (faceCamera == true) {
			billboardContainer.transform.LookAt (mainCam.transform);
		}
	
		RaycastHit hit;
		Debug.DrawRay(mainCam.transform.position, billboardCollider.transform.position - mainCam.transform.position, Color.green);
		if (Physics.Raycast (mainCam.transform.position, billboardCollider.transform.position - mainCam.transform.position, out hit, 100.0f)) {
			hitObject = hit.transform.gameObject;
			if (hit.collider != null && hit.collider == billboardCollider.GetComponent<BoxCollider>()) {
				billboard.GetComponent<MeshRenderer> ().enabled = true;
				StartCoroutine(on2());
			} else {
				billboard.GetComponent<MeshRenderer> ().enabled = false;
				StartCoroutine(off2());
			}
		}
	}

	public IEnumerator on2(){
		if (faceCamera == true) {
			yield return new WaitForSeconds (0.05f);
		} else {
			yield return new WaitForSeconds (0.02f);
		}
		billboard2.GetComponent<MeshRenderer> ().enabled = true;
	}
	public IEnumerator off2(){
		if (faceCamera == true) {
			yield return new WaitForSeconds (0.05f);
		} else {
			yield return new WaitForSeconds (0.02f);
		}
		billboard2.GetComponent<MeshRenderer> ().enabled = false;
	}
}
