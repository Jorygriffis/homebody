﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class timeContainer : MonoBehaviour {

	public bool realTime = false;
	public float currentTime;
	public int currentMinute;
	private int current24Hour;
	public int currentHour;
	public bool init = false;

	void Update () {
		if (realTime == false) {
			currentTime += (Time.deltaTime/60);
		} else {
			currentTime = ((System.DateTime.Now.Hour * 60)+(System.DateTime.Now.Minute));
		}
		current24Hour = Mathf.FloorToInt (currentTime / 60);
		if (current24Hour > 12) {
			currentHour = current24Hour - 12;
		} else {
			currentHour = current24Hour;
		}
		currentMinute = (Mathf.FloorToInt(currentTime) - (current24Hour * 60));
		init = true;
	}
}
