﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sceneContainer : MonoBehaviour {

	public bool autoStart = false;
	public string autoStartScene = "loopHub";
	public bool flashback = true;
	public inkDialogue dialogManager;

	void Start () {
		if (autoStart) {
			//dialogManager.StartStory (autoStartScene, transform.root.gameObject);
			StartCoroutine (autoStartDelay());
		}
	}
	void Update () {
		if (Input.GetKeyDown(KeyCode.K)) {
			if (flashback) {
				StartCoroutine (dialogManager.loadScene ("house"));
			} else {
				dialogManager.StartStory ("loopHub", transform.root.gameObject);
			}
		}
	}
	public IEnumerator autoStartDelay(){
		yield return new WaitForSeconds (0.03f);
		dialogManager.StartStory (autoStartScene, transform.root.gameObject);
		yield break;
	}
}
