﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class powerMain : MonoBehaviour {
	private PushButton pButton;
	private powerContainer pContainer;

	void Start () {
		pButton = gameObject.GetComponent<PushButton> ();
		pContainer = GameObject.Find ("Manager").GetComponent<powerContainer> ();
	}

	void Update () {
		if (pButton.Pressed){
			if (pContainer.housePower > 0) {
				pContainer.reservePower = pContainer.reservePower + pContainer.housePower + pContainer.atticPower;
				pContainer.housePower = 0;
				pContainer.atticPower = 0;
			} else if (pContainer.reservePower > 0) {
				pContainer.housePower = 1;
				pContainer.reservePower = pContainer.reservePower - 1;
			}
			pButton.Pressed = false;
		}
	}
}
