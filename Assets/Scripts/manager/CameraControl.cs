﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

public class CameraControl : MonoBehaviour {

	[Tooltip("Dominant room of this camera angle.")]
	public GameObject mainRoom;
	[Tooltip("Scene ArrayContainer.")]
	public ArrayContainer Container;
	[Tooltip("Rooms visible from this camera angle--all others will be culled.")]
	public GameObject[] OwnRooms;
	[Tooltip("Lights visible from this camera angle--all others will be culled.")]
	public GameObject[] OwnLights;
	[Tooltip("Objects to hide from this camera angle--must be added to arrayContainer objectsToHide list.")]
	public GameObject[] objectsToHide;
	[Tooltip("Master camera for this camera angle.")]
	public GameObject OwnCamera;

	[Tooltip("Flip camera direction on Z axis.")]
	public bool flipZ = false;
	[HideInInspector][Tooltip("Flip camera direction on X axis.")]
	public bool flipX = false;
	[ShowIf("flipZ")][Tooltip("Line at which to flip camera direction, measured as distance away on Z axis.")]
	public float flipAxis;
	[ShowIf("flipZ")][Tooltip("North-facing camera relative to this object's Z axis.")]
	public GameObject CameraN;
	[ShowIf("flipZ")][Tooltip("South-facing camera relative to this object's Z axis.")]
	public GameObject CameraS;
	[Tooltip("True if ownCamera is active in hierarchy.")]
	public bool OwnCameraActive;

	[Tooltip("Time since last transition to this camera angle.")]
	private float transitionTime = -0.1f;
	[Tooltip("Minimum time before camera transition.")]
	private float transitionTweak = 1;

	[Tooltip("If true, scene fog is enabled on this camera angle.")]
	public bool renderFog = true;
	[BoxGroup("Input debug")][Tooltip("True if mouse is held.")]
	public bool mouseHeld;
	[BoxGroup("Input debug")][Tooltip("If true, spawn debugCube at screenPointToRay hit point.")]
	public bool rayDebug;
	[ShowIf("rayDebug")][BoxGroup("Input debug")][Tooltip("GameObject to spawn at ray hit point.")]
	public GameObject debugCube;
	public bool transitionLimit = true;
	[Tooltip("NPC near camera fade setting. Should set to false if it's impossible for NPC to occlude camera.")]
	public bool NPCFade = true;

	[HideInInspector]
	public bool isDark = false;
	[HideInInspector]
	public GameObject Player;
	private GameObject CurrentMainCamera;
	private LayerMask ScreenPickMask;
	private Dithering dithComp;
	private Transform hitChild;
	private Camera OwnCam;

	void Start (){
		Player = Container.Player;
		dithComp = OwnCamera.GetComponent<Dithering> ();
		ScreenPickMask = Container.ScreenPickMask;
	}

	void Update (){
		if (mainRoom != null) {
			isDark = mainRoom.GetComponent<roomContainer> ().isDark;
		}
		if (OwnCameraActive == false && transitionTime > -0.1f) {
			transitionTime = transitionTime - Time.deltaTime;
		}
		if (Container.Retro) {
			dithComp.enabled = true;
		} else {
			dithComp.enabled = false;
		}
		if (OwnCamera.activeInHierarchy == true) {
			OwnCameraActive = true;
			if (Container.interactionView) {

			} else {
				Ray ray = OwnCam.ScreenPointToRay (Input.mousePosition);
				RaycastHit hit;
				if (Input.GetMouseButtonDown (0)) {
					Player.GetComponent<playerController>().canInteract = false;
					if (Physics.Raycast (ray, out hit, 100.0f, ScreenPickMask)) {
 
						if (hit.collider.gameObject.tag == "NPC") {
							Player.GetComponent<playerController> ().pickedObject = hit.transform.root.gameObject.GetComponent<NPC>().selfContainer.gameObject;
						} else {
							Player.GetComponent<playerController> ().pickedObject = hit.collider.gameObject;
						}

						if (Player.GetComponent<playerController> ().pickedObject.tag == "Interactive" && Player.GetComponent<playerController>().interactInterrupt < 0.1f) {
							Container.interactOff = false;
							hitChild = Player.GetComponent<playerController> ().pickedObject.GetComponent<InteractionContainer> ().walkPoint;
							Player.GetComponent<playerController> ().ScreenPickPos = hitChild.position;
						} else if (Player.GetComponent<playerController> ().pickedObject.tag == "Room") {
							//do nothing
						} else {
							if (rayDebug) {
								GameObject Cuber = Instantiate (debugCube, hit.point, OwnCamera.transform.rotation) as GameObject;
							}
							if (Player.GetComponent<playerController> ().keyInput == false) {
								Container.interactOff = true;
							}
							mouseHeld = true;
						}
					}
					Player.GetComponent<playerController>().canInteract = true;
				}
				if (mouseHeld) {
					if (Physics.Raycast (ray, out hit, 100.0f, ScreenPickMask)) {
						if (hit.collider.gameObject.tag == "Interactive" || hit.collider.gameObject.tag == "Room") {
							// do nothing
						} else {
							Player.GetComponent<playerController> ().ScreenPickPos = hit.point;
						}
					}
					if (Input.GetMouseButtonUp (0)) {
						mouseHeld = false;
					}
				}
			}
		} else {
			OwnCameraActive = false;
		}
	}

	void OnTriggerStay(Collider thing){
		if (thing.tag == "Player") {
			//Player.GetComponent<playerController> ().cameraColliders.Add (gameObject);
			if (Player.transform.position.y < OwnCamera.transform.position.y) {
				if (OwnCameraActive == false && Container.colliderMode == true && Player.GetComponent<playerController> ().cameraColliders.Count < 2) {
					RoomControl ();
				}
			}
		}
	}

	void OnTriggerEnter(Collider thing){
		if (thing.tag == "Player") {
			if (OwnCameraActive == false && Container.colliderMode == true) {
				RoomControl();
			}
		}
	}

	public void RoomControl (){
		if (transitionTime <= 0 || transitionLimit == false) {
			foreach (GameObject camera in Container.Cameras) {
				camera.tag = "Camera";
				camera.SetActive (false);
			}
			if (flipZ == true) {
				if (Player.transform.position.z > flipAxis) {
					OwnCamera = CameraN;
				} else {
					OwnCamera = CameraS;
				}
			} else if (flipX == true) {
				if (Player.transform.position.x > flipAxis) {
					OwnCamera = CameraN;
				} else {
					OwnCamera = CameraS;
				}
			}
			OwnCam = OwnCamera.GetComponent<Camera> ();
			OwnCamera.SetActive (true);
//			hiddenToggle = false;
			if (renderFog) {
				RenderSettings.fog = true;
			} else {
				RenderSettings.fog = false;
			}
			transitionTime = transitionTweak;
			OwnCamera.tag = "MainCamera";
			Container.mainCam = gameObject;
			foreach (GameObject room in Container.Rooms) {
				room.SetActive (false);
			}
			foreach (GameObject ownroom in OwnRooms) {
				ownroom.SetActive (true);
			}
			foreach (GameObject light in Container.Lights) {
				light.SetActive (false);
			}
			foreach (GameObject ownLight in OwnLights) {
				ownLight.SetActive (true);
			}
			if (Container.ObjectsToHide != null) {
				foreach (GameObject hiddenMain in Container.ObjectsToHide) {
					if (hiddenMain != null) {
						hiddenMain.SetActive (true);
					}
				}
			}
			if (objectsToHide != null) {
				foreach (GameObject hidden in objectsToHide) {
					hidden.SetActive (false);
				}
			}
		}
	}
}
