﻿using UnityEngine;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using RootMotion.FinalIK;
using Rewired;
using Ink.Runtime;

public class ArrayContainer : MonoBehaviour {

	[RequiredAttribute][BoxGroup("Character Prefabs")]
	public GameObject CliffPrefab;
	[RequiredAttribute][BoxGroup("Character Prefabs")]
	public GameObject EmilyPrefab;
	[RequiredAttribute][BoxGroup("Character Prefabs")]
	public GameObject FrancinePrefab;
	[RequiredAttribute][BoxGroup("Character Prefabs")]
	public GameObject GaryPrefab;
	[RequiredAttribute][BoxGroup("Character Prefabs")]
	public GameObject LauraPrefab;
	[RequiredAttribute][BoxGroup("Character Prefabs")]
	public GameObject MeganPrefab;
	[RequiredAttribute][BoxGroup("Character Prefabs")]
	public GameObject PetePrefab;
	public bool playerConcealed = false;
	public bool playerHidden = false;
	[RequiredAttribute][BoxGroup("Manually set per-scene")]
	public GameObject Player;
	[RequiredAttribute][BoxGroup("Manually set per-scene")]
	public GameObject dialogueCanvas;
	[BoxGroup("Manually set per-scene")][Tooltip("List of objects to be selectively hidden.")]
	public GameObject[] ObjectsToHide;
	[BoxGroup("Manually set per-scene")][Tooltip("Enable/Disable positional room culling - disabling may be useful for smaller environments.")]
	public bool roomCulling = true;

	[BoxGroup("Set by Ink")]
	public string playerName = "Crobus";
	[BoxGroup("Set by Ink")]
	public string missingKidName = "Adobus";
	public Player player;
	[Tooltip("If true, certain house-specific features are disabled.")][BoxGroup("Set by Ink")]
	public bool flashback = false;

	[BoxGroup("Set by in-game Options")]
	public bool rightStickMode = false;
	[BoxGroup("Set by in-game Options")]
	public bool Retro;

	[RequiredAttribute][BoxGroup("UI Assets / Layout")]
	public Font interactFont;
	[BoxGroup("UI Assets / Layout")]
	public float textYOffset;
	[BoxGroup("UI Assets / Layout")]
	public float npcXOffset;
	[BoxGroup("UI Assets / Layout")]
	public float npcYOffset;
	[RequiredAttribute][BoxGroup("UI Assets / Layout")]
	public Texture2D interactable;
	[RequiredAttribute][BoxGroup("UI Assets / Layout")]
	public Texture2D interactableSelected;
	[RequiredAttribute][BoxGroup("UI Assets / Layout")]
	public Texture2D interactableSmall;
	[RequiredAttribute][BoxGroup("UI Assets / Layout")]
	public Texture2D interactableBig;
	[RequiredAttribute][BoxGroup("UI Assets / Layout")]
	public Texture2D interactableNPC;
	[RequiredAttribute][BoxGroup("UI Assets / Layout")]
	public Texture2D interactableNPCSelected;

	[BoxGroup("Control tweaks")][Tooltip("Minimum distance to interactable.")]
	public float interactableDistance = 2f;
	[BoxGroup("Control tweaks")][Tooltip("Used to adjust double-click speed.")]
	public float clickDifference = 0.25f;
	[BoxGroup("Control tweaks")][Tooltip("Layer mask for mouse click movement input.")]
	public LayerMask ScreenPickMask;
	[BoxGroup("Control tweaks")][Tooltip("Layer mask for mouse click interaction input.")]
	public LayerMask InteractMask;

	[HideInInspector]
	public bool interactionView;
	[HideInInspector]
	public bool interactablePick;
	[HideInInspector]
	public bool interactOff;
	[HideInInspector]
	public bool colliderMode = true;

	private float iconSizeTime = 0;
	[HideInInspector]
	public float leastAngle;
	private GameObject camExitTrigger;
	[HideInInspector]
	public GameObject currentInteractable;
	[HideInInspector]
	public GameObject chosenInteractable;
	[HideInInspector]
	public GameObject[] Interactables;
	[HideInInspector]
	public GameObject mainCam;
	[HideInInspector]
	public bool doubleClick;
	private bool singleClick;
	private float clickTime = 0;
	private int obj;
	[HideInInspector]
	public bool rightStickMoved = false;
	private GameObject pauser;
	private string exitText;
	//[HideInInspector]
	public GameObject[] Rooms;
	//[HideInInspector]
	public GameObject[] Cameras;
	//[HideInInspector]
	public GameObject[] Lights;
	[HideInInspector]
	public List<GameObject> highlightedInteractables = new List<GameObject>();

	public void Awake () {
		player = Rewired.ReInput.players.GetPlayer (0);
	}

	void Start () {
		Screen.SetResolution (960, 540, true);
		float interactableDistanceBase = interactableDistance;

//POPULATE WORLD LISTS
		if (roomCulling == true) {
			Rooms = GameObject.FindGameObjectsWithTag ("Room");
		}
		Cameras = GameObject.FindGameObjectsWithTag ("Camera");
		Interactables = GameObject.FindGameObjectsWithTag ("Interactive");
		Lights = GameObject.FindGameObjectsWithTag ("Light");
		foreach (GameObject camera in Cameras) {
			camera.SetActive (false);
		}

//GET PAUSE CONTROLLER
		pauser = GameObject.Find("pauseMenu");
	}
		
	void Update () {

		if (playerConcealed) {
			playerHidden = true;
		} else {
			playerHidden = false;
		}

//MODIFY INTERACTABLEDISTANCE (BASED ON DARKNESS, ETC)
		if (Player.GetComponent<playerController> ().isDark == true) {
			interactableDistance = 1.4f;
		} else {
			interactableDistance = 1.9f;
		}

//CHOOSE CURRENTINTERACTABLE, POPULATE HIGHLIGHTEDINTERACTABLES
		leastAngle = 75;
		highlightedInteractables.Clear ();
		foreach (GameObject interactable in Interactables) {
			if (interactable.GetComponent<InteractionContainer> ().inInteractRange == true && interactable.GetComponent<InteractionContainer> ().iconOverride == false) {
				highlightedInteractables.Add (interactable);
			}
			if (interactable.GetComponent<InteractionContainer> ().angleToPlayer < leastAngle) {
				leastAngle = interactable.GetComponent<InteractionContainer> ().angleToPlayer;
				currentInteractable = interactable;
			}
		}

//SORT HIGHLIGHTEDINTERACTABLES BY X POSITION
		highlightedInteractables.Sort(sortByXPosition);

//GET CURRENTINTERACTABLE INDEX, CHOOSE CHOSENINTERACTABLE
		if (rightStickMode == true) {
			if (chosenInteractable != null) {
				if (player.GetButtonDown ("RHorizontal")) {
					rightStickMoved = true;
					obj = highlightedInteractables.IndexOf (chosenInteractable);
					if (obj < (highlightedInteractables.Count - 1)) {
						obj = obj + 1;
						chosenInteractable = highlightedInteractables [obj];
					}
				}
				if (player.GetNegativeButtonDown ("RHorizontal")) {
					rightStickMoved = true;
					obj = highlightedInteractables.IndexOf (chosenInteractable);
					if (obj >= 1) {
						obj = obj - 1;
						chosenInteractable = highlightedInteractables [obj];
					}
				}
				if (rightStickMoved == false) {
					chosenInteractable = currentInteractable;
				}
			} else if (highlightedInteractables != null && currentInteractable != null) {
				chosenInteractable = currentInteractable;
			} else if (highlightedInteractables != null) {
				if (player.GetButtonDown ("RHorizontal") || player.GetNegativeButtonDown ("RHorizontal")){
					rightStickMoved = true;
					chosenInteractable = highlightedInteractables[0];
				}
			}

			if (chosenInteractable != null && chosenInteractable.GetComponent<InteractionContainer> ().inInteractRange == false) {
				rightStickMoved = false;
				chosenInteractable = null;
			}
			if (player.GetAxisTimeActive("Horizontal") > 0.3f || player.GetAxisTimeActive("Vertical") > 0.3f) {
				rightStickMoved = false;
			}
		} else {
			if (currentInteractable == null) {
				chosenInteractable = null;
			} else {
				chosenInteractable = currentInteractable;
			}
		}

//DISABLE BAD CURRENTINTERACTABLE
		if (currentInteractable != null) {
			if (currentInteractable.GetComponent<InteractionContainer> ().angleToPlayer >= 75) {
				currentInteractable = null;
			}
		}

//DOUBLE-CLICK TEST
		clickTime -= Time.deltaTime;
		if (mainCam != null) {
			if (doubleClick) {
				if (Player.GetComponent<UnityEngine.AI.NavMeshAgent> ().enabled == true && Player.GetComponent<UnityEngine.AI.NavMeshAgent> ().remainingDistance < 0.4f && mainCam.GetComponent<CameraControl> ().mouseHeld == false) {
					doubleClick = false;
				}
			}
			if (clickTime < 0) {
				singleClick = false;
			}
			if (Input.GetMouseButtonDown (0)) {
				if (singleClick) {
					doubleClick = true;
					clickTime = 0.05f;
				} else {
					singleClick = true;
					clickTime = clickDifference;
				}
			}
		}

//ENABLE/DISABLE PAUSE MENU
		if (player.GetButtonDown("Start")){
			togglePauseMenu();
		}
	}

	void OnGUI(){
		GUI.skin.font = interactFont;
	
		if (dialogueCanvas.GetComponent<inkDialogue> ().canvasChildCount == 0) {
//INTERACTABLE UI ICONS
			if (Player.GetComponent<playerController> ().interactMode == false && pauser.GetComponent<Canvas> ().enabled == false) {
				if (Player.GetComponent<playerController> ().keyInput == true) {
					foreach (GameObject highlighted in highlightedInteractables) {
						var position = Camera.main.WorldToScreenPoint (new Vector3 (highlighted.GetComponent<InteractionContainer> ().targetPos.x, highlighted.GetComponent<InteractionContainer> ().targetPos.y + highlighted.GetComponent<InteractionContainer> ().uiOffset, highlighted.GetComponent<InteractionContainer> ().targetPos.z));
						position.x = Mathf.Clamp (position.x - 8, 5, Screen.width - 25);
						position.y = Mathf.Clamp (position.y + 8, 5, Screen.height - 25);
						var textsize = GUI.skin.label.CalcSize (new GUIContent (interactable));
						if (highlighted.GetComponent<InteractionContainer> ().iconOverride == false) {
							if (highlighted == chosenInteractable && Player.GetComponent<playerController> ().keyInput == true) {
								if (highlighted.GetComponent<dialogueTrigger> () != null) {
									GUI.Label (new Rect (position.x, Screen.height - (position.y), textsize.x, textsize.y), interactableNPCSelected);
									if (highlighted.GetComponent<InteractionContainer> ().interactName != null) {
										var textlabelsize = GUI.skin.label.CalcSize (new GUIContent (highlighted.GetComponent<InteractionContainer> ().interactName));
										GUI.Label (new Rect (position.x + npcXOffset - (textlabelsize.x * 0.5f), (Screen.height - position.y) + npcYOffset, (textlabelsize.x + 1), textlabelsize.y), highlighted.GetComponent<InteractionContainer> ().interactName);
									}
								} else {
									GUI.Label (new Rect (position.x, Screen.height - position.y, textsize.x, textsize.y), interactableSelected);
									if (highlighted.GetComponent<InteractionContainer> ().interactName != null) {
										var textlabelsize = GUI.skin.label.CalcSize (new GUIContent (highlighted.GetComponent<InteractionContainer> ().interactName));
										if (position.x > (Screen.width * 0.5f)) {
											GUI.Label (new Rect (position.x - (textlabelsize.x + 1.75f), Screen.height - (position.y + textYOffset), (textlabelsize.x + 1), textlabelsize.y), highlighted.GetComponent<InteractionContainer> ().interactName);
										} else {
											GUI.Label (new Rect (position.x + (textsize.x + 1.75f), Screen.height - (position.y + textYOffset), (textlabelsize.x + 1), textlabelsize.y), highlighted.GetComponent<InteractionContainer> ().interactName);
										}
									}
								}
							} else {
								if (highlighted.GetComponent<dialogueTrigger> () != null) {
									GUI.Label (new Rect (position.x, Screen.height - (position.y), textsize.x, textsize.y), interactableNPC);
								} else {
									GUI.Label (new Rect (position.x, Screen.height - position.y, textsize.x, textsize.y), interactable);
								}
							}
						}
					}
				} else if (Player.GetComponent<playerController> ().keyInput == false) {
					Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
					RaycastHit hit;
					if (Physics.Raycast (ray, out hit, 100.0f, ScreenPickMask)) {
						//Debug.Log (hit.transform.gameObject.name);
						if (hit.transform.gameObject.tag == "Interactive") {
							var textsize = GUI.skin.label.CalcSize (new GUIContent (hit.collider.gameObject.GetComponent<InteractionContainer> ().interactName));
							GUI.Label (new Rect ((Screen.width * 0.5f) - (textsize.x * 0.5f), Screen.height - 25f, (textsize.x + 1), textsize.y), hit.transform.gameObject.GetComponent<InteractionContainer> ().interactName);
						}
						if (hit.transform.root.gameObject.tag == "NPC") {
							var textsize = GUI.skin.label.CalcSize (new GUIContent (hit.transform.root.gameObject.GetComponent<NPC> ().npcName));
							GUI.Label (new Rect ((Screen.width * 0.5f) - (textsize.x * 0.5f), Screen.height - 25f, (textsize.x + 1), textsize.y), hit.transform.root.gameObject.GetComponent<NPC> ().npcName);
						}
					}
				}

//INTERACT MODE CURSOR
			} else if (Player.GetComponent<playerController> ().interactViewSelection != null && Player.GetComponent<playerController> ().keyInput == true && pauser.GetComponent<Canvas> ().enabled == false) {
				var position = Camera.main.WorldToScreenPoint (Player.GetComponent<playerController> ().selector.transform.position);
				position.x = Mathf.Clamp (position.x - 8, 5, Screen.width - 25);
				position.y = Mathf.Clamp (position.y + 12, 5, Screen.height - 25);
				RaycastHit hit;
				if (Physics.Raycast (mainCam.transform.position, Player.GetComponent<playerController> ().selector.transform.position - mainCam.transform.position, out hit, 100.0f, ScreenPickMask)) {
					if (hit.collider.gameObject.tag == "Interactive") {
						iconSizeTime += Time.deltaTime;
						if (iconSizeTime > 0.15f) {
							var textsize = GUI.skin.label.CalcSize (new GUIContent (interactable));
							GUI.Label (new Rect (position.x, Screen.height - position.y, textsize.x, textsize.y), interactableBig);
						} else {
							var textsize = GUI.skin.label.CalcSize (new GUIContent (interactable));
							GUI.Label (new Rect (position.x, Screen.height - position.y, textsize.x, textsize.y), interactable);
						}
					} else {
						iconSizeTime = 0;
						var textsize = GUI.skin.label.CalcSize (new GUIContent (interactable));
						GUI.Label (new Rect (position.x, Screen.height - position.y, textsize.x, textsize.y), interactableSmall);
					}
				}
			}

//INTERACT MODE CANCEL TEXT
			if (Player.GetComponent<playerController> ().interactMode == true) {
				exitText = string.Concat ("press ", player.controllers.maps.GetFirstElementMapWithAction ("Cancel", true).elementIdentifierName, " to leave");
				var exitTextSize = GUI.skin.label.CalcSize (new GUIContent (exitText));
				GUI.Label (new Rect (5, Screen.height - 16, exitTextSize.x + 1, exitTextSize.y), exitText);
			}
		}
	}
	
	public void togglePauseMenu(){
		if (pauser.GetComponent<Canvas>().enabled == true){
			pauser.GetComponent<Canvas>().enabled = false;
			Time.timeScale = 1.0f;
		} else {
			pauser.GetComponent<Canvas>().enabled = true;
			Time.timeScale = 0.0f;
		}
	}

	public void chooseMissingKid () {
		List<string> kids = new List<string> ();
		kids.Add ("Cliff");
		kids.Add ("Emily");
		kids.Add ("Francine");
		kids.Add ("Gary");
		kids.Add ("Laura");
		kids.Add ("Megan");
		kids.Add ("Pete");
		int kidIndex = 666;
		foreach (string kidName in kids) {
			if (kidName == playerName) {
				kidIndex = kids.IndexOf(kidName);
			}
		}
		if (kidIndex != 666) {
			kids.RemoveAt (kidIndex);
		}
		int randomKid = Random.Range (0, kids.Count);
		missingKidName = kids [randomKid];
		kids.RemoveAt (randomKid);

		}

	public void disableBadKids () {
		List<GameObject> NPCS = new List<GameObject> ();
		NPCS.AddRange (GameObject.FindGameObjectsWithTag ("NPC"));
		foreach (GameObject person in NPCS) {
			if (person.GetComponent<NPC> ()!= null && person.GetComponent<NPC> ().npcName == playerName) {
				person.SetActive(false);
			}
			if (person.GetComponent<NPC> ()!= null && person.GetComponent<NPC> ().npcName == missingKidName) {
				person.SetActive(false);
			}
		}
	}

	static int sortByXPosition(GameObject obj1, GameObject obj2){
		float obj1Position = (Camera.main.WorldToScreenPoint (obj1.GetComponent<InteractionContainer> ().targetPos)).x;
		float obj2Position = (Camera.main.WorldToScreenPoint (obj2.GetComponent<InteractionContainer> ().targetPos)).x;
		return obj1Position.CompareTo (obj2Position);
	}

	public void NPCRoomTest (GameObject kidToTest) {
		foreach (GameObject room in Rooms) {
			room.SetActive (true);
		}
		RaycastHit Hit;
		if (Physics.Raycast (new Vector3 (kidToTest.transform.position.x, kidToTest.transform.position.y + 0.2f, kidToTest.transform.position.z), Vector3.down, out Hit, 10f, Player.GetComponent<playerController>().roomCheckMask)) {
			Debug.DrawRay (new Vector3 (kidToTest.transform.position.x, kidToTest.transform.position.y + 0.2f, kidToTest.transform.position.z), Vector3.down, Color.yellow);
			if (Hit.transform != null) {
				kidToTest.GetComponent<NPC> ().currentRoom = Hit.transform.gameObject;
				string locationVar = string.Concat (kidToTest.GetComponent<NPC> ().npcName, "Location");
				Debug.Log (locationVar);
				dialogueCanvas.GetComponent<inkDialogue> ().story.variablesState [locationVar] = Hit.transform.gameObject.name;
				string whatRoom = string.Concat (kidToTest.GetComponent<NPC>().npcName, " is in the ", Hit.transform.gameObject.name);
				Debug.Log (whatRoom);
				if (Hit.transform.gameObject.GetComponent<roomContainer> () != null) {
					kidToTest.GetComponent<NPC> ().isDark = Hit.transform.gameObject.GetComponent<roomContainer> ().isDark;
				}
			}
		}
		foreach (GameObject room in Rooms) {
			room.SetActive (false);
		}
		foreach (GameObject ownroom in Camera.main.transform.root.gameObject.GetComponent<CameraControl>().OwnRooms) {
			ownroom.SetActive (true);
		}
	}

	public void playerInit () {
		Player.GetComponent<playerController> ().playerName = playerName;
		Player.GetComponent<playerController> ().enabled = true;
	}

	public void CorePrefab (string charName, GameObject creator) {
		Debug.Log (charName);
		if (charName == "Cliff") {
			GameObject chosenCore = Instantiate (CliffPrefab, creator.transform);
			creator.GetComponent<playerController> ().humeObject = chosenCore;
		} else if (charName == "Emily") {
			GameObject chosenCore = Instantiate (EmilyPrefab, creator.transform);
			creator.GetComponent<playerController> ().humeObject = chosenCore;
		} else if (charName == "Francine") {
			GameObject chosenCore = Instantiate (FrancinePrefab, creator.transform);
			creator.GetComponent<playerController> ().humeObject = chosenCore;
		} else if (charName == "Laura") {
			GameObject chosenCore = Instantiate (LauraPrefab, creator.transform);
			creator.GetComponent<playerController> ().humeObject = chosenCore;
		} else {
			GameObject chosenCore = Instantiate (FrancinePrefab, creator.transform);
			creator.GetComponent<playerController> ().humeObject = chosenCore;
		} 
	}
}
