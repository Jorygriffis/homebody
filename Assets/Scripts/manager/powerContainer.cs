﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class powerContainer : MonoBehaviour {

	public bool powerOn = true;
	public int atticPower = 1;
	public int housePower = 1;
	public int reservePower = 0;

	void Start () {
		
	}

	void Update () {
		if (housePower > 0) {
			powerOn = true;
		} else {
			powerOn = false;
		}
	}
}
