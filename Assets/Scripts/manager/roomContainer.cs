﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class roomContainer : MonoBehaviour {

	public GameObject mainLight;
	public bool isDark = false;
	private bool fullyDark = false;

	void Start () {
		if (isDark == true) {
			fullyDark = true;
		}
	}
	void Update () {
		if (mainLight != null && mainLight.GetComponent<Light>().enabled == false) {
			isDark = true;
		} else if (fullyDark == true) {
			isDark = true;
		} else {
			isDark = false;
		}
	}
}
