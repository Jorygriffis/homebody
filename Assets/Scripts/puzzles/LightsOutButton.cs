﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LightsOutButton : MonoBehaviour {

	public Material offMaterial;
	public Material onMaterial;
	public Material winMaterial;

	public LightsOutButton northButton;
	public LightsOutButton southButton;
	public LightsOutButton eastButton;
	public LightsOutButton westButton;
	public GameObject lightBulbOff;
	public GameObject lightBulbOn;
	public PushButton pButton;
	private bool CrobusAdobus = false;

	public GameObject winBulbOff;
	public GameObject winBulbOn;

	public bool buttonHit;
	public bool buttonOn;
	public bool winButton;
	public bool winActive;
	private bool winReady;

	public List<LightsOutButton> otherButtons = new List<LightsOutButton> ();

	void Start () {
		pButton = gameObject.GetComponent<PushButton>();
	}

	void Update () {
		if (pButton.Pressed == true) {
			Debug.Log ("LightsOut Triggered");
			buttonHit = true;
			pButton.Pressed = false;
		}
		if (buttonHit) {
			if (CrobusAdobus) {
				//activation goes here
				winActive = !winActive;
			} else {
				buttonOn = !buttonOn;
				if (northButton != null) {
					northButton.buttonOn = !northButton.buttonOn;
				}
				if (southButton != null) {
					southButton.buttonOn = !southButton.buttonOn;
				}
				if (eastButton != null) {
					eastButton.buttonOn = !eastButton.buttonOn;
				}
				if (westButton != null) {
					westButton.buttonOn = !westButton.buttonOn;
				}
			}
			buttonHit = false;
		}
		if (buttonOn) {
			if (winButton) {
				winReady = true;
				foreach (LightsOutButton otherButton in otherButtons) {
					if (otherButton.buttonOn) {
						winReady = false;
					} 
				}
				if (winReady) {
					//win material
					winActive = true;
					gameObject.GetComponent<Renderer> ().material = winMaterial;
				} else {
					//on material
					winActive = false;
					gameObject.GetComponent<Renderer> ().material = onMaterial;
				}
			} else {
				//on material
				gameObject.GetComponent<Renderer> ().material = onMaterial;
				winReady = false;
			}
			if (lightBulbOn != null) {
				lightBulbOn.SetActive (true);
				lightBulbOff.SetActive (false);
			}
		} else {
			//off material
			winActive = false;
			gameObject.GetComponent<Renderer> ().material = offMaterial;
			if (lightBulbOn != null) {
				lightBulbOff.SetActive (true);
				lightBulbOn.SetActive (false);
			}
		}
		if (winBulbOn != null) {
			if (winActive == true) {
				winBulbOn.SetActive (true);
				winBulbOff.SetActive (false);
			} else {
				winBulbOn.SetActive (false);
				winBulbOff.SetActive (true);
			}
		}
	}
}
