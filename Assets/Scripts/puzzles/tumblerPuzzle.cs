﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tumblerPuzzle : MonoBehaviour {

	public GameObject tumbler1;
	public int tumbler1Number = 0;
	public GameObject tumbler2;
	public int tumbler2Number = 0;
	public GameObject tumbler3;
	public int tumbler3Number = 0;
	public float speedMultiplier = 1;
	private float t = 0;
	private int toTurn = 1;
	private PushButton pushButton;
	private float tumbler1Angle = 0;
	private float tumbler2Angle = 0;
	private float tumbler3Angle = 0;
	public Door doorToUnlock;

	void Start () {
		pushButton = gameObject.GetComponent<PushButton> ();
	}

	void Update () {
		if (tumbler1Number == 1 && tumbler2Number == 1 && tumbler3Number == 1) {
			doorToUnlock.locked = false;
		} else {
			doorToUnlock.locked = true;
		}
		if (pushButton.Pressed == true) {
			if (t == 0) {
				turnTumblers ();
			}
			t += Time.deltaTime * speedMultiplier;
			if (t > 1) {
				toTurn = toTurn + 1;
				t = 0;
			}
		} else {
			toTurn = 1;
			t = 0;
		}
		tumbler1Angle = Mathf.LerpAngle (tumbler1Angle, tumbler1Number * 120, Time.deltaTime * 4);
		tumbler2Angle = Mathf.LerpAngle (tumbler2Angle, tumbler2Number * 120, Time.deltaTime * 4);
		tumbler3Angle = Mathf.LerpAngle (tumbler3Angle, tumbler3Number * 120, Time.deltaTime * 4);
		tumbler1.transform.eulerAngles = new Vector3 (-90f, tumbler1Angle, 0);
		tumbler2.transform.eulerAngles = new Vector3 (-90f, tumbler2Angle, 0);
		tumbler3.transform.eulerAngles = new Vector3 (-90f, tumbler3Angle, 0);
	}

	void turnTumblers (){
		if (toTurn == 1) {
			tumbler1Angle = tumbler1Number * 120;
			tumbler1Number = tumbler1Number + 1;
		}
		if (toTurn == 2) {
			tumbler1Number = tumbler1Number + 1;
			tumbler2Number = tumbler2Number + 1;
		}
		if (toTurn > 2) {
			tumbler1Number = tumbler1Number + 1;
			tumbler2Number = tumbler2Number + 1;
			tumbler3Number = tumbler3Number + 1;
		}
		if (tumbler1Number == 3) {
			tumbler1Number = 0;
		}
		if (tumbler2Number == 3) {
			tumbler2Number = 0;
		}
		if (tumbler3Number == 3) {
			tumbler3Number = 0;
		}
	}
}
