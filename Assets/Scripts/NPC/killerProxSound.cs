﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class killerProxSound : MonoBehaviour {

	public AudioSource droneSource;
	public AudioSource screechSource;
	private NPC selfNPCScript;
	public float distToPlayer;
	public float droneDist;
	public float screechDist;
	public float screechOffset;
	public float droneOffset;
	private float droneVolMax;
	private float screechVolMax;

	void Start () {
		selfNPCScript = gameObject.GetComponent<NPC> ();
		droneVolMax = droneSource.volume;
		screechVolMax = screechSource.volume;
		droneSource.Play();
		screechSource.Play();
	}

	void Update () {
		if (selfNPCScript.followDistance == Mathf.Infinity) {
			distToPlayer = Vector3.Distance (transform.position, selfNPCScript.Player.transform.position);
		} else {
			distToPlayer = selfNPCScript.followDistance;
		}

		if (distToPlayer < droneDist + droneOffset) {
			if (distToPlayer <= droneDist) {
				droneSource.volume = Mathf.Lerp(droneSource.volume, 1f * droneVolMax, Time.deltaTime);
			} else {
				float droneDistMod = distToPlayer - droneDist;
				droneSource.volume = Mathf.Lerp(droneSource.volume, ((droneOffset - droneDistMod) / droneOffset) * droneVolMax, Time.deltaTime);
			}
		} else {
			droneSource.volume = Mathf.Lerp(droneSource.volume, 0f, Time.deltaTime);
		}
		if (distToPlayer < screechDist + screechOffset) {
//			if (selfNPCScript.followDistance <= screechDist) {
//				screechSource.volume = Mathf.Lerp(screechSource.volume, 1f * screechVolMax, Time.deltaTime * 1.2f);
//			} else {
				float screechDistMod = distToPlayer - screechDist;
				float screechVolMod = ((screechOffset - screechDistMod) / screechOffset) * screechVolMax;
			if (Vector3.Distance (transform.position, selfNPCScript.Player.transform.position) < 8) {
				if (screechVolMod < 0.2f) {
					screechSource.volume = Mathf.Lerp (screechSource.volume, 0f, Time.deltaTime * 1.2f);
				} else {
					screechSource.volume = Mathf.Lerp (screechSource.volume, screechVolMod, Time.deltaTime * 1.2f);
				}
			}
//			}
		} else {
			screechSource.volume = Mathf.Lerp(screechSource.volume, 0f, Time.deltaTime * 1.2f);
		}
	}
}
