﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;
using UnityEngine.AI;

public class NPC : MonoBehaviour {

	public GameObject humeObject;
	public string npcName = "creature";
	public GameObject currentRoom;
	public bool isDark = false;
	public bool dialogOverride = false;
	public bool followPlayer = false;
	public bool activeFollow = false;
	public float followThreshold = 1f;
	public float followDistance = 666f;
	public bool lookAtPlayer = false;
	[HideInInspector]
	public GameObject Player;
	private float moveSpeed = 666f;
	private float previousMoveSpeed = 666f;
	private Vector3 previousPos;
	private Vector3 lastPlayerPos;
	private Animator animator;
	public float moveAnimThreshold = 1f;
	private AnimationState breatheAnimation;
	public InteractionContainer selfContainer;
	public GameObject lookTarget;
	public bool turnedToPlayer = false;
	public Quaternion startRotation;
	public float quatAngle;
	private float turnTime = 0;
	private bool lookAtPlayerDist = false;
	private float agentSpeed;
	private LookAtIK lookAt;

	private string lastAnim;

	void Start () {
		lookAt = humeObject.GetComponent<LookAtIK> ();
		Player = GameObject.Find ("player");
		if (npcName == Player.GetComponent<playerController> ().playerName) {
			gameObject.SetActive (false);
		}
		selfContainer = humeObject.GetComponent<humeHelper> ().NPCinteractable.GetComponent<InteractionContainer>();
		selfContainer.interactName = npcName;
		lookTarget = humeObject.GetComponent<humeHelper> ().lookTarget;
		animator = humeObject.GetComponent<Animator> ();
		//animator ["BaseTestBreathe"].blendMode = AnimationBlendMode.Additive;
		//animator ["BaseTestBreathe"].layer = 10;
		agentSpeed = gameObject.GetComponent<NavMeshAgent> ().speed;
	}

	void Update () {

		if (gameObject.GetComponent<NavMeshAgent>().isOnOffMeshLink) {
			gameObject.GetComponent<NavMeshAgent> ().speed = agentSpeed * 0.3f;
		} else {
			gameObject.GetComponent<NavMeshAgent> ().speed = agentSpeed;
		}
			
		if (Input.GetKeyDown (KeyCode.I)) {
			Player.GetComponent<playerController>().Container.NPCRoomTest (gameObject);
		}
		followDistance = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ().remainingDistance;
		if (followPlayer == true) {
			if (activeFollow == false) {
				gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ().isStopped = true;
				if (Vector3.Distance (gameObject.transform.position, Player.transform.position) > followThreshold * 1.25f) {
					activeFollow = true;
				}
			} else {
				gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ().isStopped = false;
				if (Vector3.Distance (Player.transform.position, lastPlayerPos) > 1) {
					lastPlayerPos = Player.transform.position;
					gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ().destination = Player.transform.position;
				}
				if (Vector3.Distance (gameObject.transform.position, Player.transform.position) <= followThreshold) {
					activeFollow = false;
				}
			}
		} else {
			gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ().isStopped = true;
			activeFollow = true;
		}

		//DEBUG MOVESPEED
		previousMoveSpeed = moveSpeed;
		moveSpeed = ((transform.position - previousPos).magnitude) / Time.deltaTime;
		humeObject.GetComponent<Animator>().SetFloat("moveSpeed", moveSpeed);
		previousPos = transform.position;

		//BREATHE ANIMATION
		//animator.Blend ("BaseTestBreathe", 1f, 0f);

		//MOVE ANIMATIONS
		if (dialogOverride == false) {
			if (moveSpeed > moveAnimThreshold || previousMoveSpeed > moveAnimThreshold) {
				crossFadeAnim ("BaseTestWalk", 0.1f);
				//animator.Play ("BaseTestWalk");
			} else {
				crossFadeAnim ("BaseTestIdle", 0.2f);
				//animator.Play ("BaseTestIdle");
			}
		}

		quatAngle = Mathf.Abs (Quaternion.Angle (transform.rotation, startRotation));

		Vector3 targetDir = Player.transform.position - transform.position;
		if (Vector3.Distance(Player.transform.position, transform.position) < 1f && Mathf.Abs (Vector3.Angle (targetDir, transform.forward)) < 85f && turnedToPlayer == false) {
			lookAtPlayerDist = true;
		} else {
			lookAtPlayerDist = false;
		}
		if (lookAtPlayer == true || lookAtPlayerDist == true) {
			turnTime = 0;
			if (Mathf.Abs (Vector3.Angle (targetDir, transform.forward)) > 100f) {
				if (turnedToPlayer == false) {
					startRotation = transform.rotation;
					turnedToPlayer = true;
				}
			}
			if (turnedToPlayer == true && Mathf.Abs(Vector3.Angle(targetDir, transform.forward)) > 3f) {
				Quaternion rotation = Quaternion.LookRotation (targetDir, Vector3.up);
				transform.rotation = Quaternion.Lerp (transform.rotation, rotation, Time.deltaTime * 3.5f);
			}
			lookAt.solver.IKPosition = Vector3.Lerp (lookAt.solver.IKPosition, Player.GetComponent<playerController>().humeObject.GetComponent<humeHelper>().headObject.position, Time.deltaTime * 12);
		} else if (lookAtPlayer != true) {
			if (turnedToPlayer) {
				lookAt.solver.IKPosition = Vector3.Lerp (lookAt.solver.IKPosition, lookTarget.transform.position, Time.deltaTime * 12);
				turnTime += Time.deltaTime;
				if (turnTime >= 0.4f) {
					if (quatAngle > 3f) {
						transform.rotation = Quaternion.Lerp (transform.rotation, startRotation, Time.deltaTime * 3.5f);
					} else {
						turnedToPlayer = false;
					}
				}
			} else {
				lookAt.solver.IKPosition = Vector3.Lerp (lookAt.solver.IKPosition, lookTarget.transform.position, Time.deltaTime * 9);
			}
		}
	}

	public void crossFadeAnim(string animName, float fadeLength) {
		if (lastAnim != null) {
			if (lastAnim == animName) {
				//Debug.Log ("already playing animation");
				//do nothing
			} else {
				//Debug.Log ("playing animation because it isn't already playing or there is a goof-up");
				animator.CrossFade (animName, fadeLength);
			}
		}
		lastAnim = animName;
	}
}
