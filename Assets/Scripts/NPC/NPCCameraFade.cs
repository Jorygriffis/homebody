﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCCameraFade : MonoBehaviour {

	public bool canFade = true;
	public float currentDist = 666f;
	public float fadeDist = 1f;
	public float cutoffValue = 0;
	public Material selfMaterial;
	public GameObject mainCamera;

	void Start () {
		selfMaterial = Material.Instantiate (gameObject.GetComponent<SkinnedMeshRenderer> ().material);
		gameObject.GetComponent<SkinnedMeshRenderer> ().material = selfMaterial;
	}

	void Update () {
		mainCamera = GameObject.FindGameObjectWithTag ("MainCamera");
		if (canFade == true && mainCamera.transform.root.gameObject.GetComponent<CameraControl>().NPCFade == true) {
			cutoffValue = fadeDist - (currentDist / fadeDist) * 2;
			currentDist = Vector3.Distance (new Vector3 (gameObject.transform.root.position.x, mainCamera.transform.position.y, gameObject.transform.root.position.z), mainCamera.transform.position);
			if (currentDist < fadeDist) {
				selfMaterial.SetFloat ("_Cutoff", cutoffValue);
			} else {
				cutoffValue = 0f;
				selfMaterial.SetFloat ("_Cutoff", 0f);
			}
		} else {
			cutoffValue = 0f;
			selfMaterial.SetFloat ("_Cutoff", 0f);
		}
	}
}
