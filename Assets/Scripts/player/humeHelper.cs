﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class humeHelper : MonoBehaviour {

	public Animator animator;
	public GameObject flashlightObj;
	public GameObject meshObject;
	public Transform headObject;
	public GameObject NPCinteractable;
	public GameObject lookTarget;
	public NPCCameraFade cameraFader;
	public float moveSpeed;

	void Update () {
		animator.SetFloat("moveSpeed", moveSpeed);
	}
}
