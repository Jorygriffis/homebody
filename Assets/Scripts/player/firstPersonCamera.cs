﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class firstPersonCamera : MonoBehaviour {

	private Player player;
	public Vector3 startRotation;
	public float rotX;
	public float rotY;
	public float maxX;
	public float maxY;
	public float minX;
	public float minY;
	public float clampAngle = 80f;
	private float mouseFactor = 100f;
	private bool mouseEnable = true;
	private GameObject Player;
	private bool init = false;

	void Start () {
		startRotation = gameObject.transform.localRotation.eulerAngles;
		rotX = startRotation.x;
		rotY = startRotation.y;
		Player = GameObject.FindGameObjectWithTag ("Player");
		maxX = rotX + 18f;
		minX = rotX - 18f;
		maxY = rotY + 18f;
		minY = rotY - 18f;
		player = Rewired.ReInput.players.GetPlayer (0);
		init = true;
	}

	void OnEnable () {
		if (init) {
			transform.rotation = Quaternion.Euler (startRotation);
			rotX = startRotation.x;
			rotY = startRotation.y;
		}
	}

	void Update () {
		if (Player.GetComponent<playerController> ().keyInput == false) {
			float mouseX = -Input.GetAxis ("Mouse Y");
			float mouseY = Input.GetAxis ("Mouse X");

			Quaternion localRotation = Quaternion.Euler (Mathf.Clamp (rotX += mouseX * mouseFactor * Time.deltaTime, minX, maxX), Mathf.Clamp (rotY += mouseY * mouseFactor * Time.deltaTime, minY, maxY), 0.0f);
			transform.localRotation = localRotation;
		} else {
			float vertSum = Mathf.Clamp (player.GetAxis ("Vertical") + player.GetAxis ("RVertical"), -1, 1);
			float horSum = Mathf.Clamp (player.GetAxis ("Horizontal") + player.GetAxis ("RHorizontal"), -1, 1);
			transform.localRotation = Quaternion.Euler (startRotation.x - vertSum * clampAngle, startRotation.y + horSum * clampAngle, 0.0f);
		}
	}

	void OnDisable () {
		transform.rotation = Quaternion.Euler (startRotation);
	}
}
