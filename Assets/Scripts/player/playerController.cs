﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RootMotion.FinalIK;
using Rewired;
using NaughtyAttributes;

public class playerController : MonoBehaviour {

	public GameObject humeObject;
	[BoxGroup("Set by Ink")]
	public string playerName = "Crobus";
	[BoxGroup("Set by Ink")][Tooltip("Overrides idle/walk animation routine for Ink scripted animation.")]
	public bool dialogOverride = false;

	[RequiredAttribute][Tooltip("Invisible selection GameObject for interaction mode cursor.")]
	public GameObject selector;
	[RequiredAttribute]
	public ArrayContainer Container;

	[BoxGroup("Set by in-game Options")][Tooltip("If true, use tank controls. If false, camera relative controls.")]
	public bool tankMode = false;
	[BoxGroup("Set by in-game Options")][Tooltip("Flashlight automatic on/off toggle.")]
	public bool flashlightAuto = true;

	[BoxGroup("Character movement tweaks")]
	public float interactionTurnRate;
	[BoxGroup("Character movement tweaks")]
	public float walkSpeed = 1.85f;
	[BoxGroup("Character movement tweaks")]
	public float runSpeed = 2.7f;
	[BoxGroup("Character movement tweaks")][Tooltip("Minimum movement threshold - meant to minimize unintended movement.")]
	public float moveMinimum = 0.01f;
	[BoxGroup("Character movement tweaks")][Tooltip("Minimum rotation angle to turn to interactable.")]
	public float rotationMinimum = 25f;
	[BoxGroup("Character movement tweaks")][Tooltip("Minimum movement speed to trigger movement animation.")]
	public float moveAnimThreshold = 1f;

	[BoxGroup("Debug")][Tooltip("Room player is standing in.")]
	public GameObject currentRoom;
	[BoxGroup("Debug")]
	public GameObject interactionViewCamera;
	[BoxGroup("Debug")][Tooltip("Player move speed debug.")]
	public float moveSpeed = 0;
	[BoxGroup("Debug")][Tooltip("Player move target.")]
	public Vector3 ScreenPickPos;
	[BoxGroup("Debug")]
	public GameObject closestTrigger;
	[BoxGroup("Debug")]
	public GameObject pickedObject;

	[HideInInspector][Tooltip("Player GameObject's FinalIK interaction system.")]
	public InteractionSystem pInteractSys;
	private InteractionTrigger targetTrigTest;
	[HideInInspector]
	public float interactInterrupt = 0f;
	[HideInInspector]
	public bool isDark = false;
	[HideInInspector]
	public float distanceToInteractable = 666f;
	private Rigidbody selfRigidBody;
	private Vector3 previousPos;
	[HideInInspector]
	public Animator animator;
	private bool canWalk = true;
	private bool canWalkInt = true;
	[HideInInspector]
	public bool canWalkExt = true;

	[HideInInspector]
	public List<GameObject> cameraColliders;
	private LookAtIK lookAt;
	private Transform lookTarget;
	[HideInInspector][Tooltip("LayerMask for room check raycast--should be set to Room.")]
	public LayerMask roomCheckMask;
	private float previousMoveSpeed = 0;
	private bool interactToggle = false;
	private bool continueTurn = false;
	private bool stickToggle = false;
	private float lightGoal = 0;
	private float lightTemp = 0;
	private float lightTime = 0;
	private float lightFactor = 1;
	private bool lightAutoOn = false;
	private bool lightManualOn = false;
	private bool lightDarkToggle = false;
	[HideInInspector]
	public float targetAngle;
	private GameObject camExitTrigger;
	[HideInInspector]
	public GameObject interactViewSelection;
	[HideInInspector]
	public bool running;
	[HideInInspector]
	public float playerAngle;
	[HideInInspector]
	public float turnLerp = 5;
	[HideInInspector]
	public float joyBoundary = 20;
	[HideInInspector]
	public float stickCamHold = 0;
	[HideInInspector]
	public float camAngle = 0;
	[HideInInspector]
	public float stickAngle;
	[HideInInspector]
	public bool canInteract = true;
	[HideInInspector]
	public bool keyInput = true;
	[HideInInspector]
	public bool interactMode = false;
	[HideInInspector]
	public bool hidingAnimation = false;
	private bool doubleClick;
	private GameObject mainCam;
	private UnityEngine.AI.NavMeshAgent agent;
	private string lastAnim;

	private Player player;

	public void Awake () {
		player = Rewired.ReInput.players.GetPlayer (0);
		roomCheckMask |= (1 << LayerMask.NameToLayer ("Room"));
	}

	void Start () {
		
		runSpeed = runSpeed * 1.65f;
		walkSpeed = walkSpeed * 1.65f;

		#if UNITY_EDITOR
		runSpeed = runSpeed / 1.65f;
		walkSpeed = walkSpeed / 1.65f;
		#endif

		Container.CorePrefab (playerName, gameObject);
		gameObject.GetComponent<flashlight> ().Init ();
		humeObject.GetComponent<humeHelper> ().NPCinteractable.SetActive(false);
		if (humeObject.GetComponent<humeHelper> ().cameraFader != null) {
			humeObject.GetComponent<humeHelper> ().cameraFader.canFade = false;
		}
		pInteractSys = humeObject.GetComponent<InteractionSystem> ();
		pInteractSys.characterCollider = gameObject.GetComponent<BoxCollider> ();
		agent = GetComponent<UnityEngine.AI.NavMeshAgent> ();
		selfRigidBody = gameObject.GetComponent<Rigidbody> ();
		animator = humeObject.GetComponent<humeHelper> ().animator;

		//GET PLAYER LOOKATIK
		lookAt = humeObject.GetComponent<LookAtIK>();
		lookTarget = humeObject.transform.Find ("looktarget");
	}

	void OnTriggerStay (Collider camTestCollision) {
		if (camTestCollision.gameObject.GetComponent<CameraControl> () != null) {
			cameraColliders.Add (camTestCollision.gameObject);
		}
	}

	void Update () {
		
		//Debug.Log (cameraColliders.Count);

		if (agent.isOnOffMeshLink) {
			Debug.Log ("player is on off mesh link");
		}
		interactInterrupt = Mathf.Clamp01 (interactInterrupt - Time.deltaTime);
		int closestTriggerIndex = pInteractSys.GetClosestTriggerIndex ();
		if (closestTriggerIndex != -1) {
			closestTrigger = pInteractSys.GetClosestInteractionObjectInRange ().gameObject;
		} else {
			closestTrigger = null;
		}

//COMBINED CANWALK
		if (canWalkInt == true && canWalkExt == true) {
			canInteract = true;
			canWalk = true;
		} else if (canWalkExt == false) {
			canWalk = false;
			canInteract = false;
		} else {
			canWalk = false;
		}

//CURRENT ROOM TEST
		RaycastHit Hit;
		if (Physics.Raycast (new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y + 0.2f, gameObject.transform.position.z), Vector3.down, out Hit, 10f, roomCheckMask)) {
			if (Hit.transform.gameObject != null) {
				currentRoom = Hit.transform.gameObject;
				if (currentRoom.GetComponent<roomContainer> () != null) {
					isDark = currentRoom.GetComponent<roomContainer> ().isDark;
				}
			} else {
				isDark = false;
			}
		}

//DEBUG MOVESPEED
		previousMoveSpeed = moveSpeed;
		moveSpeed = ((transform.position - previousPos).magnitude) / Time.deltaTime;
		humeObject.GetComponent<Animator>().SetFloat("moveSpeed", moveSpeed);
		previousPos = transform.position;

//MOVE ANIMATIONS
		if (hidingAnimation == true) {
			crossFadeAnim ("BaseTestCrouch", 0.4f);
		} else if (dialogOverride == false) {
			if (Input.GetKey (KeyCode.UpArrow) || moveSpeed > moveAnimThreshold || previousMoveSpeed > moveAnimThreshold) {
				if (player.GetButton ("Run") || Container.doubleClick) {
					crossFadeAnim("BaseTestRun", 0.1f);
				} else {
					crossFadeAnim("BaseTestWalk", 0.1f);
				}
			} else {
				crossFadeAnim("BaseTestIdle", 0.2f);
			}
		}
			
//FIND MAINCAMERA
		mainCam = GameObject.FindGameObjectWithTag ("MainCamera");

//RUN SPEED
		if (Container.doubleClick) {
			//animator ["BaseTestWalk"].speed = 1.6f;
			agent.speed = runSpeed;
		} else {
			//animator ["BaseTestWalk"].speed = 1.2f;
			agent.speed = walkSpeed;
		}

//CONTROLLER/KEYBOARD CONTROL
		if (canWalk == true) {
			if (player.GetButton ("Horizontal") || player.GetNegativeButton ("Horizontal") || player.GetButton ("Vertical") || player.GetNegativeButton ("Vertical")) {
				if (gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ().enabled == true) {
					agent.destination = gameObject.transform.position;
					agent.isStopped = true;
					keyInput = true;
				}
			}
		}

		if (canWalk == true && tankMode == true) {
			if (interactMode == false) {
				if (player.GetNegativeButton("Horizontal")) {
					transform.Rotate (0, -290 * Mathf.Abs(player.GetAxis("Horizontal")) * Time.deltaTime, 0);
				} else if (player.GetButton("Horizontal")) {
					transform.Rotate (0, 290 * player.GetAxis("Horizontal") * Time.deltaTime, 0);
				}
				if (player.GetButton("Vertical")) {
					if (player.GetButton("Run")) {
						running = true;
						//animator ["BaseTestWalk"].speed = Mathf.Clamp(Mathf.Abs(player.GetAxis("Vertical")), 0.5f, 1f) * 1.6f;
						transform.position += transform.forward * player.GetAxis("Vertical") * Time.deltaTime * runSpeed * 1.07f;
					} else {
						running = false;
						//animator ["BaseTestWalk"].speed = Mathf.Clamp(Mathf.Abs(player.GetAxis("Vertical")), 0.5f, 1f) * 1.2f;
						transform.position += transform.forward * player.GetAxis("Vertical") * Time.deltaTime * walkSpeed * 1.07f;
					}
				} else if (player.GetNegativeButton("Vertical")) {
					//animator ["BaseTestWalk"].speed = -0.8f;
					crossFadeAnim ("BaseTestWalkBackwards", 0.2f);
					transform.position += transform.forward * -player.GetAxis("Vertical") * Time.deltaTime * -1.75f;
				}
			}
		}

		if (canInteract && player.GetButtonDown("Submit")) {
			keyInput = true;
			if (interactMode == false) {
				if (gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ().enabled == true && Container.chosenInteractable != null && interactInterrupt < 0.1f && Container.chosenInteractable.activeInHierarchy == true && Container.chosenInteractable.name != "exittrigger") {
					pickedObject = Container.chosenInteractable;
					ScreenPickPos = Container.chosenInteractable.GetComponent<InteractionContainer> ().walkPoint.transform.position;
					Container.interactOff = false;
					agent.isStopped = false;
					agent.destination = ScreenPickPos;
				}
			} else if (interactViewSelection != null) {
				interactViewSelection.GetComponent<InteractionContainer> ().activated = true;
			}
		}
			
//INTERACTION VIEW CONTROLS
		camExitTrigger = GameObject.Find ("exittrigger");
		if (camExitTrigger != null && camExitTrigger.activeInHierarchy == true) {
			interactMode = true;
			interactionViewCamera = camExitTrigger.gameObject.GetComponent<InteractionContainer> ().targetObj;
			if (interactionViewCamera != null) {
				if (interactToggle == false) {
					if (interactionViewCamera.GetComponent<LightSwitchCameraEnable> ().startSelection != null) {
						interactViewSelection = interactionViewCamera.GetComponent<LightSwitchCameraEnable> ().startSelection;
						selector.transform.position = interactionViewCamera.GetComponent<LightSwitchCameraEnable> ().startSelection.transform.position;
					}
					interactToggle = true;
				}
				if (interactViewSelection != null) {
					selector.transform.position = Vector3.Lerp (selector.transform.position, interactViewSelection.transform.position, Time.deltaTime * 35);
					if (player.GetButtonRepeating ("Vertical") || player.GetButtonRepeating ("RVertical")) {
						if (interactViewSelection.GetComponent<InteractionContainer> ().interactViewNav [0] != null) {
							interactViewSelection = interactViewSelection.GetComponent<InteractionContainer> ().interactViewNav [0];
						}
					} else if (player.GetNegativeButtonRepeating ("Vertical") || player.GetNegativeButtonRepeating ("RVertical")) {
						if (interactViewSelection.GetComponent<InteractionContainer> ().interactViewNav [1] != null) {
							interactViewSelection = interactViewSelection.GetComponent<InteractionContainer> ().interactViewNav [1];
						}
					} else if (player.GetButtonRepeating ("Horizontal") || player.GetButtonRepeating ("RHorizontal")) {
						if (interactViewSelection.GetComponent<InteractionContainer> ().interactViewNav [2] != null) {
							interactViewSelection = interactViewSelection.GetComponent<InteractionContainer> ().interactViewNav [2];
						}
					} else if (player.GetNegativeButtonRepeating ("Horizontal") || player.GetNegativeButtonRepeating ("RHorizontal")) {
						if (interactViewSelection.GetComponent<InteractionContainer> ().interactViewNav [3] != null) {
							interactViewSelection = interactViewSelection.GetComponent<InteractionContainer> ().interactViewNav [3];
						}
					}
				}
			}
			if (player.GetButtonDown ("Cancel")) {
				camExitTrigger.GetComponent<InteractionContainer> ().activated = true;
			}
		} else {
			interactViewSelection = null;
			interactToggle = false;
			interactionViewCamera = null;
			camExitTrigger = null;
			interactMode = false;
		}

//CAMERA RELATIVE CONTROL
		if (tankMode == false && interactMode == false && canWalk == true) {
			if (player.GetAxis ("Horizontal") != 0f || player.GetAxis ("Vertical") != 0f) {
				stickAngle = (Mathf.Atan2 (-player.GetAxis ("Vertical"), player.GetAxis ("Horizontal")) * Mathf.Rad2Deg) + 90;
				if (stickToggle == false) {
					camAngle = Camera.main.transform.eulerAngles.y;
					if (Camera.main.gameObject.GetComponent<CameraFollow>() != null) {
						camAngle = camAngle + Camera.main.gameObject.GetComponent<CameraFollow>().relativeControlOffset;
					}
					stickCamHold = stickAngle + 400;
				}
				stickToggle = true;
				if (player.GetButton ("DigitalPress") == true) {
					turnLerp = 30;
				} else {
					turnLerp = 13;
				}
				playerAngle = Mathf.LerpAngle (playerAngle, stickAngle + camAngle, Time.deltaTime * turnLerp);
				transform.eulerAngles = new Vector3 (0, playerAngle, 0);
				float axisSpeed = (Mathf.Clamp (Mathf.Max (Mathf.Abs (player.GetAxis ("Horizontal")), Mathf.Abs (player.GetAxis ("Vertical"))), 0.5f, 1f));
				if (player.GetButton ("Run")) {
					running = true;
					//animator ["BaseTestWalk"].speed = axisSpeed * 1.6f;
					transform.position += transform.forward * axisSpeed * Time.deltaTime * runSpeed * 1.07f;
				} else {
					running = false;
					//animator ["BaseTestWalk"].speed = axisSpeed * 1.3f;
					transform.position += transform.forward * axisSpeed * Time.deltaTime * walkSpeed * 1.07f;
				}
				if (Mathf.Abs (stickCamHold - (stickAngle + 400)) > joyBoundary) {
					stickToggle = false;
				}
			} else {
				stickToggle = false;
			}
		}
			
//FLASHLIGHT AUTOMATIC LEVEL

		if (flashlightAuto == true) {
		if (isDark == true) {
			lightGoal = 1;
			lightFactor = 0.8f;
		} else {
			lightGoal = 0;
			lightFactor = 1.1f;
		}
		if (lightTemp != lightGoal) {
			if (isDark == true) {
				lightTime = -0.2f;
			} else {
				lightTime = 0;
			}
		}
		lightTemp = lightGoal;
		lightTime += Time.deltaTime * lightFactor;
		gameObject.GetComponent<flashlight> ().lightLevel = Mathf.Clamp01 (Mathf.Lerp(gameObject.GetComponent<flashlight> ().lightLevel, lightGoal, lightTime));

			if (lightTime > 0.5f && gameObject.GetComponent<flashlight> ().lightLevel < 0.04f) {
				lightDarkToggle = false;
				lightAutoOn = false;
			}
			if (isDark == true) {
				if (lightDarkToggle == false) {
					lightAutoOn = true;
					lightManualOn = true;
					lightDarkToggle = true;
				}
			}
		} else {
			gameObject.GetComponent<flashlight> ().lightLevel = 1;
			lightAutoOn = true;
		}

//FLASHLIGHT MANUAL CONTROL
		if (player.GetButtonDown("Flashlight")){
			lightManualOn = !lightManualOn;
		}
		if (lightManualOn == true && lightAutoOn == true) {
			gameObject.GetComponent<flashlight> ().flashlightOn = true;
		} else {
			gameObject.GetComponent<flashlight> ().flashlightOn = false;
		}

//MOVE ON CLICK
		if (Input.GetMouseButton (0) && canWalk == true) {
			keyInput = false;
			if (interactMode == false) {
				if (gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ().enabled == true && Vector3.Distance (gameObject.transform.position, ScreenPickPos) > moveMinimum) {
					agent.isStopped = false;
					agent.destination = ScreenPickPos;
				}
			}
		}

		if (continueTurn == true && targetAngle > 4f) {
			Debug.Log ("continue turn");
			transform.rotation = Quaternion.RotateTowards (transform.rotation, Quaternion.LookRotation ((new Vector3 (pickedObject.GetComponent<InteractionContainer> ().turnPointPos.x, gameObject.transform.position.y, pickedObject.GetComponent<InteractionContainer> ().turnPointPos.z)) - transform.position), Time.deltaTime * interactionTurnRate);
			if (targetAngle < 5f) {
				continueTurn = false;
			}
		}
	}

//INTERACTION COROUTINE -- INCLUDES CHECKS FOR WALKPOINT, ETC
	void Interact() {
		StartCoroutine (waitTime ());

		targetTrigTest = pickedObject.GetComponent<InteractionContainer>().walkPoint.GetComponent<InteractionTrigger>();
		if (targetTrigTest != null) {
			int closestTriggerIndex = pInteractSys.GetClosestTriggerIndex ();
			if (closestTriggerIndex == -1) return;
			if (!pInteractSys.TriggerEffectorsReady (closestTriggerIndex)) return;
			Container.interactOff = true;
			pInteractSys.TriggerInteraction (closestTriggerIndex, false);
			pickedObject.GetComponent<InteractionContainer> ().activated = true;
		} else if (pickedObject.GetComponent<InteractionObject> () != null) {
			Container.interactOff = true;
			if (pickedObject.GetComponent<InteractionContainer> ().leftHand == true) {
				pInteractSys.StartInteraction (FullBodyBipedEffector.LeftHand, pickedObject.GetComponent<InteractionObject> (), true);
			} else {
				pInteractSys.StartInteraction (FullBodyBipedEffector.RightHand, pickedObject.GetComponent<InteractionObject> (), true);
			}
			pickedObject.GetComponent<InteractionContainer> ().activated = true;
		} else if (pickedObject.GetComponentInChildren<InteractionObject>() != null) {
			InteractionObject iObj = pickedObject.GetComponentInChildren<InteractionObject> ();
			Container.interactOff = true;
			if (pickedObject.GetComponent<InteractionContainer> ().leftHand == true) {
				pInteractSys.StartInteraction (FullBodyBipedEffector.LeftHand, pickedObject.GetComponent<InteractionObject> (), true);
			} else {
				pInteractSys.StartInteraction (FullBodyBipedEffector.RightHand, iObj, true);
			}
			pickedObject.GetComponent<InteractionContainer> ().activated = true;
		} else {
			Container.interactOff = true;
			pickedObject.GetComponent<InteractionContainer> ().activated = true;
		}
		canInteract = true;
	}

	void LateUpdate() {
//INTERACT TRIGGER / POSITIONING

		if (pickedObject != null && pickedObject.tag == "Interactive" && Container.interactOff == false && canInteract == true){
			if (pickedObject.activeInHierarchy == false) {
				pickedObject = null;
			}
			Vector3 targetDir = new Vector3 (pickedObject.GetComponent<InteractionContainer> ().turnPointPos.x, transform.position.y, pickedObject.GetComponent<InteractionContainer> ().turnPointPos.z) - transform.position;
			targetAngle = Vector3.Angle (transform.forward, targetDir);
			distanceToInteractable = Vector3.Distance (gameObject.transform.position, new Vector3 (ScreenPickPos.x, gameObject.transform.position.y, ScreenPickPos.z));
			if (distanceToInteractable < pickedObject.GetComponent<InteractionContainer> ().usableDistance) {
				canWalkInt = false;
				if (pickedObject.transform.root.gameObject.tag == "NPC") {
					agent.isStopped = true;
				}
				if (pickedObject.GetComponent<InteractionContainer> ().rotatePlayer == true) {
					if (targetAngle > rotationMinimum) {
						Debug.Log ("turning to interactable");
						transform.rotation = Quaternion.RotateTowards (transform.rotation, Quaternion.LookRotation ((new Vector3 (pickedObject.GetComponent<InteractionContainer> ().turnPointPos.x, gameObject.transform.position.y, pickedObject.GetComponent<InteractionContainer> ().turnPointPos.z)) - transform.position), Time.deltaTime * interactionTurnRate * 1.2f);
					} else {
						//continueTurn = true;
						interactInterrupt += pickedObject.GetComponent<InteractionContainer> ().interruptTime;
						Interact ();
					}
				} else {
					Interact ();
				}
			} else if (pickedObject.GetComponent<InteractionContainer> ().walkToOverride == true) {
				interactInterrupt += pickedObject.GetComponent<InteractionContainer> ().interruptTime;
				Interact ();
			} else if (interactMode == true) {
				Debug.Log ("interact mode");
			}

// STOP PLAYER TRYING TO MOVE IF DESTINATION REACHED
		} else if (gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ().enabled == true && gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ().remainingDistance < 0.1f) {
			if (agent.hasPath == true){
				Debug.Log ("player at goal");
				agent.ResetPath();
			} 
		}

//LOOK AT CURRENTINTERACTABLE
		if (Container.currentInteractable != null) {
			if (Container.chosenInteractable != null && Container.chosenInteractable.GetComponent<InteractionContainer> ().angleToPlayer < 150) {
				lookAt.solver.IKPosition = Vector3.Lerp (lookAt.solver.IKPosition, Container.chosenInteractable.GetComponent<InteractionContainer> ().targetPos, Time.deltaTime * 14);
			} 
		} else {
			lookAt.solver.IKPosition = Vector3.Lerp (lookAt.solver.IKPosition, lookTarget.transform.position, Time.deltaTime * 30);
		}

		cameraColliders.Clear ();
	}

//WALK OVERRIDE FOR SCRIPTED MOVEMENT
	public void walkOverride(){
		if (Vector3.Distance (gameObject.transform.position, ScreenPickPos) > moveMinimum) {
			agent.isStopped = false;
			agent.destination = ScreenPickPos;
		}
	}

//INTERACT PAUSE COROUTINE
	public IEnumerator waitTime(){
		canWalkInt = false;
		yield return new WaitForSeconds (pickedObject.GetComponent<InteractionContainer> ().interruptTime);
		canWalkInt = true;
		yield break;
	}

	public void crossFadeAnim(string animName, float fadeLength) {
		if (lastAnim != null) {
			if (lastAnim == animName) {
				//Debug.Log ("already playing animation");
				//do nothing
			} else {
				//Debug.Log ("playing animation because it isn't already playing or there is a goof-up");
				animator.CrossFade (animName, fadeLength);
			}
		}
		lastAnim = animName;
	}
}
