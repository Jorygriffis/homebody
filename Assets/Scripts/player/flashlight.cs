﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flashlight : MonoBehaviour {

	public GameObject flashlightObj;
	public bool flashlightOn = false;
	public float lightLevel = 1;
	private float lightMax;
	private bool init = false;

	public void Init () {
		init = true;
		flashlightObj = gameObject.GetComponent<playerController> ().humeObject.GetComponent<humeHelper> ().flashlightObj;
		lightMax = flashlightObj.GetComponent<Light> ().intensity;
	}

	void Update () {
		if (flashlightOn == true && init == true) {
			flashlightObj.SetActive (true);
			flashlightObj.GetComponent<Light> ().intensity = lightMax * lightLevel;
		} else {
			flashlightObj.SetActive (false);
		}
	}
}
