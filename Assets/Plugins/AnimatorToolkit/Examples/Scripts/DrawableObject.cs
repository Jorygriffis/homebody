﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BFS;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu()]
public class DrawableObject : ScriptableObject, IDrawable
{
	private float m_FloatValue;
	private string m_StringValue;
	private Vector3 m_Vector3Value;

	public float FloatValue
	{
		get { return m_FloatValue; }
	}

	public string StringValue
	{
		get { return m_StringValue; }
	}

	public Vector3 Vector3Value
	{
		get { return m_Vector3Value; }
	}

	#if UNITY_EDITOR
	public void Draw()
	{
		m_FloatValue = EditorGUILayout.FloatField("Float Value", m_FloatValue);
		m_StringValue = EditorGUILayout.TextField("String Value", m_StringValue);
		m_Vector3Value = EditorGUILayout.Vector3Field("Vector3 Value", m_Vector3Value);
	}
	#else
	public void Draw(){}
	#endif

}
