﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InheritanceExample : WindowExample
{
	private enum TestType
	{
		Type1 = 0,
		Type2
	}

	[AnimatorEvent]
	public override void TestObject(Transform i_Transform)
	{
		Debug.Log("InheritanceExample: " + i_Transform);
		base.TestObject(i_Transform);
	}

	[AnimatorEvent]
	public void TestMono(MonoBehaviour i_Behaviour)
	{
		Debug.Log("InheritanceExample: " + i_Behaviour);
	}

	[AnimatorEvent]
	private void TestVoid()
	{
		Debug.Log("Test Void");
	}

	[AnimatorEvent]
	public void TestInt(int i_Int)
	{
		Debug.Log(i_Int);
	}

	[AnimatorEvent]
	public void TestFloat(float i_Float)
	{
		Debug.Log(i_Float);
	}

	[AnimatorEvent]
	public void TestString(string i_String)
	{
		Debug.Log(i_String);
	}

	[AnimatorEvent]
	private void TestEnum(TestType i_Type)
	{
		Debug.Log(i_Type);
	}

	[AnimatorEvent]
	private void TestVector3(Vector3 i_Vector)
	{
		Debug.Log(i_Vector);
	}

	[AnimatorEvent]
	private void TestDrawable(DrawableObject i_Obj)
	{
		Debug.Log("Float: " + i_Obj.FloatValue + " | String: " + i_Obj.StringValue + " | Vector3: " + i_Obj.Vector3Value);
	}
}