﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowExample : MonoBehaviour
{
	[AnimatorEvent]
	private void ShowLog(string i_Log)
	{
		Debug.Log(i_Log);
	}

	[AnimatorEvent]
	private void ShowMessage(string i_Message)
	{
		Debug.Log(i_Message);
	}

	[AnimatorEvent]
	public virtual void TestObject(Transform i_Transform)
	{
		Debug.Log("WindowExample: " + i_Transform);
	}
}
