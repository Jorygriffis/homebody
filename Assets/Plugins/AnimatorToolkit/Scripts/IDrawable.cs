﻿namespace BFS
{
	public interface IDrawable
	{
		void Draw();
	}
}