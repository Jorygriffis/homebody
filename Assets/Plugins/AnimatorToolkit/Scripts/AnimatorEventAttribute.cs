﻿using System;

// This attribute is only valid on a method.
[AttributeUsage(AttributeTargets.Method)]
public class AnimatorEventAttribute : Attribute {}