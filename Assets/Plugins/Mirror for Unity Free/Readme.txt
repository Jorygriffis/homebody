Mirror for Unity Free
---------------------

The example scene uses the following CC-BY resources:

 - LP_001 model downloaded from http://www.blendswap.com/blends/view/74529
 - 73617__jus__lakeside-birds-2 sound downloaded from https://www.freesound.org/people/jus/sounds/73617/

Basic Instructions
------------------

1) Attach MirrorControl script to the GameObject that you want to be the mirror.

2) Select the local reflection axis (this should be the normal to the mirror's surface).

3) You can optionally select and/or create both the main or the reflection cameras. If not all the stuff will be done automatically: cameras will be assigned from the scene main camera and the reflection camera will be created. In addition script will add other required components and will manage camera settings.

4) You must create a mirrors surface. For doing so use the MirrorMask material in the shaders folder. You can use the MirrorMask in the prefabs folder.

   You can use the mirror prefab provided instead of doing steps 1-4.

5) Finally if you want to use skyboxes you must use the skybox provided in the prefabs folder, because currently reflection is not compatible with standard unity skyboxes depth rendering.

This toolkit is still in beta, more updates will be released soon!