using UnityEngine;
using System.Collections;

public class IncreaseTexturePosition: MonoBehaviour {
	public Vector2 deltaPosition = Vector3.zero;

	void FixedUpdate() {
		if (gameObject.GetComponent<Renderer>() != null) {
			if (gameObject.GetComponent<Renderer>().material != null) {
				gameObject.GetComponent<Renderer>().material.mainTextureOffset += deltaPosition;
			}
		}
	}
}
