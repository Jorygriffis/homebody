using UnityEngine;
using System.Collections;

public class OscillatePositionOrRotation: MonoBehaviour {
	public bool useLocalCoordinates = false;
	protected Vector3 originalPosition = Vector3.zero;
	protected Vector3 originalRotation = Vector3.zero;
	protected Vector3 originalLocalPosition = Vector3.zero;
	protected Vector3 originalLocalRotation = Vector3.zero;
	public Vector3 amplitudePosition = Vector3.zero;
	public Vector3 frequencyPosition = Vector3.one;
	public Vector3 amplitudeRotation = Vector3.zero;
	public Vector3 frequencyRotation = Vector3.one;
	public float velocityMultiplier = 1f;
	protected float time = 0f;

	void Start () {
		originalPosition = gameObject.transform.position;
		originalRotation = gameObject.transform.eulerAngles;
		originalLocalPosition = gameObject.transform.localPosition;
		originalLocalRotation = gameObject.transform.localEulerAngles;
	}

	void FixedUpdate() {
		//time += Time.deltaTime * velocityMultiplier;
		time += Time.fixedDeltaTime * velocityMultiplier;
		
		if (useLocalCoordinates) {
			/*
			Vector3 localPosition = new Vector3(
				amplitudePosition.x * Mathf.Sin(time * frequencyPosition.x),
				amplitudePosition.y * Mathf.Sin(time * frequencyPosition.y),
				amplitudePosition.z * Mathf.Sin(time * frequencyPosition.z)
			);
			gameObject.transform.position = originalPosition;
			gameObject.transform.position = gameObject.transform.TransformPoint(localPosition);
			*/
			gameObject.transform.localPosition = originalLocalPosition + new Vector3(
				amplitudePosition.x * Mathf.Sin(time * frequencyPosition.x),
				amplitudePosition.y * Mathf.Sin(time * frequencyPosition.y),
				amplitudePosition.z * Mathf.Sin(time * frequencyPosition.z)
			);
	
			gameObject.transform.localEulerAngles = originalLocalRotation + new Vector3(
				amplitudeRotation.x * Mathf.Sin(time * frequencyRotation.x),
				amplitudeRotation.y * Mathf.Sin(time * frequencyRotation.y),
				amplitudeRotation.z * Mathf.Sin(time * frequencyRotation.z)
			);
		} else {
			gameObject.transform.position = originalPosition + new Vector3(
				amplitudePosition.x * Mathf.Sin(time * frequencyPosition.x),
				amplitudePosition.y * Mathf.Sin(time * frequencyPosition.y),
				amplitudePosition.z * Mathf.Sin(time * frequencyPosition.z)
			);
	
			gameObject.transform.eulerAngles = originalRotation + new Vector3(
				amplitudeRotation.x * Mathf.Sin(time * frequencyRotation.x),
				amplitudeRotation.y * Mathf.Sin(time * frequencyRotation.y),
				amplitudeRotation.z * Mathf.Sin(time * frequencyRotation.z)
			);
		}
	}
}
