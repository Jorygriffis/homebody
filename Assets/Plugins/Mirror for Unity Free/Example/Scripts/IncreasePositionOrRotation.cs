using UnityEngine;
using System.Collections;

public class IncreasePositionOrRotation: MonoBehaviour {
	public Vector3 deltaPosition = Vector3.zero;
	public Vector3 deltaRotation = Vector3.zero;

	void FixedUpdate() {
		gameObject.transform.localPosition += deltaPosition;
		gameObject.transform.localEulerAngles += deltaRotation;
	}
}
