using UnityEngine;
using System.Collections;

public class MirrorControl: MonoBehaviour {
	public enum ReflectionAxis {
		back,
		forward,
		down,
		up,
		left,
		right
	}
	public enum Clipping {
		none,
		nearClipPlane
	}

	public ReflectionAxis mirrorLocalReflectionAxis = ReflectionAxis.back;
	public GameObject mirrorObject = null;
	public Camera mainCamera = null;
	protected bool mainCameraWasAssignedDinamically = false;
	public Camera reflectionCamera = null;

	//public LayerMask mainCameraCullingMask = LayerMask.NameToLayer("Everything");
	public LayerMask mainCameraCullingMask = ~0;
	//public LayerMask reflectionCameraCullingMask = LayerMask.NameToLayer("Everything");
	public LayerMask reflectionCameraCullingMask = ~0;
	
	public Clipping mirrorClipping = Clipping.nearClipPlane;
	public float mirrorClippingOffset = 0f;
	public float mirrorClippingMultiplier = 1f;

	public Vector3 getMirrorAxis(Transform mirrorTransform, ReflectionAxis whichAxis) {
		switch (whichAxis) {
		default:
		case ReflectionAxis.back:
			return -mirrorTransform.forward;
		case ReflectionAxis.forward:
			return mirrorTransform.forward;
		case ReflectionAxis.down:
			return -mirrorTransform.up;
		case ReflectionAxis.up:
			return mirrorTransform.up;
		case ReflectionAxis.left:
			return -mirrorTransform.right;
		case ReflectionAxis.right:
			return mirrorTransform.right;
		}
	}

	public Vector3 getProyectedVector(Vector3 vector, Vector3 axis) {
		return Vector3.Dot(vector, axis.normalized) * axis.normalized;
	}

	public Vector3 getMirrorDirection(Vector3 vector, Vector3 planeNormal) {
		return vector - 2 * getProyectedVector(vector, planeNormal);
	}
	
	public Vector3 getMirrorPosition(Vector3 vector, Vector3 planePosition, Vector3 planeNormal) {
		return getMirrorDirection(vector - planePosition, planeNormal) + planePosition;
	}
	
	void Update() {
		if (mainCameraWasAssignedDinamically) {
			mainCamera = Camera.main;
		} else {
			if (mainCamera == null) {
				mainCamera = Camera.main;
				if (mainCamera != null) mainCameraWasAssignedDinamically = true;
			}
		}

		if (reflectionCamera == null) {
			GameObject newGameObject = new GameObject();
			if (newGameObject != null) {
				newGameObject.transform.parent = gameObject.transform;
				newGameObject.name = "Automatically created Reflection Camera";
				newGameObject.AddComponent<Camera>();
				Camera camera = (Camera)newGameObject.GetComponent("Camera");
				if (camera != null) {
					if (mainCamera != null) {
						camera.backgroundColor = mainCamera.backgroundColor;
						camera.clearFlags = mainCamera.clearFlags;
						camera.depth = mainCamera.depth - 0.1f;
						camera.nearClipPlane = mainCamera.nearClipPlane;
						camera.farClipPlane = mainCamera.farClipPlane;
						camera.fieldOfView = mainCamera.fieldOfView;
						camera.orthographicSize = mainCamera.orthographicSize;
						camera.orthographic = mainCamera.orthographic;
						camera.transparencySortMode = mainCamera.transparencySortMode;
						camera.useOcclusionCulling = mainCamera.useOcclusionCulling;
					}
					reflectionCamera = camera;
				}
			}
		}
		
		if (mirrorObject == null) {
			mirrorObject = gameObject;
		}
		
		if ((mainCamera != null) && (reflectionCamera != null) && (mirrorObject != null)) {
			if (mainCamera.gameObject.GetComponent("MainCameraRenderControl") == null) {
				mainCamera.gameObject.AddComponent<MainCameraRenderControl>();
			}
			if (reflectionCamera.gameObject.GetComponent("ReflectionCameraRenderControl") == null) {
				reflectionCamera.gameObject.AddComponent<ReflectionCameraRenderControl>();
			}
			
			mainCamera.cullingMask = mainCameraCullingMask;
			reflectionCamera.cullingMask = reflectionCameraCullingMask;
			mainCamera.clearFlags = CameraClearFlags.Depth;
			
			Vector3 reflectionAxis = getMirrorAxis(mirrorObject.transform, mirrorLocalReflectionAxis);
			reflectionCamera.transform.position = getMirrorPosition(mainCamera.transform.position, mirrorObject.transform.position, reflectionAxis);
			Vector3 forward = getMirrorDirection(mainCamera.transform.forward, reflectionAxis).normalized;
			Vector3 up = getMirrorDirection(mainCamera.transform.up, reflectionAxis).normalized;
			reflectionCamera.transform.rotation = Quaternion.LookRotation(forward, up);
			
			float nearClipPlane = mainCamera.nearClipPlane;
			switch (mirrorClipping) {
			case Clipping.nearClipPlane:
				Vector3 nearClipPlaneVector = getProyectedVector(reflectionCamera.transform.position - mirrorObject.transform.position, reflectionAxis);
				nearClipPlane = nearClipPlaneVector.magnitude * mirrorClippingMultiplier + mirrorClippingOffset;
				break;
			}
			reflectionCamera.projectionMatrix = Matrix4x4.Perspective(mainCamera.fieldOfView, mainCamera.aspect, nearClipPlane, mainCamera.farClipPlane) * Matrix4x4.Scale(new Vector3(-1, 1, 1));
		}
	}
}
