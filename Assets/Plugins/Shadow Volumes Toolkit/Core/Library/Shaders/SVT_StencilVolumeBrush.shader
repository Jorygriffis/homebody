// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shadow Volumes Toolkit
// Copyright 2013 Gustav Olsson
Shader "Shadow Volumes/Stencil Volume Brush"
{
	Properties
	{
		_alphaTex ("Alpha Texture", 2D) = "white" {}
		_alphaVal ("Alpha Value", float) = 0.5
	}
	SubShader
	{
		Tags
		{
			"Queue" = "AlphaTest+511"
			"IgnoreProjector" = "True"
		}
		
		// Linearly interpolate the scene color towoards the shadow color
		Pass
		{
			Stencil
			{
				Ref 1
				Comp Greater
			}
			
			Lighting Off
			Cull Back
			ZTest LEqual
			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha, Zero One
			
			Fog
			{
				Mode Off
			}
			
			CGPROGRAM
			#pragma vertex ScreenVertex
//			#pragma vertex vertboy
			#pragma fragment ScreenColorFragment
			float4 _shadowVolumeColor;
			float _alphaVal;
			sampler2D _alphaTex;

			struct VertexInput
			{
				float4 vertex : SV_POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct VertexOutput
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			VertexOutput ScreenVertex(VertexInput input)
			{
				VertexOutput output;
				
				output.vertex = UnityObjectToClipPos(input.vertex);

				output.uv = input.uv;
				
				return output;
			}

//			float4 vertboy (float4 vertex : POSITION) : SV_POSITION
//			{
//				return mul(UNITY_MATRIX_MVP, vertex);
//			}

			float4 ScreenColorFragment(VertexOutput IN) : COLOR
			{	
				fixed4 alphaBoy = tex2D(_alphaTex, IN.uv);
				fixed4 col = _shadowVolumeColor * _alphaVal * alphaBoy.a;
				return col;
			}
			ENDCG
		}
	}
}