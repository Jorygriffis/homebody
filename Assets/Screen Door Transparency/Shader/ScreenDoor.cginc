// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

#ifndef SCREENDOOR_INCLUDED
#define SCREENDOOR_INCLUDED

float4 CalcScreenPos (float4 v, float sz)
{
	float4 p = UnityObjectToClipPos(v);
	p = ComputeScreenPos(p);
	p = float4(p.xy * _ScreenParams.xy / sz, 0, p.w);
	return p;
}
float Dither4x4 (float2 p)
{
	static const float Matrix[16] =
	{
		 1.0 / 17.0,  9.0 / 17.0,  3.0 / 17.0, 11.0 / 17.0,
		13.0 / 17.0,  5.0 / 17.0, 15.0 / 17.0,  7.0 / 17.0,
		 4.0 / 17.0, 12.0 / 17.0,  2.0 / 17.0, 10.0 / 17.0,
		16.0 / 17.0,  8.0 / 17.0, 14.0 / 17.0,  6.0 / 17.0
	};
	int2 ip = int2(p);
	int index = ((ip.x % 4) * 4) + ip.y % 4;
#ifdef GPU_SUPPORT_ARRAY_INDEX
	return Matrix[index];
#else
	float r = 0;
	if (index == 0)  r = Matrix[0];
	if (index == 1)  r = Matrix[1];
	if (index == 2)  r = Matrix[2];
	if (index == 3)  r = Matrix[3];
	if (index == 4)  r = Matrix[4];
	if (index == 5)  r = Matrix[5];
	if (index == 6)  r = Matrix[6];
	if (index == 7)  r = Matrix[7];
	if (index == 8)  r = Matrix[8];
	if (index == 9)  r = Matrix[9];
	if (index == 10) r = Matrix[10];
	if (index == 11) r = Matrix[11];
	if (index == 12) r = Matrix[12];
	if (index == 13) r = Matrix[13];
	if (index == 14) r = Matrix[14];
	if (index == 15) r = Matrix[15];
	return r;
#endif
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
sampler2D _MainTex, _BumpTex;
float _Cutoff, _PatternSize, _NearFadeDistance, _FarFadeStart, _FarFadeEnd;
half _Glossiness, _Metallic;

struct Input
{
	float2 uv_MainTex;
	float3 worldPos;
	float4 scrpos;
	float fr;
};
void vert (inout appdata_full v, out Input o)
{
	UNITY_INITIALIZE_OUTPUT(Input, o);
	o.scrpos = CalcScreenPos (v.vertex, _PatternSize);

#ifdef SD_DISTANCE_FADING
	float4 vp = mul(UNITY_MATRIX_MV, v.vertex);
	float dist = length(vp.xyz);
	if (dist < _NearFadeDistance)
		o.fr = 1.0 - smoothstep(_ProjectionParams.y, _NearFadeDistance, dist);
	else if (dist > _FarFadeStart)
		o.fr = smoothstep(_FarFadeStart, _FarFadeEnd, dist);
	else
		o.fr = 0.0;
#else
	o.fr = 1.0;
#endif
}
// SURFACE_OUTOUT is a macro, define in *.shader source.
void screenDoor_finalcolor (Input IN, SURFACE_OUTOUT o, inout fixed4 color)
{
	float2 v = IN.scrpos.xy / IN.scrpos.w;
	float f = Dither4x4(v);
	clip(f - IN.fr * _Cutoff);
}

#endif