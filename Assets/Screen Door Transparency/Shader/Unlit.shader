Shader "Screen Door Transparency/Unlit" {
	Properties {
		_MainTex ("Main", 2D) = "white" {}
		_PatternSize ("Pattern Size", Float) = 2
		_Cutoff ("Alpha Cutoff", Float) = 0.5
		_NearFadeDistance ("Near Fade Distance", Float) = 0.5
	}
	SubShader {
		Tags { "RenderType" = "Opaque" }
		Cull Off
		
		CGPROGRAM
		#pragma surface surf Unlit vertex:vert finalcolor:screenDoor_finalcolor
		#pragma multi_compile _ SD_DISTANCE_FADING
		#define SURFACE_OUTOUT SurfaceOutput
		#include "ScreenDoor.cginc"		
		void surf (Input IN, inout SurfaceOutput o)
		{
			fixed4 tc = tex2D(_MainTex, IN.uv_MainTex);
			o.Albedo = tc.rgb;
			o.Alpha = tc.a;
		}
		inline fixed4 LightingUnlit (SurfaceOutput s, fixed3 lightDir, fixed atten)
		{
			fixed4 c = fixed4(1, 1, 1, 1);
			c.rgb = s.Albedo;
			c.a = s.Alpha;
			return c;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
