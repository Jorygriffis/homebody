Shader "Screen Door Transparency/Diffuse" {
	Properties {
		_MainTex ("Main", 2D) = "white" {}
		_PatternSize ("Pattern Size", Float) = 2
		_Cutoff ("Alpha Cutoff", Float) = 0.5
		_NearFadeDistance ("Near Fade Distance", Float) = 0.5
	}
	SubShader {
		Tags { "RenderType" = "Opaque" }

		CGPROGRAM
		#pragma surface surf Lambert vertex:vert finalcolor:screenDoor_finalcolor
		#pragma multi_compile _ SD_DISTANCE_FADING
		#define SURFACE_OUTOUT SurfaceOutput
		#include "ScreenDoor.cginc"
		void surf (Input IN, inout SurfaceOutput o)
		{
			fixed4 tc = tex2D(_MainTex, IN.uv_MainTex);
			o.Albedo = tc.rgb;
			o.Alpha = tc.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
