Shader "Screen Door Transparency/Standard" {
	Properties {
		_MainTex ("Main", 2D) = "white" {}
		_BumpTex ("Bump", 2D) = "bump" {}
		_Glossiness ("Smoothness", Range(0, 1)) = 0.5
		_Metallic ("Metallic", Range(0, 1)) = 0
		_PatternSize ("Pattern Size", Float) = 2
		_Cutoff ("Alpha Cutoff", Float) = 0.5
		_NearFadeDistance ("Near Fade Distance", Float) = 0.5
	}
	SubShader {
		Tags { "RenderType" = "Opaque" }

		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard vertex:vert finalcolor:screenDoor_finalcolor
		#pragma multi_compile _ SD_DISTANCE_FADING
		#define SURFACE_OUTOUT SurfaceOutputStandard
		#include "ScreenDoor.cginc"
		void surf (Input IN, inout SurfaceOutputStandard o)
		{
			fixed4 tc = tex2D(_MainTex, IN.uv_MainTex);
			o.Normal = UnpackNormal(tex2D(_BumpTex, IN.uv_MainTex));
			o.Albedo = tc.rgb;
			o.Alpha = tc.a;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
