Thank you for downloading !
Screen Door Transparency bring classic stipple patterns technique to unity3d world !

Screen Door Transparency is a classic technique in realtime rendering.
It uses alpha testing to fake transparency. Fast and cheap:
  => No alpha blending.
  => Pixels are written to the depth buffer, NO sorting required !
  => Can easily be integrated with any existing materials.

With this technique, you can:
  => Fade out objects near the max draw distance.
  => Fade out objects at camera near clip plane.
  => Blend between different levels of detail objects.
  => Simulate transparency without alpha blending (Not Need Sorting Anymore).

Three shaders included.
  => "Screen Door Transparency/Unlit" screen door transparency with unlit texture.
  => "Screen Door Transparency/Diffuse" screen door transparency with lighted texture.
  => "Screen Door Transparency/Bumped Diffuse" screen door transparency with bump mapping.
  => "Screen Door Transparency/Standard" screen door transparency with unity built-in standard shading.
It's very easy to integrate screen door transparency to your own shaders.
Please refer these shaders in package.

Note: 
  we find some GPU do NOT support dynamic array access.
  so we use macro "GPU_SUPPORT_ARRAY_INDEX" in "ScreenDoor.cginc" to separate shader use / not-use dynamic array access.
  you can define it on GPU support dynamic array access, get best performance.
  
"Demo.unity" demonstrate how to:
  => Fade out objects near the max draw distance.
  => Fade out objects at camera near clip plane.
  => Simulate transparency.

We also open for customer's requests. If you need new features, please let us know.

If you like it, please give us a good review on asset store. We will keep moving !
Any suggestion or improvement you want, please contact qq_d_y@163.com.
Hope we can help more and more unity3d developers.