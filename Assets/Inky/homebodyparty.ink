CONST PARTYMAIN = 3333

=== Flashback_Party ===
//these are flashbacks we'll return to repeatedly throughout the game. they're used as filler, but are useful to the player for exploring social dynamics and earning extra points with other kids, and are useful narratively by showing causality and connecting dots between the surreal and mundane elements of the story.
{MostPointsCheck()}
{secondMostPointsCheck()}
{
    - Flashback_Party == 1:
    You're on the street outside your group friend Derek's apartment, obligingly attending a party that you're not thrilled about. You make your way up to the third floor, dodging bugs attracted to the shielded lights on the exposed balcony.
    
    There's a delay after you knock on the door. You wait for a while, managing a rising urge to call it a night, when the door finally opens. It's Derek.
    
    "Hhey, how's it--hi! How are you. Who are you."
    
    + Hi{PlayerName !="Cliff":!}{PlayerName =="Cliff":.} I'm {PlayerName}, {BFF=="Crobus":{MostPoints}}{BFF!="Crobus":{BFF}}'s friend.
        "Oh, hey! Good to have--to meet you. {BFF=="Crobus":{MostPoints}}{BFF!="Crobus":{BFF}}'s here waiting for you. Come on in." ->Flashback_Party_Interval
    + Hi{PlayerName !="Cliff":!}{PlayerName =="Cliff":.} I'm {PlayerName}, {SecondMostPoints}'s friend.
        "Oh, hey! {getPronounCaps(SecondMostPoints, "subject")}'s here already, chilling in the back. Come on in." ->Flashback_Party_Interval
    - Flashback_Party == 2:
    It takes some work, but you manage to settle into a good social groove at Derek's party. 
    - else:
    The party at Derek's house continues. 
}

=== Flashback_Party_Interval ===
Derek moves to the side of the door, gesturing for you to come inside. You squeeze past him and he shuts the door behind you. The dimly lit room in front of you is populated by pockets of friend groups, standing close and yelling to be heard over French electronica. You manage to spot a number of your friends--
->Flashback_Party_Interval2

=== Flashback_Party_Interval2 ===
~Flashback = true
~FlashbackNumber = 420
{flashbackLocation(PARTYMAIN)}
{
    - BFF != "Crobus": 
        <>{BFF} seems especially glad that you're here. 
    - MostPoints != "Crobus":
        <>{MostPoints} seems especially glad that you're here. 
}
{
    - Hater != "Crobus": 
        <> {Hater}, on the other hand, always seems to be on the other side of the room--is {getPronoun(Hater, "subject")} avoiding you?
}
->Flashback_Party_hub

=== Flashback_Party_hub ===
<-CharactersPresentTest

+ [Look around.]
    You're in a crowded living room during a party at your group friend Derek's apartment. Things are moving but have started to settle down into discrete conversational groups.
    
    There's a digital clock on the wall. It's flashing 12:00.
    ->Flashback_Party_hub

=== Flashback_Party_End ===
->LoopHub