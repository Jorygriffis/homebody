==loopHub==
~totalLoopCount = totalLoopCount + 1

{resetStatus()}

LOADSCENE house
->DONE



==houseMainHub==
//automatic house scripting
->DONE



==flashback_Party==
//repeating party scene, changes based on bff, hater status

{
    - flashback_Party == 1:
    You're on the street outside your group friend Derek's apartment, obligingly attending a party that you're not thrilled about. You make your way up to the third floor, dodging bugs attracted to the shielded lights on the exposed balcony.
    
    There's a delay after you knock on the door. You wait for a while, managing a rising urge to call it a night, when the door finally opens. It's Derek.
    
    "Hhey, how's it--hi! How are you. Who are you."
    
    + Hi{playerName !="Cliff":!}{playerName =="Cliff":.} I'm {playerName}, {BFF=="null":{MostPoints}}{BFF!="null":{BFF}}'s friend.
        "Oh, hey! Good to have--to meet you. {BFF=="null":{MostPoints}}{BFF!="null":{BFF}} and I went to school together." ->flashback_Party_Interval
    + Hi{playerName !="Cliff":!}{playerName =="Cliff":.} I'm {playerName}, {MissingKid}'s friend.
        "Oh, hey! Good to have--to meet you. {BFF=="null":{MostPoints}}{BFF!="null":{BFF}} and I went to school together." ->flashback_Party_Interval
    + Hi{playerName !="Cliff":!}{playerName =="Cliff":.} I'm {playerName}.
        "Hey, good to meet--good to see you, {playerName}!" ->flashback_Party_Interval
    - flashback_Party == 2:
    It takes some work, but you manage to settle into a good social groove at Derek's party. 
    - else:
    The party at Derek's house continues. 
}

==flashback_Party_Interval==
Derek moves to the side of the door, gesturing for you to come inside. You squeeze past him and he shuts the door behind you. The dimly lit room in front of you is populated by pockets of friend groups, standing close and yelling to be heard over French electronica. You manage to spot a number of your friends--
->flashback_Party_Interval2

==flashback_Party_Interval2==
{
    - BFF != "null": 
        <>{BFF} seems especially glad that you're here. 
    - MostPoints != "null":
        <>{MostPoints} seems especially glad that you're here. 
}
{
    - Hater != "null": 
        <> {Hater}, on the other hand, always seems to be on the other side of the room--is {getPronoun(Hater, "subject")} avoiding you?
}
->flashback_Party_hub

==flashback_Party_hub==
+ [Look around.]
    You're in a crowded living room during a party at your group friend Derek's apartment. Things are moving but have started to settle down into discrete conversational groups.
    
    There's a digital clock on the wall. It's flashing 12:00.
    ->flashback_Party_hub

==flashback_Party_End==
->loopHub



==flashback_Party_BFF_Talk==
//once BFF is chosen
//player kid and bff go on a stroll and talk about life. the player wants to talk about the murder loop but can't.
~Speaker = BFF
Derek's party continues apace. The sun has fully set now; the crisp night air coming in through the window at war with the stifling humidity of the body-filled apartment.
Just as you're thinking about how badly you need to get outside, {BFF} approaches.
{
    - BFF == "Cliff": "Hey. It's getting late and I need to grab some smokes before the Shell closes. Want to come with?"
    - BFF == "Emily": "Oh my God. {playerName}, I am supremely exhausted of Derek's shitty fucking friends."
    Are you ok?
    "Oh. Oh yeah, everything's okay. They just suck. You wanna go outside for a while?"
    - BFF == "Francine": "Hey there, {playerName}! Having a good time?" she asks, and before you can say more than a syllable of an answer, continues, "Oh oh yeah, me too. I'm holding up very well to the social pressures of maintaining humorous conversation for an entire evening. That said, I think I might go for a walk. Would you like to come?"
    - BFF == "Laura": "Hi, {playerName}. I was wondering if you wanted to go out for some fresh air--honestly, I don't do well at parties. The mix of social anxieties and the most toxic possible food and drink is not kind to my system. What do you say?"
    - BFF == "Megan": "Hey, {playerName}! Listen, I'm having a pretty nice time but I'm pretty overstimulated. I'm going to end up staring at a wall all night if I don't go and get some air. Want to come?"
    - else: "Hi, {playerName}! Things here are a little slow right now. I want to be here for the second wind, but I was wondering if you'd want to go for a walk for a few."
}
Before long, you're out on the sidewalk with {BFF}. The light pollution renders the night sky a deep mauve and the waning moon sits low in the sky. You drift from streetlight to streetlight.
{
    - BFF == "Cliff": ->Flashback_BFF_Talk_Cliff1
    - else: ->Flashback_BFF_Talk_Generic1
}

=== Flashback_BFF_Talk_Cliff1 ===
"I'm not the biggest fan of parties. That might already be obvious." Cliff silently investigates whether or not to keep speaking.
->Flashback_BFF_Talk_Generic1

=== Flashback_BFF_Talk_Generic1 ===
"{Speaker!="Cliff":No matter how many conversations I have about how it's normal not to {Speaker=="Francine":enjoy}{Speaker!="Francine":be into} parties, I still feel so frustrated by them sometimes." {BFF} picks up {getPronoun(BFF, "possessive")} pace, {getPronoun(BFF, "possessive")} shoes scraping against the uneven sidewalk. "}{Speaker == "Emily":Like, }{Speaker == "Laura":Like, }{Speaker == "Gary":Like, }{Speaker == "Pete":Like, }I'm supposed to feel bad that I don't enjoy yelling at strangers in a hot, dark room? {Swear(BFF)}{SwearLevel==1:God.}{SwearLevel==2:Piss.} {Speaker=="Francine":It's so agitating.}{Speaker!="Francine":I hate it.}"
+ "There's a social element to it, too[."], though. It's important to people, they want you there."
    "Yeah. And it's not like that doesn't mean anything to me? But I wish I could just tick that box without spending an entire night on it."
+ "You have to try to make the most of it."
    "I have been. And I do. But that doesn't stop me from feeling like I'm the only person frustrated by this{SwearLevel==0:.}{SwearLevel==1:<> stuff.}{SwearLevel==2:<> shit.} And being afraid other people are judging me for it."
+ "I think just showing up is usually enough, though."
    "I know that's true," {BFF} says, "but {SwearLevel==0:God,}{SwearLevel==1:man,}{SwearLevel==2:fuck,} I'm afraid that it isn't. I'm afraid that whatever I do won't be enough. Like 'just dropping by' kind of tips the scale, you know?"
- "{BFF!="Francine":I guess }I don't know what I'm talking about. I'm just always afraid that I'm not doing enough somehow. I guess this is what being thirty is, huh? {Speaker!="Cliff":Coming to understand the amount of work you're supposed to be doing exactly when you start getting too tired to do it.}{Speaker=="Cliff":Being old enough to stop caring but too young to give up.}"
"I guess this {SwearLevel<2:stuff}{SwearLevel==2:shit} is all socially constructed anyway, though," {BFF} says, trying to course-correct. "And it's not like I think we're the smartest people in the world for thinking of it."
The night air is completely still as {BFF} and you make your way down the street. Outside your cone of vision, a moon-faced figure moves from buiding to building, just out of sight, watching you.
"The thing I'm most afraid of, though," {getPronoun(BFF, "subject")} continues, "is that these things are true, that {Speaker !="Cliff":love is transactional and }life is a zero-sum game."
+ "Like everyone is just taking advantage of you.["] Like all your friends are swimming in your money pile."
    "It feels {SwearLevel==0:bad}{SwearLevel>0:shitty}, right? I just want to trust people."
+ "I'm so afraid of accidentally betraying people."
    "Right?" {BFF} laughs. "Like those kinds of things just happen by accident."
+ ["It's good to have safe spaces from that feeling."]"Irrational as it is, it's good to know people who don't make you feel that way."
- "I'm glad I have you in my life. I don't feel like I'm gonna drop the ball and somehow ruin things with you," {getPronoun(BFF, "subject")} says, pausing before continuing, "I trust you."
+ ["I'm tired of watching you die."]
+ ["We need to figure out how to kill that thing."]
+ ["I need to get all of us out of this house."]
- "{
    -playerName=="Cliff":You too,
    -playerName=="Francine":And I trust you, too,
    -playerName=="Emily":Aww. I trust you, too,
    -else:I trust you too,
    }
    <>" you say.
->loopHub



==flashback_1==
//the kids meet at francine's to plan the trip. the missing kid acts weird.

The sunset brightly dithers through the accordian blinds of {playerName=="Francine":your}{playerName!="Francine":Francine's} living room windows. You and all of your tightest friend group are all here, arranged on couches and chairs and floor-pillows, to discuss an upcoming trip out into the desert, coinciding with the Perseid meteor shower.
You're going to be renting a house--
{
    -playerName=="Cliff":
    <>unfortunately, no one but you is really comfortable roughing it.
    -playerName=="Emily":
    <>thank fucking God, because you don't want to have to deal with spiders or scorpions or ticks or whatever.
    -playerName=="Megan":
    <>thankfully, as you're planning on bringing whatever consoles you can conveniently carry. You have no misgivings about combining nature time and game time.
    -else:
    <>thankfully, as you have no illusions regarding your place in nature.
}
The idle conversation in the room has slowed to a low rumble.
->Flashback_1_hub

=== Flashback_1_hub ===
//<-CharactersPresentTest

+ [Look around.]
{
    - playerName == "Francine": You're in the living area of your apartment. All your friends are here. You're looking forward to cleaning up.
    - else: You're in the living area of Francine's apartment. It's clean. All your friends are here.
}
    ->Flashback_1_hub

=== Flashback_1_Dialog ===
{
    - Speaker == "Megan":
    ->Flashback1_Megan
    - Speaker == "Emily":
    ->Flashback1_Emily
    - else:
    ->Flashback1_Generic
}
=== Flashback1_Megan ===
"{playerName}! When we get to the house we're gonna have some serious downtime before the hike the next day. I want to bring along some shitty old video games, but Pete thinks that's a dumb thing to do," Megan says.
"That is not what I said!" Pete interjects, "I said all of us bringing desktop computers so 'everyone can LAN it up like it's 2003' was a waste of a weekend in a cool old mystery house."
"OK, I cherry picked my example. Still, I mean, the place will have a TV, right?"
->Flashback1_MissingKid

=== Flashback1_Emily ===
"I'm looking forward to spacing out in the desert, aren't you, {playerName}?" Emily asks.
+ "Sure!"
+ "Definitely."
+ "Wait, how do you mean "spacing out"?["]
    <> You're talking pharmaceutically, aren't you?"
    "You know it! What could go wrong with mushrooms in the hot sun?" 
- She kicks back on the couch. "Incidentally... this place does have AC, right?"
->Flashback1_MissingKid

=== Flashback1_Generic ===
"Hey, {playerName}!" {Speaker} says, looking up at you from a throw pillow on the floor, "Are you looking forward to the trip? {Speaker=="Laura":Or at least to some isolation?}"
+ "Sure I am."
+ "Nervous but excited, yes."
+ "I'm apprehensive about it."
    {Swear(Speaker)}"{SwearLevel==2:Shit, }I'm sorry! But listen, we'll make the best of it."
- "Especially since the place is such a {SwearLevel==0:neat}{SwearLevel>0:rad} old building. Does this place even have electricity?"
->Flashback1_MissingKid

=== Flashback1_MissingKid ===
+"I'm sure it does.["]
    <> We're not spending this kind of money on a hovel. Only the finest airbnb for us. Probably smart to confirm, though." "You can probably ask...
+"I don't think I want to stay there if it doesn't.["]
    <> Maybe we should double-check with...
+"Dang, I'm not sure...["]
-<> Wait, who is actually renting the house, again?"
"It's {MissingKid}," {Speaker} says, casting a glance in {MissingKid}'s direction. {getPronounCaps(MissingKid, "subject")} snaps into engagement with {Speaker}.
"That's right!" {MissingKid} says, excitedly, and with promotional vigor, "The place looks amazing. It's a craftsman from the early 1900s. Was moved there from Pasadena in the 30s. Supposed to be one of the most isolated properties on airbnb in this state."
{MissingKid} speaks with an odd steadiness and total confidence in {getPronoun(MissingKid, "possessive")} subject matter. "The house is too remote for internet access and too far out for a cell signal. We'll be complete outside any kind of potential contact." {getPronounCaps(MissingKid, "subject")} slowly turns to look in your direction.
As {getPronoun(MissingKid, "subject")} does, you watch as {getPronoun(MissingKid, "possessive")} usually {MissingKid=="Pete":jovial}{MissingKid=="Laura":sweet}{MissingKid=="Cliff":soulful}{MissingKid=="Emily":lively}{MissingKid=="Francine":thoughtful}{MissingKid=="Megan":kind}{MissingKid=="Gary":loving} eyes gradually turn glassy and listless as they turn to face you.
"You're going to love it, {playerName}." {MissingKid} speaks seemingly directly to you. No one else hears.

It is dark.
->loopHub



==flashback_HaterTalk==
//once hater is chosen and creature is defeated
->loopHub



==flashback_a==
//fallback dream, no requirements
VAR flashbackaDistance = 10
VAR flashbackaTurns = 0
VAR aComplete = false

~aComplete = true
Billowing smoke rises in a tilted column into the moonlit blue desert sky. Below, the remains of a crashed plane burn a brilliant orange, the only light source around for miles. You are the sole survivor, though you're losing strength fast. You've managed to drag yourself a few hundred feet from the fire.
->flashback_a_hub

==flashback_a_hub==
+[Keep going.]
    ~flashbackaTurns = flashbackaTurns +1
    ~flashbackaDistance = flashbackaDistance +1
    {
        - flashbackaTurns < 6: 
            {flashbackaTurns < 3: You continue to drag yourself away from the flames.}
            {flashbackaTurns > 2: You continue to drag yourself away from the figure.}
    }
+[Wait.]
    ~flashbackaTurns = flashbackaTurns + 1
    {flashbackaTurns < 6: You stop struggling for a while.}

- ~flashbackaDistance = flashbackaDistance - 2

{flashbackaTurns == 2: A figure appears over the crest of a sand dune, barely visible.}
{
    - flashbackaTurns == 3: 
    {
        - flashbackaDistance > 6: The figure seems to notice your movement as you scramble away. You can't discern the details. 
        - flashbackaDistance < 7: The figure walks quickly towards you. You still can't discern the details.
    }
}
{
    - flashbackaTurns == 4: 
    {
        - flashbackaDistance < 6: The figure begins to pull a backpack off its shoulders as it approaches you.
        - flashbackaDistance > 5: The figure hastens its pace, holding the straps of its backpack as it runs.
    }
}
{
    - flashbackaTurns == 5: 
    {
        - flashbackaDistance < 5: The figure's halting gait shows signs of exhaustion. Whoever they are, they seem relieved to have reached you. Even at this close distance, you can't make out its facial features.
        - flashbackaDistance > 4: You exhaust yourself scrambling against the soft desert sand. The figure closes in on you.
    }
}
{flashbackaTurns == 6: As the figure moves close enough to loom over you, it stretches and distorts into a mockery of a human body. Its now-enormous, wet eyes glisten in the moonlight as they extrude outward towards you. Its shoulders dislocate and its impossibly long forearms are bent on sinewy shoulders to reach out to hold you. Your senses fail.}
{flashbackaTurns == 6: It is dark.}
{flashbackaTurns == 6: ->loopHub}
->flashback_a_hub



==flashback_b==
//fallback dream, once hater is chosen
VAR bComplete = false

You're alone in your apartment. It's bright as early morning, like you've just woken up and opened the blinds and your eyes are still adjusting. You have no idea what time of day it actually is. ->flashback_b_hub

==flashback_b_hub==
+ [Check the clock.]
    The clock isn't right. It's flashing 12:00. ->flashback_b_hub
+ [Look out the window.]
    Out the main window of the living area you see {Hater} standing in profile, lightly silhouetted by the sunlight. As you notice {getPronoun(Hater, "object")}, {getPronoun(Hater, "subject")} walks out of your view. You run outside--what felt like your apartment is a small family house in a suburb so vacant you can feel the curvature of the earth. 
    
- In the yawning sky above you is a circular hole, too vast to comprehend but seemingly infinitely wide, through which you can make out the entirety of the existence.

+ [Climb into the hole.]    
    You climb on your roof and set up a ladder, eager to climb into the hole.
    
    Your memory becomes a blur as you crest the opening and begin to plummet into eternity. The infinity of your newfound understanding is some comfort to you as you are distorted beyond recognition by the ravages of time-without-time.
+ [Do nothing.]
    You stare at the hole for a while, as your disbelief fades to bewilderment and finally to resignation. You go inside to make food, knowing that eventually you will be claimed by the void.
    - ~bComplete = true
    ->loopHub
    
    
    
==flashback_c==
//fallback dream, no requirements
VAR cComplete = false
VAR flashbackcTurns = 0

You're in the foyer of a house you think you remember. 
->flashback_c_hub

==flashback_c_hub==
+ [Look around.]
    {flashbackcTurns == 0: The foyer is constructed almost exclusively out of a rich, orange-red wood that radiates a warmth that makes it feel like a refuge from the wind and cold outside. The two-storey cathedral ceiling is tall enough that you unconsciously avoid looking up into it, as the green-papered walls recede into an ominous and overbearing darkness above.}
    {flashbackcTurns == 1: You're in the house's tall, beautiful foyer. There is a faceless figure watching you from just outside your vision.}
    ->flashback_c_hub
+ [Check the time on the grandfather clock.]
    The clock is stopped at 12:00.
    ->flashback_c_hub
+ [Move...]
    ++[...to the living room.]
        You begin to head through the large door into the living room
    ++[...to the vestibule.]
        You start to step through the wooden cutout door into the vestibule
    ++[...to the upstairs study.]
        You reach for the stairway banister
    ++[...to the upstairs hallway.]
        You reach for the stairway banister
    ++[...out the front door.]
        You turn to step out the front door
    ++[...Never mind.]
        ->flashback_c_hub

- {
    - flashbackcTurns == 0: 
        <>, but as you do you are distracted by the sound of bare feet skittering across the house's old wood. Through the living room door you catch a glimpse of a small figure disappearing into the kitchen... and then another darts out of your view on the upstairs landing.
    ~flashbackcTurns = 1
    ->flashback_c_hub
    - flashbackcTurns == 1:
        <>, but are surprised by more of the strange figures. One scrambles into the room from the kitchen vestibule, then one from upstairs, and more, until you are surrounded. They are smaller than average humans, featureless, with orange-red skin. They are unclothed, and their feet slap against the polished floor with a sound like stretching leather.
        
        One of the creatures grabs you by the arm as they encircle you. Another reaches you, throwing its arm around your waist, and then another puts its hand on your shoulder.
        
        A half-dozen of the beings surround you, their lukewarm skin tight against yours as they hold you in an embrace. Their tight grip relaxes as they feel more comfortable around you.
        
        Over your shoulder, out of your sight, a large moon-faced figure watches, its two left eyes full of anger and hurt.
        ~cComplete = true
}
->loopHub