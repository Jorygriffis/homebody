INCLUDE dialogue.ink
INCLUDE scenes.ink


VAR totalLoopCount = 1
VAR playerName = "null"
VAR MissingKid = "null"
VAR BFF = "null"
VAR Hater = "null"
VAR MostPoints = "null"

VAR PlayerLocation = "null"

VAR Speaker = "null"
VAR SwearLevel = 1
VAR NPCFollowTest = false

VAR CreechLocation = "null"
VAR CliffPoints = 0
VAR CliffLocation = "null"
VAR CliffStatus = "alive"
VAR EmilyPoints = 0
VAR EmilyLocation = "null"
VAR EmilyStatus = "alive"
VAR FrancinePoints = 0
VAR FrancineLocation = "null"
VAR FrancineStatus = "alive"
VAR GaryPoints = 0
VAR GaryLocation = "null"
VAR GaryStatus = "alive"
VAR LauraPoints = 0
VAR LauraLocation = "null"
VAR LauraStatus = "alive"
VAR MeganPoints = 0
VAR MeganLocation = "null"
VAR MeganStatus = "alive"
VAR PetePoints = 0
VAR PeteLocation = "null"
VAR PeteStatus = "alive"

VAR CliffAdd = false
VAR EmilyAdd = false
VAR FrancineAdd = false
VAR GaryAdd = false
VAR LauraAdd = false
VAR MeganAdd = false
VAR PeteAdd = false

==function locationCorrect(x)==
{
    - x == "Attic":
        ~return "attic"
    - x == "Bathroom":
        ~return "bathroom"
    - x == "Study":
        ~return "study"
    - x == "Bedroom":
        ~return "bedroom"
    - x == "Cellar":
        ~return "cellar"
    - x == "Foyer":
        ~return "foyer"
    - x == "Garage":
        ~return "garage"
    - x == "Hallway":
        ~return "hallway"
    - x == "Hallway2":
        ~return "hallway"
    - x == "Kitchen":
        ~return "kitchen"
    - x == "LivingRoom":
        ~return "living room"
    - x == "MasterBathroom":
        ~return "master bathroom"
    - x == "MasterBedroom":
        ~return "master bedroom"
    - x == "Outbuilding":
        ~return "outbuilding"
}
~return

==function Swear(x)==
{
    - x == "Cliff":
        ~SwearLevel = 1
    - x == "Emily":
        ~SwearLevel = 2
    - x == "Laura":
        ~SwearLevel = 1
    - x == "Megan":
        ~SwearLevel = 2
    - x == "Pete":
        ~SwearLevel = 2
    - else:
        ~SwearLevel = 0
}
~return

=== function AddPoints(x, y) ===
{
    - x == "Cliff":
        {
            - CliffAdd == true:
            - else:
                ~ CliffAdd = true
                ~ CliffPoints = CliffPoints + y
        }
    - x == "Emily":
        {
            - EmilyAdd == true:
            - else:
                ~ EmilyAdd = true
                ~ EmilyPoints = EmilyPoints + y
        }
    - x == "Francine":
        {
            - FrancineAdd == true:
            - else:
                ~ FrancineAdd = true
                ~ FrancinePoints = FrancinePoints + y
        }
    - x == "Gary":
        {
            - GaryAdd == true:
            - else:
                ~ GaryAdd = true
                ~ GaryPoints = GaryPoints + y
        }
    - x == "Laura":
        {
            - LauraAdd == true:
            - else:
                ~ LauraAdd = true
                ~ LauraPoints = LauraPoints + y
        }
    - x == "Megan":
        {
            - MeganAdd == true:
            - else:
                ~ MeganAdd = true
                ~ MeganPoints = MeganPoints + y
        }
    - x == "Pete":
        {
            - PeteAdd == true:
            - else:
                ~ PeteAdd = true
                ~ PetePoints = PetePoints + y
        }
}
~return

==function getPronoun(x, y)==
{
- y == "subject":
    {
    - x == "Cliff":
        ~return "he"
    - x == "Gary":
        ~return "he"
    - x == "Pete":
        ~return "he"
    - else:
        ~return "she"
    }
- y == "object":
    {
    - x == "Cliff":
        ~return "him"
    - x == "Gary":
        ~return "him"
    - x == "Pete":
        ~return "him"
    - else:
        ~return "her"
    }
- y == "possessive":
    {
    - x == "Cliff":
        ~return "his"
    - x == "Gary":
        ~return "his"
    - x == "Pete":
        ~return "his"
    - else:
        ~return "her"
    }
}

==function getPronounCaps(x, y)==
{
- y == "subject":
    {
    - x == "Cliff":
        ~return "He"
    - x == "Gary":
        ~return "He"
    - x == "Pete":
        ~return "He"
    - else:
        ~return "She"
    }
- y == "object":
    {
    - x == "Cliff":
        ~return "Him"
    - x == "Gary":
        ~return "Him"
    - x == "Pete":
        ~return "Him"
    - else:
        ~return "Her"
    }
- y == "possessive":
    {
    - x == "Cliff":
        ~return "His"
    - x == "Gary":
        ~return "His"
    - x == "Pete":
        ~return "His"
    - else:
        ~return "Her"
    }
}

==function Greeting(x)==
{
    - x == "Emily":
        ~return "Hi"
    - x == "Gary":
        ~return "Hi"
    - x == "Megan":
        ~return "Hi"
    - else:
        ~return "Hey"
}

==function resetStatus==
~CliffStatus = "alive"
~EmilyStatus = "alive"
~FrancineStatus = "alive"
~GaryStatus = "alive"
~LauraStatus = "alive"
~MeganStatus = "alive"
~PeteStatus = "alive"
{
    - MissingKid == "Cliff":
        ~CliffStatus = "missing"
    - MissingKid == "Emily":
        ~EmilyStatus = "missing"
    - MissingKid == "Francine":
        ~FrancineStatus = "missing"
    - MissingKid == "Gary":
        ~GaryStatus = "missing"
    - MissingKid == "Laura":
        ~LauraStatus = "missing"
    - MissingKid == "Megan":
        ~MeganStatus = "missing"
    - MissingKid == "Pete":
        ~PeteStatus = "missing"
}
~return

==tempMainMenu==
welcome
+[new game]
    are you sure?
    ++yes
        CLEARSAVE
        ->tempNameChoice
    ++no
        ->tempMainMenu
+[continue]
    LOADSCENE house
    ->DONE

==tempNameChoice==
pick a kid.
+Cliff
    ~playerName = "Cliff"
+Emily
    ~playerName = "Emily"
+Francine
    ~playerName = "Francine"
+Gary
    ~playerName = "Gary"
+Laura
    ~playerName = "Laura"
+Megan
    ~playerName = "Megan"
+Pete
    ~playerName = "Pete"
-LOADSCENE house
->DONE

==debugRoom==
this is the debug room menu

+[return to the house]
LOADSCENE house

+[take me to the auto dialog room]
LOADSCENE tempScene1

+[cancel]

-->DONE

==autoDialogTemp==
this is the auto dialog from the autodialog script.
it's working, hurray
->DONE