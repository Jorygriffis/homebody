==NPCStart ==
FOLLOWTEST
Hi {playerName}!
->NPCHub

==NPCHub==
<-followOptions

<-genericTalk_Topic

+[Where is...]
    ->locationAsk

+Bye.
    See you!
    ->DONE
- ->NPCHub
  
==followOptions==
{
    - NPCFollowTest == false:
    +Follow me.
        FOLLOWPLAYER
        Ok!
        
        ->DONE
}
{
    -NPCFollowTest == true:
    +Stop following me.
        STOPFOLLOWPLAYER
        Got it!
        
        ->DONE
}

==locationAsk==
{
    - Speaker == "Cliff":
    - playerName == "Cliff":
    - else: +[...Cliff?]
        Where is Cliff?
        {locationTest("Cliff", CliffLocation)}
        ->NPCHub
}
{
    - Speaker == "Emily":
    - playerName == "Emily":
    - else: +[...Emily?]
        Where is Emily?
        {locationTest("Emily", EmilyLocation)}
        ->NPCHub
}
{
    - Speaker == "Francine":
    - playerName == "Francine":
    - else: +[...Francine?]
        Where is Francine?
        {locationTest("Francine", FrancineLocation)}
        ->NPCHub
}
{
    - Speaker == "Gary":
    - playerName == "Gary":
    - else: +[...Gary?]
        Where is Gary?
        {locationTest("Gary", GaryLocation)}
        ->NPCHub
}
{
    - Speaker == "Laura":
    - playerName == "Laura":
    - else: +[...Laura?]
        Where is Laura?
        {locationTest("Laura", LauraLocation)}
        ->NPCHub
}
{
    - Speaker == "Megan":
    - playerName == "Megan":
    - else: +[...Megan?]
        Where is Megan?
        {locationTest("Megan", MeganLocation)}
        ->NPCHub
}
{
    - Speaker == "Pete":
    - playerName == "Pete":
    - else: +[...Pete?]
        Where is Pete?
        {locationTest("Pete", PeteLocation)}
        ->NPCHub
}

==function locationTest(x, y)==
{
    - MissingKid == x:
        {x} isn't here.
    - else:
        {x} is in the {locationCorrect(y)}.
}
~return

==genericTalk_Topic==

VAR GeneralTalkCount = 0

{
- GeneralTalkCount == 0:
+ So what's the plan for the weekend? #player_handsonhips
    {AddPoints(Speaker, 1)}
    ~GeneralTalkCount = GeneralTalkCount + 1
    {
        - Speaker == "Megan":
        I'm down for anything. #anim_handsonhips
        ...Honestly it's kind of embarrassing to admit, but even with all the nature around, I'm stoked to have a chance to chill and play games with friends.
        - Speaker == "Francine":
        I'm already enjoying the environs enough. I think tomorrow we'll be hiking out to the desert to try to watch the Perseids, which should be exciting.
        - Speaker == "Pete":
        We are waking up slowly tomorrow. That is all I know. That is all I care about.
        - Speaker == "Cliff": 
        I'm happy doing whatever.
        - else:
        {shuffle:
        -
        I don't know! I feel like {MissingKid} had a better idea than anyone. Maybe once {getPronoun(MissingKid, "subject")} shows up we'll figure something specific out.
        -
        I'm not sure, but I know the plan is to leave sometime in the evening to watch the Perseids. We'll know more once {MissingKid} shows up.
        }
    }
}
{
- GeneralTalkCount == 1:
+ This house is pretty creepy, huh?
    {AddPoints(Speaker, 1)}
    ~GeneralTalkCount = GeneralTalkCount + 1
    {
        - Speaker == "Francine":
        I think it's beautiful. I'm not particularly frightened by inanimate objects.
        - Speaker == "Megan":
        It is. I kind of hate it but kind of love it? It gives off major 7th Guest vibes.
        - Speaker == "Cliff":
        Yeah. I want to get some air.
        Maybe tomorrow, right?
        Yeah. Tomorrow.
        - Speaker == "Gary":
        Totally. I feel like somebody's weird uncle should bequeath us his fortune for staying here.
        - else:
        Yes, it is. I can't believe how dark it is outside, too.
    }
}
{
- GeneralTalkCount == 2:
+ Has anyone heard from {MissingKid}?
    {AddPoints(Speaker, 1)}
    ~GeneralTalkCount = 0
        We had a thread going. {Speaker=="Francine":The last thing}{Speaker!="Francine":Last thing} we heard{Speaker == "Francine":<> was that }{Speaker!="Francine":, }{getPronoun(MissingKid, "subject")} was on {getPronoun(MissingKid, "possessive")} way. That was around 6:30?
        {Speaker!="Cliff":{getPronounCaps(MissingKid, "subject")} should be here soon.}
}
-->NPCHub