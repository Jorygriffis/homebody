INCLUDE homebodyflashbacks.ink
INCLUDE homebodyhouse.ink
INCLUDE homebodytalk.ink
INCLUDE homebodyparty.ink


CONST MISSING = 420
CONST DEAD = 666
CONST ALIVE = 777
CONST PLAYER = 421
CONST CREECH = 999
CONST HIDDEN = 66666666
VAR PlayerName = "StartName"
VAR BFF = "Crobus"
VAR Hater = "Crobus"
VAR MissingKid = "Crobus"
VAR DoorGetter = "Crobus"
VAR CreechLocation = CREECH
VAR CliffPoints = 0
VAR CliffLocation = KITCHEN
VAR CliffStatus = ALIVE
VAR EmilyPoints = 0
VAR EmilyLocation = LIVINGROOM
VAR EmilyStatus = ALIVE
VAR FrancinePoints = 0
VAR FrancineLocation = STUDY
VAR FrancineStatus = ALIVE
VAR GaryPoints = 0
VAR GaryLocation = LIVINGROOM
VAR GaryStatus = ALIVE
VAR LauraPoints = 0
VAR LauraLocation = LIVINGROOM
VAR LauraStatus = ALIVE
VAR MeganPoints = 0
VAR MeganLocation = LIVINGROOM
VAR MeganStatus = ALIVE
VAR PetePoints = 0
VAR PeteLocation = KITCHEN
VAR PeteStatus = ALIVE
VAR Speaker = "Crobus"
VAR PlayerLocation = FRONTLAWN
VAR SwearLevel = 0
VAR CreechMoveTimer = 0
VAR CreechKillTimer = 0
VAR CreechKill = false
VAR CliffDies = false
VAR EmilyDies = false
VAR FrancineDies = false
VAR GaryDies = false
VAR LauraDies = false
VAR MeganDies = false
VAR PeteDies = false
VAR KidMurd = false
VAR KidMoveTimer = 0
VAR CreechMove = false
VAR CliffMoved = false
VAR EmilyMoved = false
VAR FrancineMoved = false
VAR GaryMoved = false
VAR LauraMoved = false
VAR MeganMoved = false
VAR PeteMoved = false
VAR PlayerAlone = false
VAR Waiting = false
VAR Blackout = false
VAR MoveDanger = false
VAR CreechUnit = 1
//VAR BFFGoal = 3
VAR BFFGoal = 3
VAR MostPoints = "Crobus"
VAR highestPoints = 0
VAR Follower = "Crobus"
//VAR FollowGoal = 1
VAR FollowGoal = 1
VAR AllDead = false
VAR CliffAdd = false
VAR EmilyAdd = false
VAR FrancineAdd = false
VAR GaryAdd = false
VAR LauraAdd = false
VAR MeganAdd = false
VAR PeteAdd = false
VAR CreechMinute = 39
VAR CreechMinuteNormal = 39
VAR CreechMinuteSlow = 44
VAR CreechMinuteFast = 30
VAR CreechMinuteSlow2 = 58
VAR FollowerLeave = false
VAR CreechDead = false
VAR HouseExplode = false
VAR CreechDebug = true
VAR HaterLevel = 1
VAR HaterLevel1Done = false
VAR HaterGoal = "Generic"
VAR HaterGoalLocation = GARAGE
VAR HaterFollowPlayer = false
VAR HaterMove = false
VAR HaterPresent = false
VAR HaterLocation = HIDDEN
VAR HaterCellar = false
VAR HaterAttic = 0
VAR AtticScene = false
VAR HaterLevel1SceneTrigger = false
VAR UsedCellarMachine = false
VAR UsedGenerator = false
VAR HaterMode = "Least"
VAR AtticHaterSceneDone = false
VAR UsedOutbuildingBreaker = false
VAR NoSelfDestruct = false
VAR AtticMachineDischarged = false
VAR FrontDoor = false
VAR SawCreech = false
VAR HaterMin = 0
VAR SecondMostPoints = "Crobus"




->PreGame_NameChoice

=== function MiniReset ===
~CreechKill = false
~CreechLocation = CREECH
~NoSelfDestruct = false
~CellarMachineLocked = true
~HaterAttic = 0
~HaterCellar = false
~HaterGoal = "Generic"
~HaterGoalLocation = GARAGE
~HaterFollowPlayer = false
~HaterMove = false
~HaterPresent = false
~HouseExplode = false
~CreechDead = true
~Follower = "Crobus"
~MoveDanger = false
~Flashback = true
~Scary = false
~CliffStatus = ALIVE
~EmilyStatus = ALIVE
~FrancineStatus = ALIVE
~GaryStatus = ALIVE
~LauraStatus = ALIVE
~MeganStatus = ALIVE
~PeteStatus = ALIVE
~CliffDies = false
~EmilyDies = false
~FrancineDies = false
~GaryDies = false
~LauraDies = false
~MeganDies = false
~PeteDies = false
~AllDead = false
~CliffAdd = false
~EmilyAdd = false
~FrancineAdd = false
~GaryAdd = false
~LauraAdd = false
~MeganAdd = false
~PeteAdd = false
{HaterDude()}
{MostPointsCheck()}
~return

=== function VarReset ===
~CliffGreeting = false
~EmilyGreeting = false
~FrancineGreeting = false
~GaryGreeting = false
~LauraGreeting = false
~MeganGreeting = false
~PeteGreeting = false
~FrontDoor = false
~NoSelfDestruct = false
~CellarMachineLocked = true
~HaterAttic = 0
~HaterCellar = false
~HaterGoal = "Generic"
~HaterGoalLocation = GARAGE
~HaterFollowPlayer = false
~HaterMove = false
~HaterPresent = false
~HouseExplode = false
~CreechDead = false
~GeneratorOn = false
~AtticPower = 1
~HousePower = 1
~ReservePower = 0
~GuestHouseTunnelOpen = false
~CellarTunnelOpen = false
~GuestHouseOpen = false
~AtticMachineCovered = true
~AtticMachineFired = false
~CreechMinute = CreechMinuteNormal
~CliffAdd = false
~EmilyAdd = false
~FrancineAdd = false
~GaryAdd = false
~LauraAdd = false
~MeganAdd = false
~PeteAdd = false
~Follower = "Crobus"
~AllDead = false
~MoveDanger = true
~FlashbackNumber = 666
~Flashback = false
~Blackout = false
~PowerOn = true
~PowerFlipped = false
~CellarOpen = false
~CreechMove = false
~KidMoveTimer = 0
~CreechKillTimer = 0
~CreechKill = false
~CurrentHour = 7
~CurrentMinute = 31
~CreechMoveTimer = 0
~TriedFrontDoor = false
~DoorGetterLeft = false
~Scary = false
~PowerOn = true
~PowerFlipped = false
~CreechLocation = CREECH
~CliffStatus = ALIVE
~EmilyStatus = ALIVE
~FrancineStatus = ALIVE
~GaryStatus = ALIVE
~LauraStatus = ALIVE
~MeganStatus = ALIVE
~PeteStatus = ALIVE
~CliffDies = false
~EmilyDies = false
~FrancineDies = false
~GaryDies = false
~LauraDies = false
~MeganDies = false
~PeteDies = false
~CliffLocation = KITCHEN
~EmilyLocation = LIVINGROOM
~FrancineLocation = STUDY
~GaryLocation = LIVINGROOM
~LauraLocation = LIVINGROOM
~MeganLocation = LIVINGROOM
~PeteLocation = KITCHEN
{
    - PlayerName == "Cliff":
        ~CliffLocation = PLAYER
    - PlayerName == "Emily":
        ~EmilyLocation = PLAYER
    - PlayerName == "Francine":
        ~FrancineLocation = PLAYER
    - PlayerName == "Gary":
        ~GaryLocation = PLAYER
    - PlayerName == "Laura":
        ~LauraLocation = PLAYER
    - PlayerName == "Megan":
        ~MeganLocation = PLAYER
    - PlayerName == "Pete":
        ~PeteLocation = PLAYER
}
{
    - DoorGetter == "Cliff":
        ~CliffLocation = FOYER
    - DoorGetter == "Emily":
        ~EmilyLocation = FOYER
    - DoorGetter == "Francine":
        ~FrancineLocation = FOYER
    - DoorGetter == "Gary":
        ~GaryLocation = FOYER
    - DoorGetter == "Laura":
        ~LauraLocation = FOYER
    - DoorGetter == "Megan":
        ~MeganLocation = FOYER
    - DoorGetter == "Pete":
        ~PeteLocation = FOYER
}
{
    - MissingKid == "Cliff":
        ~CliffStatus = MISSING
        ~CliffLocation = MISSING
    - MissingKid == "Emily":
        ~EmilyLocation = MISSING
        ~EmilyStatus = MISSING
    - MissingKid == "Francine":
        ~FrancineLocation = MISSING
        ~FrancineStatus = MISSING
    - MissingKid == "Gary":
        ~GaryLocation = MISSING
        ~GaryStatus = MISSING
    - MissingKid == "Laura":
        ~LauraLocation = MISSING
        ~LauraStatus = MISSING
    - MissingKid == "Megan":
        ~MeganLocation = MISSING
        ~MeganStatus = MISSING
    - MissingKid == "Pete":
        ~PeteLocation = MISSING
        ~PeteStatus = MISSING
}
{
    - Hater != "Crobus":
    {
        - HaterLevel1Done != true:
        ~HaterLevel = 1
    }
}
{
    - HaterLevel == 1:
    {
        - HaterLevel1Done:
        {
            - UsedCellarMachine == true:

                    ~HaterLevel = 2
            
        }
    }
}

=== LoopReset ===
{VarReset()}
{HaterDude()}
{MostPointsCheck()}
->Intro

=== function AddTime(x) ===
//BFF: {BFF}
//HATER: {Hater}
//HATER MOVE MODE: {HaterGoal}
//FRANCINELOCATION: {FrancineLocation}
//GARYLOCATION: {GaryLocation}

{
    - CreechLocation == PlayerLocation:
        ~SawCreech = true
        ~Scary = true
}
{
    - Flashback == true:
    - Scary == false:
    {
        - CliffLocation == PlayerLocation:
        {
            - Hater == "Cliff":
            - DoorGetter == "Cliff":
            - else:
            {
            - CliffGreeting == false:
                ~CliffGreeting = true
                    {
                    - FrontDoor == true:
                    {shuffle:
                        - {BFF == "Cliff": "Hey, {PlayerName}!"}{BFF != "Cliff": "Hey, {PlayerName},"} Cliff says.
                        - 
                    }
                    - else:
                    {shuffle:
                        - "Oh, {PlayerName}. Didn't know you were here," Cliff says.
                        -
                        -
                    }
                    }
            }
        }

    }
    {
        - EmilyLocation == PlayerLocation:
        {
            - Hater == "Emily":
            - DoorGetter == "Emily":
            - else:
            {
            - EmilyGreeting == false:
                ~EmilyGreeting = true
                    {
                    - FrontDoor == true:
                    {shuffle:
                        - {BFF == "Emily": "Hi, {PlayerName}! I'm glad you're here!"}{BFF != "Emily": "Hi, {PlayerName}!"} Emily says.
                        - 
                    }
                    - else:
                    {shuffle:
                        - "Oh hi, {PlayerName}. Didn't know you were here," Emily says.
                        -
                        -
                    }
                    }
            }
        }

    }
    {
        - FrancineLocation == PlayerLocation:
        {
            - Hater == "Francine":
            - DoorGetter == "Francine":
            - else:
            {
            - FrancineGreeting == false:
                ~FrancineGreeting = true
                    {
                    - FrontDoor == true:
                    {shuffle:
                        - {BFF == "Francine": "Hi, {PlayerName}!"}{BFF != "Francine": "Hello, {PlayerName},"} Francine says.
                        - 
                    }
                    - else:
                    {shuffle:
                        - "Oh, I didn't know you'd arrived, {PlayerName}," Francine says.
                        -
                        -
                    }
                    }
            }
        }

    }
    {
        - GaryLocation == PlayerLocation:
        {
            - Hater == "Gary":
            - DoorGetter == "Gary":
            - else:
            {
            - GaryGreeting == false:
                ~GaryGreeting = true
                    {
                    - FrontDoor == true:
                    {shuffle:
                        - {BFF == "Gary": "Hey there, {PlayerName}!"}{BFF != "Gary": "Hey, {PlayerName},"} Gary says.
                        - 
                    }
                    - else:
                    {shuffle:
                        - "Oh hey! Didn't know you were here, {PlayerName}," Gary says.
                        -
                        -
                    }
                    }
            }
        }

    }
    {
        - LauraLocation == PlayerLocation:
        {
            - Hater == "Laura":
            - DoorGetter == "Laura":
            - else:
            {
            - LauraGreeting == false:
                ~LauraGreeting = true
                    {
                    - FrontDoor == true:
                    {shuffle:
                        - {BFF == "Laura": "Hey, {PlayerName}!"}{BFF != "Laura": "Hi, {PlayerName},"} Laura says.
                        - 
                    }
                    - else:
                    {shuffle:
                        - "{PlayerName}? Hey, I didn't know you were here," Laura says.
                        -
                        -
                    }
                    }
            }
        }

    }
    {
        - MeganLocation == PlayerLocation:
        {
            - Hater == "Megan":
            - DoorGetter == "Megan":
            - else:
            {
            - MeganGreeting == false:
                ~MeganGreeting = true
                    {
                    - FrontDoor == true:
                    {shuffle:
                        - {BFF == "Megan": "{PlayerName}! Hi!"}{BFF != "Megan": "{PlayerName}!"} Megan says.
                        - 
                    }
                    - else:
                    {shuffle:
                        - "{PlayerName}! When did you get here?" Megan says.
                        -
                        -
                    }
                    }
            }
        }

    }
    {
        - PeteLocation == PlayerLocation:
        {
            - Hater == "Pete":
            - DoorGetter == "Pete":
            - else:
            {
            - PeteGreeting == false:
                ~PeteGreeting = true
                    {
                    - FrontDoor == true:
                    {shuffle:
                        - {BFF == "Pete": "Hey, {PlayerName}!"}{BFF != "Pete": "Hey, {PlayerName}, what's up?"} Pete says.
                        - 
                    }
                    - else:
                    {shuffle:
                        - "Oh, {PlayerName}. Didn't know you were here," Pete says.
                        -
                        -
                    }
                    }
            }
        }

    }
}
{
    - CreechDebug == true:
{
    - CreechMove == false:
    {
        - CurrentMinute > CreechMinute:
            ~CreechMove = true
            {
                - PlayerLocation == MASTERBEDROOM:
                    ~CreechLocation = HALLWAY2
                - else:
                    ~CreechLocation = MASTERBEDROOM
            }
    }
}
}
{
    - Blackout == false:
    {
        - CurrentMinute > CreechMinute + 9:
            {blackout()}
    }
}
{KidTimer(x)}
{CreechTimer(x)}
~CurrentMinute = CurrentMinute + x 
{
- CurrentMinute > 59:
    ~temp MinuteDiff = CurrentMinute - 60
    ~CurrentHour = CurrentHour + 1 
    ~CurrentMinute = MinuteDiff
}
{
    - NoSelfDestruct == false:
{
    - CurrentHour == 8:
    {
        - CurrentMinute == 1:
        There's an ominous rumble in the house's foundation.
        - CurrentMinute == 3:
        The floor shifts slightly beneath you as the house trembles again.
        - CurrentMinute == 4:
        Plaster falls from the ceiling as the rumble grows into a greater, more audible tremor.
        - CurrentMinute == 5:
        ~HouseExplode = true
    }
}
}

~return

=== function blackout ===
{PowerOn == true:
    Suddenly, the power in the house goes out!
    ~PowerOn = false
    ~ReservePower = ReservePower + HousePower
    ~HousePower = 0
    ~Blackout = true
}
~return

=== function AddPoints(x, y) ===
{
    - x == "Cliff":
        {
            - CliffAdd == true:
            - else:
                //~ CliffAdd = true
                ~ CliffPoints = CliffPoints + y
        }
    - x == "Emily":
        {
            - EmilyAdd == true:
            - else:
                //~ EmilyAdd = true
                ~ EmilyPoints = EmilyPoints + y
        }
    - x == "Francine":
        {
            - FrancineAdd == true:
            - else:
                //~ FrancineAdd = true
                ~ FrancinePoints = FrancinePoints + y
        }
    - x == "Gary":
        {
            - GaryAdd == true:
            - else:
                //~ GaryAdd = true
                ~ GaryPoints = GaryPoints + y
        }
    - x == "Laura":
        {
            - LauraAdd == true:
            - else:
                //~ LauraAdd = true
                ~ LauraPoints = LauraPoints + y
        }
    - x == "Megan":
        {
            - MeganAdd == true:
            - else:
                //~ MeganAdd = true
                ~ MeganPoints = MeganPoints + y
        }
    - x == "Pete":
        {
            - PeteAdd == true:
            - else:
                //~ PeteAdd = true
                ~ PetePoints = PetePoints + y
        }
}
{MostPointsCheck()}
~return

=== function MoveHater(x) ===
{
    - Hater == "Cliff":
        ~CliffLocation = x
    - Hater == "Emily":
        ~EmilyLocation = x
    - Hater == "Francine":
        ~FrancineLocation = x
    - Hater == "Gary":
        ~GaryLocation = x
    - Hater == "Laura":
        ~LauraLocation = x
    - Hater == "Megan":
        ~MeganLocation = x
    - Hater == "Pete":
        ~PeteLocation = x
}
~return

=== function MoveHaterRandom ===
~temp HaterTemp = GARAGE
{shuffle:
    -
    ~HaterTemp = GARAGE
    {
        - PlayerLocation == GARAGE:
            ~HaterTemp = GUESTBEDROOM
    }
    -
    ~HaterTemp = GUESTBEDROOM
    {
        - PlayerLocation == GUESTBEDROOM:
            ~HaterTemp = VESTIBULE
    }
    -
    ~HaterTemp = HALLWAY2
    {
        - PlayerLocation == HALLWAY2:
            ~HaterTemp = VESTIBULE
    }
}
{
    - Hater == "Cliff":
        ~CliffLocation = HaterTemp
    - Hater == "Emily":
        ~EmilyLocation = HaterTemp
    - Hater == "Francine":
        ~FrancineLocation = HaterTemp
    - Hater == "Gary":
        ~GaryLocation = HaterTemp
    - Hater == "Laura":
        ~LauraLocation = HaterTemp
    - Hater == "Megan":
        ~MeganLocation = HaterTemp
    - Hater == "Pete":
        ~PeteLocation = HaterTemp
}
~return

=== function SetPlayerLocation(x) ===
{AloneCheck()}
{
    -PlayerLocation == CreechLocation: 
        ~MoveDanger = true
    -else:
        ~MoveDanger = false
}
~PlayerLocation = x
~return

=== function flashbackLocation(x) ===
~PlayerLocation = x
    {
        - PlayerName != "Cliff":
            ~CliffLocation = x
    }
    {
        - PlayerName != "Emily":
            ~EmilyLocation = x
    }
    {
        - PlayerName != "Francine":
            ~FrancineLocation = x
    }
    {
        - PlayerName != "Gary":
            ~GaryLocation = x
    }
    {
        - PlayerName != "Laura":
            ~LauraLocation = x
    }
    {
        - PlayerName != "Megan":
            ~MeganLocation = x
    }
    {
        - PlayerName != "Pete":
            ~PeteLocation = x
    }
~return

=== GoToLocation ===
{
    - HouseExplode == true:
        You are knocked to the ground as the floor beneath you is ripped into pieces by the sudden disintegration of the house's foundation. The walls around you are shredded, and a blinding light remains where they once stood. A terrible heat briefly consumes you.
        
        It is dark.
        ->FlashbackHub
}

{
    - Hater != "Crobus":
    {
        - HaterLevel == 0:
        
        - HaterLevel == 1:
    {
        - HaterLevel1Done != true:
            ~HaterLevel1SceneTrigger = true
//          ~HaterLevel1Done = true
        - else:
            {
                - HaterPresent == true:
                {
                    - Scary:
                {Hater} notices you approaching {getPronoun(Hater, "object")} and quickly sneaks from the room.
                {MoveHaterRandom()}    
                    - else:
                {Hater} notices you looking in {getPronoun(Hater, "possessive")} direction and walks away.
                {MoveHaterRandom()}
                }
            }
    }
        - HaterLevel == 2:
            {
                - CurrentMinute > 34:
                    {
                    - CurrentMinute > 35:
                    {
                        - HaterCellar == true:
                        {
                            - PlayerLocation == CELLAR:
                            {
                                - CurrentMinute < 39:
                                In your peripheral vision you notice {Hater} running up the cellar stairs.
                                {MoveHater(ATTIC)}
                                ~HaterCellar = false
                                ~HaterAttic = 1
                                - else:
                                {blackout()}
                                In your peripheral vision you notice {Hater} running up the cellar stairs--did {getPronoun(Hater, "subject")} shut off the power?
                                {MoveHater(HIDDEN)}
                                ~HaterCellar = false
                                ~HaterAttic = 1
                            }
                            - CurrentMinute == 38:
                                ~CellarMachineLocked = false
                                {blackout()}
                        }
                        - HaterAttic == 1:
                        //HATER IS IN ATTIC
                        {
                            - PlayerLocation == ATTIC:
                            {
                                - AtticMachineDischarged == true:
                                {
                                    - AtticPower == 3:
                                    {
                                    - AtticMachineCovered == false:
                                        {MoveHater(ATTIC)}
                                        ~AtticScene = true
                                    }
                                }
                            }
                        }
                        - else:
                        HATER DONE
                    }
                    - else:
                    {MoveHater(CELLAR)}
                    {PlayerLocation == VESTIBULE: You notice {Hater} disappearing into the cellar.}
                    ~HaterCellar = true
                    }
                - else:
                {
                    - HaterPresent == true:
                    {Hater} is alarmed to see you and quickly darts away.
                    {MoveHaterRandom()}
                }
            }
    }
}

{
    - MoveDanger == true:
        ~Scary = true
        {
            - Waiting == true:
                ~Waiting = false
            - else:
            {
                - PlayerAlone == true:
                {
                    - CreechMoveTimer > -1:
                        {shuffle:
                        -
                        - The creature lunges for you as you begin to run, but you escape!
                        - The creature lunges for you as you begin to run, but you escape!
                        - 
                        {
                            - Follower == BFF:
                            {
                                - BFF != "Crobus":
                                        The creature lunges for you as you begin to run, but {Follower} pushes it away!
                                - else:
                                The creature lunges for you as you begin to run, but you escape!
                            }
                            - else:
                            The creature lunges for you as you begin to run, but you escape!
                        }
                        - 
                        {
                            - Follower == BFF:
                            {
                                - BFF != "Crobus":
                                    The creature lunges for you as you begin to run, but {Follower} pushes it away!
                                - else:
                                The creature lunges for you as you begin to run, but you escape!
                            }
                            - else:
                            The creature lunges for you as you begin to run, but you escape!
                        }
                        -
                        {
                            - Follower == BFF:
                            {
                                - BFF != "Crobus":
                                    {shuffle:
                                        - The creature lunges for you as you begin to run, but {Follower} pushes it away!
                                        - The creature lunges for you as you begin to run, but {Follower} pushes it away!
                                        - ->PlayerMoveDie
                                    }
                                - else:
                                ->PlayerMoveDie
                            }
                            - else:
                            ->PlayerMoveDie
                        }
                        }
                }
                - PlayerAlone == false:
                {
                    - CreechMoveTimer > -1:
                        {shuffle:
                        -
                        - The creature lunges for you as you begin to run, but you escape!
                        - The creature lunges for you as you begin to run, but you escape!
                        }
                }
            }
        }
}
{
    -Scary == true:
        <-DoorGetterMoveSilent
}
{
    - CreechDead == false:
    {
        -CliffDies == true: <-CliffKill
        -EmilyDies == true: <-EmilyKill
        -FrancineDies == true: <-FrancineKill
        -GaryDies == true: <-GaryKill
        -LauraDies == true: <-LauraKill
        -MeganDies == true: <-MeganKill
        -PeteDies == true: <-PeteKill
    }
}
{
    - AtticScene == true:
    ->AtticHaterScene
}
{
    - HaterLevel1SceneTrigger == true:
    ->HaterLevel1Scene
}
{
    -CreechKill == true: ->PlayerDie
    -PlayerLocation == FRONTLAWN: ->Front_Lawn
    -PlayerLocation == OUTBUILDINGEXT: ->Outbuilding_Ext
    -PlayerLocation == OUTBUILDINGINT: ->Outbuilding_Int
    -PlayerLocation == FOYER: ->Foyer
    -PlayerLocation == LIVINGROOM: ->LivingRoom
    -PlayerLocation == KITCHEN: ->Kitchen
    -PlayerLocation == VESTIBULE: ->Vestibule
    -PlayerLocation == GARAGE: ->Garage
    -PlayerLocation == STUDY: ->Study
    -PlayerLocation == GUESTBEDROOM: ->GuestBedroom
    -PlayerLocation == MASTERBEDROOM: ->MasterBedroom
    -PlayerLocation == BATHROOM: ->Bathroom
    -PlayerLocation == HALLWAY1: ->Hallway1
    -PlayerLocation == HALLWAY2: ->Hallway2
    -PlayerLocation == CELLAR: ->Cellar
    -PlayerLocation == ATTIC: ->Attic
    -PlayerLocation == GUESTHOUSE: ->GuestHouse
    
    -PlayerLocation == FLASHBACK1: ->Flashback_1_hub
    -PlayerLocation == PARTYMAIN: ->Flashback_Party_hub
}

=== function AloneCheck ===
~PlayerAlone = true
{
    - PlayerLocation == CliffLocation: 
        {
            -Hater == "Cliff":
            -CliffStatus == ALIVE:
            ~PlayerAlone = false
        }
}
{
    - PlayerLocation == EmilyLocation: 
        {
            -Hater == "Emily":
            -EmilyStatus == ALIVE:
            ~PlayerAlone = false
        }
}
{
    - PlayerLocation == FrancineLocation: 
        {
            -Hater == "Francine":
            -FrancineStatus == ALIVE:
            ~PlayerAlone = false
        }
}
{
    - PlayerLocation == GaryLocation: 
        {
            -Hater == "Gary":
            -GaryStatus == ALIVE:
            ~PlayerAlone = false
        }
}
{
    - PlayerLocation == LauraLocation: 
        {
            -Hater == "Laura":
            -LauraStatus == ALIVE:
            ~PlayerAlone = false
        }
}
{
    - PlayerLocation == MeganLocation: 
        {
            -Hater == "Megan":
            -MeganStatus == ALIVE:
            ~PlayerAlone = false
        }
}
{
    - PlayerLocation == PeteLocation: 
        {
            -Hater == "Pete":
            -PeteStatus == ALIVE:
            ~PlayerAlone = false
        }
}
~return

=== function MoveToPlayer(x) ===
~temp TempLocation = "Crobus"
~temp GoalLocation = PlayerLocation
~temp StartLocation = CreechLocation
{
    - x == "Creech":
        ~StartLocation = CreechLocation
    - x == "Cliff":
        ~StartLocation = CliffLocation
    - x == "Emily":
        ~StartLocation = EmilyLocation
    - x == "Francine":
        ~StartLocation = FrancineLocation
            {
                - Scary == false:
                {
                    - Follower != "Francine":
                        ~GoalLocation = LIVINGROOM
                }
            }
    - x == "Gary":
        ~StartLocation = GaryLocation
    - x == "Laura":
        ~StartLocation = LauraLocation
    - x == "Megan":
        ~StartLocation = MeganLocation
    - x == "Pete":
        ~StartLocation = PeteLocation
}

{
    - PlayerLocation == GUESTHOUSE:
    {
        - x == "Creech":
            {AllDeadTest()}
            {
            - CreechLocation != PlayerLocation:
            {
                - AllDead == true:
                    {
                        - StartLocation == CELLAR:
                            ~ GoalLocation = GUESTHOUSE
                        - StartLocation == VESTIBULE:
                            ~ GoalLocation = CELLAR
                        - else:
                            ~ GoalLocation = VESTIBULE
                    }
                - else:
                    ~ GoalLocation = LIVINGROOM
            }
            }
        - else:
            {
                - x == Follower:
                    {
                        - StartLocation == CELLAR:
                            ~ GoalLocation = GUESTHOUSE
                        - StartLocation == VESTIBULE:
                            ~ GoalLocation = CELLAR
                        - else:
                            ~ GoalLocation = VESTIBULE
                    }
                - x == Hater:
                    {
                        - HaterGoal == "Player":
                    {
                        - StartLocation == CELLAR:
                            ~ GoalLocation = GUESTHOUSE
                        - StartLocation == VESTIBULE:
                            ~ GoalLocation = CELLAR
                        - else:
                            ~ GoalLocation = VESTIBULE
                    }
                    }
                - else:
                    ~ GoalLocation = LIVINGROOM
            }
    }
}
{
    - PlayerLocation == CELLAR:
    {
        - x == "Creech":
            {AllDeadTest()}
            {
            - CreechLocation != PlayerLocation:
            {
                - AllDead == true:
                    {
                        - StartLocation == VESTIBULE:
                            ~ GoalLocation = CELLAR
                        - else:
                            ~ GoalLocation = VESTIBULE
                    }
                - else:
                    ~ GoalLocation = LIVINGROOM
            }
            }
        - else:
            {
                - x == Follower:
                    {
                        - StartLocation == VESTIBULE:
                            ~ GoalLocation = CELLAR
                        - else:
                            ~ GoalLocation = VESTIBULE
                    }
                - x == Hater:
                    {
                        - HaterGoal == "Player":
                    {
                        - StartLocation == VESTIBULE:
                            ~ GoalLocation = CELLAR
                        - else:
                            ~ GoalLocation = VESTIBULE
                    }
                    }
                - else:
                    ~ GoalLocation = LIVINGROOM
            }
    }
}
{
    - PlayerLocation == ATTIC:
    {
        - x == "Creech":
            {AllDeadTest()}
            {
            - CreechLocation != PlayerLocation:
            {
                - AllDead == true:
                    {
                        - StartLocation == HALLWAY2:
                            {
                                - AtticScene:
                                ~return
                            }
                            ~ GoalLocation = ATTIC
                        - else:
                            ~ GoalLocation = HALLWAY2
                    }
                - else:
                    ~ GoalLocation = LIVINGROOM
            }
            }
        - else:
            {
                - x == Follower:
                    {
                        - StartLocation == HALLWAY2:
                        {
                            - Follower == BFF:
                                ~ GoalLocation = ATTIC
                            - else:
                                {Follower}, too scared to follow into the attic, stops and looks into the darkness above them.
                                {StopFollowingSilent()}
                        }
                        - else:
                            ~ GoalLocation = HALLWAY2
                    }
                - x == Hater:
                    {
                        - HaterGoal == "Player":
                    {
                        - StartLocation == HALLWAY2:
                            ~ GoalLocation = ATTIC
                        - else:
                            ~ GoalLocation = HALLWAY2
                    }
                    }
                - else:
                    ~ GoalLocation = LIVINGROOM
            }
    }
}

{
    - StartLocation == FOYER:
    {
        - GoalLocation == LIVINGROOM:
            ~TempLocation = LIVINGROOM
        - GoalLocation == KITCHEN:
            ~TempLocation = VESTIBULE
        - GoalLocation == GARAGE:
            ~TempLocation = VESTIBULE
        - GoalLocation == VESTIBULE:
            ~TempLocation = VESTIBULE
        - GoalLocation == STUDY:
            ~TempLocation = STUDY
        - else:
            ~TempLocation = HALLWAY1
    }

    - StartLocation == KITCHEN:
    {
        - GoalLocation == LIVINGROOM:
            ~TempLocation = LIVINGROOM
        - else:
            ~TempLocation = VESTIBULE
    }

    - StartLocation == GARAGE: 
        ~TempLocation = VESTIBULE
        
    - StartLocation == CELLAR: 
    {
        - GoalLocation == GUESTHOUSE:
            ~TempLocation = GUESTHOUSE
        - else:
            ~TempLocation = VESTIBULE
    }
    - StartLocation == GUESTHOUSE:
        ~TempLocation = CELLAR    
        
    - StartLocation == VESTIBULE:
    {
        - GoalLocation == KITCHEN:
            ~TempLocation = KITCHEN
        - GoalLocation == GARAGE:
            ~TempLocation = GARAGE
        - GoalLocation == CELLAR:
            ~TempLocation = CELLAR
        - else:
            ~TempLocation = FOYER
    }

    - StartLocation == LIVINGROOM:
    {
        - GoalLocation == KITCHEN:
            ~TempLocation = KITCHEN
        - else:
            ~TempLocation = FOYER
    }

    - StartLocation == STUDY:
    {
        - GoalLocation == HALLWAY1:
            ~TempLocation = HALLWAY1
        - GoalLocation == GUESTBEDROOM:
            ~TempLocation = GUESTBEDROOM
        - GoalLocation == HALLWAY2:
            ~TempLocation = GUESTBEDROOM
        - GoalLocation == MASTERBEDROOM:
            ~TempLocation = GUESTBEDROOM
        - GoalLocation == BATHROOM:
            ~TempLocation = HALLWAY1
        - else:
            ~TempLocation = FOYER
    }

    - StartLocation == MASTERBEDROOM:
        ~TempLocation = HALLWAY2

    - StartLocation == GUESTBEDROOM:
    {
        - GoalLocation == HALLWAY1:
            ~TempLocation = HALLWAY2
        - GoalLocation == STUDY:
            ~TempLocation = STUDY
        - GoalLocation == HALLWAY2:
            ~TempLocation = HALLWAY2
        - GoalLocation == MASTERBEDROOM:
            ~TempLocation = HALLWAY2
        - GoalLocation == BATHROOM:
            ~TempLocation = HALLWAY2
        - else:
            ~TempLocation = STUDY
    }

    - StartLocation == HALLWAY1:
    {
        - GoalLocation == STUDY:
            ~TempLocation = STUDY
        - GoalLocation == HALLWAY2:
            ~TempLocation = HALLWAY2
        - GoalLocation == BATHROOM:
            ~TempLocation = BATHROOM
        - GoalLocation == GUESTBEDROOM:
            ~TempLocation = HALLWAY2
        - GoalLocation == MASTERBEDROOM:
            ~TempLocation = HALLWAY2
        - else:
            ~TempLocation = FOYER
    }

    - StartLocation == HALLWAY2:
    {
        - GoalLocation == MASTERBEDROOM:
            ~TempLocation = MASTERBEDROOM
        - GoalLocation == GUESTBEDROOM:
            ~TempLocation = GUESTBEDROOM
        - GoalLocation == STUDY:
            ~TempLocation = GUESTBEDROOM
        - GoalLocation == ATTIC:
            ~TempLocation = ATTIC
        - else:
            ~TempLocation = HALLWAY1
    }

    - StartLocation == BATHROOM:
        ~TempLocation = HALLWAY1
        
    - StartLocation == ATTIC:
        ~TempLocation = HALLWAY2
}
{
    - x == "Creech":
        ~CreechLocation = TempLocation
        {
            - CreechLocation == PlayerLocation:
                The creature {EnterLine()}
                ~Scary = true
        }
    - x == "Cliff":
        ~CliffLocation = TempLocation
        ~CliffMoved = true
        //CLIFF MOVE
        {CliffLocation == PlayerLocation: Cliff {EnterLine()}}
    - x == "Emily":
        ~EmilyLocation = TempLocation
        ~EmilyMoved = true
        //EMILY MOVE
        {EmilyLocation == PlayerLocation: Emily {EnterLine()}}
    - x == "Francine":
        ~FrancineLocation = TempLocation
        ~FrancineMoved = true
        //FRANCINE MOVE
        {FrancineLocation == PlayerLocation: Francine {EnterLine()}}
    - x == "Gary":
        ~GaryLocation = TempLocation
        ~GaryMoved = true
        //GARY MOVE
        {GaryLocation == PlayerLocation: Gary {EnterLine()}}
    - x == "Laura":
        ~LauraLocation = TempLocation
        ~LauraMoved = true
        //LAURA MOVE
        {GaryLocation == PlayerLocation: Laura {EnterLine()}}
    - x == "Megan":
        ~MeganLocation = TempLocation
        ~MeganMoved = true
        //MEGAN MOVE
        {MeganLocation == PlayerLocation: Megan {EnterLine()}}
    - x == "Pete":
        ~PeteLocation = TempLocation
        ~PeteMoved = true
        //PETE MOVE
        {PeteLocation == PlayerLocation: Pete {EnterLine()}}
}

=== function EnterLine ===
{
    - PlayerLocation == HALLWAY1:
        walks down the hall toward you.
    - PlayerLocation == HALLWAY2:
        walks down the hall toward you.
    - PlayerLocation == ATTIC:
        walks up the stairs towards you.
    - else:
        walks into the room.
}
~return

=== function AllDeadTest ===
~AllDead = true
{
    - MissingKid != "Cliff":
        {
            - PlayerName != "Cliff":
                {
                    - CliffStatus == ALIVE:
                        {
                        - Follower == "Cliff":
                            {
                             - CliffLocation != PlayerLocation:
                                ~AllDead = false
                            }
                        - Hater == "Cliff":
                        - else:
                        ~AllDead = false
                        }
                }
        }
}
{
    - MissingKid != "Emily":
        {
            - PlayerName != "Emily":
                {
                    - EmilyStatus == ALIVE:
                        {
                        - Follower == "Emily":
                            {
                             - EmilyLocation != PlayerLocation:
                                ~AllDead = false
                            }
                        - Hater == "Emily":
                        - else:
                        ~AllDead = false
                        }
                }
        }
}
{
    - MissingKid != "Francine":
        {
            - PlayerName != "Francine":
                {
                    - FrancineStatus == ALIVE:
                        {
                        - Follower == "Francine":
                            {
                             - FrancineLocation != PlayerLocation:
                                ~AllDead = false
                            }
                        - Hater == "Francine":
                        - else:
                        ~AllDead = false
                        }
                }
        }
}
{
    - MissingKid != "Gary":
        {
            - PlayerName != "Gary":
                {
                    - GaryStatus == ALIVE:
                        {
                        - Follower == "Gary":
                            {
                             - GaryLocation != PlayerLocation:
                                ~AllDead = false
                            }
                        - Hater == "Gary":
                        - else:
                        ~AllDead = false
                        }
                }
        }
}
{
    - MissingKid != "Laura":
        {
            - PlayerName != "Laura":
                {
                    - LauraStatus == ALIVE:
                        {
                        - Follower == "Laura":
                            {
                             - LauraLocation != PlayerLocation:
                                ~AllDead = false
                            }
                        - Hater == "Laura":
                        - else:
                        ~AllDead = false
                        }
                }
        }
}
{
    - MissingKid != "Megan":
        {
            - PlayerName != "Megan":
                {
                    - MeganStatus == ALIVE:
                        {
                        - Follower == "Megan":
                            {
                             - MeganLocation != PlayerLocation:
                                ~AllDead = false
                            }
                        - Hater == "Megan":
                        - else:
                        ~AllDead = false
                        }
                }
        }
}
{
    - MissingKid != "Pete":
        {
            - PlayerName != "Pete":
                {
                    - PeteStatus == ALIVE:
                        {
                        - Follower == "Pete":
                            {
                             - PeteLocation != PlayerLocation:
                                ~AllDead = false
                            }
                        - Hater == "Pete":
                        - else:
                        ~AllDead = false
                        }
                }
        }
}
~return

=== CharactersPresentTest ===
~temp CanTalk = false

{
    - CliffLocation == PlayerLocation: 
        {
            -CliffStatus == ALIVE:
                ~CanTalk = true
        }
}
{
    - EmilyLocation == PlayerLocation: 
        {
            -EmilyStatus == ALIVE:
                ~CanTalk = true
        }
}
{
    - FrancineLocation == PlayerLocation: 
        {
            -FrancineStatus == ALIVE:
                ~CanTalk = true
        }
}
{
    - GaryLocation == PlayerLocation: 
        {
            -GaryStatus == ALIVE:
                ~CanTalk = true
        }
}
{
    - LauraLocation == PlayerLocation: 
        {
            -LauraStatus == ALIVE:
                ~CanTalk = true
        }
}
{
    - MeganLocation == PlayerLocation: 
        {
            -MeganStatus == ALIVE:
                ~CanTalk = true
        }
}
{
    - PeteLocation == PlayerLocation: 
        {
            -PeteStatus == ALIVE:
                ~CanTalk = true
        }
}

{
    - CanTalk == true:
        +[Talk to...]
                <-CharacterOptionCheck
}

+[Wait.]
    ~Waiting = true
    {AddTime(1)}
    ->GoToLocation

=== CharacterOptionCheck ===
{
    - CliffLocation == PlayerLocation: 
        {
            -CliffStatus == DEAD:
            -else: <-CliffTalk
        }
}
{
    - EmilyLocation == PlayerLocation: 
        {
            -EmilyStatus == DEAD:
            -else: <-EmilyTalk
        }
}
{
    - FrancineLocation == PlayerLocation: 
        {
            -FrancineStatus == DEAD:
            -else: <-FrancineTalk
        }
}
{
    - GaryLocation == PlayerLocation: 
        {
            -GaryStatus == DEAD:
            -else: <-GaryTalk
        }
}
{
    - LauraLocation == PlayerLocation: 
        {
            -LauraStatus == DEAD:
            -else: <-LauraTalk
        }
}
{
    - MeganLocation == PlayerLocation: 
        {
            -MeganStatus == DEAD:
            -else: <-MeganTalk
        }
}
{
    - PeteLocation == PlayerLocation: 
        {
            -PeteStatus == DEAD:
            -else: <-PeteTalk
        }
}
+[Never mind.]
    ->GoToLocation

=== CharacterLocationCheck ===
{
    - CliffLocation == PlayerLocation:
        {
            - CliffStatus == DEAD: Cliff's body is here.
            - Scary == false:
                {
                    - CliffLocation == KITCHEN: Cliff is here, cooking some kind of vegetable dish.
                        ~temp cliffcooking = true
                }
            - else: Cliff is here.
        }
}
{
    - EmilyLocation == PlayerLocation: 
        {
            - EmilyStatus == DEAD: Emily's body is here.
            - else: Emily is here.
        }
}
{
    - FrancineLocation == PlayerLocation: 
        {
            - FrancineStatus == DEAD: Francine's body is here.
            - Scary == false:
                {
                    - FrancineLocation == STUDY: Francine is here, quietly poring over the old books and scientific ephemera on display.
                }
            - else: Francine is here.
        }
}
{
    - GaryLocation == PlayerLocation: 
        {
            - GaryStatus == DEAD: Gary's body is here.
            - else: Gary is here.
        }
}
{
    - LauraLocation == PlayerLocation: 
        {
            - LauraStatus == DEAD: Laura's body is here.
            - else: Laura is here.
        }
}
{
    - MeganLocation == PlayerLocation:
        { 
            - MeganStatus == DEAD: Megan's body is here.
            - Scary == false:
                {
                    - Follower != "Megan":
                {
                    - MeganLocation == LIVINGROOM: Megan is here, sitting on the floor in front of a strange video game console, eyes glued to the screen of an old TV.
                    - else: Megan is here.
                }
                }
            - else: Megan is here.
        }
}
{
    - PeteLocation == PlayerLocation:
        {
            - PeteStatus == DEAD: Pete's body is here.
            - Scary == false:
                {
                    - PeteLocation == KITCHEN: Pete is here, doing something involving bread crumbs.
                }
            - else: Pete is here.
        }
}
{
    - CreechLocation == PlayerLocation: 
        {   
            - CreechMoveTimer > 0: There is a creature here, looming over you.
            - else: There is a creature here.
        }
}
->DONE

=== function KidTimer(x) ===
~KidMoveTimer = KidMoveTimer + x
//KID MOVE TRY
{
    - Scary == true:
    //SCARY IS TRUE
    {
    - CreechLocation != PlayerLocation:
            //CREECH IS NOT WITH PLAYER
            {
                -CliffLocation != PlayerLocation:
                    {
                    -CliffMoved == true:
                    -CliffStatus == ALIVE:
                        {
                            - Follower == "Cliff":
                                {
                                - KidMoveTimer > 0:
                                    {MoveToPlayer("Cliff")}
                                    ~CliffMoved = true
                                }    
                            - CliffPoints >5:
                            {shuffle:
                                - KidMoveTimer > 0:
                                {MoveToPlayer("Cliff")}
                                ~CliffMoved = true
                                -
                            }
                            - else:
                            {
                                - KidMoveTimer > 1:
                                {shuffle:
                                -
                                - {MoveToPlayer("Cliff")} //CLIFF MOVED
                                }
                            }
                        }
                    }
            }
            {
                -EmilyLocation != PlayerLocation:
                    {
                    -EmilyMoved == true:
                    -EmilyStatus == ALIVE:
                        {
                            - Follower == "Emily":
                                {
                                - KidMoveTimer > 0:
                                    {MoveToPlayer("Emily")}
                                    ~EmilyMoved = true
                                }  
                            - EmilyPoints >5:
                            {shuffle:
                                - KidMoveTimer > 0:
                                {MoveToPlayer("Emily")}
                                ~EmilyMoved = true
                                -
                            }
                            - else:
                            {
                                - KidMoveTimer > 1:
                                {shuffle:
                                -
                                - {MoveToPlayer("Emily")} //EMILY MOVED
                                }
                            }
                        }
                    }
            }
            {
                -FrancineLocation != PlayerLocation:
                    {
                    -FrancineMoved == true:
                    -FrancineStatus == ALIVE:
                        {
                            - Follower == "Francine":
                                {
                                - KidMoveTimer > 0:
                                    {MoveToPlayer("Francine")}
                                    ~FrancineMoved = true
                                }  
                            - FrancinePoints >5:
                            {shuffle:
                                - KidMoveTimer > 0:
                                {MoveToPlayer("Francine")}
                                ~FrancineMoved = true
                                -
                            }
                            - else:
                            {
                                - KidMoveTimer > 1:
                                {shuffle:
                                -
                                - {MoveToPlayer("Francine")} //FRANCINE MOVED
                                }
                            }
                        }
                    }
            }
            {
                -GaryLocation != PlayerLocation:
                    {
                    -GaryMoved == true:
                    -GaryStatus == ALIVE:
                        {
                            - Follower == "Gary":
                                {
                                - KidMoveTimer > 0:
                                    {MoveToPlayer("Gary")}
                                    ~GaryMoved = true
                                }  
                            - GaryPoints >5:
                            {shuffle:
                                - KidMoveTimer > 0:
                                {MoveToPlayer("Gary")}
                                ~GaryMoved = true
                                -
                            }
                            - else:
                            {
                                - KidMoveTimer > 1:
                                {shuffle:
                                -
                                - {MoveToPlayer("Gary")} //GARY MOVED
                                }
                            }
                        }
                    }
            }
            {
                -LauraLocation != PlayerLocation:
                    {
                    -LauraMoved == true:
                    -LauraStatus == ALIVE:
                        {
                            - Follower == "Laura":
                                {
                                - KidMoveTimer > 0:
                                    {MoveToPlayer("Laura")}
                                    ~LauraMoved = true
                                }  
                            - LauraPoints >5:
                            {shuffle:
                                - KidMoveTimer > 0:
                                {MoveToPlayer("Laura")}
                                ~LauraMoved = true
                                -
                            }
                            - else:
                            {
                                - KidMoveTimer > 1:
                                {shuffle:
                                -
                                - {MoveToPlayer("Laura")} //LAURA MOVED
                                }
                            }
                        }
                    }
            }
            {
                -MeganLocation != PlayerLocation:
                //MEGAN IS NOT WITH PLAYER
                    {
                    -MeganMoved == true:
                    -MeganStatus == ALIVE:
                    //MEGAN IS ALIVE
                        {
                            - Follower == "Megan":
                                {
                                - KidMoveTimer > 0:
                                    {MoveToPlayer("Megan")}
                                    ~MeganMoved = true
                                }  
                            - MeganPoints >5:
                            //MEGAN GOOD
                            {shuffle:
                                - KidMoveTimer > 0:
                                //MEGAN GOOD MOVE
                                {MoveToPlayer("Megan")}
                                ~MeganMoved = true
                                -
                            }
                            - else:
                            //MEGAN BAD
                            {
                                - KidMoveTimer > 1:
                                {shuffle:
                                - //MEGAN BAD FAIL TO MOVE
                                - {MoveToPlayer("Megan")} //MEGAN BAD MOVED
                                }
                            }
                        }
                    }
            }
            {
                -PeteLocation != PlayerLocation:
                    {
                    -PeteMoved == true:
                    -PeteStatus == ALIVE:
                        {
                            - Follower == "Pete":
                                {
                                - KidMoveTimer > 0:
                                    {MoveToPlayer("Pete")}
                                    ~PeteMoved = true
                                }  
                            - PetePoints >5:
                            {shuffle:
                                - KidMoveTimer > 0:
                                {MoveToPlayer("Pete")}
                                ~PeteMoved = true
                                -
                            }
                            - else:
                            {
                                - KidMoveTimer > 1:
                                {shuffle:
                                -
                                - {MoveToPlayer("Pete")} //PETE MOVED
                                }
                            }
                        }
                    }
            }
            {
                - KidMoveTimer > 1:
                    ~CliffMoved = false
                    ~EmilyMoved = false
                    ~FrancineMoved = false
                    ~GaryMoved = false
                    ~LauraMoved = false
                    ~MeganMoved = false
                    ~PeteMoved = false
                    ~KidMoveTimer = 0
            }
    }
    - else:
    {
        - CliffLocation != PlayerLocation:
            {
                - CliffStatus == ALIVE:
                    {
                        - Follower == "Cliff":
                        {
                        - KidMoveTimer > 0:
                            {MoveToPlayer("Cliff")}
                        }
                        - Hater == "Cliff":
                        {
                        - HaterLevel1Done != true:
                            {
                            - KidMoveTimer > 0:
                                {MoveToPlayer("Cliff")}
                            }
                        }
                    }
            }
    }
    {
        - EmilyLocation != PlayerLocation:
            {
                - EmilyStatus == ALIVE:
                    {
                        - Follower == "Emily":
                        {
                        - KidMoveTimer > 0:
                            {MoveToPlayer("Emily")}
                        }
                        - Hater == "Emily":
                        {
                        - HaterLevel1Done != true:
                            {
                            - KidMoveTimer > 0:
                                {MoveToPlayer("Emily")}
                            }
                        }
                    }
            }
    }
	{
        - FrancineLocation != PlayerLocation:
            {
                - FrancineStatus == ALIVE:
                    {
                        - Follower == "Francine":
                        {
                        - KidMoveTimer > 0:
                            {MoveToPlayer("Francine")}
                        }
                        - Hater == "Francine":
                        {
                        - HaterLevel1Done != true:
                            {
                            - KidMoveTimer > 0:
                                {MoveToPlayer("Francine")}
                            }
                        }
                        - else:
                            {
                                - CurrentMinute > 36:
                                {
                                    - KidMoveTimer >= 0:
                                        {MoveToPlayer("Francine")}
                                }
                            }
                    }
            }
    }
	{
        - GaryLocation != PlayerLocation:
            {
                - GaryStatus == ALIVE:
                    {
                        - Follower == "Gary":
                        {
                        - KidMoveTimer > 0:
                            {MoveToPlayer("Gary")}
                        }
                        - Hater == "Gary":
                        {
                        - HaterLevel1Done != true:
                            {
                            - KidMoveTimer > 0:
                                {MoveToPlayer("Gary")}
                            }
                        }
                    }
            }
    }
	{
        - LauraLocation != PlayerLocation:
            {
                - LauraStatus == ALIVE:
                    {
                        - Follower == "Laura":
                        {
                        - KidMoveTimer > 0:
                            {MoveToPlayer("Laura")}
                        }
                        - Hater == "Laura":
                        {
                        - HaterLevel1Done != true:
                            {
                            - KidMoveTimer > 0:
                                {MoveToPlayer("Laura")}
                            }
                        }
                    }
            }
    }
	{
        - MeganLocation != PlayerLocation:
            {
                - MeganStatus == ALIVE:
                    {
                        - Follower == "Megan":
                        {
                        - KidMoveTimer > 0:
                            {MoveToPlayer("Megan")}
                        }
                        - Hater == "Megan":
                        {
                        - HaterLevel1Done != true:
                            {
                            - KidMoveTimer > 0:
                                {MoveToPlayer("Megan")}
                            }
                        }
                    }
            }
    }
	{
        - PeteLocation != PlayerLocation:
            {
                - PeteStatus == ALIVE:
                    {
                        - Follower == "Pete":
                        {
                        - KidMoveTimer > 0:
                            {MoveToPlayer("Pete")}
                        }
                        - Hater == "Pete":
                        {
                        - HaterLevel1Done != true:
                            {
                            - KidMoveTimer > 0:
                                {MoveToPlayer("Pete")}
                            }
                        }
                    }
            }
    }
}
//HaterPresent test
{
    - Hater == "Cliff":
        {
            - CliffLocation == PlayerLocation:
            ~HaterPresent = true
            - else:
            ~HaterPresent = false
        }
    - Hater == "Emily":
        {
            - EmilyLocation == PlayerLocation:
            ~HaterPresent = true
            - else:
            ~HaterPresent = false
        }
    - Hater == "Francine":
        {
            - FrancineLocation == PlayerLocation:
            ~HaterPresent = true
            - else:
            ~HaterPresent = false
        }
    - Hater == "Gary":
        {
            - GaryLocation == PlayerLocation:
            ~HaterPresent = true
            - else:
            ~HaterPresent = false
        }
    - Hater == "Laura":
        {
            - LauraLocation == PlayerLocation:
            ~HaterPresent = true
            - else:
            ~HaterPresent = false
        }
    - Hater == "Megan":
        {
            - MeganLocation == PlayerLocation:
            ~HaterPresent = true
            - else:
            ~HaterPresent = false
        }
    - Hater == "Pete":
        {
            - PeteLocation == PlayerLocation:
            ~HaterPresent = true
            - else:
            ~HaterPresent = false
        }
}

=== function CreechTimer(x) ===
{
- CreechDead == false:
{ 
    - CreechMove == true:
    ~CreechMoveTimer = CreechMoveTimer + x
    {CanMurdKid()}
    {
        - PowerOn: 
        {
            - LoopCount == 0:
                ~CreechUnit = 0
            - LoopCount > 3:
            {shuffle:
                - 
                ~CreechUnit = 1
                -
                ~CreechUnit = 1
                -
                ~CreechUnit = 2
            }
            - else:
            ~CreechUnit = 1
        }
        - else: ~CreechUnit = 0
    }
    {
        - CreechMoveTimer > CreechUnit:
        {
            - KidMurd == "Cliff":
            {
                - Follower == "Cliff":
                {shuffle:
                    - ~CliffDies = true
                    - ~CliffDies = true
                    -
                }
                - else:
                ~CliffDies = true
            }
                ~KidMurd = "Crobus"
            - KidMurd == "Emily":
            {
                - Follower == "Emily":
                {shuffle:
                    - ~EmilyDies = true
                    - ~EmilyDies = true
                    -
                }
                - else:
                ~EmilyDies = true
            }
                ~KidMurd = "Crobus"
            - KidMurd == "Francine":
            {
                - Follower == "Francine":
                {shuffle:
                    - ~FrancineDies = true
                    - ~FrancineDies = true
                    -
                }
                - else:
                ~FrancineDies = true
            }
                ~KidMurd = "Crobus"
            - KidMurd == "Gary":
            {
                - Follower == "Gary":
                {shuffle:
                    - ~GaryDies = true
                    - ~GaryDies = true
                    -
                }
                - else:
                ~GaryDies = true
            }
                ~KidMurd = "Crobus"
            - KidMurd == "Laura":
            {
                - Follower == "Laura":
                {shuffle:
                    - ~LauraDies = true
                    - ~LauraDies = true
                    -
                }
                - else:
                ~LauraDies = true
            }
                ~KidMurd = "Crobus"
            - KidMurd == "Megan":
            {
                - Follower == "Megan":
                {shuffle:
                    - ~MeganDies = true
                    - ~MeganDies = true
                    -
                }
                - else:
                ~MeganDies = true
            }
                ~KidMurd = "Crobus"
            - KidMurd == "Pete":
            {
                - Follower == "Pete":
                {shuffle:
                    - ~PeteDies = true
                    - ~PeteDies = true
                    -
                }
                - else:
                ~PeteDies = true
            }
                ~KidMurd = "Crobus"
            - CreechLocation == PlayerLocation:
                //~CreechKillTimer = CreechKillTimer + 1
                {
                    - CreechKillTimer > 0:
                        {
                            - Follower == BFF:
                                {
                                - BFF != "Crobus":
                                    {shuffle:
                                    -
                                    The creature lunges for you, but {Follower} is able to push it away!
                                    -
                                    ~CreechKill = true
                                    }
                                    - else:
                                    ~CreechKill = true
                                }
                            - else:
                                ~CreechKill = true
                        }
                }
                ~CreechKillTimer = CreechKillTimer + 1
            - else:
                ~CreechMoveTimer = 0
                ~CreechKillTimer = 0
                {MoveToPlayer("Creech")}
        }
    }
}
}
=== function CanMurdKid ===

{
    - CreechLocation != CliffLocation:
    - CliffStatus == DEAD:
    - else:
        ~KidMurd = "Cliff"
}
{
    - CreechLocation != EmilyLocation:
    - EmilyStatus == DEAD:
    - else:
        ~KidMurd = "Emily"
}
{
    - CreechLocation != FrancineLocation:
    - FrancineStatus == DEAD:
    - else:
        ~KidMurd = "Francine"
}
{
    - CreechLocation != GaryLocation:
    - GaryStatus == DEAD:
    - else:
        ~KidMurd = "Gary"
}
{
    - CreechLocation != LauraLocation:
    - LauraStatus == DEAD:
    - else:
        ~KidMurd = "Laura"
}
{
    - CreechLocation != MeganLocation:
    - MeganStatus == DEAD:
    - else:
        ~KidMurd = "Megan"
}
{
    - CreechLocation != PeteLocation:
    - PeteStatus == DEAD:
    - else:
        ~KidMurd = "Pete"
}
~return

=== function ClockTime ===
~temp TempMinute = 0
{
- CurrentMinute < 10:
    ~TempMinute = "0" + CurrentMinute
- else: 
    ~TempMinute = CurrentMinute
}
~PrintTime = CurrentHour + ":" + TempMinute
~return PrintTime

=== function SetSpeaker(x) ===
~Speaker = x
~return

=== function getPronoun(x, y) ===
{
- y == "subject":
    {
    - x == "Cliff":
        ~return "he"
    - x == "Gary":
        ~return "he"
    - x == "Pete":
        ~return "he"
    - else:
        ~return "she"
    }
- y == "object":
    {
    - x == "Cliff":
        ~return "him"
    - x == "Gary":
        ~return "him"
    - x == "Pete":
        ~return "him"
    - else:
        ~return "her"
    }
- y == "possessive":
    {
    - x == "Cliff":
        ~return "his"
    - x == "Gary":
        ~return "his"
    - x == "Pete":
        ~return "his"
    - else:
        ~return "her"
    }
}

=== function getPronounCaps(x, y) ===
{
- y == "subject":
    {
    - x == "Cliff":
        ~return "He"
    - x == "Gary":
        ~return "He"
    - x == "Pete":
        ~return "He"
    - else:
        ~return "She"
    }
- y == "object":
    {
    - x == "Cliff":
        ~return "Him"
    - x == "Gary":
        ~return "Him"
    - x == "Pete":
        ~return "Him"
    - else:
        ~return "Her"
    }
- y == "possessive":
    {
    - x == "Cliff":
        ~return "His"
    - x == "Gary":
        ~return "His"
    - x == "Pete":
        ~return "His"
    - else:
        ~return "Her"
    }
}

=== function Swear(x) ===
{
    - x == "Cliff":
        ~SwearLevel = 1
    - x == "Emily":
        ~SwearLevel = 2
    - x == "Laura":
        ~SwearLevel = 1
    - x == "Megan":
        ~SwearLevel = 2
    - x == "Pete":
        ~SwearLevel = 2
    - else:
        ~SwearLevel = 0
}
~return

=== function Greeting(x) ===
{
    - x == "Emily":
        ~return "Hi"
    - x == "Gary":
        ~return "Hi"
    - x == "Megan":
        ~return "Hi"
    - else:
        ~return "Hey"
}

=== PreGame_NameChoice ===

Pick a Kid.

* [Cliff]
    ~PlayerName = "Cliff"
    ~CliffLocation = PLAYER
* [Emily]
    ~PlayerName = "Emily"
    ~EmilyLocation = PLAYER
* [Francine]
    ~PlayerName = "Francine"
    ~FrancineLocation = PLAYER
* [Gary]
    ~PlayerName = "Gary"
    ~GaryLocation = PLAYER
* [Laura]
    ~PlayerName = "Laura"
    ~LauraLocation = PLAYER
* [Megan]
    ~PlayerName = "Megan"
    ~MeganLocation = PLAYER
* [Pete]
    ~PlayerName = "Pete"
    ~PeteLocation = PLAYER
    //NAME CHOSEN... GOING TO MISSINGKID RANDOM
- -> PreGame_MissingKid

=== PreGame_MissingKid ===
//PICKING RANDOM KID
{shuffle:
    - ~MissingKid = "Cliff" 
    - ~MissingKid = "Emily"
    - ~MissingKid = "Francine"
    - ~MissingKid = "Gary"
    - ~MissingKid = "Laura"
    - ~MissingKid = "Megan"
    - ~MissingKid = "Pete"
}
//COMPARING TO PLAYERNAME
{ MissingKid != PlayerName:
    {
        - MissingKid == "Cliff":
            ~CliffStatus = MISSING
            ~CliffLocation = MISSING
        - MissingKid == "Emily":
            ~EmilyLocation = MISSING
            ~EmilyStatus = MISSING
        - MissingKid == "Francine":
            ~FrancineLocation = MISSING
            ~FrancineStatus = MISSING
        - MissingKid == "Gary":
            ~GaryLocation = MISSING
            ~GaryStatus = MISSING
        - MissingKid == "Laura":
            ~LauraLocation = MISSING
            ~LauraStatus = MISSING
        - MissingKid == "Megan":
            ~MeganLocation = MISSING
            ~MeganStatus = MISSING
        - MissingKid == "Pete":
            ~PeteLocation = MISSING
            ~PeteStatus = MISSING
    }
    //GOING TO DOORGETTER
    ->PreGame_DoorGetter
- else:
    //SAME AS PLAYER... TRYING AGAIN
    ->PreGame_MissingKid
}

=== PreGame_DoorGetter ===
//RANDOM DOORGETTER
{shuffle:
    - ~DoorGetter = "Cliff" 
    - ~DoorGetter = "Emily"
    - ~DoorGetter = "Francine"
    - ~DoorGetter = "Gary"
    - ~DoorGetter = "Laura"
    - ~DoorGetter = "Megan"
    - ~DoorGetter = "Pete"
}
//DOORGETTER CHOSEN... COMPARING TO PLAYER AND MISSINGKID
{ DoorGetter != PlayerName:
    //DIFFERENT FROM PLAYER
    { 
        - DoorGetter != MissingKid:
        {
            - DoorGetter == "Cliff":
                ~CliffLocation = FOYER
            - DoorGetter == "Emily":
                ~EmilyLocation = FOYER
            - DoorGetter == "Francine":
                ~FrancineLocation = FOYER
            - DoorGetter == "Gary":
                ~GaryLocation = FOYER
            - DoorGetter == "Laura":
                ~LauraLocation = FOYER
            - DoorGetter == "Megan":
                ~MeganLocation = FOYER
            - DoorGetter == "Pete":
                ~PeteLocation = FOYER
        }
        //DIFFERENT FROM MISSINGKID... CHOOSE LOOP
        ->PreGame_LoopType
        - else: 
        //SAME AS MISSINGKID, TRYING AGAIN
        ->PreGame_DoorGetter
    }
//SAME AS PLAYER, TRYING AGAIN
- else:
    ->PreGame_DoorGetter
}

=== PreGame_LoopType ===
->Intro
Begin the game with a full loop or mini loop?
(This isn't a feature of the full game, just for demo purposes.)
* [Full loop.]
    ->Intro
* [Mini loop.]
    ->MiniLoop


=== DoorGetterMove ===
{
    - DoorGetterLeft == true:
    - Scary == true:
    - Follower == DoorGetter:
    - else:
        ~FrontDoor = true
        {
            - DoorGetter == "Cliff":
                Cliff heads to the kitchen as you set down your bag and begin to leave the foyer.
                ~CliffLocation = KITCHEN
            - DoorGetter == "Emily":
                Emily heads to the living room as you set down your bag and begin to leave the foyer.
                ~EmilyLocation = LIVINGROOM
            - DoorGetter == "Francine":
                Francine heads upstairs to the study as you set down your bag and begin to leave the foyer.
                ~FrancineLocation = Study
            - DoorGetter == "Gary":
                Gary heads to the living room as you set down your bag and begin to leave the foyer.
                ~GaryLocation = LIVINGROOM
            - DoorGetter == "Laura":
                Laura heads to the living room as you set down your bag and begin to leave the foyer.
                ~LauraLocation = LIVINGROOM
            - DoorGetter == "Megan":
                Megan heads to the living room as you set down your bag. She plops down in front of the TV.
                ~MeganLocation = LIVINGROOM
            - DoorGetter == "Pete":
                Pete heads to the kitchen as you set down your bag and begin to leave the foyer.
                ~PeteLocation = KITCHEN
        }
}
~DoorGetterLeft = true
->DONE

=== DoorGetterMoveSilent ===
{
    - DoorGetterLeft == true:
    - Scary == true:
    - Follower == DoorGetter:
    - else:
        {
            - DoorGetter == "Cliff":
                ~CliffLocation = KITCHEN
            - DoorGetter == "Emily":
                ~EmilyLocation = LIVINGROOM
            - DoorGetter == "Francine":
                ~FrancineLocation = Study
            - DoorGetter == "Gary":
                ~GaryLocation = LIVINGROOM
            - DoorGetter == "Laura":
                ~LauraLocation = LIVINGROOM
            - DoorGetter == "Megan":
                ~MeganLocation = LIVINGROOM
            - DoorGetter == "Pete":
                ~PeteLocation = KITCHEN
        }
}
~DoorGetterLeft = true
->DONE

=== PlayerDie ===
The creature kills you!
->FlashbackHub

=== PlayerMoveDie ===
The creature lunges for you as you begin to run. The creature kills you!
->FlashbackHub


=== CliffKill ===
{
    - PlayerLocation == CliffLocation:
        {randomDeathLine("Cliff")}
    - else:
        You hear Cliff scream!
}
{
    - Follower == "Cliff":
        {StopFollowingSilent()}
}
~Scary = true
~CliffStatus = DEAD
~CliffDies = false
->DONE

=== EmilyKill ===
{
    - PlayerLocation == EmilyLocation:
        {randomDeathLine("Emily")}
    - else:
        You hear Emily scream!
}
{
    - Follower == "Emily":
        {StopFollowingSilent()}
}
~Scary = true
~EmilyStatus = DEAD
~EmilyDies = false
->DONE

=== FrancineKill ===
{
    - PlayerLocation == FrancineLocation:
        {randomDeathLine("Francine")}
    - else:
        You hear Francine scream!
}
{
    - Follower == "Francine":
        {StopFollowingSilent()}
}
~Scary = true
~FrancineStatus = DEAD
~FrancineDies = false
->DONE

=== GaryKill ===
{
    - PlayerLocation == GaryLocation:
        {randomDeathLine("Gary")}
    - else:
        You hear Gary scream!
}
{
    - Follower == "Gary":
        {StopFollowingSilent()}
}
~Scary = true
~GaryStatus = DEAD
~GaryDies = false
->DONE

=== LauraKill ===
{
    - PlayerLocation == LauraLocation:
        {randomDeathLine("Laura")}
    - else:
        You hear Laura scream!
}
{
    - Follower == "Laura":
        {StopFollowingSilent()}
}
~Scary = true
~LauraStatus = DEAD
~LauraDies = false
->DONE

=== MeganKill ===
{
    - PlayerLocation == MeganLocation:
        {randomDeathLine("Megan")}
    - else:
        You hear Megan scream!
}
{
    - Follower == "Megan":
        {StopFollowingSilent()}
}
~Scary = true
~MeganStatus = DEAD
~MeganDies = false
->DONE

=== PeteKill ===
{
    - PlayerLocation == PeteLocation:
        {randomDeathLine("Pete")}
    - else:
        You hear Pete scream!
}
{
    - Follower == "Pete":
        {StopFollowingSilent()}
}
~Scary = true
~PeteStatus = DEAD
~PeteDies = false
->DONE

=== MiniLoopChoice ===
{
- "Cliff" != MissingKid:
    {
        - PlayerName != "Cliff":
+ [Talk to Cliff.]
    {AddPoints("Cliff", 1)} {SetSpeaker("Cliff")}
    }
}

{
- "Emily" != MissingKid:
    {
        - PlayerName != "Emily":
+ [Talk to Emily.]
    {AddPoints("Emily", 1)} {SetSpeaker("Emily")}
    }
}

{
- "Francine" != MissingKid:   
    {
        - PlayerName != "Francine":
+ [Talk to Francine.]
    {AddPoints("Francine", 1)} {SetSpeaker("Francine")}
    }
}

{
- "Gary" != MissingKid:    
+ [Talk to Gary.]
    {
        - PlayerName != "Gary":
    {AddPoints("Gary", 1)} {SetSpeaker("Gary")}
    }
}

{
- "Laura" != MissingKid:   
    {
        - PlayerName != "Laura":
+ [Talk to Laura.]
    {AddPoints("Laura", 1)} {SetSpeaker("Laura")}
    }
}

{
- "Megan" != MissingKid:  
    {
        - PlayerName != "Megan":
+ [Talk to Megan.]
    {AddPoints("Megan", 1)} {SetSpeaker("Megan")}
    }
}

{
- "Pete" != MissingKid:   
    {
        - PlayerName != "Pete":
+ [Talk to Pete.]
    {AddPoints("Pete", 1)} {SetSpeaker("Pete")}
    }
}
+ [Wait it out.]
    ->MiniLoop_End

- {
    - Speaker == MissingKid: 
        {missingKidLine(Speaker)}
        ->MiniLoopChoice
    - Speaker == Hater:
        You attempt to chat with {Speaker}. {getPronounCaps(Speaker, "subject")} regards you with cold eyes. It gets under your skin. {Speaker} walks away.
        ->MiniLoopChoice
    - else:
        You chat with {Speaker}, who regards you warmly.
}
->MiniLoop_End

=== function HaterDude ===
{
    - BFF == "Crobus":
    {
        - CliffPoints > BFFGoal:
            ~BFF = "Cliff"
        - EmilyPoints > BFFGoal:
            ~BFF = "Emily"
        - FrancinePoints > BFFGoal:
            ~BFF = "Francine"
        - GaryPoints > BFFGoal:
            ~BFF = "Gary"
        - LauraPoints > BFFGoal:
            ~BFF = "Laura"
        - MeganPoints > BFFGoal:
            ~BFF = "Megan"
        - PetePoints > BFFGoal:
            ~BFF = "Pete"
    }
    - BFF == "Bazinga":
    {
        - Hater == "Crobus":
            ~temp HaterPointCeiling = BFFGoal
            {
            - CliffPoints > 0:
                {
                    - CliffPoints < HaterPointCeiling:
                        {shuffle:
                                -
                            ~Hater = "Cliff"
                            ~HaterPointCeiling = CliffPoints
                        }
                    - CliffPoints == HaterPointCeiling:
                        {shuffle:
                            -
                            ~Hater = "Cliff"
                            ~HaterPointCeiling = CliffPoints
                            -
                        }
                }
            }
            {
            - EmilyPoints > 0:
                {
                    - EmilyPoints < HaterPointCeiling:
                        {shuffle:
                                -
                            ~Hater = "Emily"
                            ~HaterPointCeiling = EmilyPoints
                        }
                    - EmilyPoints == HaterPointCeiling:
                        {shuffle:
                            -
                            ~Hater = "Emily"
                            ~HaterPointCeiling = EmilyPoints
                            -
                        }
                }
            }
            {
            - FrancinePoints > 0:
                {
                    - FrancinePoints < HaterPointCeiling:
                        {shuffle:
                                -
                            ~Hater = "Francine"
                            ~HaterPointCeiling = FrancinePoints
                        }
                    - FrancinePoints == HaterPointCeiling:
                        {shuffle:
                            -
                            ~Hater = "Francine"
                            ~HaterPointCeiling = FrancinePoints
                            -
                        }
                }
            }
            {
            - GaryPoints > 0:
                {
                    - GaryPoints < HaterPointCeiling:
                        {shuffle:
                                -
                            ~Hater = "Gary"
                            ~HaterPointCeiling = GaryPoints
                        }
                    - GaryPoints == HaterPointCeiling:
                        {shuffle:
                            -
                            ~Hater = "Gary"
                            ~HaterPointCeiling = GaryPoints
                            -
                        }
                }
            }
            {
            - LauraPoints > 0:
                {
                    - LauraPoints < HaterPointCeiling:
                        {shuffle:
                                -
                            ~Hater = "Laura"
                            ~HaterPointCeiling = LauraPoints
                        }
                    - LauraPoints == HaterPointCeiling:
                        {shuffle:
                            -
                            ~Hater = "Laura"
                            ~HaterPointCeiling = LauraPoints
                            -
                        }
                }
            }
            {
            - MeganPoints > 0:
                {
                    - MeganPoints < HaterPointCeiling:
                        {shuffle:
                                -
                            ~Hater = "Megan"
                            ~HaterPointCeiling = MeganPoints
                        }
                    - MeganPoints == HaterPointCeiling:
                        {shuffle:
                            -
                            ~Hater = "Megan"
                            ~HaterPointCeiling = MeganPoints
                            -
                        }
                }
            }
            {
            - PetePoints > 0:
                {
                    - PetePoints < HaterPointCeiling:
                        {shuffle:
                                -
                            ~Hater = "Pete"
                            ~HaterPointCeiling = PetePoints
                        }
                    - PetePoints == HaterPointCeiling:
                        {shuffle:
                            -
                            ~Hater = "Pete"
                            ~HaterPointCeiling = PetePoints
                            -
                        }
                }
            }
        }
        - else:
        ~HaterMin = 0
        {
            - Hater == "Crobus":
                {
                    - CliffPoints > HaterMin:
                    {
                        - BFF != "Cliff":
                        ~Hater = "Cliff"
                        ~HaterMin = CliffPoints
                    }
                }
                {
                    - EmilyPoints > HaterMin:
                    {
                        - BFF != "Emily":
                        ~Hater = "Emily"
                        ~HaterMin = EmilyPoints
                    }
                }
                {
                    - FrancinePoints > HaterMin:
                    {
                        - BFF != "Francine":
                        ~Hater = "Francine"
                        ~HaterMin = FrancinePoints
                    }
                }
                {
                    - GaryPoints > HaterMin:
                    {
                        - BFF != "Gary":
                        ~Hater = "Gary"
                        ~HaterMin = GaryPoints
                    }
                }
                {
                    - LauraPoints > HaterMin:
                    {
                        - BFF != "Laura":
                        ~Hater = "Laura"
                        ~HaterMin = LauraPoints
                    }
                }
                {
                    - MeganPoints > HaterMin:
                    {
                        - BFF != "Megan":
                        ~Hater = "Megan"
                        ~HaterMin = MeganPoints
                    }
                }
                {
                    - PetePoints > HaterMin:
                    {
                        - BFF != "Pete":
                        ~Hater = "Pete"
                        ~HaterMin = PetePoints
                    }
                }
        }
}
~return

=== function secondMostPointsCheck ===
~HaterMin = 0
        {
            - Hater != "Crobus":
                ~SecondMostPoints = Hater
            - Hater == "Crobus":
                {
                    - CliffPoints > HaterMin:
                    {
                        - BFF != "Cliff":
                        ~SecondMostPoints = "Cliff"
                        ~HaterMin = CliffPoints
                    }
                }
                {
                    - EmilyPoints > HaterMin:
                    {
                        - BFF != "Emily":
                        ~SecondMostPoints = "Emily"
                        ~HaterMin = EmilyPoints
                    }
                }
                {
                    - FrancinePoints > HaterMin:
                    {
                        - BFF != "Francine":
                        ~SecondMostPoints = "Francine"
                        ~HaterMin = FrancinePoints
                    }
                }
                {
                    - GaryPoints > HaterMin:
                    {
                        - BFF != "Gary":
                        ~SecondMostPoints = "Gary"
                        ~HaterMin = GaryPoints
                    }
                }
                {
                    - LauraPoints > HaterMin:
                    {
                        - BFF != "Laura":
                        ~SecondMostPoints = "Laura"
                        ~HaterMin = LauraPoints
                    }
                }
                {
                    - MeganPoints > HaterMin:
                    {
                        - BFF != "Megan":
                        ~SecondMostPoints = "Megan"
                        ~HaterMin = MeganPoints
                    }
                }
                {
                    - PetePoints > HaterMin:
                    {
                        - BFF != "Pete":
                        ~SecondMostPoints = "Pete"
                        ~HaterMin = PetePoints
                    }
                }
        }
~return

=== function MostPointsCheck ===
{
- CliffPoints > 0:
    {
        - CliffPoints > highestPoints:
            ~MostPoints = "Cliff"
            ~highestPoints = CliffPoints
        - CliffPoints == highestPoints:
            {shuffle:
                -
                ~MostPoints = "Cliff"
                ~highestPoints = CliffPoints
                -
            }
    }
}
{
- EmilyPoints > 0:
    {
        - EmilyPoints > highestPoints:
            ~MostPoints = "Emily"
            ~highestPoints = EmilyPoints
        - EmilyPoints == highestPoints:
            {shuffle:
                -
                ~MostPoints = "Emily"
                ~highestPoints = EmilyPoints
                -
            }
    }
}
{
- FrancinePoints > 0:
    {
        - FrancinePoints > highestPoints:
            ~MostPoints = "Francine"
            ~highestPoints = FrancinePoints
        - FrancinePoints == highestPoints:
            {shuffle:
                -
                ~MostPoints = "Francine"
                ~highestPoints = FrancinePoints
                -
            }
    }
}
{
- GaryPoints > 0:
    {
        - GaryPoints > highestPoints:
            ~MostPoints = "Gary"
            ~highestPoints = GaryPoints
        - GaryPoints == highestPoints:
            {shuffle:
                -
                ~MostPoints = "Gary"
                ~highestPoints = GaryPoints
                -
            }
    }
}
{
- LauraPoints > 0:
    {
        - LauraPoints > highestPoints:
            ~MostPoints = "Laura"
            ~highestPoints = LauraPoints
        - LauraPoints == highestPoints:
            {shuffle:
                -
                ~MostPoints = "Laura"
                ~highestPoints = LauraPoints
                -
            }
    }
}
{
- MeganPoints > 0:
    {
        - MeganPoints > highestPoints:
            ~MostPoints = "Megan"
            ~highestPoints = MeganPoints
        - MeganPoints == highestPoints:
            {shuffle:
                -
                ~MostPoints = "Megan"
                ~highestPoints = MeganPoints
                -
            }
    }
}
{
- PetePoints > 0:
    {
        - PetePoints > highestPoints:
            ~MostPoints = "Pete"
            ~highestPoints = PetePoints
        - PetePoints == highestPoints:
            {shuffle:
                -
                ~MostPoints = "Pete"
                ~highestPoints = PetePoints
                -
            }
    }
}
~return

=== function StopFollowing ===
{
    - Follower == "Cliff":
        {
            - Scary == true:
                Cliff stops following you.
                ~Follower = "Crobus"
            - else:
                Cliff heads back to the kitchen.
                ~CliffLocation = KITCHEN
                ~Follower = "Crobus"
        }
    - Follower == "Emily":
        {
            - Scary == true:
                Emily stops following you.
                ~Follower = "Crobus"
            - else:
                Emily heads back to the living room.
                ~EmilyLocation = LIVINGROOM
                ~Follower = "Crobus"
        }
    - Follower == "Francine":
        {
            - Scary == true:
                Francine stops following you.
                ~Follower = "Crobus"
            - else:
                Francine heads back to the upstairs study.
                ~FrancineLocation = STUDY
                ~Follower = "Crobus"
        }
    - Follower == "Gary":
        {
            - Scary == true:
                Gary stops following you.
                ~Follower = "Crobus"
            - else:
                Gary heads back to the living room.
                ~GaryLocation = LIVINGROOM
                ~Follower = "Crobus"
        }
    - Follower == "Laura":
        {
            - Scary == true:
                Laura stops following you.
                ~Follower = "Crobus"
            - else:
                Laura heads back to the living room.
                ~LauraLocation = LIVINGROOM
                ~Follower = "Crobus"
        }
    - Follower == "Megan":
        {
            - Scary == true:
                Megan stops following you.
                ~Follower = "Crobus"
            - else:
                Megan heads back to the living room.
                ~MeganLocation = LIVINGROOM
                ~Follower = "Crobus"
        }
    - Follower == "Pete":
        {
            - Scary == true:
                Pete stops following you.
                ~Follower = "Crobus"
            - else:
                Pete heads back to the kitchen.
                ~PeteLocation = KITCHEN
                ~Follower = "Crobus"
        }
}
~return
=== function randomDeathLine(x) ===
{shuffle:
- The creature buries a knife in {x}'s heart. {getPronounCaps(x, "subject")} falls to the floor.
- The creature catches {x} and puts a knife in {getPronoun(x, "possessive")} back.
- The creature stabs {x} repeatedly in the stomach. {getPronounCaps(x, "subject")} falls to the floor.
- The creature gets its knife in {x}'s neck. There's a lot of blood. {getPronounCaps(x, "subject")} falls to the floor.
- {x} nearly escapes the creature, but it manages to kick {getPronoun(x,"object")} in the back of the leg, sending {getPronoun(x,"object")} to the floor. It kills {getPronoun(x,"object")} with a knife to the head.
- The creature digs its knife into {x}'s side, sending {getPronoun(x,"object")} to the ground, where the creature stabs {getPronoun(x,"object")} several more times.
- The creature puts its knife in {x}'s face. {getPronounCaps(x, "subject")} falls on {getPronoun(x,"possessive")} back.
- The creature stabs {x} in the chest. {getPronounCaps(x, "subject")} collapses in a heap.
- The creature charges {x}. You can't make out the details, but it stands back up and {x} doesn't.
}
=== function StopFollowingSilent ===
{
    - Follower == "Cliff":
~Follower = "Crobus"
    - Follower == "Emily":
~Follower = "Crobus"
    - Follower == "Francine":
~Follower = "Crobus"
    - Follower == "Gary":
~Follower = "Crobus"
    - Follower == "Laura":
~Follower = "Crobus"
    - Follower == "Megan":
~Follower = "Crobus"
    - Follower == "Pete":
~Follower = "Crobus"
}
~return

=== function HaterLocationTest ===
{
    - Hater == "Cliff":
        ~HaterLocation = CliffLocation
    - Hater == "Emily":
        ~HaterLocation = EmilyLocation
    - Hater == "Francine":
        ~HaterLocation = FrancineLocation
    - Hater == "Gary":
        ~HaterLocation = GaryLocation
    - Hater == "Laura":
        ~HaterLocation = LauraLocation
    - Hater == "Megan":
        ~HaterLocation = MeganLocation
    - Hater == "Pete":
        ~HaterLocation = PeteLocation
}
~return