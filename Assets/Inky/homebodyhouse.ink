CONST FOYER = 1
CONST LIVINGROOM = 2
CONST KITCHEN = 3
CONST STUDY = 4
CONST FRONTLAWN = 5
CONST OUTBUILDINGEXT = 6
CONST OUTBUILDINGINT = 7
CONST VESTIBULE = 8
CONST GARAGE = 9
CONST HALLWAY1 = 10
CONST HALLWAY2 = 11
CONST GUESTBEDROOM = 12
CONST MASTERBEDROOM = 13
CONST BATHROOM = 14
CONST CELLAR = 15
CONST ATTIC = 16
CONST GUESTHOUSE = 17
VAR CliffGreeting = false
VAR EmilyGreeting = false
VAR FrancineGreeting = false
VAR GaryGreeting = false
VAR LauraGreeting = false
VAR MeganGreeting = false
VAR PeteGreeting = false
VAR CellarOpen = false
VAR DoorGetterLeft = false
VAR Garage_Drywall = 0
VAR TriedFrontDoor = false
VAR Scary = false
VAR PowerOn = true
VAR PowerFlipped = false
VAR CurrentHour = 7
VAR CurrentMinute = 30
VAR PrintTime = "Woops"
VAR GuestHouseOpen = false
VAR GuestHouseKnowledge = false
VAR CellarLock = true
VAR CellarLockKnowledge = false
VAR AtticChuteKnowledge = false
VAR AtticMachineCovered = true
VAR AtticMachineFired = false
VAR GuestHouseTunnelKnowledge = false
VAR GuestHouseTunnelOpen = false
VAR CellarTunnelOpen = false
VAR AtticPower = 1
VAR HousePower = 1
VAR ReservePower = 0
VAR GeneratorOn = false
VAR GuestHousePageFound = false
VAR BiblePageFound = false
VAR AtticTwoPersonBoxOpen = false
VAR CellarMachineKnowledge = false
VAR CellarMachineLocked = true


=== Intro ===

{
    - Intro == 1: You arrive at the house around 7:30. You're late, but not too late. Not that there's much to do until morning, anyway. There's a long, accomodating stretch of driveway to park your car on, which is one of the benefits of renting an airbnb in the middle of nowhere. You pull into place behind your friends' cars, your headlights casting glancing shadows across the facade of the house.
    - else:
        You arrive at the house around 7:30.
}

You step out of your car, grabbing your bag from the passenger seat as you head up to the front sidewalk.

-> Front_Lawn



=== Front_Lawn ===
{SetPlayerLocation(FRONTLAWN)}

+[Head inside.]
    ->Front_Lawn_HeadInside
+[Look around.]
    ->Front_Lawn_Look
+{Front_Lawn_Look > 0} [Go to the side of the house.]
    {AddTime(2)}
    You make your way through the brush at the right side of the house and approach the outbuilding.
    ->Outbuilding_Ext

=== Front_Lawn_Look ===
{Front_Lawn_Look < 2: It's dark. The house is a midsized cottage in kind of a late craftstman style. The wood-shingled walls are a deep green-gray; earthy yet just unnatural enough to stand out against the gently quivering trees that surround it.}
{Front_Lawn_Look == 1: <> Through the rustle of the thinly-leaved branches of the forest, you notice a dim light being cast over a small outbuilding to the right of the house.}
{
    - Front_Lawn_Look > 1: 
    {
    - PowerOn == true: The house, eerie yet beautiful in its funereal colors, looms above you. An inviting light shines through the thick glass of the front door. To the right is a dimly-lit old outbuilding.
    - PowerOn == false: The house, eerie yet beautiful in its funereal colors, looms above you. Through the darkness you can make out the porch leading to the entrance to the house, and a clearing to the right, where sits an old outbuilding.
    }
}
->Front_Lawn

=== Front_Lawn_HeadInside ===
You step onto the front porch and try the front doorknob. It's locked. You give the door a heavy knock.
{AddTime(1)} After a few moments, {DoorGetter} answers.

~Speaker = DoorGetter
{
-Hater == DoorGetter:
<> {getPronounCaps(DoorGetter, "subject")} looks at you like you've done something wrong. "Hello, {PlayerName}," {getPronoun(DoorGetter, "subject")} says detachedly, pulling open the door without another word.
-else:
{
    - PowerOn == true:
"{Greeting(Speaker)}, {PlayerName}! You made it. We've just been waiting on you and {MissingKid}." {DoorGetter} holds the door open and steps aside to let you in. "Come inside and check this place out, it's {Swear(Speaker)} {SwearLevel == 1: freaking} {SwearLevel == 2: fucking} amazing. {PowerFlipped: Did you notice anything weird outside? The power {SwearLevel == 0:went out}{SwearLevel == 1:blipped out}{SwearLevel == 2:crapped out} earlier.}"
    - else:
"{Greeting(Speaker)}, {PlayerName}. Glad you're here, we're still hoping {MissingKid} shows up soon. The power just went out, this is a mess. Don't get me wrong, the place is {Swear(Speaker)}{SwearLevel == 1: freaking} {SwearLevel == 2: fucking} cool. I'm just hoping the lights are back on by morning."
}
}
You squeeze past {DoorGetter} into the foyer of the house. {getPronounCaps(DoorGetter, "subject")} shuts the door once you're through.
    ->Foyer



=== Outbuilding_Ext ===
{SetPlayerLocation(OUTBUILDINGEXT)}

+[Look around.]
{PowerOn: The outbuilding has an old metal dome light casting a halo around its door, which is slightly ajar. The old wood of the small shack in front of you extends back into a slightly larger concrete structure. To the left, an orange light shines through a side window of the house. Your friends are inside, but you can't make out any details.}
{PowerOn == false: The moon reflects off the glass of the darkened house and silhouettes the outbuilding beside it.}
    ->Outbuilding_Ext
    
+[Enter the outbuilding.]
    {AddTime(1)} The bare wood of the dusty outbuilding door creaks as you step through.
        ->Outbuilding_Int
    
+[Return to the front of the house.]
    {AddTime(1)} You walk back to the front lawn.
        ->Front_Lawn



=== Outbuilding_Int ===
{SetPlayerLocation(OUTBUILDINGINT)}

{ 
    - PowerOn == true:
        +[Turn the power breaker off.]
            You press the toggle, shutting off the power to the house.
            ~UsedOutbuildingBreaker = true
            ~PowerFlipped = true
            ~PowerOn = false
            ~ReservePower = HousePower + AtticPower
            ~HousePower = 0
            ~AtticPower = 0
            ->GoToLocation
    - PowerOn == false:
        +[Turn the power breaker on.]
            You press the toggle, turning on the power.
            ~PowerFlipped = true
            ~PowerOn = true
            ~ReservePower = ReservePower - 2
            ~HousePower = 1
            ~AtticPower = 1
            ->GoToLocation
}

{
    - Outbuilding_Int_Look > 0:
    {
    - GuestHouseOpen == false:
    +[Unlock the gate.]
    {
        - GuestHouseKnowledge == true:
            You enter the combination to unlock the gate to the guest house.
            ~GuestHouseOpen = true
            ->GoToLocation
        - else:
            You don't know the combination for the gate lock. You try a few different combinations of colored tumblers, but nothing works.
            ->GoToLocation
    }
    }
}

+[Look around.]
    ->Outbuilding_Int_Look

+[Move...]
    {
        - GuestHouseOpen == true:
        ++[...through the gate to the guest house.]
        {SetPlayerLocation(GUESTHOUSE)}
        You step through the open gate and down a flight of stairs into the guest house. {AddTime(1)}
        ->GuestHouse
    }
    ++[...back outside.]
    {AddTime(1)} You leave the outbuilding and return to the field outside the house.
        ->Outbuilding_Ext

=== Outbuilding_Int_Look ===
{
    - Outbuilding_Int_Look == 1:
The outbuilding is a small dirt-floored space, approximately eight by eight feet, and appears to have been hastily constructed on the exterior of a larger brick and concrete structure. A series of pipes extruding from the wall lead to a water main and a large power breaker toggle.
    - Outbuilding_Int_Look > 1:
You're in the small, dusty outbuilding. There's a large power breaker toggle.
}

The entrance to the larger building is barred with an iron gate{GuestHouseOpen == true:, which is open}{GuestHouseOpen == false:, which is locked. Through a hole in the frame of the gate you can see the details of the locking mechanisms--three distinct tumblers with buttons for operation. The colored faces of the tumblers need to be arranged in the correct order}. 

The door is open behind you.
    ->Outbuilding_Int


=== Foyer ===
{SetPlayerLocation(FOYER)}

<-CharactersPresentTest 

+[Look around.]
    ->Foyer_Look
    
+[Check the time on the grandfather clock.]
    The grandfather clock reads {ClockTime()}.
        ->Foyer

+[Move...]
    ++[...to the living room.]
        <-DoorGetterMove
        {SetPlayerLocation(LIVINGROOM)}
        You step through the wide doorway into the living room. {AddTime(1)} 
        ->GoToLocation
    ++[...to the vestibule.]
        <-DoorGetterMove
        {SetPlayerLocation(VESTIBULE)}
        You walk through the wooded entryway into the vestibule. {AddTime(1)} 
        ->GoToLocation
    ++[...to the upstairs study.]
        <-DoorGetterMove
        {SetPlayerLocation(STUDY)}
        You walk up the creaking stairs and enter the door to the study on the second floor. {AddTime(1)} 
        ->GoToLocation
    ++[...to the upstairs hallway.]
        <-DoorGetterMove
        {SetPlayerLocation(HALLWAY1)}
        You walk up the creaking stairs and enter the southern hallway on the second floor. {AddTime(1)} 
        ->GoToLocation
    ++[...out the front door.]
        ->Foyer_FrontDoor
    ++[...Never mind.]
        ->Foyer
    
=== Foyer_Look ===
{
    - PowerOn == false: It's dark. You can make out the void of the open foyer above you.
    - else:
    {Foyer_Look==1:The foyer is constructed almost exclusively out of a rich, orange-red wood that radiates a warmth that makes it feel like a refuge from the wind and cold outside. The two-storey cathedral ceiling is tall enough that you unconsciously avoid looking up into it, as the green-papered walls recede into an ominous and overbearing darkness above.}
    {Foyer_Look>1:You're in the house's tall, beautiful foyer.}
}

<> A {Foyer_Look==1:magisterial }grandfather clock is ticking softly against the back wall.

Stairs to the left lead upstairs into the study and into an upstairs hallway. An open doorway to the right leads into the living room. To the back, an entrance leads to the vestibule between the foyer, kitchen, and garage.
    
    <-CharacterLocationCheck
    ->Foyer
    
=== Foyer_FrontDoor ===
{
- Hater != DoorGetter:
    {
    - TriedFrontDoor==false: 
    ~TriedFrontDoor = true
    You twist the knob of the front door and find it locked.
    {
    - DoorGetterLeft==false:
    "Oh, {Swear(DoorGetter)}{SwearLevel==2:shit.}{SwearLevel==1:crap.}{SwearLevel==0:what?} It's locked?" {DoorGetter} comes to look. 
    {
    -DoorGetter == "Cliff": <> "Is it jammed?"
    -else: <> "Is it jammed or something?"
    }
    <> {getPronounCaps(DoorGetter, "subject")} tries the doorknob frustratedly. "{Swear(DoorGetter)}{SwearLevel==2:Fuck.}{SwearLevel==1:What the hell?}{SwearLevel==0:Oh man.} I mean, I guess there's a back door too. And the garage door. 
    {PowerOn == false: <> That is, if we can get the power back on.}
    {
    -DoorGetter == "Francine": <> Still, we'd better mention to the owner that it was like this when we got here. They could try to charge us for it."
    -DoorGetter == "Megan": <> I mean, there's a bunch of ways out. Still, like, the front door of a house stuck closed is weird. Makes me feel claustrophobic."
    -else: <> At least we can still get out.{DoorGetter=="Emily": Creepy, though.}{DoorGetter=="Pete": I guess it can wait til morning.}"
    {AddTime(2)}
    }
    }
    - else: You wiggle the knob on the front door again, checking it for any hesitation.
    It's still locked.
    {AddTime(1)}
    }
}
->Foyer



=== LivingRoom ===
{SetPlayerLocation(LIVINGROOM)}

<-CharactersPresentTest

+[Look around.]
    ->LivingRoom_Look
    
+[Move...]
    ++[...to the foyer.]
        {SetPlayerLocation(FOYER)}
        You return through the large doorway into the foyer. {AddTime(1)}
        ->GoToLocation
    ++[...to the kitchen.]
        {SetPlayerLocation(KITCHEN)}
        You move through the living room door into the kitchen. {AddTime(1)}
        ->GoToLocation
    ++[...Never mind.]
        ->LivingRoom

=== LivingRoom_Look ===
{
    - PowerOn == false: It's dark. The living room is virtually black, save for the moonlight silhouetting the TV through the window behind it.
    - LivingRoom_Look == 1:
        The living room is gently but invitingly lit by a floor lamp. It's an attractive multipurpose space, with a long couch, a wide and short coffee table, and numerous tall shelves peppered with books and ephemera. A cabinet-style CRT TV is positioned against the window, as if to distract from the darkness outside.
    - else:
        You're in the living room. It's pretty spacious.
}
    
    <-CharacterLocationCheck
    ->LivingRoom


    
=== Kitchen ===
{SetPlayerLocation(KITCHEN)}

<-CharactersPresentTest

{
    - PowerOn == true:
    +[Check the time on the kitchen stove clock.]
        {
            - PowerFlipped == true:
                The digital clock is blinking a reading of 12:00.
                ->Kitchen
            - else:
                The digital clock reads {ClockTime()}.
                ->Kitchen
        }
}

+[Look around.]
    ->Kitchen_Look
    
+[Move...]
    ++[...to the living room.]
        {SetPlayerLocation(LIVINGROOM)}
        You head back out into the living room. {AddTime(1)}
        ->GoToLocation
    ++[...to the vestibule.]
        {SetPlayerLocation(VESTIBULE)}
        You move into the vestibule. {AddTime(1)} 
        ->GoToLocation
    ++[...Never mind.]
        ->Kitchen

=== Kitchen_Look ===
{
    - PowerOn == false: It's dark. The moonlight reflects off the kitchen tile.
    - else:
        The kitchen is laid out and decorated in a nostalgic, 1970s kind of idiom.  To the left a small kitchen table and several chairs are arranged beside a curved window nook. To the right, a kitchen island--really more of a peninsula--separates the dining area from the main kitchen counter.
    
    <> The digital clock above the stove illuminates an otherwise dark corner of the room in a dim red.
}

Two parallel doors lead to the living room and the vestibule, which itself opens into the foyer and garage.
    <-CharacterLocationCheck
    ->Kitchen


    
=== Study ===
{SetPlayerLocation(STUDY)}

<-CharactersPresentTest

+[Look around.]
    ->Study_Look

+[Look at the books.]
    ->Study_Books

+[Move...]
    ++[...to the foyer.]
        {SetPlayerLocation(FOYER)}
        You step out of the study and return down the stairs into the main area of the foyer. {AddTime(1)} 
        ->GoToLocation
    ++[...to the guest bedroom.]
        {SetPlayerLocation(GUESTBEDROOM)}
        You head through the back door into the guest bedroom. {AddTime(1)} 
        ->GoToLocation
    ++[...to the hallway.]
        {SetPlayerLocation(HALLWAY1)}
        You step out of the study and into the upstairs hallway. {AddTime(1)} 
        ->GoToLocation
    ++[...Never mind.]
        ->Study

=== Study_Look ===
{
    - PowerOn == false: It's dark. The history of the old study feels as if it's closing in around you.
    - else:
    {Study_Look==1: The study is a gallery-like space whose numerous wall-lining bookshelves and mounted artworks have visibly been neglected by their current owner. Against the wall opposite the entrance is a thick wood work desk, covered in the trappings of an artistic/financial/documentarian/engineering project. The room's highly decorated dome ceiling gives the impression of a yawning void above you.}
    {Study_Look>1: You're in the house's dusty, formerly austere, second-floor study.}
}

    Doors on opposite sides of the room lead to a guest bedroom and back out into the foyer.
    
    <-CharacterLocationCheck
    ->Study
    
=== Study_Books ===
{AddTime(1)}
You scan the spines of some of the books on the shelf.
++[Baghavad Gita.]
    You begin to read the Baghavad Gita. It's interesting but not really applicable to your life.
++[Dracula. Bram Stoker.]
    You begin to read Dracula by Bram Stoker. It's gothic to a fault.
++[Frankenstein; or, The Modern Prometheus. Mary Shelley.]
    You begin to read Frankenstein by Mary Shelley. It's a bummer.
++[The Holy Bible, King James version.]
    You begin to read the Holy Bible, King James version. It's preachy.
    {
        - BiblePageFound:
        +++[Turn to page 253.]
            {
            -GuestHouseKnowledge == false:
            You turn to page 255, finding the spot where 253/254 was torn away at the leaf. Page 255 is shaded over with three large squares, arranged on top of each other: the first in green, the second in yellow, and the third in red. You internalize the pattern.
            - else:
            You turn to page 255 and observe three colored squares: the first in green, the second in yellow, and the third in red.
            }
            ~GuestHouseKnowledge = true
            ->GoToLocation
    }
    +++[Read something else.]
        ->Study_Books
    +++[Stop reading.]
        ->GoToLocation
++[Shobogenzo (Treasury of the Right Dharma Eye). Dogen.]
    You begin to read Shobogenzo by Dogen. You think about your practice.
-- ++[Read something else.]
    ->Study_Books
++[Stop reading.]
    ->GoToLocation


    
=== Vestibule ===
{SetPlayerLocation(VESTIBULE)}
{
    - PowerOn == true:
        {
            - CellarLock == true:
                ~CellarOpen = false
            - else:
                ~CellarOpen = true
        }
    - PowerOn == false:
        A red light faintly pulses from behind the cellar door.
        ~CellarOpen = true
}

<-CharactersPresentTest

{
    - Vestibule_Look > 0:
+[Toggle the lock.]
    {
    - CellarLockKnowledge == true:
        {
            - CellarLock == true:
                You flip the cellar lock into the unlocked position. 
                ~CellarLock = false
            - else:
                You flip the cellar lock into the locked position. 
                ~CellarLock = true
        }
    - else:
        You don't know how to operate the lock.
    }
    ->GoToLocation
}

+[Look around.]
    ->Vestibule_Look

+[Move...]
    ++[...to the foyer.]
        {SetPlayerLocation(FOYER)}
        You move into the foyer. {AddTime(1)} 
        ->GoToLocation
    ++[...to the kitchen.]
        {SetPlayerLocation(KITCHEN)}
        You move into the kitchen. {AddTime(1)} 
        ->GoToLocation
    ++[...to the garage.]
        {SetPlayerLocation(GARAGE)}
        You move into the garage. {AddTime(1)} 
        ->GoToLocation
    ++[...to the cellar.]
        {
            - CellarOpen == true:
                {
                - Follower != "Crobus":
                    {
                    - Follower != BFF:
                        {Follower} hesitates as you pull open the door into the cellar.
                        {
                            - Scary == true:
                                "It's beyond scary down there, {PlayerName}," {getPronoun(Follower, "subject")} says. "I don't think I trust you or this situation enough."
                            - else:
                                "I love ya, {PlayerName}, but we're super explicitly not supposed to go down there," {getPronoun(Follower, "subject")} says. "I don't think I trust you or this situation enough."
                        }
                        {StopFollowing()}
                    }
                }
                {SetPlayerLocation(CELLAR)}
                You head down the dirty stairwell into the cellar. {AddTime(1)}
                ->GoToLocation
            - CellarOpen == false:
                You try the knob on the recessed cellar door. It's locked.
                {AddTime(1)}
                ->GoToLocation
        }
    ++[...Never mind.]
        ->Vestibule

=== Vestibule_Look ===
{
    - PowerOn == false: It's dark. The vestibule feels even more claustrophobic than its small size. A pulsing red light comes from the cellar door.
    - else:
    {Vestibule_Look==1: The vestibule is a short hallway separating the kitchen, living room, and garage. It's lined with painted drywall and undecorated trim, like it was constructed as part of a recent renovation, and looks out of step with its austere surroundings.}
    {Vestibule_Look>1: You're in the vestibule between the living room, kitchen, and garage.}
}
{
    - CellarOpen:To the left, a recessed door leads to the cellar. The electronic lock on the door is open. 
    - else:To the left, a recessed door leads to the cellar. The electronic lock is engaged.
}
<> In the corner next to the cellar door is a control panel for the lock{CellarLockKnowledge:, which you know how to operate}.
   
    <-CharacterLocationCheck
    ->Vestibule


    
=== Garage ===
{SetPlayerLocation(GARAGE)}

<-CharactersPresentTest

+[Look around.]
    ->Garage_Look

+[Move...]
    ++[...to the vestibule.]
        {SetPlayerLocation(VESTIBULE)}
        You head up a small step and back into the vestibule. {AddTime(1)} 
        ->GoToLocation
    ++[...Never mind.]
        ->Garage

=== Garage_Look ===
{
    - PowerOn == false: It's dark. All the garage gives you to go on is the smell of gasoline and and grass clippings.
    - else:
    {Garage_Look == 1: The garage, half-lit and unfinished, seems to disinvite you the moment you step inside. The cold air is heady with gasoline and grass clippings, and the ducts above you quietly hum with recirculated dust. A collection of typical household oddities is stacked in the corner and arranged on a utilitarian shelf. The drywall paneling is partially completed and in places is pulled back to reveal older wooden construction.}
    {Garage_Look > 1: The half-lit garage is repellent, but musty in a nostalgic way. The drywall paneling is partially completed and in places is pulled back to reveal older wooden construction.}
}

A shaded door leads back into the house through the vestibule.
<-CharacterLocationCheck
->Garage



=== Hallway1 ===
{SetPlayerLocation(HALLWAY1)}

<-CharactersPresentTest

+[Look around.]
    ->Hallway1_Look
    
+[Move...]
    ++[...to the foyer.]
        {SetPlayerLocation(FOYER)}
        You step out of the hallway and return down the stairs into the main area of the foyer. {AddTime(1)} 
        ->GoToLocation
    ++[...to the study.]
        {SetPlayerLocation(STUDY)}
        You step out of the hallway and turn the corner into the study. {AddTime(1)} 
        ->GoToLocation
    ++[...to the bathroom.]
        {SetPlayerLocation(BATHROOM)}
        You open the door at the end of the hall and enter the bathroom. {AddTime(1)} 
        ->GoToLocation
    ++[...to the northern end of the hallway.]
        {SetPlayerLocation(HALLWAY2)}
        You walk down the darkened hallway and around the corner. {AddTime(1)} 
        ->GoToLocation
    ++[...Never mind.]
        ->Hallway1

=== Hallway1_Look ===
{Hallway1_Look==1: The longer southern portion of the hallway is panelled in dark wood and, lacking any artificial light, is lit only by moonlight coming in through the recessed windows that dot the right side wall. A large painting is hung on the opposite wall.}
{Hallway1_Look>1: You stand in the southern portion of the house's second-floor hallway.}

Ahead of you, the hallway turns a corner to the left. Doors lead to the bathroom and study, and stairs lead down into the foyer.
    
    <-CharacterLocationCheck
    ->Hallway1
 
 
    
=== Hallway2 ===
{SetPlayerLocation(HALLWAY2)}

<-CharactersPresentTest

+[Look around.]
    ->Hallway2_Look
    
+[Move...]
    ++[...to the guest bedroom.]
        {SetPlayerLocation(GUESTBEDROOM)}
        You walk through the door on the left into the guest bedroom. {AddTime(1)} 
        ->GoToLocation
    ++[...to the master bedroom.]
        {SetPlayerLocation(MASTERBEDROOM)}
        You walk into the house's master bedroom. {AddTime(1)} 
        ->GoToLocation
    ++[...to the southern end of the hallway.]
        {SetPlayerLocation(HALLWAY1)}
        You head around the corner to the other end of the darkened hallway. {AddTime(1)} 
        ->GoToLocation
    ++[...up to the attic.]
        {
            - PlayerName == "Cliff": ->Hallway2_Attic_Reach
            - PlayerName == "Francine": ->Hallway2_Attic_Reach
            - PlayerName == "Pete": ->Hallway2_Attic_Reach
            - Follower != "Crobus": ->Hallway2_Attic_Follower
            - else:
                You can't reach the cord to open the attic stairs.
                ->GoToLocation
        }
    ++[...Never mind.]
        ->Hallway2

=== Hallway2_Look ===
{Hallway2_Look==1: The end of the hallway is capped with a large window nook, looking out into an eerie treeline of black silhouettes.}
{Hallway2_Look>1: You're in the second-floor hallway, at the northern end.}

To your right, the hallway turns a corner back to the front of the house. Doors lead to the guest bedroom and master bedroom. Above you is a door that leads to the attic, with a pull cord for entry.         
{
            - PlayerName == "Cliff": 
            - PlayerName == "Francine": 
            - PlayerName == "Pete":
            - else:
                <> You won't be able to reach it without help.
}

    <-CharacterLocationCheck
    ->Hallway2

=== Hallway2_Attic_Reach ===
{
    - Follower != "Crobus":
    {
        - Follower != BFF:
            {Follower}, nervous to go into the attic and not trusting your intentions enough, stays behind as you pull down the attic stairs and climb into the darkness above.
            {StopFollowingSilent()}
            {SetPlayerLocation(ATTIC)}
            ->GoToLocation
        - else:
            You reach up and grab the cord, pulling down the attic stairs. You climb up.
            {SetPlayerLocation(ATTIC)}
            ->GoToLocation        
    }
    - else:
        You reach up and grab the cord, pulling down the attic stairs. You climb up.
        {SetPlayerLocation(ATTIC)}
        ->GoToLocation
}

=== Hallway2_Attic_Follower ===
{
    - Scary != true:
        "Hey, {Follower}," you ask. "Could you help me open those stairs? I want to check out the attic."
    - else:
        You gesture towards the attic door. "{Follower}, help me open the attic!"
}
{
    - Follower == BFF:
        "OK!" {getPronoun(Follower, "subject")} says.
        {
            - Follower == "Cliff": ->Hallway2_Attic_FollowerReach
            - Follower == "Francine": ->Hallway2_Attic_FollowerReach
            - Follower == "Pete": ->Hallway2_Attic_FollowerReach
            - else: ->Hallway2_Attic_FollowerHelp
        }
    - else:
        {
            - Scary == true:
                    {Follower} shakes {getPronoun(Follower, "possessive")} head "no."
                    ->GoToLocation
//            {
//                - Follower == "Cliff": ->Hallway2_Attic_FollowerReach
//                - Follower == "Francine": ->Hallway2_Attic_FollowerReach
//                - Follower == "Pete": ->Hallway2_Attic_FollowerReach
//                - else: ->Hallway2_Attic_FollowerHelp
//            }
            - else:
            {Swear(Follower)}"{SwearLevel == 2:Fuck, }{SwearLevel == 1:Damn, }I'm not sure, {PlayerName}," {getPronoun(Follower, "subject")} says. 
            {
                - Follower == "Francine": <> "We're not supposed to go up there.
                - Follower == "Cliff": <> "The owner said not to go up there. God knows he has a camera up there and we're gonna end up getting charged.
                - else: <> "The owner said not to go up there.
            }
            <> I'll help you open it but I don't think I want to be involved with whatever's up there."
            ~FollowerLeave = true
            {
                - Follower == "Cliff": ->Hallway2_Attic_FollowerReach
                - Follower == "Francine": ->Hallway2_Attic_FollowerReach
                - Follower == "Pete": ->Hallway2_Attic_FollowerReach
                - else: ->Hallway2_Attic_FollowerHelp
            }
        }
        ->GoToLocation
}
->GoToLocation

=== Hallway2_Attic_FollowerReach ===
{Follower} reaches up and pulls open the attic stairs. You climb up.
{
    - FollowerLeave == true:
        {StopFollowing()}
}
~FollowerLeave = false
{SetPlayerLocation(ATTIC)}
->GoToLocation

=== Hallway2_Attic_FollowerHelp ===
{Follower} plants a knee and gives you a boost to the cord that opens the attic stairs. You climb up.
{
    - FollowerLeave == true:
        {StopFollowing()}
}
~FollowerLeave = false
{SetPlayerLocation(ATTIC)}
->GoToLocation


    
=== GuestBedroom ===
{SetPlayerLocation(GUESTBEDROOM)}

<-CharactersPresentTest

+[Look around.]
    ->GuestBedroom_Look
    
+[Move...]
    ++[...to the hallway.]
        {SetPlayerLocation(HALLWAY2)}
        You head through the door to the northern part of the hallway. {AddTime(1)} 
        ->GoToLocation
    ++[...to the study.]
        {SetPlayerLocation(STUDY)}
        You head through the door to the study. {AddTime(1)} 
        ->GoToLocation
    ++[...Never mind.]
        ->GuestBedroom

=== GuestBedroom_Look ===
{
    - PowerOn == false: It's dark. The cramped guest bedroom offers only a faint light through the window on the opposite wall.
    - else:
    {GuestBedroom_Look==1: The guest bedroom appears at some point to have been the bedroom of a child (or children.) Two small twin beds sit on alternate sides of the room, and nearer their headboards the ceiling begins to slope downward toward the window along with the angle of the ceiling.}
    {GuestBedroom_Look>1: You're in the cramped guest bedroom.}
}

Doors on opposite sides of the room lead to the study and out into the northern part of the hallway.
    
    <-CharacterLocationCheck
    ->GuestBedroom


    
=== MasterBedroom ===
{SetPlayerLocation(MASTERBEDROOM)}

<-CharactersPresentTest

+[Look around.]
    ->MasterBedroom_Look

+[Use the computer.]
    ->MasterBedroom_Computer

+[Move...]
    ++[...to the hallway.]
        {SetPlayerLocation(HALLWAY2)}
        You walk back out the door to the northern part of the hallway. {AddTime(1)} 
        ->GoToLocation
    ++[...Never mind.]
        ->MasterBedroom

=== MasterBedroom_Look ===
{
    - PowerOn == false: It's dark. The spacious master bedroom becomes an inky void into which you can project your insecurities.
    - else:
    {MasterBedroom_Look==1: The master bedroom is at a successful intersection of rustic and luxurious, its cathedral ceiling suggesting both economy of space and a kind of folksy grandeur. A California king bed sits opposite the door, and against it a strip window offers a view of the moonlit river valley below.}
    {MasterBedroom_Look>1: You're in the spacious master bedroom.}
}
<> A computer sits on a desk in the corner of the room.

The door behind you leads back out into the end of the hallway.
    
    <-CharacterLocationCheck
    ->MasterBedroom
    
=== MasterBedroom_Computer ===
{MasterBedroom_Computer==1: The computer is of the early desktop PC variety, portable and non-expandable, with an old and highly curved CRT monitor. The plastic housing is yellowed and the computer produces a noticable coil whine.}
{MasterBedroom_Computer>1: You sit at the old computer.}
{BiblePageFound==false:<> There's a wilted piece of paper pinned betwen the monitor and the case.}

->MasterBedroom_Computer_Hub

=== MasterBedroom_Computer_Hub ===
+[Turn on the computer.]
    The computer doesn't turn on.
    ->MasterBedroom_Computer_Hub
+[Look at the paper.]
    {
        - BiblePageFound == false:
            You pull the paper from beneath the monitor, finding it to be page 253 of the Bible, King James version. You internalize the number.
        - else:
            It's page 253 of the Bible, King James version.
    }
    ~BiblePageFound = true
    ->MasterBedroom_Computer_Hub
+[Stop using the computer.]
    You step away from the computer.
    ->GoToLocation


    
=== Bathroom ===
{SetPlayerLocation(BATHROOM)}

<-CharactersPresentTest

+[Look around.]
    ->Bathroom_Look

+[Move...]
    ++[...to the hallway.]
        {SetPlayerLocation(HALLWAY2)}
        You walk back out the door to the northern part of the hallway. {AddTime(1)} 
        ->GoToLocation
    ++[...Never mind.]
        ->Bathroom

=== Bathroom_Look ===
{
    - PowerOn == false: It's dark. You can feel the cold of the bathroom porcelain in the air. You're afraid to touch anything.
    - else:
    {Bathroom_Look==1: The bathroom is small and functional, and is patterned in shades of blue and green. A freestanding ceramic tub with a fabric curtain surrounding it dominate your sense of the room. A basin-style sink sits beneath an elliptical mirror in the back corner, and a toilet has been squeezed into the corner nearest the entrance with just enough clearance as to not block the door.}
    {Bathroom_Look>1: You're in the small, cold, old-fashioned bathroom.}
}

The door behind you leads back out into the end of the hallway.
    
    <-CharacterLocationCheck
    ->Bathroom
    
    

=== Cellar ===
{SetPlayerLocation(CELLAR)}

<-CharactersPresentTest
{PowerOn == false: The control panel emits a pulsing red light.}
{
    - Cellar_Look > 0:
        *[Look at the schematic.]
            You take a close look at the schematic drawing. It's a diagram of the cellar door lock from the vestibule, with notes about how to operate it. You internalize the details.
            ~CellarLockKnowledge = true
            ->GoToLocation
}

{
    - GuestHouseTunnelKnowledge:
        {
            - CellarTunnelOpen == false:
                +[Open the hidden tunnel entrance.]
                    You pick at the plasterwork around the tunnel entrance, revealing a stone tunnel receding into the darkness.
                    ~CellarTunnelOpen = true
                    {AddTime(1)}
                    ->GoToLocation
        }
}

+[Operate the control panel.]
    ->Cellar_Machine
+[Look around.]
    ->Cellar_Look

+[Move...]
    ++{CellarTunnelOpen}[...through the tunnel to the guest house.]
        {SetPlayerLocation(GUESTHOUSE)}
        You crawl through the tunnel into the guest house{GuestHouseTunnelOpen == false:, pushing open the panel blocking the exit before you rise to your feet}. {AddTime(2)} 
        ~GuestHouseTunnelOpen = true
        ->GoToLocation
    ++[...upstairs.]
        {SetPlayerLocation(VESTIBULE)}
        You head back to the corner of the cellar and return up the stairs to the vestibule. {AddTime(1)} 
        ->GoToLocation
    ++[...Never mind.]
        ->Cellar

=== Cellar_Look ===
{
    - PowerOn == false:
        {Cellar_Look==1: The cellar is unfinished in the extreme, all bare brick and stone, seemingly no darker for the lack of power. The stonework curves into a series of support arches that recede toward the back of the room. The impacted dirt of the floor crunches slightly under your feet as you walk.}
        {Cellar_Look>1: You're in the house's damp old cellar. It's dark.}
    - else:
        {Cellar_Look==1: The cellar is unfinished in the extreme, all bare brick and stone lit by a single bulb in a dome fixture. The stonework curves into a series of support arches that recede toward the back of the room. The impacted dirt of the floor crunches slightly under your feet as you walk.}
        {Cellar_Look>1: You're in the house's damp old cellar. It's dark except for a single bare lightbulb.}
}

{CellarLockKnowledge == false: A schematic drawing is stapled to the wall by the door. }A musty stairwell leads back to the foyer {CellarTunnelOpen==true: and an opening near the floor leads to a tunnel}.

{PowerOn==true:Against the back wall is a strange control panel.}{PowerOn==false:Against the back wall is a strange control panel, emitting a pulsing light that fleetingly casts the whole room in ominous red.}
    
    <-CharacterLocationCheck
    ->Cellar
    
    
    
=== Attic ===
{SetPlayerLocation(ATTIC)}

<-CharactersPresentTest

{
    - Attic_Look > 0:
        {
        - CellarMachineKnowledge == false:
        +[Open the trunk.]
            {
            - Follower != "Crobus":
                {Follower} helps you open the releases on the trunk. Inside are a bunch of seemingly random documents related to the construction of the house.
                
                Among them, you find a worn manual page with the image of a lock and a series of four symbols--a combination. You internalize the details.
                ~CellarMachineKnowledge = true
                ->GoToLocation
            - else:
                The trunk is locked. You squeeze the lock handle on the side nearest you, but it doesn't accomplish anything. There's another handle on the opposite side--too far to reach--and presumably both need to be used at the same time. You may need some help.
                ->GoToLocation
            }
        - else:
        +[Look through the trunk.]
            The trunk doesn't seem to contain anything else of specific interest.
            ->GoToLocation
        }
        {
        - AtticMachineCovered == true:
        + [Pull the sheet off the machine.]
            You {PlayerName == "Gary":flourishingly }pull the sheet off of the machine. Getting a look at the strange machine, you also become aware of a number of diodes leading to cables that stretch to the ceiling, arrayed to a number of emitters hanging in the middle of the room.
            ~AtticMachineCovered = false
            ->Attic
        - else:
        + [Operate the machine.]
            ->Attic_Machine
        }
        * [Open the brass door.]
            The door leads to what appears to be the house's chimney, and opens again below you in the living room. It's disgusting, but you think you could safely climb down the chute.
            ~AtticChuteKnowledge = true
            ->GoToLocation
}
+[Look around.]
    ->Attic_Look

+[Move...]
    {
        - AtticChuteKnowledge:
            ++[...down the chute into the living room.]
                {SetPlayerLocation(LIVINGROOM)}
                You shimmy down the chute into the living room. {AddTime(1)}
                ->GoToLocation
    }
    ++[...downstairs.]
        {SetPlayerLocation(HALLWAY2)}
        You return down the stairs to the end of the second-floor hallway. {AddTime(1)} 
        ->GoToLocation
    ++[...Never mind.]
        ->Attic

=== Attic_Look ===
{Attic_Look==1: The attic is dusty and dark, lit only by the moonlight coming in through a set of windows at the back of the room. The attic is larger than you expected, though most of the floor plan isn't really usable because of the low, slanted ceilings.}
{Attic_Look>1: You're in the house's attic. It's dirty and dark.}
{AtticMachineFired: <> There's a scorch mark on the floor.}

There's a short railing surrounding the stairs back down into the hallway. {AtticChuteKnowledge == true: A chute leads down the chimney into the living room.}{AtticChuteKnowledge == false: There's a smallish brass door to the rear left of the room.} {AtticMachineCovered == true: Amid stacks of ephemera there's a sheet auspiciously draped over some kind of machine.} {AtticMachineCovered == false: There is a strange machine against the rear wall.} A large trunk is situated against the wall.
    
    <-CharacterLocationCheck
    ->Attic

=== Attic_Machine ===
{AddTime(1)}
{Attic_Machine == 1: The machine is arcane and bizarre. Its only interface is a large lever that orbits around its center and an array of lights across a panel at the top.}

{CreechMinute == CreechMinuteSlow2: <> The lever is in the far left position.}
{CreechMinute == CreechMinuteSlow: <> The lever is in the left position.}
{CreechMinute == CreechMinuteNormal: <> The lever is in the middle position.}
{CreechMinute == CreechMinuteFast: <> The lever is in the right position.}

The lights read thusly:
{
    - AtticPower == 0:
    <>    █ █ █
    - AtticPower == 1:
    <>    ░ █ █
    - AtticPower == 2:
    <>    ░ ░ █
    - AtticPower == 3:
    <>    ░ ░ ░
}
{
    - CreechMinute != CreechMinuteSlow2:
        + [Put the lever in the far left position.]
            {
                - AtticPower > 1:
            You push the lever into the far left position.
            ~CreechMinute = CreechMinuteSlow2
            ->GoToLocation
                - else:
            You try to push the lever into the far left position, but without adequate power the machine resists. The lever rests in the left position.
            ~CreechMinute = CreechMinuteSlow
            ->GoToLocation
            }
}
{
    - CreechMinute != CreechMinuteSlow:
        + [Put the lever in the left position.]
            You push the lever into the left position.
            ~CreechMinute = CreechMinuteSlow
            ->GoToLocation
}
{
    - CreechMinute != CreechMinuteNormal:
        + [Put the lever in the middle position.]
            You push the lever into the middle position.
            ~CreechMinute = CreechMinuteNormal
            ->GoToLocation
}  
{
    - CreechMinute != CreechMinuteFast:
        + [Put the lever in the right position.]
            You push the lever into the right position.
            ~CreechMinute = CreechMinuteFast
            ->GoToLocation
}
+ [Put the lever in the far right position.]
    {
        - AtticPower > 2:
    You push the lever into the far right position, and there's an audible crackle of electricity as a burst of energy explodes in the middle of the room.{CreechLocation == PlayerLocation: The creature, looming over you, is seemingly vaporized with a blinding flash.} The lever returns to the right position and the lights on the machine fade.
    
    The power appears to have gone out.
    ~AtticMachineDischarged = true
    ~PowerOn = false
    ~ReservePower = ReservePower + AtticPower + HousePower
    ~AtticPower = 0
    ~HousePower = 0
    ~AtticMachineFired = true
    ~CreechMinute = CreechMinuteFast
    {
        - CreechLocation == PlayerLocation:
        ~CreechDead = true
        ~CreechLocation = CREECH
        {HaterLocationTest()}
        {
            - HaterLocation == ATTIC:
            ->HaterCreechDeathScene
            - HaterAttic == 1:
            ->HaterCreechDeathScene
        }
    }
    ->GoToLocation
        - else:
    You try to push the lever into the far right position, but without adequate power the machine resists. The lever rests in the right position.
    ~CreechMinute = CreechMinuteFast
    ->GoToLocation
    }
+[Leave it alone.]
->Attic

    
    
=== GuestHouse ===
{SetPlayerLocation(GUESTHOUSE)}
<-DoorGetterMoveSilent

<-CharactersPresentTest

{
    - GuestHouse_Look > 0:
    {
        - GeneratorOn:
        +[Turn the generator off.]
        You switch off the generator.
        ~GeneratorOn = false
        ~ReservePower = ReservePower - 1
        ->GoToLocation
        - GeneratorOn == false:
        +[Turn the generator on.]
        ~UsedGenerator = true
        You switch on the generator.
        ~GeneratorOn = false
        ~ReservePower = ReservePower + 1
        ->GoToLocation
    }
    {
        - GuestHouseTunnelOpen == false:
        +[Move the discolored wall panel.]
            You pull on the wall panel and it comes loose, revealing a musty brick tunnel receding into darkness. {AddTime(1)}
            ~GuestHouseTunnelOpen = true
            ->GoToLocation
    }
}

+[Look around.]
    ->GuestHouse_Look

+[Move...]
    ++{GuestHouseTunnelOpen}[...through the tunnel into the cellar.]
        {SetPlayerLocation(CELLAR)}
        You crawl through the tunnel into the cellar{CellarTunnelOpen == false:, pushing down a false wall portion and dusting yourself off as you rise to your feet}.
        ~GuestHouseTunnelKnowledge = true
        ~CellarTunnelOpen = true
        {AddTime(2)}
        ->GoToLocation
    ++[...out through the gate.]
        The gate out of the guest house is locked.
        ->GoToLocation
    ++[...Never mind.]
        ->GuestHouse

=== GuestHouse_Look ===
{GuestHouse_Look==1: The guest house is unfurnished but attractive, with a brutalist sense defined by raw concrete and, most importantly, a large pane window that faces out into a minimalist garden and down to the river valley below.}
{GuestHouse_Look>1: You're in the isolated guest house.}
{GuestHouseTunnelOpen == false:<> Your eye is drawn to a conspicuously discolored wall panel on the left side of the room.} There is a control panel set into the wall that appears to control a midsize generator just outside the window.

There is a gate leading to the outbuilding{GuestHouseTunnelOpen:<> and a tunnel leading to the house's cellar}.
    
    <-CharacterLocationCheck
    ->GuestHouse


    
=== Cellar_Machine ===
{Cellar_Machine==1:The machine presents an array of lights that seem to represent the distribution of electricity around the house. The lights are arranged in a number of rows, seemingly showing the amount of power given to certain areas.}

{
    - AtticPower == 0:
        █ █ █
    - AtticPower == 1:
        ░ █ █
    - AtticPower == 2:
        ░ ░ █
    - AtticPower == 3:
        {
            - Hater != "Crobus":
                ~HaterLevel = 2
                ~HaterAttic = 1
                {MoveHater(ATTIC)}
        }
        ░ ░ ░
}
{
    - HousePower == 0:
        █
        ~PowerOn = false
    - HousePower == 1:
        ░
        ~PowerOn = true
}
{
    - ReservePower == 0:
        █ █ █ █
    - ReservePower == 1:
        ░ █ █ █
    - ReservePower == 2:
        ░ ░ █ █
    - ReservePower == 3:
        ░ ░ ░ █
    - ReservePower == 4:
        ░ ░ ░ ░
}

{
    - CellarMachineLocked == false:
        ->Cellar_Machine_Controls
    - CellarMachineLocked == true:
        A lockbox covers the controls of the machine. Four dials, each covered in a series of symbols, face out to you.
        {
            - CellarMachineKnowledge == true:
            + [Unlock the controls.]
                You input the combination to open the controls.
                ~CellarMachineLocked = false
                ->Cellar_Machine_Controls
            - <> You don't know how to open it.
        }
        + [Do nothing.]
            ->GoToLocation
}

=== Cellar_Machine_Controls ===
{
    - ReservePower > 0:
    {
    - AtticPower < 3:
    +[Increase the first row.]
        ~ReservePower = ReservePower - 1
        ~AtticPower = AtticPower + 1
        ->Cellar_Machine
    }
}
{
    - AtticPower > 0:
    +[Decrease the first row.]
        ~ReservePower = ReservePower + 1
        ~AtticPower = AtticPower - 1
        ->Cellar_Machine
}
{
    - ReservePower > 0:
    {
    - HousePower < 1:
    +[Increase the second row.]
        ~ReservePower = ReservePower - 1
        ~HousePower = HousePower + 1
        {
            - HousePower > 0:
            The lights in the cellar come on.
        }
        ->Cellar_Machine
    }
}
{
    - HousePower > 0:
    +[Decrease the second row.]
        ~ReservePower = ReservePower + 1
        ~HousePower = HousePower - 1
        {
            - HousePower == 0:
            The lights in the cellar go out.
        }
        ->Cellar_Machine
}
+[Do nothing.]
    {AddTime(1)}
    ->GoToLocation