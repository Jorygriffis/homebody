CONST FLASHBACK1 = 100
VAR LoopCount = 0
VAR Flashback = false
VAR FlashbackNumber = 666
VAR flashbackaDistance = 10
VAR flashbackaTurns = 0
VAR flashbackcTurns = 0
VAR aComplete = false
VAR cComplete = false
VAR bComplete = false
VAR Hint1 = false
VAR Hint2 = false
VAR flashback2complete = false


=== FlashbackHub ===
~LoopCount = LoopCount +1
{MiniReset()}
{AtticHaterSceneDone == true: ->Flashback_Hater_Talk}
{
    - Flashback_BFF_Talk == 0:
        {
            - Flashback_Party > 0:
                {Hater != "Crobus": ->Flashback_BFF_Talk}
        }
}
{
    -LoopCount>3:
    {
        - Flashback_Party ==0:
            ->Flashback_Party
    }
}
{
    - Hint1 == false:
    {
        - UsedCellarMachine == false:
        {
            - LoopCount > 3:
            ~Hint1 = true
            ->Flashback_MissingKid_Hint
        }
    }
}
{
    - Hint2 == false:
    {
        - UsedCellarMachine == true:
        {
            - LoopCount > 6:
            ~Hint2 = true
            ->Flashback_MissingKid_Hint
        }
    }
}
{LoopCount==1: ->Flashback_1}
{
    - aComplete == false: 
    {shuffle:
    - ->Flashback_a
    -
    }
}
{
    - bComplete == false:
        {Hater != "Crobus": ->Flashback_b}
}
{
    - cComplete == false:
        {highestPoints >= BFFGoal - 2: ->Flashback_c}
}
{
    - flashback2complete == false: 
    {shuffle:
    - ->Flashback_2
    -
    -
    }
}
->Flashback_Party



=== Act_1 ===
kids explore the house -- coming to understand the loop -- working for bff -- killing the creature/escaping but still loop restarts

limited flashbacks / fantasy
->LoopHub



=== Flashback_1 ===
~Flashback = true
~FlashbackNumber = 1
{flashbackLocation(FLASHBACK1)}
The sunset brightly dithers through the accordian blinds of {PlayerName=="Francine":your}{PlayerName!="Francine":Francine's} living room windows. Your closest friends are here, arranged on couches and chairs and floor-pillows, to discuss an upcoming trip out into the desert, coinciding with the Perseid meteor shower.
You're going to be renting a house--
{
    -PlayerName=="Cliff":
    <>unfortunately, no one but you is really comfortable roughing it.
    -PlayerName=="Emily":
    <>thank fucking God, because you don't want to have to deal with spiders or scorpions or ticks or whatever.
    -PlayerName=="Megan":
    <>thankfully, as you're planning on bringing whatever consoles you can conveniently carry. You have no misgivings about combining nature time and game time.
    -else:
    <>thankfully, as you have no illusions regarding your place in nature.
}
The idle conversation in the room has slowed to a low rumble.
->Flashback_1_hub

=== Flashback_1_hub ===
<-CharactersPresentTest

+ [Look around.]
{
    - PlayerName == "Francine": You're in the living area of your apartment. All your friends are here. You're looking forward to cleaning up.
    - else: You're in the living area of Francine's apartment. It's clean. All your friends are here.
}
    ->Flashback_1_hub

=== Flashback_1_Dialog ===
{
    - Speaker == "Megan":
    ->Flashback1_Megan
    - Speaker == "Emily":
    ->Flashback1_Emily
    - else:
    ->Flashback1_Generic
}
=== Flashback1_Megan ===
"{PlayerName}! When we get to the house we're gonna have some serious downtime before the hike the next day. I want to bring along some shitty old video games, but Pete thinks that's a dumb thing to do," Megan says.
"That is not what I said!" Pete interjects, "I said all of us bringing desktop computers so 'everyone can LAN it up like it's 2003' was a waste of a weekend in a cool old mystery house."
"OK, I cherry picked my example. Still, I mean, the place will have a TV, right?"
->Flashback1_MissingKid

=== Flashback1_Emily ===
"I'm looking forward to spacing out in the desert, aren't you, {PlayerName}?" Emily asks.
+ "Sure!"
+ "Definitely."
+ "Wait, how do you mean "spacing out"?["]
    <> You're talking pharmaceutically, aren't you?"
    "You know it! What could go wrong with mushrooms in the hot sun?" 
- She kicks back on the couch. "Incidentally... this place does have AC, right?"
->Flashback1_MissingKid

=== Flashback1_Generic ===
"Hey, {PlayerName}!" {Speaker} says, looking up at you from a throw pillow on the floor, "Are you looking forward to the trip? {Speaker=="Laura":Or at least to some isolation?}"
+ "Sure I am."
+ "Nervous but excited, yes."
+ "I'm apprehensive about it."
    {Swear(Speaker)}"{SwearLevel==2:Shit, }I'm sorry! But listen, we'll make the best of it."
- "Especially since the place is such a {SwearLevel==0:neat}{SwearLevel>0:rad} old building. Does this place even have electricity?"
->Flashback1_MissingKid

=== Flashback1_MissingKid ===
+"I'm sure it does.["]
    <> We're not spending this kind of money on a hovel. Only the finest airbnb for us. Probably smart to confirm, though." "You can probably ask...
+"I don't think I want to stay there if it doesn't.["]
    <> Maybe we should double-check with...
+"Dang, I'm not sure...["]
-<> Wait, who is actually renting the house, again?"
"It's {MissingKid}," {Speaker} says, casting a glance in {MissingKid}'s direction. {getPronounCaps(MissingKid, "subject")} snaps into engagement with {Speaker}.
"That's right!" {MissingKid} says, excitedly, and with promotional vigor, "The place looks amazing. It's a craftsman from the early 1900s. Was moved there from Pasadena in the 30s. Supposed to be one of the most isolated properties on airbnb in this state."
{MissingKid} speaks with an odd steadiness and total confidence in {getPronoun(MissingKid, "possessive")} subject matter. "The house is too remote for internet access and too far out for a cell signal. We'll be complete outside any kind of potential contact." {getPronounCaps(MissingKid, "subject")} slowly turns to look in your direction.
As {getPronoun(MissingKid, "subject")} does, {getPronoun(MissingKid, "subject")} regards you with an earnest smile, {getPronoun(MissingKid, "possessive")} eyes as {MissingKid=="Pete":jovial}{MissingKid=="Laura":sweet}{MissingKid=="Cliff":soulful}{MissingKid=="Emily":lively}{MissingKid=="Francine":thoughtful}{MissingKid=="Megan":kind}{MissingKid=="Gary":loving} as you remember them.
"You're going to love it, {PlayerName}." {MissingKid} speaks seemingly directly to you. No one else hears.

It is dark.
->LoopHub



=== Flashback_BFF_Talk ===
~Speaker = BFF
Derek's party continues apace. The sun has fully set now; the crisp night air coming in through the window at war with the stifling humidity of the body-filled apartment.
Just as you're thinking about how badly you need to get outside, {BFF} approaches.
{
    - BFF == "Cliff": "Hey. It's getting late and I need to grab some smokes before the Shell closes. Want to come with?"
    - BFF == "Emily": "Oh my God. {PlayerName}, I am supremely exhausted of Derek's shitty fucking friends."
    Are you ok?
    "Oh. Oh yeah, everything's okay. They just suck. You wanna go outside for a while?"
    - BFF == "Francine": "Hey there, {PlayerName}! Having a good time?" she asks, and before you can say more than a syllable of an answer, continues, "Oh oh yeah, me too. I'm holding up very well to the social pressures of maintaining humorous conversation for an entire evening. That said, I think I might go for a walk. Would you like to come?"
    - BFF == "Laura": "Hi, {PlayerName}. I was wondering if you wanted to go out for some fresh air--honestly, I don't do well at parties. The mix of social anxieties and the most toxic possible food and drink is not kind to my system. What do you say?"
    - BFF == "Megan": "Hey, {PlayerName}! Listen, I'm having a pretty nice time but I'm pretty overstimulated. I'm going to end up staring at a wall all night if I don't go and get some air. Want to come?"
    - else: "Hi, {PlayerName}! Things here are a little slow right now. I want to be here for the second wind, but I was wondering if you'd want to go for a walk for a few."
}
Before long, you're out on the sidewalk with {BFF}. The light pollution renders the night sky a deep mauve and the waning moon sits low on the horizon. You drift from streetlight to streetlight.
{
    - BFF == "Cliff": ->Flashback_BFF_Talk_Cliff1
    - else: ->Flashback_BFF_Talk_Generic1
}

=== Flashback_BFF_Talk_Cliff1 ===
"I'm not the biggest fan of parties. That might already be obvious." Cliff silently investigates whether or not to keep speaking.
->Flashback_BFF_Talk_Generic1

=== Flashback_BFF_Talk_Generic1 ===
"{Speaker!="Cliff":No matter how many conversations I have about how it's normal not to {Speaker=="Francine":enjoy}{Speaker!="Francine":be into} parties, I still feel so frustrated by them sometimes." {BFF} picks up {getPronoun(BFF, "possessive")} pace, {getPronoun(BFF, "possessive")} shoes scraping against the uneven sidewalk. "}{Speaker == "Emily":Like, }{Speaker == "Laura":Like, }{Speaker == "Gary":Like, }{Speaker == "Pete":Like, }I'm supposed to feel bad that I don't enjoy yelling at strangers in a hot, dark room? {Swear(BFF)}{SwearLevel==1:God.}{SwearLevel==2:Piss.} {Speaker=="Francine":It's so agitating.}{Speaker!="Francine":I hate it.}"
+ "There's a social element to it, too[."], though. It's important to people, they want you there."
    "Yeah. And it's not like that doesn't mean anything to me? But I wish I could just tick that box without spending an entire night on it."
+ "You have to try to make the most of it."
    "I have been. And I do. But that doesn't stop me from feeling like I'm the only person frustrated by this{SwearLevel==0:.}{SwearLevel==1:<> stuff.}{SwearLevel==2:<> shit.} And being afraid other people are judging me for it."
+ "I think just showing up is usually enough, though."
    "I know that's true," {BFF} says, "but {SwearLevel==0:God,}{SwearLevel==1:man,}{SwearLevel==2:fuck,} I'm afraid that it isn't. I'm afraid that whatever I do won't be enough. Like 'just dropping by' kind of tips the scale, you know?"
- "{BFF!="Francine":I guess }I don't know what I'm talking about. I'm just always afraid that I'm not doing enough somehow. I guess this is what being thirty is, huh? {Speaker!="Cliff":Coming to understand the amount of work you're supposed to be doing exactly when you start getting too tired to do it.}{Speaker=="Cliff":Being old enough to stop caring but too young to give up.}"
"I guess this {SwearLevel<2:stuff}{SwearLevel==2:shit} is all socially constructed anyway, though," {BFF} says, trying to course-correct. "And it's not like I think we're the smartest people in the world for thinking of it."
The night air is completely still as {BFF} and you make your way down the street. Outside your cone of vision, a moon-faced figure moves from buiding to building, just out of sight, watching you.
"The thing I'm most afraid of, though," {getPronoun(BFF, "subject")} continues, "is that these things are true, that {Speaker !="Cliff":love is transactional and }life is a zero-sum game."
+ "Like everyone is just taking advantage of you.["] Like all your friends are swimming in your money pile."
    "It feels {SwearLevel==0:bad}{SwearLevel>0:shitty}, right? I just want to trust people."
+ "I'm so afraid of accidentally betraying people."
    "Right?" {BFF} laughs. "Like those kinds of things just happen by accident."
+ ["It's good to have safe spaces from that feeling."]"Irrational as it is, it's good to know people who don't make you feel that way."
- "I'm glad I have you in my life. I don't feel like I'm gonna drop the ball and somehow ruin things with you," {getPronoun(BFF, "subject")} says, pausing before continuing, "I trust you."
+ ["I'm tired of watching you die."]
+ ["We need to figure out how to kill that thing."]
+ ["I need to get all of us out of this house."]
- "{
    -PlayerName=="Cliff":You too,
    -PlayerName=="Francine":And I trust you, too,
    -PlayerName=="Emily":Aww. I trust you, too,
    -else:I trust you too,
    }
    <>" you say.
->LoopHub



=== Act_2 ===
enemy causing trouble -- player pushes against boundaries more, other kids get suspicious -- life stuff, kids are 30 -- enemy and player come to terms
->LoopHub



=== Flashback_2 ===
You and {MostPoints} go into the woods to find something you remember. There's an old fort there. You and {MostPoints} built it. The place gets surrounded by creatures, animals. 

In the fray, you are knocked on your back and {MostPoints} is surrounded by the creatures. You pick up a large branch and raise it over your head.

*[Hit the fort.]
    You swing the makeshift club down into the leg of the rickety fort, bringing it to the ground and sending the creatures scattering in a panic. You and {MostPoints} are melancholy about the fort being destroyed, as it resembles a part of your shared identity being lost.
    
*[Hit the creatures.]
    You swing the makeshift club downward toward the creatures, who are barely convinced to scatter in response--thought not without causing more injury to {MostPoints} in the process. You take {getPronoun(MostPoints, "possessive")} arm over your shoulders and help {getPronoun(MostPoints, "possessive")} to walk limpingly out of the woods.
- ->LoopHub



=== Flashback_Hater_Talk ===
//this scene is preceded by an attempt to reach the room at the top in direct competition with the hater. 
{
    - PlayerName == "Francine":
        There's a knock at the door of your apartment. You're expecting your friends for a meeting about your upcoming trip, but not for almost another hour. You open the door--it's {Hater}, who you haven't talked to in a while--not since you drifted apart. You invite {getPronoun(Hater, "object")} in, telling her that the others were going to be a while because they left to get some last-minute supplies.
    - else:
        You knock at the door of Francine's apartment, only a little bit early for a meeting about the trip you're taking with your friends. You steel yourself for the meeting as the descending sun warms your shoulders. Eventually, the door opens--it's
        {
            - Hater == "Francine":
        <> Francine. She answers the door with a smile, though her eyes show a clear strain to produce an air of hospitality and friendship. Knowing you're here for a meeting of everyone coming to the airbnb, she invites you in but tells you it will be awhile until anyone else arrives, as the others have gone to pick up a few supplies.
            - else:
        <> {Hater}, unexpectedly. {getPronounCaps(Hater, "subject")} invites you in, and tells you it will be awhile until anyone else arrives, as Francine and the others have gone to pick up a few supplies and {getPronoun(Hater, "subject")} stayed to hold down the fort. 
        }
}
You sit across from each other at {PlayerName=="Francine":your modest}{PlayerName!="Francine":the} kitchen table. The air is heavy with the distance between you.
+ "So, what 'supplies' did the others go for?"
    {Hater} exhales amusedly through {getPronoun(Hater, "possessive")} nose. "
    {
        - MissingKid=="Pete":
        <>Pete thought we needed to stock up on edibles. {Hater != "Emily":I thought it was kind of impulsive.}
        {
            - PlayerName != "Emily":
            {Hater != "Emily":<> Emily was all in, though.}
        }
        <>So they went off to a dispensary."
        - MissingKid=="Cliff":
        <>Well, we stocked up on beer, but Cliff criticized our choices. Said the goal was quantity, not quality. So they went to Costco to see what sixty dollars could get them."
        - MissingKid=="Megan":
        <>I don't know. Some video game thing--a multitap. It involved Bomberman."
        - else:
        <>It involved snacks. Definitely nothing important. But I guess that's a part of the process."
    }
+ "How have you been, {Hater}?"
    "I've been ok. 
    {
        - Hater=="Francine":<> Things continue apace.
        - Hater=="Cliff":<> Good as I ever am.
        - Hater=="Gary":<> Keeping my head up pretty well. Maintaining. Talking too much. You know.
        - else:<> Keeping my head up.
    }
    <> How about you?"
    ++ "Good.["] Fine. Not bad."
    ++ "I'm fine."
    ++  "Not great[."], but that's how it goes."
    "Yeah," {Hater} replies.
+ [Say nothing.]
    "I'm looking forward to checking out the airbnb house," {Hater} says. "The place looks amazing, and I haven't been out in that direction in a long time."
    ++ "Yeah.[ The desert is cool."]"
    ++ "Yeah.[ Architecture is cool."]"
    ++ "Yeah.[ Astronomy is cool."]"
    --<> You hestitate to speak again. You used to be better at this.
- There's a long pause. {Hater} is the first person to try to address it.
"I, uh... I've been feeling lately like I have to get a grip on some things in my life." {Hater} looks at you expectantly.
+ ["Are you talking about me?"]
+ ["Are you okay?"]
+ ["Are you apologizing?"]
- "Oh, yeah?" you say, half-baked.
"Yeah. I feel like I get controlled a lot by negative emotions. And that's not a healthy way to be. And..."
There's a pause as {Hater} gathers {getPronoun(Hater, "possessive")} thoughts.
"This is such {Swear(Hater)}{SwearLevel==0:a difficult subject}{SwearLevel==1:hard stuff}{SwearLevel==2:hard shit} to talk about."
+"Are you talking about me?"
    "Yeah. Y-yeah, I guess I am. I feel like I have a habit of pushing people away.
+"Are you okay?"
    "I, uh. I am. Things are good. I want to be sure things are good with you.
+"Are you apologizing?"
    "Ssort of? Yeah. Yes, I am. I guess. 
    
-<> Sorry I have kind of been treating you like a stranger. It's hard to get out of my shell some days."

+ "I get it.["] I have a problem with this kind of thing. But I'm glad we're talking about it now. Though
+ "I'm sorry too.["] I don't want you to feel like this is all you. All we did was not talk for a while. That kind of thing is easy to fix. But
+ "I don't know why this stuff is so hard, but it is.["] Like, we didn't even fall out or something. We just stopped talking for a while. It should be meaningless. But
    
- <> doing it is completely terrifying to me."
{Hater} looks surprised. "Yeah?"

"Yeah," you say, taking a pause. "I'm just so {Swear(PlayerName)}{SwearLevel==1:god damn}{SwearLevel==2:fucking} afraid of everything. I try to think about the point where it turned, where fear went from being a healthy motivator to being an oppressor. I try to pinpoint it but I can't figure it out. And it's just so exhausting and it just limits me in every aspect of my life."
"{PlayerName!="Francine":Like, }I'm so afraid people are going to find any of this out. I try to present like I'm just anxious or just a homebody or something but the truth is I'm actively afraid of leaving my house. It's embarassing.
{
    - PlayerName=="Francine":<> I must look like such a fucking asshole, like I'm looking at all my friends and seeing nothing but variables. But I'm so scared and overwhelmed and all of that is just piling up on top of me. I can't breathe."
    - else:
    <> I want to be able to take it easy, to just sit around with my friends."
}

+ "I want it to stop.["] 
    <> {PlayerName!="Francine":Like, }I don't want to die or to just drink my days away or something. I want things to turn, and change. For this stuff to have been the first two acts of a story with a happy fucking ending."
    
    "I want credits to roll."
+ "I just want to be able to talk about it.["]
    <>I want to be able to talk about this without being afraid of people disowning me."
+ "I'm afraid of living without this feeling.["]
    <>That might be the worst thing about it. Like, if this is all there is... if there's not a way out. I don't want these things to stop. I want to live in this valley forever."
    
-+ "I honestly don't know what to do.["] I don't know how to stop getting murdered and I don't know how to get out of that fucking loop."
+ "I keep waiting for this to change.["] But I don't think it will. I don't think I'm ever going to stop getting killed by that fucking creature."
+ "I'm sick of dreaming about my teeth falling out[."] and I'm sick of getting killed by that fucking creature."
- "I want to get out of that house so {PlayerName!="Francine":bad}{PlayerName=="Francine":badly}, {Hater}."
There's silence as {Hater} considers {getPronoun(Hater, "possessive")} response.
"I'm here with you, {PlayerName}."

THAT IS THE END OF ACT 2 OF THE STORY THANK YOU FOR PLAYING
->END

=== Flashback_Hater_Talk_Scratch ===
You sit and chat--small talk, at first, and then gradually moving on from mere tension-breakers to something more substantive. It feels good. You talk for a while about your neuroses, realizing that talking about something so personal cuts close to a dangerous amount of emotional expsoure. Still, you continue on, guarded but generous. More generous than you have been in a while.

Things reach an emotional crescendo. You're glad to talk about your pain. And you're glad to talk about the shared experience of being caught in a time loop, pursued and killed by a murderer, all while being unable to talk about it with the people you care about. You resolve to do something about it.

THAT'S IT FOR NOW THAT'S A CLIFFHANGER
->END


=== Act_3 ===
enemy and player are allies, working together -- kids push against the boundaries of the loop -- live long enough to escape
->LoopHub



=== LoopHub ===
->LoopReset
+[Full loop.]
->LoopReset
+[Mini loop.]
->MiniLoop
->DONE

=== MiniLoop ===
You arrive at the house around 7:00. {DoorGetter} lets you inside. You find that {MissingKid} hasn't shown up yet.
->MiniLoopChoice

=== MiniLoop_End ===
You begin to settle in. Suddenly, a creature kills everyone, including you.
->FlashbackHub



=== Flashback_d ===
{MissingKid} is a ghost.
->LoopHub



=== Flashback_a ===
//this dream can happen anytime
~aComplete = true
Billowing smoke rises in a tilted column into the moonlit blue desert sky. Below, the remains of a crashed plane burn a brilliant orange, the only light source around for miles. You are the sole survivor, though you're losing strength fast. You've managed to drag yourself a few hundred feet from the fire.
->Flashback_a_hub

=== Flashback_a_hub ===
+[Keep going.]
    ~flashbackaTurns = flashbackaTurns +1
    ~flashbackaDistance = flashbackaDistance +1
    {
        - flashbackaTurns < 6: 
            {flashbackaTurns < 3: You continue to drag yourself away from the flames.}
            {flashbackaTurns > 2: You continue to drag yourself away from the figure.}
    }
+[Wait.]
    ~flashbackaTurns = flashbackaTurns + 1
    {flashbackaTurns < 6: You stop struggling for a while.}

- ~flashbackaDistance = flashbackaDistance - 2

{flashbackaTurns == 2: A figure appears over the crest of a sand dune, barely visible.}
{
    - flashbackaTurns == 3: 
    {
        - flashbackaDistance > 6: The figure seems to notice your movement as you scramble away. You can't discern the details. 
        - flashbackaDistance < 7: The figure walks quickly towards you. You still can't discern the details.
    }
}
{
    - flashbackaTurns == 4: 
    {
        - flashbackaDistance < 6: The figure begins to pull a backpack off its shoulders as it approaches you.
        - flashbackaDistance > 5: The figure hastens its pace, holding the straps of its backpack as it runs.
    }
}
{
    - flashbackaTurns == 5: 
    {
        - flashbackaDistance < 5: The figure's halting gait shows signs of exhaustion. Whoever they are, they seem relieved to have reached you. Even at this close distance, you can't make out its facial features.
        - flashbackaDistance > 4: You exhaust yourself scrambling against the soft desert sand. The figure closes in on you.
    }
}
{flashbackaTurns == 6: As the figure moves close enough to loom over you, it stretches and distorts into a mockery of a human body. Its now-enormous, wet eyes glisten in the moonlight as they extrude outward towards you. Its shoulders dislocate and its impossibly long forearms are bent on sinewy shoulders to reach out to hold you. Your senses fail.}
{flashbackaTurns == 6: It is dark.}
{flashbackaTurns == 6: ->LoopHub}
->Flashback_a_hub



=== Flashback_b ===
//this dream takes place after hater is defined
You're alone in your apartment. It's bright as early morning, like you've just woken up and opened the blinds and your eyes are still adjusting. You have no idea what time of day it actually is. ->Flashback_b_hub

=== Flashback_b_hub ===
+ [Check the clock.]
    The clock isn't right. It's flashing 12:00. ->Flashback_b_hub
+ [Look out the window.]
    Out the main window of the living area you see {Hater} standing in profile, lightly silhouetted by the sunlight. As you notice {getPronoun(Hater, "object")}, {getPronoun(Hater, "subject")} walks out of your view. You run outside--what felt like your apartment is a small family house in a suburb so vacant you can feel the curvature of the earth. 
    
- In the yawning sky above you is a circular hole, too vast to comprehend but seemingly infinitely wide, through which you can make out the entirety of the existence.

+ [Climb into the hole.]    
    You climb on your roof and set up a ladder, eager to climb into the hole.
    
    Your memory becomes a blur as you crest the opening and begin to plummet into eternity. The infinity of your newfound understanding is some comfort to you as you are distorted beyond recognition by the ravages of time-without-time.
+ [Do nothing.]
    You stare at the hole for a while, as your disbelief fades to bewilderment and finally to resignation. You go inside to make food, knowing that eventually you will be claimed by the void.
    - ~bComplete = true
    ->LoopHub
    
    
    
=== Flashback_c ===
//this dream can happen anytime, preferably before hater is defined
You're in the foyer of a house you think you remember. 
->Flashback_c_hub

=== Flashback_c_hub ===
+ [Look around.]
    {flashbackcTurns == 0: The foyer is constructed almost exclusively out of a rich, orange-red wood that radiates a warmth that makes it feel like a refuge from the wind and cold outside. The two-storey cathedral ceiling is tall enough that you unconsciously avoid looking up into it, as the green-papered walls recede into an ominous and overbearing darkness above.}
    {flashbackcTurns == 1: You're in the house's tall, beautiful foyer. There is a faceless figure watching you from just outside your vision.}
    ->Flashback_c_hub
+ [Check the time on the grandfather clock.]
    The clock is stopped at 12:00.
    ->Flashback_c_hub
+ [Move...]
    ++[...to the living room.]
        You begin to head through the large door into the living room
    ++[...to the vestibule.]
        You start to step through the wooden cutout door into the vestibule
    ++[...to the upstairs study.]
        You reach for the stairway banister
    ++[...to the upstairs hallway.]
        You reach for the stairway banister
    ++[...out the front door.]
        You turn to step out the front door
    ++[...Never mind.]
        ->Flashback_c_hub

- {
    - flashbackcTurns == 0: 
        <>, but as you do you are distracted by the sound of bare feet skittering across the house's old wood. Through the living room door you catch a glimpse of a small figure disappearing into the kitchen... and then another darts out of your view on the upstairs landing.
    ~flashbackcTurns = 1
    ->Flashback_c_hub
    - flashbackcTurns == 1:
        <>, but are surprised by more of the strange figures. One scrambles into the room from the kitchen vestibule, then one from upstairs, and more, until you are surrounded. They are smaller than average humans, featureless, with orange-red skin. They are unclothed, and their feet slap against the polished floor with a sound like stretching leather.
        
        One of the creatures grabs you by the arm as they encircle you. Another reaches you, throwing its arm around your waist, and then another puts its hand on your shoulder.
        
        Five of the beings surround you, their lukewarm skin tight against yours as they hold you in an embrace. Their tight grip relaxes as they feel more comfortable around you.
        
        Over your shoulder, out of your sight, a large moon-faced figure watches, its two left eyes full of anger and hurt.
        ~cComplete = true
}
->LoopHub



=== Flashback_MissingKid_Hint ===
{
    - Flashback_MissingKid_Hint == 1:
In darkness you see {MissingKid}, standing solemnly. {getPronounCaps(MissingKid, "possessive")} eyeline slowly rises up from the floor, meeting yours.
    - else:
You're alone in the darkness, yet slowly you gather the sense of the house unfurled around you; every board, brick, and nail laid out for you to inspect. You hear {MissingKid}'s voice--a voice you barely remember.
}
{
    - Hint2:
    "You see yourself in the attic of the house, and a machine at full power, and a blinding flash."
    - else:
    {
        - CellarMachineKnowledge:
    "You see yourself in the cellar of the house, palms sweaty as you manipulate a bank of twinkling lights on a machine that needs more power."
        - else:
    "You see yourself in the cellar of the house, palms sweaty as you tear against the secure housing of a machine. You see a trunk in the attic."
    }
}
{
    - Flashback_MissingKid_Hint == 1:
    "You see yourself, coping in ways you understand and in ways you don't."
    
    There's a kind of cellophane crinkle that grows into a crunch that you seem to hear with your spine as {MissingKid}'s body begins to collapse downwards into itself.
    - else:
    The voice seems to fade, slipping from your memory.
}
{
    - Flashback_MissingKid_Hint == 1:
    "...and you see me, against a tree in the woods beyond the treeline, my throat cut.
    {
        - Hater != "Crobus":
        <> You blame yourself, as you know {Hater} does, and you know {getPronoun(Hater, "object")} is coming for you. And, whatever happens, you know {getPronoun(Hater, "object")} will die hating you."
        - else:
        <> You blame yourself, in spite of your best efforts not to."
    }
    - else:
    {
        - Hater != "Crobus":
        "...and you see {Hater} preparing to use it against you. And you know that {getPronoun(Hater, "object")} won't be convinced to stop."
        - else:
        "...and you know that even though you will tire, there is a pain that will not stop."
    }
}
{Flashback_MissingKid_Hint == 1: What's left of {MissingKid} is a crumpled puck on the ground in front of you.}

It is dark.
->LoopHub

->DONE