VAR TalkWelcome = false
VAR GeneralTalkCount = 0
VAR GeneralTalkHater = 0
VAR GeneralTalkScary = 0
VAR GeneralTalkScaryMissing = 0

=== GenericTalk ===
{AddTime(1)}
{
    - FlashbackNumber == 666:
    ~Flashback = false
}
{
- Speaker != Hater:
{
    - Flashback == false:
    {
        - TalkWelcome == false:
        ~Flashback = true
        ->Talk_Welcome
    }
    {
        - Follower == Speaker:
            +[Stop following me.]
                {StopFollowing()}
                ->GoToLocation
        - else:
            +[Come with me.]
                <-FollowAsk
    }
    {
    - CreechLocation != PlayerLocation:
        {
            - Scary == false:
        <-GenericTalk_Topic
            - else:
        <-GeneralTalk_ScaryQuestions
        }
    }
    +[Where is...]
        <-LocationAsk
    +[Chat.]
        You chat with {Speaker}.
        {
            - Speaker == "Cliff":
                {AddPoints("Cliff", 1)}
            - Speaker == "Emily":
                {AddPoints("Emily", 1)}
            - Speaker == "Francine":
                {AddPoints("Francine", 1)}
            - Speaker == "Gary":
                {AddPoints("Gary", 1)}
            - Speaker == "Laura":
                {AddPoints("Laura", 1)}
            - Speaker == "Megan":
                {AddPoints("Megan", 1)}
            - Speaker == "Pete":
                {AddPoints("Pete", 1)}
        }
        ->GoToLocation
}
}

=== GenericTalk_Topic ===
{
- GeneralTalkCount == 0:
+ "So what's the plan for the weekend?"
    {AddPoints(Speaker, 1)}
    ~GeneralTalkCount = GeneralTalkCount + 1
    {
        - Speaker == "Megan":
        "I'm down for anything," Megan says, hesitating. "...Honestly it's kind of embarrassing to admit, but even with all the nature around, I'm stoked to have a chance to chill and play games with friends."
        - Speaker == "Francine":
        "I'm already enjoying the environs enough. I think tomorrow we'll be hiking out to the desert to try to watch the Perseids, which should be exciting."
        - Speaker == "Pete":
        "We are waking up slowly tomorrow. That is all I know. That is all I care about."
        - Speaker == "Cliff": 
        "I'm happy doing whatever."
        - else:
        {shuffle:
        -
        "I don't know! I feel like {MissingKid} had a better idea than anyone. Maybe once {getPronoun(MissingKid, "subject")} shows up we'll figure something specific out."
        -
        "I'm not sure, but I know the plan is to leave sometime in the evening to watch the Perseids. We'll know more once {MissingKid} shows up."
        }
    }
    ->GoToLocation
}
{
- GeneralTalkCount == 1:
+ "This house is pretty creepy, huh?"
    {AddPoints(Speaker, 1)}
    ~GeneralTalkCount = GeneralTalkCount + 1
    {
        - Speaker == "Francine":
        "I think it's beautiful. I'm not particularly frightened by inanimate objects."
        - Speaker == "Megan":
        "It is. I kind of hate it but kind of love it? It gives off major 7th Guest vibes."
        - Speaker == "Cliff":
        "Yeah. I want to get some air."
        Maybe tomorrow, right?
        "Yeah. Tomorrow."
        - Speaker == "Gary":
        "Totally. I feel like somebody's weird uncle should bequeath us his fortune for staying here."
        - else:
        "Yes, it is. I can't believe how dark it is outside, too."
    }
    ->GoToLocation
}
{
- GeneralTalkCount == 2:
+ "Has anyone heard from {MissingKid}?"
    {AddPoints(Speaker, 1)}
    ~GeneralTalkCount = 0
        "We had a thread going. {Speaker=="Francine":The last thing}{Speaker!="Francine":Last thing} we heard{Speaker == "Francine":<> was that }{Speaker!="Francine":, }{getPronoun(MissingKid, "subject")} was on {getPronoun(MissingKid, "possessive")} way. That was around 6:30?"
        {Speaker!="Cliff":"{getPronounCaps(MissingKid, "subject")} should be here soon."}
        
    ->GoToLocation
}
{
- Hater != "Crobus":
    <-GeneralTalk_HaterQuestions
}
->DONE

=== GeneralTalk_HaterQuestions ===
{
    - GeneralTalkHater == 0:
        + "Do you know what's up with {Hater}?"
        {AddPoints(Speaker, 1)}
        ~GeneralTalkHater = GeneralTalkHater + 1
        "I was {Speaker=="Francine":going to}{Speaker!="Francine":gonna} ask you the same thing, {PlayerName}," {Speaker} says. "I know there's been some distance but I thought you two were friends."
        "To answer your question, though, I have no idea." ->GoToLocation
}
{
    - GeneralTalkHater > 0:
        + "Has {Hater} said anything to you about me?"
        ~GeneralTalkHater = GeneralTalkHater + 1
        {AddPoints(Speaker, 1)}
        {
            - Speaker == BFF:
            "To be honest, {PlayerName}, yeah. {Hater} was trying to convince me you were up to something and that we shouldn't let you in. {getPronounCaps(Hater, "subject")} said you were plotting something, that you were going to shut off the power? {Swear(Speaker)}{SwearLevel==0:It was}{SwearLevel==1:Freaking}{SwearLevel==2:Fuckin'} weird."
            - Speaker == "Cliff":
            "I don't think {Hater} is thinking clearly. That's all I'll say."
            - Speaker == "Emily":
            "Are you guys fighting? Fuck, that's really sad to think about!"
            - Speaker == "Pete":
            "I think you guys both need to chill out. {getPronounCaps(Hater, "subject")} said some pretty dicey stuff before you showed up."
            - else:
            "I don't really know what to say. {getPronounCaps(Hater, "subject")}'s been saying a lot of things."
        }
        ->GoToLocation
}
->DONE

=== GeneralTalk_ScaryQuestions ===
{Swear(PlayerName)}
{
    - GeneralTalkScary == 0:
    + "What {SwearLevel>0:the hell }is going on?"
    ~GeneralTalkScary = GeneralTalkScary + 1
    {AddPoints(Speaker, 1)}
    "I have no idea! This is so messed up!"
    ->GoToLocation
}
{
- GeneralTalkScary == 1:
    {
    - SawCreech:
        + "What {SwearLevel==1:the hell}{SwearLevel==2:the fuck} is that thing?"
        ~GeneralTalkScary = GeneralTalkScary + 1
        {AddPoints(Speaker, 1)}
        {Swear(Speaker)}
        {
        - GeneralTalkScaryMissing == false:
            ~GeneralTalkScaryMissing = true
        "{Speaker=="Francine":God, }{SwearLevel>2:Shit, }I don't know! Does this have something to do with {MissingKid}?"
            ++ "Do you think [{getPronoun(MissingKid, "subject")} was killed?"]that thing, that person, killed {getPronoun(MissingKid, "object")}?"
                "{Speaker!="Cliff":I'm so scared. }I hope not. I have no idea." ->GoToLocation
            ++ "Could that thing be {MissingKid}?"
                "{SwearLevel==0:Oh my God,}{SwearLevel==1:Crap,}{SwearLevel==2:Fuck,} {PlayerName}, are you serious? Do you really think that's possible?"
                {Speaker} visibly gives your idea some weight. ->GoToLocation
            ++ "I hope {getPronoun(MissingKid, "subject")}'s okay."
                "Me too."
        - else:
        "{Speaker=="Francine":God, }{SwearLevel>2:Shit, }I don't know!"
        }
        ->GoToLocation
    - else:
        ~GeneralTalkScary = GeneralTalkScary + 1
    }
}
{
    - GeneralTalkScary == 2:
        + "Are you ok?"
        ~GeneralTalkScary = 0
        {AddPoints(Speaker, 1)}
        {Speaker} doesn't say anything.
        ->GoToLocation
}
->DONE



=== FollowAsk ===
{
    - Speaker == "Cliff":
    {
        - Hater == "Cliff":
            {noFollowLine()}
        - CliffPoints > FollowGoal:
            {followLine()}
            ~Follower = "Cliff"
        - else:
            {noFollowLine()}
    }
    - Speaker == "Emily":
    {
        - Hater == "Emily":
            {noFollowLine()}
        - EmilyPoints > FollowGoal:
            {followLine()}
            ~Follower = "Emily"
        - else:
            {noFollowLine()}
    }
    - Speaker == "Francine":
    {
        - Hater == "Francine":
            {noFollowLine()}
        - FrancinePoints > FollowGoal:
            {followLine()}
            ~Follower = "Francine"
        - else:
            {noFollowLine()}
    }
    - Speaker == "Gary":
    {
        - Hater == "Gary":
            {noFollowLine()}
        - GaryPoints > FollowGoal:
            {followLine()}
            ~Follower = "Gary"
        - else:
            {noFollowLine()}
    }
    - Speaker == "Laura":
    {
        - Hater == "Laura":
            {noFollowLine()}
        - LauraPoints > FollowGoal:
            {followLine()}
            ~Follower = "Laura"
        - else:
            {noFollowLine()}
    }
    - Speaker == "Megan":
    {
        - Hater == "Megan":
            {noFollowLine()}
        - MeganPoints > FollowGoal:
            {followLine()}
            ~Follower = "Megan"
        - else:
            {noFollowLine()}
    }
    - Speaker == "Pete":
    {
        - Hater == "Pete":
            {noFollowLine()}
        - PetePoints > FollowGoal:
            {followLine()}
            ~Follower = "Pete"
        - else:
            {noFollowLine()}
    }
}
->GoToLocation

=== function followLine ===
{
    - Follower != "Crobus":
        {StopFollowing()}
        {Follower} stops following you.
}
{
    - Scary == true:
        You gesture for {Speaker} to follow you.
        {shuffle:
            - {getPronounCaps(Speaker, "subject")} nods and moves toward you.
            - {getPronounCaps(Speaker, "subject")} creeps toward you.
        }
    - else:
        {shuffle:
            - {Greeting(PlayerName)}, {Speaker}, come with me!
            - {Greeting(PlayerName)}, {Speaker}, come check this out.
        }
        {shuffle:
            - "Sure thing, {PlayerName}." {getPronounCaps(Speaker, "subject")} moves over to you.
            - "OK." {getPronounCaps(Speaker, "subject")} heads your way.
        }
}
~return

=== function noFollowLine ===
{
    - Scary == true:
        You gesture for {Speaker} to follow you.
        {shuffle:
            - {getPronounCaps(Speaker, "subject")} shakes {getPronoun(Speaker, "possessive")} head "no."
            - {getPronounCaps(Speaker, "subject")} looks at you warily. {getPronounCaps(Speaker, "possessive")} eyes say "no."
        }
    - else:
        {shuffle:
            - "{Greeting(PlayerName)}, {Speaker}, come with me!"
            - "{Greeting(PlayerName)}, {Speaker}, come check this out."
        }
        {shuffle:
            - "No offense, {PlayerName}, but I'm 
                {
                    - Speaker == "Francine": <> a little preoccupied right now,"
                    - Speaker == "Cliff": <> busy right now,"
                    - else: <> a little busy right now,"
                }
                <> {getPronoun(Speaker, "subject")} says.
            - "Not right now, {PlayerName}," {getPronoun(Speaker, "subject")} says. "I'll catch up with you later."
        }
}
~return



=== CliffTalk ===
+ [...Cliff.]
    {SetSpeaker("Cliff")}
    { 
        - MissingKid == "Cliff":
            {missingKidLine("Cliff")} ->GoToLocation
        - Scary == false:
            You say hi to Cliff.
            ~Speaker = "Cliff"
        - else:
            You get Cliff's attention.
    }
    <-GenericTalk
    {
        - FlashbackNumber == 1:
            {AddPoints("Cliff", 1)} ->Flashback_1_Dialog
        - FlashbackNumber == 2:
            No! This isn't happening!
        - FlashbackNumber == 420:
            {
            - Hater == "Cliff":
            Cliff moves across the room, avoiding you. ->Flashback_Party_hub
            - else:
            You chat with Cliff.
            {AddPoints("Cliff", 1)} ->Flashback_Party_End
            }
    }
    {
        - Flashback == false:
        {
        - Hater == "Cliff":
            Cliff lowers his brow and grimaces, dubious that you'd even bother trying to speak to him.
            {MoveHaterRandom()}
            ->GoToLocation
        - Scary == false:
        }  
    }
    
    ++Never mind.
    
    - ->GoToLocation


    
=== EmilyTalk ===
+ [...Emily.]
    {SetSpeaker("Emily")}
    { 
        - MissingKid == "Emily":
            {missingKidLine("Emily")} ->GoToLocation
        - Scary == false:
            You say hi to Emily.
            ~Speaker = "Emily"
        - else:
            You get Emily's attention.
    }
    <-GenericTalk
    {
        - FlashbackNumber == 1:
            {AddPoints("Emily", 1)} ->Flashback_1_Dialog
        - FlashbackNumber == 2:
            No! This isn't happening!
        - FlashbackNumber == 420:
            {
            - Hater == "Emily":
            Emily moves across the room, avoiding you. ->Flashback_Party_hub
            - else:
            You chat with Emily.
            {AddPoints("Emily", 1)} ->Flashback_Party_End
            }
    }
    {
        - Flashback == false:
        {
        - Hater == "Emily":
            Emily glances in your direction but quickly looks away before your eyes can meet. She walks away.
            {MoveHaterRandom()}
            ->GoToLocation
        - Scary == false:
        }
    }
    
    ++Never mind.
    
    - ->GoToLocation
 
 
    
=== FrancineTalk ===
+ [...Francine.]
    {SetSpeaker("Francine")} 
    { 
        - MissingKid == "Francine":
            {missingKidLine("Francine")} ->GoToLocation
        - Scary == false:
            You say hi to Francine.
            ~Speaker = "Francine"
        - else:
            You get Francine's attention.
    }
    <-GenericTalk
    {
        - FlashbackNumber == 1:
            {AddPoints("Francine", 1)} ->Flashback_1_Dialog
        - FlashbackNumber == 2:
            No! This isn't happening!
        - FlashbackNumber == 420:
            {
            - Hater == "Francine":
            Francine moves across the room, avoiding you. ->Flashback_Party_hub
            - else:
            You chat with Francine.
            {AddPoints("Francine", 1)} ->Flashback_Party_End
            }
    }
    {
        - Flashback == false:
        {
        - Hater == "Francine":
            Francine nervously looks in your direction, her eyes full of contempt. She begins to speak but doesn't manage a full syllable. She walks away.
            {MoveHaterRandom()}
            ->GoToLocation
        - Scary == false:
            {
                - Follower != "Francine":
                {
                    - FrancineLocation == STUDY:
                    {
                        - Talk_Francine_Study1 == false:
                        +"How are the books?"
                        ->Talk_Francine_Study1
                    }
                }
            }
        }
    }
    
    ++Never mind.
    
    - ->GoToLocation
    
=== Talk_Francine_Study1 ===
{AddPoints("Francine", 1)}{AddTime(1)}
"From an anthropological standpoint, they're fascinating. I love seeing the contents of a person's bookshelf, and their kind of curatorial standard for the works they keep. It's exciting to paint a picture of a person based on that."
"As for the books themselves, they're not... bad. Per se. But very dated."
+"What can you tell about the owner from these books?"
"Oh, I forgot that you got here late. You didn't have the... pleasure of meeting him. He's very much a product of his time. And of a number of drugs."
"The books are similar,
+"Dated is good sometimes, right?"
"Yes, it can be. But that's why I said 'dated' instead of just 'old.' This stuff is all in the uncanny valley of antique merit--too old to love, too new to sell."
"And all of it very of-its-time, too,
+"The space sure is cool, though."
"Oh, absolutely. This place was definitely assembled by a number of people, each with a very particular eye. Eclectic. Then the owner comes and dumps a veneer of his interests on top of it."
And what would those be?
"Well, 
- <> kind of a 70s idea of being culturally progressive and learned. A bunch of different spiritual and religious texts, dusty but visibly never opened. Gary Snyder and Alan Watts. I assume this guy was more into the "Acid Test" part of the equation."
->GoToLocation


    
=== GaryTalk ===
+ [...Gary.]
    {SetSpeaker("Gary")}
    { 
        - MissingKid == "Gary":
            {missingKidLine("Gary")} ->GoToLocation
        - Scary == false:
            You say hi to Gary.
            ~Speaker = "Gary"
        - else:
            You get Gary's attention.
    }
    <-GenericTalk
    {
        - FlashbackNumber == 1:
            {AddPoints("Gary", 1)} ->Flashback_1_Dialog
        - FlashbackNumber == 2:
            No! This isn't happening!
        - FlashbackNumber == 420:
            {
            - Hater == "Gary":
            Gary moves across the room, avoiding you. ->Flashback_Party_hub
            - else:
            You chat with Gary.
            {AddPoints("Gary", 1)} ->Flashback_Party_End
            }
    }
    {
        - Flashback == false:
        {
        - Hater == "Gary":
            Gary turns his head in your direction, his eyes narrowed with contempt. He doesn't say anything. You try to turn your attention to something else.
            {MoveHaterRandom()}
            ->GoToLocation
        - Scary == false:
        }
    }
    
    ++Never mind.
    
    - ->GoToLocation


    
=== LauraTalk ===
+ [...Laura.]
    {SetSpeaker("Laura")} 
    {
        - MissingKid == "Laura":
            {missingKidLine("Laura")} ->GoToLocation
        - Scary == false:
            You say hi to Laura.
            ~Speaker = "Laura"
        - else:
            You get Laura's attention.
    }
    <-GenericTalk
    {
        - FlashbackNumber == 1:
            {AddPoints("Laura", 1)} ->Flashback_1_Dialog
        - FlashbackNumber == 2:
            No! This isn't happening!
        - FlashbackNumber == 420:
            {
            - Hater == "Laura":
            Laura moves across the room, avoiding you. ->Flashback_Party_hub
            - else:
            You chat with Laura.
            {AddPoints("Laura", 1)} ->Flashback_Party_End
            }
    }
    {
        - Flashback == false:
        {
        - Hater == "Laura":
            Laura shifts her weight uncomfortably when she notices you trying to get her attention. She casts a disdainful glance in your direction as she moves away.
            {MoveHaterRandom()}
            ->GoToLocation
        - Scary == false:
        }
    }
    
    ++Never mind.
        
    - ->GoToLocation


    
=== MeganTalk ===
+ [...Megan.]
    {SetSpeaker("Megan")}
    { 
        - MissingKid == "Megan":
            {missingKidLine("Megan")} ->GoToLocation
        - Scary == false:
            You say hi to Megan.
            ~Speaker = "Megan"
        - else:
            You get Megan's attention.
    }
    <-GenericTalk
    {
        - FlashbackNumber == 1:
            {AddPoints("Megan", 1)} ->Flashback_1_Dialog
        - FlashbackNumber == 2:
            No! This isn't happening!
        - FlashbackNumber == 420:
            {
            - Hater == "Megan":
            Megan moves across the room, avoiding you. ->Flashback_Party_hub
            - else:
            You chat with Megan.
            {AddPoints("Megan", 1)} ->Flashback_Party_End
            }
    }
    {
        - Flashback == false:
        {
        - Hater == "Megan":
            Megan quickly turns in your direction with a look of fear. She's deliberate in projecting her discomfort with you. She moves away.
            {MoveHaterRandom()}
            ->GoToLocation
        - Scary == false:
            {
                - Follower != "Megan":
                {
                    - MeganLocation == LIVINGROOM:
                        {
                        - PowerOn == true:
                            {
                                - Talk_Megan_Gaming1 == 0:
                                <-Talk_Megan_Gaming1
                                - else:
                                {
                                - Talk_Megan_Gaming2 == 0:
                                <-Talk_Megan_Gaming2
                                }
                            }
                        }
                }
                //else
            }
            
        }

    }

    ++Never mind.
    
    - ->GoToLocation
    
=== Talk_Megan_Gaming1 ===
+"What are you playing?"
    {AddPoints("Megan", 1)}{AddTime(1)}
    "This is an unlicensed 8-bit game called Pird Mystery. I'm the little block guy. I'm getting attacked by birds I guess? They're throwing eggs at me."
        **Unlicensed game?
            {AddTime(1)}
            "Yeah. These clones were popular in countries where the major consoles didn't get distributed, or were too expensive. A lot of people spent their childhoods playing these instead of the 'real' thing."
            ***It's like a divergent cultural history.
                "Right? I think it's super interesting. It feels a little weird to spend this much time sponging up somebody else's nostalgia, but I like to think I'm helping to preserve the memory of these games." 
                "Also, they're super funny and shitty? But I don't usually put that foot forward." ->GoToLocation
            ***Didn't anyone get in trouble for that?
                "I don't know. Legally it's kind of a gray area, but my guess is that nobody actually gave enough of a shit to take action on it." ->GoToLocation
        **Wait, "Pird"?
            {AddTime(1)}
            "Yeah, it says that on the front. "Paado". I don't know if they're different from normal birds."
            ***Are they supposed to be aliens?
                "That's what I'm guessing. Either aliens or some kind of parallel evolution. They're rising up against humanity. It's like The Happening." ->GoToLocation
            ***"I feel very understood by this game.["]
                <> I've always thought birds were malicious animals."
                "I totally agree. It's in the eyes. I don't trust them." ->GoToLocation
            ***Is that the mystery the title refers to?
                "I think it is! I wonder what the people at Grubsoft had in mind." ->GoToLocation
        **So you avoid the eggs?
            "I thought so at first, but turns out you actually jump OVER the eggs. It doesn't make a lot of sense. It says it's an "Egg Jump Action Game."
            {Swear(PlayerName)}{SwearLevel==2:Shit.} It's too bad that genre never took off.
            "Right?" ->GoToLocation
- ->GoToLocation

=== Talk_Megan_Gaming2 ===
+"How's your game?"
{AddPoints("Megan", 1)} Megan doesn't look away from the screen, too busy tricking birds that criss-cross the screen into shitting eggs onto another, larger bird.
"It's good!"
->GoToLocation


    
=== PeteTalk ===
+ [...Pete.]
    {SetSpeaker("Pete")} 
    { 
        - MissingKid == "Pete":
            {missingKidLine("Pete")} ->GoToLocation
        - Scary == false:
    You say hi to Pete.
    ~Speaker = "Pete"
        - else:
            You get Pete's attention.
    }
    <-GenericTalk
    {
        - FlashbackNumber == 1:
            {AddPoints("Pete", 1)} ->Flashback_1_Dialog
        - FlashbackNumber == 2:
            No! This isn't happening!
        - FlashbackNumber == 420:
            {
            - Hater == "Pete":
            Pete moves across the room, avoiding you. ->Flashback_Party_hub
            - else:
            You chat with Pete.
            {AddPoints("Pete", 1)} ->Flashback_Party_End
            }
    }
    {
        - Flashback == false:
        {
        - Hater == "Pete":
            Pete palpably considers playing it cool, but he clearly doesn't want to speak with you. He turns to pay attention to something else.
                {MoveHaterRandom()}
            ->GoToLocation
        - Scary == false:
            {
                - Follower != "Pete":
                {
                    - PeteLocation == KITCHEN:
                        {
                        - Talk_Pete_Cooking1 == false:
                        + "What are you working on?"
                        ->Talk_Pete_Cooking1
                        - else:
                        {
                            - Talk_Pete_Name1 == false:
                            + "About your name...["]
                            ->Talk_Pete_Name1
                        }
                        }
                }
            }
            {
                - Talk_Pete_Cooking1 == true:
                {
                    - Talk_Pete_Cooking2 == false:
                    +How's the cooking going?
                    ->Talk_Pete_Cooking2
                }
            }
        }
    }

    
    ++Never mind.
        
    - ->GoToLocation
    
=== Talk_Pete_Cooking1 ===
{AddPoints("Pete", 1)}{AddTime(1)}
"I'm kind of improvising a breaded eggplant. I was gonna make chicken parm, but I actually became a vegetarian yesterday? So I bought this instead."
+"Eggplant can be tricky."
    {PlayerName!= "Cliff":"For me, at least."}
"It can be! It's hard to cook it consistently without totally drying it out. A dried-out eggplant husk is a sad thing."
"This isn't my first rodeo, though."
+"Just yesterday[?"], huh? I think of it as a decision people tend to make more slowly."
"That might be true, but a lot of people probably decide it based on spur-of-the-moment revulsion, too. I read about a fish called the orange roughy--a fish that has a potential lifespan of like 75 years? And until like ten years ago it was the main source for the Filet o' Fish, so now it's severely underpopulated. That fuckin' turned me around for good."
"Or at least until I get passionate about something else. I dunno, I guess we'll see where I'm at next week."
+"Pete, your last name is Stroganoff, right?"
"It is. And I'm not Italian, but I am definitely in front of you rolling an eggplant in Italian breadcrumbs, yes. What is your point?"
- ->GoToLocation

=== Talk_Pete_Cooking2 ===
{AddPoints("Pete", 1)}
"It's going good. The altitude above sea level here is fucking perfect for cooking."
->GoToLocation

=== Talk_Pete_Name1 ===
{AddPoints("Pete", 1)}
<>Stroganoff? You've... noticed the potential for double entendre, yes?"
"Actually... NO, I hadn't! Not until today. Thank you for illuminating me."
->GoToLocation



=== LocationAsk ===
{
    - Speaker == "Cliff":
    - CliffLocation == PLAYER:
    - CliffLocation == PlayerLocation:
    - else: +[...Cliff?]
        "Where is Cliff?" you ask.
        {
        - CliffLocation == MISSING: 
            "{randomMissingLine("Cliff")}" {Speaker} says.
            ->GoToLocation
        - else:
            {
                - Hater == "Cliff":
                {
                    - CliffLocation == CELLAR:
                    "{randomHaterCellarLine("Cliff")}" {Speaker} says.
                    - else:
                    "{randomHaterLocationLine("Cliff", CliffLocation)}" {Speaker} says.
                }
                ->GoToLocation    
            }
            "{randomLocationLine("Cliff", CliffLocation)}" {Speaker} says.
        }
        ->GoToLocation
}
{
    - Speaker == "Emily":
    - EmilyLocation == PLAYER:
    - EmilyLocation == PlayerLocation:
    - else: +[...Emily?]
        "Where is Emily?" you ask.
        {
        - EmilyLocation == MISSING: 
            "{randomMissingLine("Emily")}" {Speaker} says.
            ->GoToLocation
        - else:
            {
                - Hater == "Emily":
                {
                    - EmilyLocation == CELLAR:
                    "{randomHaterCellarLine("Emily")}" {Speaker} says.
                    - else:
                    "{randomHaterLocationLine("Emily", EmilyLocation)}" {Speaker} says.
                }
                ->GoToLocation    
            }
            "{randomLocationLine("Emily", EmilyLocation)}" {Speaker} says.
        }
        ->GoToLocation
}
{
    - Speaker == "Francine":
    - FrancineLocation == PLAYER:
    - FrancineLocation == PlayerLocation:
    - else: +[...Francine?]
        "Where is Francine?" you ask.
        {
        - FrancineLocation == MISSING: 
            "{randomMissingLine("Francine")}" {Speaker} says.
            ->GoToLocation
        - else:
            {
                - Hater == "Francine":
                {
                    - FrancineLocation == CELLAR:
                    "{randomHaterCellarLine("Francine")}" {Speaker} says.
                    - else:
                    "{randomHaterLocationLine("Francine", FrancineLocation)}" {Speaker} says.
                }
                ->GoToLocation    
            }
            "{randomLocationLine("Francine", FrancineLocation)}" {Speaker} says.
        }
        ->GoToLocation
}
{
    - Speaker == "Gary":
    - GaryLocation == PLAYER:
    - GaryLocation == PlayerLocation:
    - else: +[...Gary?]
        "Where is Gary?" you ask.
        {
        - GaryLocation == MISSING: 
            "{randomMissingLine("Gary")}" {Speaker} says.
            ->GoToLocation
        - else:
            {
                - Hater == "Gary":
                {
                    - GaryLocation == CELLAR:
                    "{randomHaterCellarLine("Gary")}" {Speaker} says.
                    - else:
                    "{randomHaterLocationLine("Gary", GaryLocation)}" {Speaker} says.
                }
                ->GoToLocation    
            }
            "{randomLocationLine("Gary", GaryLocation)}" {Speaker} says.
        }
        ->GoToLocation
}
{
    - Speaker == "Laura":
    - LauraLocation == PLAYER:
    - LauraLocation == PlayerLocation:
    - else: +[...Laura?]
        "Where is Laura?" you ask.
        {
        - LauraLocation == MISSING: 
            "{randomMissingLine("Laura")}" {Speaker} says.
            ->GoToLocation
        - else:
            {
                - Hater == "Laura":
                {
                    - LauraLocation == CELLAR:
                    "{randomHaterCellarLine("Laura")}" {Speaker} says.
                    - else:
                    "{randomHaterLocationLine("Laura", LauraLocation)}" {Speaker} says.
                }
                ->GoToLocation    
            }
            "{randomLocationLine("Laura", LauraLocation)}" {Speaker} says.
        }
        ->GoToLocation
}
{
    - Speaker == "Megan":
    - MeganLocation == PLAYER:
    - MeganLocation == PlayerLocation:
    - else: +[...Megan?]
        "Where is Megan?" you ask.
        {
        - MeganLocation == MISSING: 
            "{randomMissingLine("Megan")}" {Speaker} says.
            ->GoToLocation
        - else:
            {
                - Hater == "Megan":
                {
                    - MeganLocation == CELLAR:
                    "{randomHaterCellarLine("Megan")}" {Speaker} says.
                    - else:
                    "{randomHaterLocationLine("Megan", MeganLocation)}" {Speaker} says.
                }
                ->GoToLocation    
            }
            "{randomLocationLine("Megan", MeganLocation)}" {Speaker} says.
        }
        ->GoToLocation
}
{
    - Speaker == "Pete":
    - PeteLocation == PLAYER:
    - PeteLocation == PlayerLocation:
    - else: +[...Pete?]
        "Where is Pete?" you ask.
        {
        - PeteLocation == MISSING: 
            "{randomMissingLine("Pete")}" {Speaker} says.
            ->GoToLocation
        - else:
            {
                - Hater == "Pete":
                {
                    - PeteLocation == CELLAR:
                    "{randomHaterCellarLine("Pete")}" {Speaker} says.
                    - else:
                    "{randomHaterLocationLine("Pete", PeteLocation)}" {Speaker} says.
                }
                ->GoToLocation    
            }
            "{randomLocationLine("Pete", PeteLocation)}" {Speaker} says.
        }
        ->GoToLocation
}

+[Never mind.]

->GoToLocation

=== function randomLocationLine(x,y) ===
{shuffle:
    - ~return "I think {getPronoun(x, "subject")}'s in the {getCharLocation(y)} right now,"
    - ~return "{getPronounCaps(x, "subject")}'s in the {getCharLocation(y)} right now,"
} 
=== function randomMissingLine(x) ===
{shuffle:
    - ~return "I think {getPronoun(x, "subject")}'s still not here yet,"
    - ~return "I don't know. {getPronounCaps(x, "subject")} might not be here yet,"
} 

=== function missingKidLine(x) ===
{shuffle:
    - ~return "You turn to speak to {x}, but don't find the words. {getPronounCaps(x, "subject")} looks at you reassuringly."
}

=== function randomHaterLocationLine(x,y) ===
{
    - y == HIDDEN:
    {shuffle:
        - ~return "I don't know where {getPronoun(x, "subject")} is, actually. Is {getPronoun(x, "subject")} avoiding you or something?"
        - ~return "I don't know where {getPronoun(x, "subject")} is... I saw {getPronoun(x, "object")} sneaking around earlier, though."
    } 
    - else:
    {shuffle:
        - ~return "I think {getPronoun(x, "subject")}'s in the {getCharLocation(y)}. Is {getPronoun(x, "subject")} avoiding you or something?"
        - ~return "{getPronounCaps(x, "subject")}'s in the {getCharLocation(y)} right now. I don't know why,"
    } 
}

=== function randomHaterCellarLine(x) ===
{shuffle:
    - ~return "I think {getPronoun(x, "subject")}'s in the cellar. Is {getPronoun(x, "subject")} up to something?"
    - ~return "{getPronounCaps(x, "subject")}'s in the basement. I thought we weren't supposed to go down there,"
    - ~return "I saw {getPronoun(x, "object")} going down to the cellar. Aren't we supposed to stay out of there?"
} 

=== function getCharLocation(y) ===
{
    - y == FOYER:
        ~return "foyer"
    - y == LIVINGROOM:
        ~return "living room"
    - y == STUDY:
        ~return "study"
    - y == KITCHEN:
        ~return "kitchen"
    - y == HALLWAY1:
        ~return "hallway"
    - y == HALLWAY2:
        ~return "hallway"
    - y == VESTIBULE:
        ~return "vestibule"
    - y == GARAGE:
        ~return "garage"
    - y == GUESTBEDROOM:
        ~return "guest bedroom"
    - y == MASTERBEDROOM:
        ~return "master bedroom"
    - y == CELLAR:
        ~return "cellar"
}



=== HaterLevel1Scene ===
{Hater} approaches you, regarding you with a tired expression.
+"Oh, hi, {Hater}."
+"Hey, what's up?"
+"Hello...?"
- {getPronounCaps(Hater, "subject")} seems to sigh even as {getPronoun(Hater, "subject")} begins to speak.
"{Greeting(Hater)}, {PlayerName}. Listen, I have been watching you--we've all been watching you and some of us are getting worried. Even your best friend {BFF}. You're acting strange, it's making people uncomfortable."
{getPronounCaps(Hater, "possessive")} two left eyes shift and narrow noticably.
+ "Are you feeling ok?"
    "I'm fine.
+ "I'm sorry."
    "Nice of you to say.
+ "I'm just doing what I can."
    "You are? You're scaring people, you're scaring me. That behavior comes to you naturally?
- <> Listen, I wanted to try to reason with you but I can tell you won't be reasonable. Please, just stop. Sit and play video games. Relax in a sleeping bag and play with your Ouija board or whatever. But stop."
{getPronounCaps(Hater, "subject")} backs away.
~HaterLevel1SceneTrigger = false
~HaterLevel1Done = true
{MoveHaterRandom()}
->GoToLocation



=== AtticHaterScene ===
~HaterAttic = 2
~NoSelfDestruct = true
~CreechMove = true
~CreechLocation = LIVINGROOM
{AddTime(1)}
{Swear(Hater)}
"{PlayerName}!"
You turn to see {Hater} hurriedly emerging from a dark corner of the attic.
    + "{Hater}? What are you doing up here?"
    + "I know you think I'm against you somehow.["]
        <> It's irrational."
    + "What is your fucking problem with me?"
        {getPronounCaps(Hater, "subject")} scoffs. "What {SwearLevel > 0:the fuck }{Hater == "Francine":the heck }is your problem with me?"
    - {AddTime(2)}
    
    "I've seen you sneaking up here and messing with whatever that thing is," {getPronoun(Hater, "subject")} says, gesturing toward the machine. "Everything was fine until you started this."
    + "What are you talking about?"
    + "'Fine'?["]
        <> What part of the masked-murderer scenario could be described as 'fine'?"
    + "You're the one who's been sneaking around[."] like a weirdo and avoiding eye contact."
        "Don't give me that. I've been trying to stop you from making this {SwearLevel == 2:shitshow}{SwearLevel < 2:situation} any worse.{UsedOutbuildingBreaker: Don't think I didn't see you shutting off the power in the building outside.}"
    - {AddTime(2)}
    
    "All I know is what's in front of me. I've got one friend missing, {SwearLevel == 0:God-knows-where, }probably dead. I've got four friends downstairs, dying fast. And in front of me is the one person who seems to have any power in this thing, who I've seen walking around treating my friends like toys, about to turn on a mad-scientist machine."
    + "This machine is all we've got to go on.["]
        <> I've been all over this house. There's nothing else. This machine could end it."
        "Not like this!"
    + "Power? Is that what this is about?["]
        <> You've always treated me like you know what's right for me."
        "What? You're really trying to make this into some kind of personal thing?"
    + "Please. You have to let me try."
    - You reach for the lever on the machine. {Hater} charges you, slapping your hand away from the lever and grabbing you by the wrist, throwing your weight off and sending you both toward the ground. As you fall toward your back foot you manage to pivot {Hater}'s weight around and make {getPronoun(Hater, "object")} take the brunt of the impact.
    You get back to your feet.
    {AddTime(2)}
~CreechLocation = HALLWAY2
~AtticScene = false
~HaterAttic = 3
~HaterLevel = 3
->GoToLocation



=== AtticHaterMachine_Staged ===
The creature walks up the stairs behind you.

+[Operate the machine.]

- {CreechMinute == CreechMinuteSlow2: <> The lever is in the far left position.}
{CreechMinute == CreechMinuteSlow: <> The lever is in the left position.}
{CreechMinute == CreechMinuteNormal: <> The lever is in the middle position.}
{CreechMinute == CreechMinuteFast: <> The lever is in the right position.}

The lights read thusly:
{
    - AtticPower == 0:
    <>    █ █ █
    - AtticPower == 1:
    <>    ░ █ █
    - AtticPower == 2:
    <>    ░ ░ █
    - AtticPower == 3:
    <>    ░ ░ ░
}
+ [Put the lever in the far right position.]
    You push the lever into the far right position, and there's an audible crackle of electricity as a burst of energy explodes in the middle of the room. The creature, looming over you, is seemingly vaporized with a blinding flash. The lever returns to the right position and the lights on the machine fade.
    
    The power appears to have gone out.
    ~AtticMachineDischarged = true
    ~PowerOn = false
    ~ReservePower = ReservePower + AtticPower + HousePower
    ~AtticPower = 0
    ~HousePower = 0
    ~AtticMachineFired = true
    ~CreechMinute = CreechMinuteFast
        ~CreechDead = true
        ~CreechLocation = CREECH
        ->HaterCreechDeathScene

=== HaterCreechDeathScene ===
{
- HaterAttic != 3:
A plume of smoke disperses around where the creature once stood. As the mist settles to the floor, you see {Hater} walking toward you, a stunned expression on {getPronoun(Hater, "possessive")} face.
"{PlayerName}... you did it." {getPronounCaps(Hater, "subject")} pauses, hesitating.
"I'm sorry I {Hater=="Francine":doubted you.}{Hater!="Francine":didn't believe you.}"
- else:
{Hater} and you have a brief, stunned moment of relief in seeing the creature dead.
}
For a while, there's silence.
+ "I'm sorry about what happened to {MissingKid}.["]
    <> It's my fault what happened to {getPronoun(MissingKid, "object")}."
    {Hater} is stunned. "What are you
    {
        - Hater == "Francine":
        <> referring to?"
        - Hater == "Gary":
        <> referring to?"
        - Hater == "Emily":
        <> saying?"
        - else:
        <> talking about?
    }
    I just feel so {Swear(PlayerName)}{SwearLevel == 2:fucking }responsible for everything that has happened. If it wasn't for me, {getPronoun(MissingKid, "subject")} would be here right now.
    "I think you might be too deep into this thing, {PlayerName}," {Hater} says, climbing to {getPronoun(Hater, "possessive")} feet.
    I pushed {getPronoun(MissingKid, "object")} away, I pushed you away. I've ruined everything.
    "{PlayerName}, no offense, but {Hater == "Emily":I'm like, }{Hater == "Gary":like, }not everything revolves around you. {MissingKid} didn't get callously pushed away from a friend group. {getPronounCaps(MissingKid, "subject")} got{Hater == "Emily":, like,} killed by a monster or something."
    Damn. I thought that was why you were angry at me. I guess you hated me before that.
+ "I'm sorry we haven't been talking lately."
+ "I'm sorry {HaterAttic != 3:I was acting suspiciously.}{HaterAttic==3:I threw you on the ground.}"
    {
    - HaterAttic!=3:
    {Hater} smiles sheepishly as {getPronoun(Hater, "subject")} approaches you.
    "You were. But so was I. I got in your face and, like, threatened you kinda? And you were right."
    - HaterAttic ==3:
    {Hater} cracks a smile as {getPronoun(Hater, "subject")} climbs to {getPronoun(Hater, "possessive")} feet.
    "Don't mention it."
    }
    I hope this doesn't affect our friendship? I know you're already not a fan of me, though.
- "Is that what you think this {Hater=="Francine":has been}{Hater!="Francine":is} about?" {Hater} says, with a dubious smile. "{PlayerName}, relationships don't work that way. People don't stop 'working together' because your time is some kind of currency and you haven't been paying them enough. Sometimes people don't talk. Sometimes people even drift apart."
{Swear(Hater)}
"But, {Hater == "Emily":like, }{Hater == "Gary":like, }{Hater == "Laura":like, }{SwearLevel == 2:fuck, }we have each other's numbers. We're both doing fine. We don't need to be best buddies every second of every day."
{getPronounCaps(Hater, "subject")} looks at you compassionately. You take a beat.
I think being in a murder house has made me paranoid.
"I get it."

+ "You're being pretty casual about all of our friends being dead.["]
    <> Though I guess I am, too."
+ "So that thing's dead, but we're still here."
+ "So what do we do now?"
    "I don't think there's much we can do," {Hater} says.
- "I think, deep down, we're both waiting for the other shoe to drop," {getPronoun(Hater, "subject")} says with a grimace.
That moment, the floor beneath you both is shaken by a sudden and terrible rumble, as if the foundation below is being torn apart.
"There it is."
Plaster falls from the ceiling as the rumble grows into a greater, more audible tremor.

+ Wait.
- The floor beneath you is ripped into pieces by the sudden disintegration of the house's foundation. The walls around you are shredded, and a blinding light remains where they once stood. A terrible heat briefly consumes you.
        
It is dark.
~AtticHaterSceneDone = true
->FlashbackHub



// In-house generics

=== Talk_Welcome ===
~TalkWelcome = true
{
    - Speaker != DoorGetter:
    "Hey, {PlayerName}, I don't know if {DoorGetter} filled you in but there are some {Speaker == "Francine":important}{Speaker=="Emily":SUPER IMPORTANT} rules we have to follow.
    - else:
    "So, the owner of the house gave us some {Speaker == "Francine":important}{Speaker=="Emily":SUPER IMPORTANT} rules we have to follow.
}
<> Mostly they involve not making a mess of the place and some 
{
    - Speaker == "Francine":<> remarkably specific
    - Speaker == "Emily":<> way-too-specific
    - Speaker == "Cliff":<> pointless
    - else:<> very specific
}
<> instructions about how to arrange silverware, but all that really matters is that we're not supposed to go in the cellar or the attic."

+"No problem."
    {Speaker != "Francine": "Cool!}{Speaker == "Pete": Let's get drunk!}{Speaker != "Francine":"}
+"I wouldn't want to set foot in either of those places anyway."
    "Right. In this old house? Tetanus doesn't sound great right now."
+"Wait, why?["]
    <> Now I want to know what I'm missing."
{
    - Speaker == "Francine":
    "As much as we may want to--not me, but a general 'we'--it's not something we should do. I'm not setting foot up there."
    - Speaker == "Cliff":
    "Me too... he probably has cameras running, though. Likely with a live feed. I'm not going anywhere near that."
    - else:
    "Right? There's probably a body up there. Or bodies, plural. That's one more reason I'm not going up there."
}
"Yeah, me neither. I guess there WAS a huge deposit on this rental."
- ->GoToLocation